//Related Party Details   hidDIvddRelated_Details
  function Flex_Get_Join_Related_Party(Account_Number,Customer_id){
    
    	//jQuery("#hidDIvddRelated_Details").html('<table id="Related_Details" class="easyui-datagrid" iconCls="icon-search" rownumbers="true" pagination="true"></table>');
    		jQuery('#Related_Details').datagrid({  
    	    url:'Flex_Account_DetailsServlet?Detail=Get_Join_Related_Party&Account_Number='+Account_Number+'&Customer_id='+Customer_id,  
    	    columns:[[  
    	              {field:'cust_id',title:'Customer id',width:80}, 
					  {field:'joint_name',title:'Joint  Name',width:200}, 
					  {field:'relation',title:'Relation',width:100},
					  {field:'cust_ic',title:'Cust Ic',width:100},
					  {field:'sign_typ',title:'Sign Type',width:70},
					  {field:'sign_image',title:'Signature Image',width:240}
					  
    	    ]],

    	    queryParams:{
	    		msg : 'No Joint Details for this account',
	    		vr_flg:'Y'
		    		},
		    showTableToggleBtn: true ,
		    height: 350, //Height of Grid
		    width:850 //Width of Grid  
    	}); 
    	var divTag = jQuery("#hidDIvddRelated_Details");

    	
    	divTag.dialog({
      		modal: true,
      		autoOpen: false,
      		title: " Joint Holder(s) Details ",
      		show: "blind", 
            hide: "explode" 
           
      	});
    	divTag.dialog('open');
    
    	return false;

    	
    		
    }
    


//Flex_Get_Input_Info(Account_No,Tran_cheqno,chq_no,gl_code,custid,inpLienaccountno);
function Flex_Get_Input_Info(Account_No,Tran_cheqno,chq_no,gl_code,custid,inpLienaccountno)
{ 
	jQuery.ajax({
		type:"POST",
		url:"Flex_Get_CommanDetails?Detail=Flex_Get_All&fxinpaccountno="+Account_No+"&fxinpcust_id="+custid+"&fxinpLienaccountno="+inpLienaccountno+"&fxTran_cheqno="+Tran_cheqno+"&fxchq_no="+chq_no+"&fxgl_code="+gl_code,
		data:"",
		success:function(msg){	
			if(msg=='false')
			{
				alert(' Session TimeOut ');
				window.location.href='Login.jsp';
				
			}else if(msg!='')	
						{
				alert(msg);				
			}
			else
			{
				var ss= doSearch();	
			}
		}
		}); 
	
}



//Show Column 
 function show_bal_cls(colname)
{ 
	$('#tt1').datagrid('showColumn',colname); 
}
//Hide Column 
function hide_bal_cls(colname)
{ 
	$('#tt1').datagrid('hideColumn',colname); 
}
//Get Contra Details  

function Flex_Get_Contra_Details(txn_no,acc_no,tran_date){	
			 
			var output=""; 
			jQuery('#hidDIvdd_contra').val='';
			
			jQuery('#Flx_contra_Details').datagrid({  
			    url:'Flex_TransactionInquiry_Servlet?Report=Get_Contra&flg=Y&inpaccountno='+acc_no+'&inpbatch_id='+txn_no+'&inppost_date='+tran_date ,
			    columns:[[  
						 {field:'ACCOUNTNUMBER',title:'AccountNumber',width:120},
						 {field:'ACCCCY',title:'Acc CCY',width:100},
						 {field:'INPUTBRANCH',title:'InputBranch',width:130},
						 {field:'TRAN_CODE',title:'Tran Code',width:80},
						 {field:'TRANID',title:'Batch Number',width:80},
						 {field:'POSTINGDATE',title:'PostingDate',width:75},
						 {field:'VALUEDATE',title:'ValueDate',width:75},
						 {field:'INSTRUMENTNO',title:'InstrumentNo',width:85},
						 {field:'NARRATION',title:'Narration',width:200},
						 {field:'DR_CR',title:'Status',width:60},
						 {field:'TRANAMOUNT',title:'Tran Amount',width:100,align:'right'},
						 {field:'MAKERID',title:'MakerID',width:100},
						 {field:'CHECKERID',title:'CheckerID',width:100} 	       
			    ]],
			    showTableToggleBtn: true ,
			     //report Title
			    height: 380, //Height of Grid
			    width:1224 ,  //Width of Grid
			    pagination:false  
			}); 
			 
		var divTag = jQuery("#hidDIvdd_contra");
		
		divTag.dialog({
	  		modal: true,
	  		autoOpen: false,
	  		title: "Contra Detail",
	  		show: "blind",
	  		height: "auto", //Height of Grid
	 	  	width:1250, 
	        hide: "explode"
	  	});
		divTag.dialog('open');
		//return false;
			
	}
 
//*****************************************************< TDS Schedule check Start>

function Flex_Get_acc_no_exist(Account_number)
{	
	//alert(acc_type);
	var jsons = new Array();
		jQuery.ajax({
			type:"POST",
			url:"Flex_Get_CommanDetails?Detail=Get_Account_exist_tran&Account_number="+Account_number,
			data:"",
			success:function(msg){ 
				if(msg!='')	
				{ 
					jQuery("#flex_acc_type").val(msg.split('~')[0] ); 
					var ss= doSearch();					
				}
				else
				{
					alert('Account Number does not exists on the database'); 
				}
			}
			}); 
}


function Flex_chek_customer_data(Customer_id,Customer_name, Customer_ic,inpUserBranchId)
{
	
		jQuery.ajax({
			type:"POST",
			url:"Flex_Get_CommanDetails?Detail=CHEK_CUSTOMER_DATA&Customer_id="+Customer_id+"&Customer_name="+Customer_name+"&Customer_ic="+Customer_ic+"&inpUserBranchId="+inpUserBranchId,
			data:"",
			success:function(msg){	
				if(msg=='false')
				{
					alert(' Session TimeOut ');
					window.location.href='Login.jsp';
					
				}else if(msg!='')	
							{
					alert(msg);				
				}
				else
				{
					var ss= doSearch();	
				}
			}
			});
	
//return output;
}

//*****************************************************< Branch  master field Chek  >


function CHEK_Branch_DATA(BranchCode,BranchName)
{
	

		jQuery.ajax({
			type:"POST",
			url:"Flex_Get_CommanDetails?Detail=CHEK_Branch_DATA&BranchCode="+BranchCode+"&BranchName="+BranchName,
			data:"",
			success:function(msg){	
				if(msg=='false')
				{
					alert(' Session TimeOut ');
					window.location.href='Login.jsp';
					
				}else if(msg!='')	
							{
					alert(msg);				
				}
				else
				{
					var ss= doSearch();	
				}
			}
			});
	
//return output;
}

/////CHEK_Account_DATA

function Flex_CHEK_Account_DATA(Account_Number,Account_Name,inpUserBranchId)
{
		jQuery.ajax({
			type:"POST",
			url:"Flex_Get_CommanDetails?Detail=CHEK_Account_DATA&Account_Number="+Account_Number+"&Account_Name="+Account_Name+"&inpUserBranchId="+inpUserBranchId,
			data:"",
			success:function(msg){	
				if(msg=='false')
				{
					alert(' Session TimeOut ');
					window.location.href='Login.jsp';
					
				}else if(msg!='')	
							{
					alert(msg);				
				}
				else
				{
					var ss= doSearch();	
				}
			}
			});
	
//return output;
}
/////CHEK_stop payment
function Flex_Chek_stoppayment(ACCNO,STARTCHQ_NO)
{
	jQuery.ajax({
		type:"POST",
		url:"Flex_Get_CommanDetails?Detail=check_StopPayment&ACCNO="+ACCNO+"&custchck=Y&STARTCHQ_NO="+STARTCHQ_NO,
		data:"Detail=check_StopPayment&ACCNO="+ACCNO+"&custchck=Y&STARTCHQ_NO="+STARTCHQ_NO,
		success:function(msg){	
			if(msg=='false')
			{
				alert(' Session TimeOut ');
				window.location.href='Login.jsp';
				
			}else if(msg!='')	
						{
				alert(msg);				
			}
			else
			{
				var ss= doSearch();	
			}
		}
		});

}

function Flex_UserDetail_Exist(inpUserID, inpUserName,inpUserBranchId)
{
	
	jQuery.ajax({
			type:"POST",
			url:"Flex_Get_CommanDetails?Detail=check_UserDetail&inpUserID="+inpUserID+"&inpUserName="+inpUserName+"&inpUserBranchId="+inpUserBranchId+"&custchck=Y",
			data:"Detail=check_UserDetail&inpUserID="+inpUserID+"&custchck=Y&inpUserName="+inpUserName+"&inpUserBranchId="+inpUserBranchId,
			success:function(msg){	
				if(msg=='false')
				{
					alert(' Session TimeOut ');
					window.location.href='Login.jsp';
					
		}else if(msg!='')	
							{
					alert(msg);				
				}
				else
				{
					var ss= doSearch();	
				}
			}
			});
	
}

function Flex_Chek_PODD(dd_no,Branch_name)
{

	
		jQuery.ajax({
			type:"POST",
			url:"Flex_Get_CommanDetails?Detail=check_DD_PO&dd_no="+dd_no+"&custchck=Y&Branch_name="+Branch_name,
			data:"Detail=check_DD_PO&dd_no="+dd_no+"&custchck=Y&Branch_name="+Branch_name,
			success:function(msg){	
				if(msg=='false')
				{
					alert(' Session TimeOut ');
					window.location.href='Login.jsp';
					
				}else if(msg!='')	
							{
					alert(msg);				
				}
				else
				{
					var ss= doSearch();	
				}
			}
			});
	
	}



//*****************************************************<Transaction Releted Function 

function dis_r_type_cols(r_type)
{
	$('#msgs').html('Difference Between From And To Date Should Not Be More Than 24 Months'); 
	//glcode,gldesc,glccy,t_acno,t_chqno,t_tran_amt,t_status,t_batch,,
	if(r_type=='GLTranD')
		{
			hide_bal_cls('ACCOUNTNUMBER');
			hide_bal_cls('INSTRUMENTNO');
			hide_bal_cls('MAKERID');
			hide_bal_cls('CHECKERID');
		  
			
			show_bal_cls('RELATED_CUSTOMER');
			show_bal_cls('TRAN_REF_ID');
			show_bal_cls('GLCODE1');
			//show_bal_cls('gl_code');
			
				//$('#t_acno').hide();	
				$('#t_chqno').hide();
				$('#t_tran_amt').hide();
				$('#t_status').hide();
				$('#t_batch').hide();
				$('#t_brch').hide();
			 
				$('#g_brch').show();
				$('#glcode').show();
				$('#gldesc').show();
				$('#glccy').show();
				//$('#msgs').html('Difference Between From And To Date Should Not Be More Than 24 Months'); 
		}
	else{
		show_bal_cls('ACCOUNTNUMBER');
		show_bal_cls('INSTRUMENTNO');
		show_bal_cls('MAKERID');
		show_bal_cls('CHECKERID');
	  
		
		hide_bal_cls('RELATED_CUSTOMER');
		hide_bal_cls('TRAN_REF_ID');
		hide_bal_cls('GLCODE1');
		//show_bal_cls('gl_code');
		
		$('#t_acno').show();	
		$('#t_chqno').show();
		$('#t_tran_amt').show();
		$('#t_status').show();
		$('#t_batch').show();
		$('#t_brch').show();
		
		$('#g_brch').hide();
		$('#glcode').hide();
		$('#gldesc').hide();
		$('#glccy').hide();
		} 
}
//






