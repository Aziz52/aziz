//chq_Transaction_Inquiry,hidDIvd_chq_trancation_details,Fin7_chq_Transaction_Inquiry(Account_Number,intrument_no,acid,tran_id)
  function Fin7_chq_Transaction_Inquiry(Account_Number,intrument_no,acid,tran_id){  
	  jQuery('#chq_Transaction_Inquiry').html('');
	  jQuery('#chq_Transaction_Inquiry').datagrid({  
		    url:'Fin7_TransactionEnquiry_Servlet', 
		    columns:[[  
		              {field:'INITIATINGSOLID',title:'Branch <br> Code',width:60},
						 {field:'ACCOUNTNUMBER',title:'Account Number',width:100},
						 {field:'NAME',title:'Name',width:200},
						 {field:'TRANID',title:'Tran ID',width:90},
						 {field:'TYPESUBTYPE',title:'Subtype',width:100},
						 {field:'TRANSACTIONPARTICULAR',title:'Transaction <br> Particular',width:240},
						 {field:'VALUEDATE',title:'Value Date',width:70},
						 {field:'INSTRDATE',title:'Instr Date',width:70},
						 {field:'TRANSACTIONAMOUNT',title:'Transaction </br> Amount',width:100,align:'right'},
						 {field:'TRANSACTIONCODE',title:'Transaction </br> Code',width:65},
						 {field:'TRANREMARKS',title:'Tran Remarks',width:100},
						 {field:'TRANREFNUM',title:'Tran Ref Num',width:100},
						 {field:'INSTRTYPE',title:'Instr Type',width:70},
						 {field:'INSTRNO',title:'Instr No',width:70}, 
						 //New field
						 {field:'NARRATION1',title:'Narration',width:100},
						 {field:'TRAN_DATE',title:'Tran date',width:100},
						 {field:'TRAN_CURRENCY',title:'Tran Currency',width:100},
						 {field:'REPORT_CODE',title:'Report code',width:100},
						 {field:'ENTERED_USER_ID',title:'Entered User Id',width:100},
						 {field:'POSTED_USER_ID',title:'Posted User Id',width:100},
						 {field:'VERIFIED_USER_ID',title:'Verified User Id',width:100} 
		    ]],
    	    queryParams:{
		    		msg : 'No transaction available for Selected criteria.....',
		    		flg:'Y',
		    		intrument_no:intrument_no, 
	    			inpaccountnumber :Account_Number,
	    			inptran_id :tran_id,
	    			inpacid:acid
		    		},
		     showTableToggleBtn: true , 
		    height: 280, //Height of Grid
		    width:1124, //Width of Grid
		    pageSize:100
		});  
	  //chq_Transaction_Inquiry,hidDIvd_chq_trancation_details
    	var divTag = jQuery("#hidDIvd_chq_trancation_details"); 
    	divTag.dialog({
      		modal: true,
      		autoOpen: false,
      		title: "Transaction Details ",
      		show: "blind", 
            hide: "explode" 
      	});
    	divTag.dialog('open');
    	return false;
    }

  function Fin7_chq_Transaction_Inquiry1(Account_Number,intrument_no,acid,tran_id){  
	  jQuery('#chq_Transaction_Inquiry').html('');
	  jQuery('#chq_Transaction_Inquiry').datagrid({  
		    url:'Fin7_TransactionEnquiry_Servlet', 
		    columns:[[  
		              {field:'INITIATINGSOLID',title:'Branch <br> Code',width:60},
						 {field:'ACCOUNTNUMBER',title:'Account Number',width:100},
						 {field:'NAME',title:'Name',width:200},
						 {field:'TRANID',title:'Tran ID',width:90},
						 {field:'TYPESUBTYPE',title:'Subtype',width:100},
						 {field:'TRANSACTIONPARTICULAR',title:'Transaction <br> Particular',width:240},
						 {field:'VALUEDATE',title:'Value Date',width:70},
						 {field:'INSTRDATE',title:'Instr Date',width:70},
						 {field:'TRANSACTIONAMOUNT',title:'Transaction </br> Amount',width:100,align:'right'},
						 {field:'TRANSACTIONCODE',title:'Transaction </br> Code',width:65},
						 {field:'TRANREMARKS',title:'Tran Remarks',width:100},
						 {field:'TRANREFNUM',title:'Tran Ref Num',width:100},
						 {field:'INSTRTYPE',title:'Instr Type',width:70},
						 {field:'INSTRNO',title:'Instr No',width:70}, 
						 //New field
						 {field:'NARRATION1',title:'Narration',width:100},
						 {field:'TRAN_DATE',title:'Tran date',width:100},
						 {field:'TRAN_CURRENCY',title:'Tran Currency',width:100},
						 {field:'REPORT_CODE',title:'Report code',width:100},
						 {field:'ENTERED_USER_ID',title:'Entered User Id',width:100},
						 {field:'POSTED_USER_ID',title:'Posted User Id',width:100},
						 {field:'VERIFIED_USER_ID',title:'Verified User Id',width:100} 
		    ]],
    	    queryParams:{
    	    		msg : ' Transaction Not Found if Required Please Check In Old System',
		    		flg:'Y',
		    		intrument_no:intrument_no, 
	    			inpaccountnumber :Account_Number,
	    			inptran_id :tran_id,
	    			inpacid:acid
		    		},
		     showTableToggleBtn: true , 
		    height: 280, //Height of Grid
		    width:1124, //Width of Grid
		    pageSize:100
		});  
	  //chq_Transaction_Inquiry,hidDIvd_chq_trancation_details
    	var divTag = jQuery("#hidDIvd_chq_trancation_details"); 
    	divTag.dialog({
      		modal: true,
      		autoOpen: false,
      		title: "Transaction Details ",
      		show: "blind", 
            hide: "explode" 
      	});
    	divTag.dialog('open');
    	return false;
    }

  ///Addition_Inquiry Details Start  hidDIvd_Addition_Inquiry_details Addition_Inquiry 
    function Fin7_Addition_Inquiry(Account_Number,Srl_Num,tran_id,tran_date){  
  	 // alert('Account_Number='+Account_Number);
  	 // alert('Srl_Num='+Srl_Num);
  	//  alert('tran_id='+tran_id);
  	 // alert('tran_date='+tran_date);
  	  
    	$('#txtinptran_idOutPut').val(tran_id);
    	$('#txtinptran_dateOutPut').val(tran_date);
    	$('#txtinppart_sr_numOutPut').val(Srl_Num);
  	  
  	  
  	  jQuery('#Addition_Inquiry').html('');
  	  jQuery('#Addition_Inquiry').datagrid({  
  		    url:'Fin7_TransactionEnquiry_Servlet', 
  		    columns:[[  
  		         	 {field:'TRAN_DATE_A',title:'Transaction Date',width:100},
  		         	 {field:'TRAN_AMT_A',title:'Transaction Amount',width:100,align:'right'},
  		         	 {field:'REVERSED_AMT_A',title:'Reversed Amount',width:100,align:'right'},
  		         	 {field:'REVERSING_AMT_A',title:'Reversing Amount',width:100,align:'right'},
  		         	 {field:'PARTICULAR_A',title:'Particular',width:270},
  		         	 {field:'REF_NO_A',title:'Ref No',width:100}      
  		    ]],
      	    queryParams:{
  		    		msg : 'No Additional Details available for Selected criteria.....',
  		    		flg:'Y',
  		    		inptran_date:tran_date, 
  	    			inpaccountnumber :Account_Number,
  	    			inptran_id :tran_id,
  	    			Report:'Addition_Report',
  	    			inpSrl_Num:Srl_Num
  		    		},
  		     showTableToggleBtn: true , 
  		    height: 280, //Height of Grid
  		    width:900, //Width of Grid
  		    pageSize:100
  		});  
  	  //chq_Transaction_Inquiry,hidDIvd_chq_trancation_details
      	var divTag = jQuery("#hidDIvd_Addition_Inquiry_details"); 
      	divTag.dialog({
        		modal: true,
        		autoOpen: false,
        		title: "Additional Details",
        		show: "blind", 
              hide: "explode" 
        	});
      	divTag.dialog('open');
      	return false;
     }

  
  
  function Get_int_adj_Tran_Details(acc_no,cust_detail,tran_id,tran_date ){	
		 
		check_session();
		jQuery('#chq_Transaction_Inquiry').val='';
		jQuery('#chq_Transaction_Inquiry').datagrid({  
		    url:'Fin7_TransactionEnquiry_Servlet?Report=tran_Details&flg=Y&acc_no='+acc_no+'&inptranid='+tran_id+'&inptrandate='+tran_date ,  
		    columns:[[ 
					{field:'INITIATINGSOLID',title:'Branch <br> Code',width:60},
					{field:'ACCOUNTNUMBER',title:'Account Number',width:100},
					{field:'NAME',title:'Name',width:200},
					{field:'TRANID',title:'Tran ID',width:90},
					{field:'TYPESUBTYPE',title:'Type / Subtype',width:100},
					{field:'TRANSACTIONPARTICULAR',title:'Transaction <br> Particular',width:240},
					{field:'VALUEDATE',title:'Value Date',width:70},
					{field:'INSTRDATE',title:'Instr Date',width:70},
					{field:'TRANSACTIONAMOUNT',title:'Transaction </br> Amount',width:100,align:'right'},
					{field:'TRANSACTIONCODE',title:'Transaction </br> Code',width:65},
					{field:'TRANREMARKS',title:'Tran Remarks',width:100},
					{field:'TRANREFNUM',title:'Tran Ref Num',width:100},
					{field:'INSTRTYPE',title:'Instr Type',width:70},
					{field:'INSTRNO',title:'Instr No',width:70},
					
					//New field
					{field:'NARRATION1',title:'Narration',width:100},
					{field:'TRAN_DATE',title:'Tran date',width:100},
					{field:'TRAN_CURRENCY',title:'Tran Currency',width:100},
					{field:'REPORT_CODE',title:'Report code',width:100},
					{field:'ENTERED_USER_ID',title:'Entered User Id',width:100},
					{field:'POSTED_USER_ID',title:'Posted User Id',width:100},
					{field:'VERIFIED_USER_ID',title:'Verified User Id',width:100}    
		    ]],
		    queryParams:{
	    		msg : 'No Transaction Details available for Selected criteria.....',
	    		flg:'Y'
		    	},
		    showTableToggleBtn: true , 
		    height: 300, //Height of Grid
		    width:1024 //Width of Grid
		   // pagination:false  
		    //1pageSize:tot_chq
		}); 
		//,
		var divTag = jQuery("#hidDIvd_chq_trancation_details");
		
		divTag.dialog({
	  		modal: true,
	  		autoOpen: false,
	  		title: "Transaction Details ",
	  		show: "blind",
	        hide: "explode"
	  	});
		divTag.dialog('open');
		return false;
			
	}

  ///Cheque Contra Details Start

  function  Fin7_DD_Transaction_Inquiry(Account_Number,intrument_no,inpfromdate,inptodate){  
	  jQuery('#chq_Transaction_Inquiry').html('');
	  jQuery('#chq_Transaction_Inquiry').datagrid({  
		    url:'Fin7_TransactionEnquiry_Servlet', 
		    columns:[[  
		              {field:'INITIATINGSOLID',title:'Branch <br> Code',width:60},
						 {field:'ACCOUNTNUMBER',title:'Account Number',width:100},
						 {field:'NAME',title:'Name',width:200},
						 {field:'TRANID',title:'Tran ID',width:90},
						 {field:'TYPESUBTYPE',title:'Subtype',width:100},
						 {field:'TRANSACTIONPARTICULAR',title:'Transaction <br> Particular',width:240},
						 {field:'VALUEDATE',title:'Value Date',width:70},
						 {field:'INSTRDATE',title:'Instr Date',width:70},
						 {field:'TRANSACTIONAMOUNT',title:'Transaction </br> Amount',width:100,align:'right'},
						 {field:'TRANSACTIONCODE',title:'Transaction </br> Code',width:65},
						 {field:'TRANREMARKS',title:'Tran Remarks',width:100},
						 {field:'TRANREFNUM',title:'Tran Ref Num',width:100},
						 {field:'INSTRTYPE',title:'Instr Type',width:70},
						 {field:'INSTRNO',title:'Instr No',width:70}, 
						 //New field
						 {field:'NARRATION1',title:'Narration',width:100},
						 {field:'TRAN_DATE',title:'Tran date',width:100},
						 {field:'TRAN_CURRENCY',title:'Tran Currency',width:100},
						 {field:'REPORT_CODE',title:'Report code',width:100},
						 {field:'ENTERED_USER_ID',title:'Entered User Id',width:100},
						 {field:'POSTED_USER_ID',title:'Posted User Id',width:100},
						 {field:'VERIFIED_USER_ID',title:'Verified User Id',width:100} 
		    ]],
    	    queryParams:{
		    		msg : 'No transaction available for Selected criteria.....',
		    		flg:'Y',
		    		intrument_no:intrument_no, 
	    			inpaccountnumber :Account_Number,
	    			inpfromdate :inpfromdate,
	    			inptodate :inptodate,
	    			
		    		},
		     showTableToggleBtn: true , 
		    height: 280, //Height of Grid
		    width:1124, //Width of Grid
		    pageSize:100
		});  
	  //chq_Transaction_Inquiry,hidDIvd_chq_trancation_details
    	var divTag = jQuery("#hidDIvd_chq_trancation_details"); 
    	divTag.dialog({
      		modal: true,
      		autoOpen: false,
      		title: "Transaction Details ",
      		show: "blind", 
            hide: "explode" 
      	});
    	divTag.dialog('open');
    	return false;
    }
  

  ///Cheque Contra Details Start
  
  function Fin7_get_Transaction_tran_detail(inptrandate,inpsol_id,inptran_id,inppart_tran_sr_num)
  {  
	  jQuery("#Transaction_tran_detail").html('<div align="center" style="width: 100%; padding-top: 17%;"> <img src="images/loading.gif"/><br/>Processing Please Wait ...</div>').show(); 
 
  	var output="";
  	jQuery.ajax({
  	type:"POST",
  	url:"Fin7_TransactionEnquiry_Servlet",
  	data: "Report=Transaction_ID_Details&flg=Y&page=0&rows=0&inptranid="+inptran_id+"&inptrandate="+inptrandate+"&inppart_tran_sr_num="+inppart_tran_sr_num +"&inpsol_id="+inpsol_id,
  	success:function(msg){ 
  		jQuery("#Transaction_tran_detail").html(msg); 
  	}
  	});
  	var divTag = jQuery("#Transaction_tran_detail"); 
	divTag.dialog({
  		modal: true,
  		autoOpen: false,
  		title: "Transaction Details ",
  		show: "blind",
  		height: 450, //Height of Grid
		width:1000, //Width of Grid
        hide: "explode" 
  	});
	divTag.dialog('open');
	return false; 
  }

  
  //inptrandate,inptranid,inpsol_id,inppart_tran_sr_num
  
// Fin7_TransactionEnquiry_Details_get(String tran_date, String inpsol_id,String inptran_id,String inppart_tran_sr_num)

	function Get_Tran_Contra_Details(acc_no,cust_detail,tran_id,tran_date ){	
		
		check_session();
		jQuery('#tran_Details').val='';
		jQuery('#tran_Details').datagrid({  
		    url:'Fin7_TransactionEnquiry_Servlet?Report=tran_Details&flg=Y&acc_no='+acc_no+'&inptranid='+tran_id+'&inptrandate='+tran_date ,  
		    columns:[[ 
					{field:'INITIATINGSOLID',title:'Branch <br> Code',width:60},
					{field:'ACCOUNTNUMBER',title:'Account Number',width:100},
					{field:'NAME',title:'Name',width:200},
					{field:'TRANID',title:'Tran ID',width:90},
					{field:'TYPESUBTYPE',title:'Subtype',width:100},
					{field:'TRANSACTIONPARTICULAR',title:'Transaction <br> Particular',width:240},
					{field:'VALUEDATE',title:'Value Date',width:70},
					{field:'INSTRDATE',title:'Instr Date',width:70},
					{field:'TRANSACTIONAMOUNT',title:'Transaction </br> Amount',width:100,align:'right'},
					{field:'TRANSACTIONCODE',title:'Transaction </br> Code',width:65},
					{field:'TRANREMARKS',title:'Tran Remarks',width:100},
					{field:'TRANREFNUM',title:'Tran Ref Num',width:100},
					{field:'INSTRTYPE',title:'Instr Type',width:70},
					{field:'INSTRNO',title:'Instr No',width:70},
					
					//New field
					{field:'NARRATION1',title:'Narration',width:100},
					{field:'TRAN_DATE',title:'Tran date',width:100},
					{field:'TRAN_CURRENCY',title:'Tran Currency',width:100},
					{field:'REPORT_CODE',title:'Report code',width:100},
					{field:'ENTERED_USER_ID',title:'Entered User Id',width:100},
					{field:'POSTED_USER_ID',title:'Posted User Id',width:100},
					{field:'VERIFIED_USER_ID',title:'Verified User Id',width:100}    
		    ]],
		    queryParams:{
	    		msg : 'No Contra Details available for Selected criteria.....',
	    		flg:'Y'
		    	},
		    showTableToggleBtn: true , 
		    height: 300, //Height of Grid
		    width:1024 //Width of Grid
		   // pagination:false  
		    //1pageSize:tot_chq
		}); 
		var divTag = jQuery("#hidDIvdds_Contra_details");
		 
		divTag.dialog({
	  		modal: true,
	  		autoOpen: false,
	  		title: "Contra Details ",
	  		show: "blind",
	        hide: "explode"
	  	});
		divTag.dialog('open');
		return false;
			
	}

// Cheque Contra Details End
  
  



// Fin7_Get_Input_Info('Account_No','sol_id','chq_no','user_id','schema_no','cust_id','tran_chq_no','inpLienaccountno','demo1','demo2','demo3');
function Fin7_Get_Input_Info(Account_No,sol_id,chq_no,user_id,schema_no,custid,tran_chq_no,Lienaccountno,inw_zone_code,inpout_zone_code,Inword_sol_id)
{   

	jQuery.ajax({
		type:"POST",
		url:"Fin7_Get_CommanDetails?Detail=Fin7_Get_All_sachu&fxinpAccount_No="+Account_No+"&fxinpsol_id="+sol_id+"&fxinpchq_no="+chq_no+"&fxinpuser_id="+user_id+"&fxinpschema_no="+schema_no+"&fxinpcust_id="+custid+"&fxinptran_chq_no="+tran_chq_no+"&fxinpLienaccountno="+Lienaccountno+"&fxinpinw_zone_code="+inw_zone_code+"&fxinpout_zone_code="+inpout_zone_code+"&fxinpInword_sol_id="+Inword_sol_id,
		data:"",
		success:function(msg){	
			if(msg=='false')
			{ 
				alert(' Session TimeOut ');
				window.location.href='Login.jsp'; 
			}else if(msg!='')	
				{
					alert(msg);				
				}
			else
			{
				var ss= doSearch();	
			}
		}
		}); 
	
}

//Fin7_Get_Input_Info('Account_No','sol_id','chq_no','user_id','schema_no','cust_id','tran_chq_no','inpLienaccountno','demo1','demo2','demo3');
function Fin7_Get_Input_Info1(Account_No,sol_id,schema_no,custid,limit_id,fxinpEnterd_userid,inpposted_userid,Bill_id,interest_table_code,version_number,demo7,demo8)
{   
	

	jQuery.ajax({
		type:"POST",
		url:"Fin7_Get_CommanDetails?Detail=Fin7_Get_All_sachu&fxinpAccount_No="+Account_No+"&fxinpsol_id="+sol_id  +"&fxinpschema_no="+schema_no+"&fxinpcust_id="+custid+"&fxinplimit_id="+limit_id+"&fxinpEnterd_userid="+fxinpEnterd_userid+"&fxinpposted_userid="+inpposted_userid+"&fxinpBill_id="+Bill_id +"&fxinpinterest_table_code="+interest_table_code +"&fxinpversion_number="+version_number,
		data:"",
		success:function(msg){	
			if(msg=='false')
			{ 
				alert(' Session TimeOut ');
				window.location.href='Login.jsp'; 
			}else if(msg!='')	
				{
					alert(msg);				
				}
			else
			{
				var ss= doSearch();	
			}
		}
		}); 
	
}

function Fin7_get_customer_detail(divname,Detail,accno)
{
	 
	//divname= Div Name
	//Detail= form details
	//schema= schema name 
	//accno=Account Name 
	jQuery("#" + divname).html('');
	var output="";
	jQuery.ajax({
	type:"POST",
	url:"Fin7_Get_CommanDetails",
	data: "Detail="+ Detail +"&accno="+accno,
	success:function(msg){
		//alert(msg);
		jQuery("#" + divname).html(msg);
		//jQuery("'#"+divname+"'")
	}
	});
return output;
}
function fin7_get_customer_detail_sachu(divname,Detail,accno,acctype,demo)
{ 
	 

	//divname= Div Name      D_acctype,D_accno,D_Sol_id
	//Detail= form details
	//acctype= acc type   
	//accno=Account Name
	jQuery("#" + divname).html('');	
	var output="";
	jQuery.ajax({
	type:"POST",
	url:"Fin7_Get_CommanDetails?Detail="+ Detail +"&D_acctype="+ acctype +"&D_accno="+accno+"&D_Sol_id="+demo,
	data:"",
	success:function(msg){ 
		jQuery("#" + divname).html(msg);
	}
	}); 
}



/// vijay flin7

var fin7_fmDate1;var fin7_fmDate2;
function fin7_get_min_max_date(acc_no)
{
	var str='';
	fin7_fmDate1=null;
	fin7_fmDate2=null;
	 jQuery.ajax({
       type: "POST",
       url: 'Fin7_Get_CommanDetails?Detail=Get_MinMax_date&acc_no='+acc_no,
       data:'Detail=Get_MinMax_date&acc_no='+acc_no,
       success: function (data) {		
		 if(data.split("~~").length>0)
		 {
			 fin7_fmDate1=data.split("~~")[1];
			 fin7_fmDate2=data.split("~~")[2];
		 }
		 jQuery('#msgs').html(data.split("~~")[0]); data=''; }
   });
}
//Show Column 
 function show_bal_cls(colname)
{ 
	 //colname          this function pass object (column name sep by comma like this ["col1","col2","col3"])
	 $.each(colname,function(i,j)
				{
		            $('#tt1').datagrid('showColumn',j);   
				});
	
}
//Hide Column 
function hide_bal_cls(colname)
{ 
	//colname          this function pass object (column name sep by comma like this ["col1","col2","col3"])
	 $.each(colname,function(i,j)
				{ 
		            $('#tt1').datagrid('hideColumn',j);   
				}); 
}





function FIN7_Chek_branch(BranchCode,BranchName)
{	
  
		jQuery.ajax({
			type:"POST",
			url:"Fin7_Get_CommanDetails?Detail=CHEK_Branch_DATA&BranchCode="+BranchCode+"&BranchName="+BranchName,
			data:"",
			success:function(msg){	
				if(msg=='false')
				{
					alert(' Session TimeOut ');
					window.location.href='Login.jsp';
					
				}else if(msg!='')	
							{
					alert(msg);				
				}
				else
				{
					var ss= doSearch();	
				}
			}
			}); 
}


//Account_Number Account_Name 



function Fin7_chek_customer_data(Customer_id,Customer_name,Sol_id,Account_Number,Account_Name,acc_type,STARTCHQ_NO,SI_Serial_No)
{

		jQuery.ajax({
			type:"POST",
			url:"Fin7_Get_CommanDetails?Detail=CHEK_CUSTOMER_DATA&Customer_id="+Customer_id+"&Customer_name="+Customer_name+"&Sol_id="+Sol_id+"&Account_Number="+
			Account_Number+"&Account_Name="+Account_Name+"&acc_type="+acc_type+"&STARTCHQ_NO="+STARTCHQ_NO+"&SI_Serial_No="+SI_Serial_No,
			data:"",
			success:function(msg){	
				if(msg=='false')
				{
					alert(' Session TimeOut ');
					window.location.href='Login.jsp';
					
				}else if(msg!='')	
							{
					alert(msg);				
				}
				else
				{
					var ss= doSearch();	
				}
			}
			});
	
//return output;
}



function Fin7_chek_customer_data1(Sol_id,inpuserid,inpuempid,inpusername)
{
	
		jQuery.ajax({
			type:"POST",
			url:"Fin7_Get_CommanDetails?Detail=CHEK_CUSTOMER_DATA&Sol_id="+Sol_id +"&inpuserid="+inpuserid+"&inpuempid="+inpuempid+"&inpusername="+inpusername,
			data:"",
			success:function(msg){	
				if(msg=='false')
				{
					alert(' Session TimeOut ');
					window.location.href='Login.jsp';
					
				}else if(msg!='')	
							{
					alert(msg);				
				}
				else
				{
					var ss= doSearch();	
				}
			}
			});
	
//return output;
}


function Fin7_chek_AFI_data(Sol_id,Account_Number,Customer_id,inpref_num,inpentered_user_id,inpauthorized_id)
{
	

		jQuery.ajax({
			type:"POST",
			url:"Fin7_Get_CommanDetails?Detail=CHEK_CUSTOMER_DATA&Sol_id="+Sol_id+"&Account_Number="+Account_Number+"&Customer_id="+Customer_id+"&inpref_num="+
			inpref_num+"&inpentered_user_id="+inpentered_user_id+"&inpauthorized_id="+inpauthorized_id,

			data:"",
			success:function(msg){	
				if(msg=='false')
				{
					alert(' Session TimeOut ');
					window.location.href='Login.jsp';
					
				}else if(msg!='')	
							{
					alert(msg);				
				}
				else
				{
					var ss= doSearch();	
				}
			}
			});
	
//return output;
}


function FIN7_Chek_DD_MC(dd_no,branch)
{	
		jQuery.ajax({
			type:"POST",
			url:"Fin7_Get_CommanDetails?Detail=CHEK_CUSTOMER_DATA&dd_no="+dd_no+"&branch="+branch,
			data:"",
			success:function(msg){	
				if(msg=='false')
				{
					alert(' Session TimeOut ');
					window.location.href='Login.jsp';
					
				}else if(msg!='')	
							{
					alert(msg);				
				}
				else
				{
					var ss= doSearch();	
				}
			}
			}); 
}

function FIN7_Chek_BG_DATA(inpgurantee_no,inpgurantee_amt,Sol_id,Customer_id)
{	
	
	
		jQuery.ajax({
			type:"POST",
			url:"Fin7_Get_CommanDetails?Detail=CHEK_CUSTOMER_DATA&inpgurantee_no="+inpgurantee_no+"&inpgurantee_amt="+inpgurantee_amt+"&Sol_id="+Sol_id+"&Customer_id="+Customer_id,
			data:"",
			success:function(msg){	
				if(msg=='false')
				{
					alert(' Session TimeOut ');
					window.location.href='Login.jsp';
					
				}else if(msg!='')	
							{
					alert(msg);				
				}
				else
				{
					var ss= doSearch();	
				}
			}
			}); 
}


function FIN7_Chek_LC_DATA(inpdc_no,inpregister_type,Sol_id)
{	
	
	
		jQuery.ajax({
			type:"POST",
			url:"Fin7_Get_CommanDetails?Detail=CHEK_CUSTOMER_DATA&inpdc_no="+inpdc_no+"&inpregister_type="+inpregister_type+"&Sol_id="+Sol_id,
			data:"",
			success:function(msg){	
				if(msg=='false')
				{
					alert(' Session TimeOut ');
					window.location.href='Login.jsp';
					
				}else if(msg!='')	
							{
					alert(msg);				
				}
				else
				{
					var ss= doSearch();	
				}
			}
			}); 
}

function FIN7_Chek_FBI_DATA(inpbill_id ,inpcurrecny,inpbill_amt ,Sol_id,Customer_id)
{	
	
	
		jQuery.ajax({
			type:"POST",
			url:"Fin7_Get_CommanDetails?Detail=CHEK_CUSTOMER_DATA&inpbill_id="+inpbill_id+"&inpcurrecny="+inpcurrecny+"&inpbill_amt="+inpbill_amt+"&Sol_id="+Sol_id+"&Customer_id="+Customer_id,
			data:"",
			success:function(msg){	
				if(msg=='false')
				{
					alert(' Session TimeOut ');
					window.location.href='Login.jsp';
					
				}else if(msg!='')	
							{
					alert(msg);				
				}
				else
				{
					var ss= doSearch();	
				}
			}
			}); 
}



function FIN7_Chek_Sign_DATA(Account_Number ,Customer_id)
{	
	
	
		jQuery.ajax({
			type:"POST",
			url:"Fin7_Get_CommanDetails?Detail=CHEK_CUSTOMER_DATA&Account_Number="+Account_Number+"&Customer_id="+Customer_id,
			data:"",
			success:function(msg){	
				if(msg=='false')
				{
					alert(' Session TimeOut ');
					window.location.href='Login.jsp';
					
				}else if(msg!='')	
							{
					alert(msg);				
				}
				else
				{
					var ss= doSearch();	
				}
			}
			}); 
}

function check_session()
{  	
	jQuery.get('check_log.jsp',function(responseText){
	if($.trim(responseText)=="false")
	{
		alert('Session TimeOut'); 
		window.close();
	} 
});
}
