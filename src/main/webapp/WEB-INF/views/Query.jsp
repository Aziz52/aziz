<% response.setHeader("cache-Control","no-cache,no-store, must-revalidate"); %>
<% response.setHeader("Pragma","no-cache"); %>
<% response.setHeader("Expires","0"); %>
<%@page import="java.util.ArrayList"%>
<%@page import="com.service.*"%>
<%@page import="com.servlet.*"%>
<%@page import="com.dto.*"%>
<%@page import="java.util.Iterator"%>


<html>
<head>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<meta http-equiv="X-UA-Compatible" content="IE=8">

<title>Insert title here</title>

<style type="text/css">
<!-- /*
.divClv
{
clear:both;
}

#divContainer {
    background: none repeat scroll 0 0 #FFFFFF;
    margin: auto;
    padding-top: 10px;
    width: 1174px;
}
#divMain {
    background: none repeat scroll 0 0 #F6F6F6;
    border: 2px solid #DEDEDE;
    height: auto;
    padding: 11px;
}
#divLeft {
    background: none repeat scroll 0 0 black;
    border: 1px solid #C4D6E5;
    float: left;
    height: 700px;
    width: 230px;
}
#divRight {
    background: none repeat scroll 0 0 black;
    float: right;
    height: 700px;
    vertical-align: top;
    width: 912px;
}
#divRightTop {
    background: none repeat scroll 0 0 yellow;
    height: 440px;
    margin: 5px;
    width: 902px;
}
#divRightBottom {
    background: none repeat scroll 0 0 yellow;
    height: 245px;
    margin: 5px;
    width: 902px;
}

#divBottom {
    background: blue;
    height: 245px;   
    width: 1148px;
}
*/
.divClv {
	CLEAR: both
}

body {
	font-family: Verdana, Arial, sans serif;
}

#divContainer {
	WIDTH: 980px;
	BACKGROUND: #ffffff 0px 0px;
	PADDING-TOP: 10px
	
}

#divMain {
	BORDER-BOTTOM: #dedede 2px solid;
	BORDER-LEFT: #dedede 2px solid;
	PADDING-BOTTOM: 11px;
	PADDING-LEFT: 11px;
	PADDING-RIGHT: 11px;
	BACKGROUND: #f6f6f6 0px 0px;
	HEIGHT: auto;
	BORDER-TOP: #dedede 2px solid;
	BORDER-RIGHT: #dedede 2px solid;
	PADDING-TOP: 11px
}

#divLeft {
	WIDTH: 180px;
	FLOAT: left;
	HEIGHT: 945px;
	BORDER: #c4d6e5 1px solid;
	overflow: auto;
}

#divRight {
	FLOAT: right;
	HEIGHT: 700px;
	VERTICAL-ALIGN: top
}

#divRightTop {
	MARGIN: 5px;
	WIDTH: 755px;
	HEIGHT: 440px;
	border: 1px solid #c4d6e5;
	overflow: auto;
}

#divRightBottom {
    border: 1px solid #C4D6E5;
  
    margin: 5px;
    overflow: auto;
   
}

#divRightBottom22 {
    border: 1px solid #C4D6E5;
   
    margin: 5px;
    overflow: auto;
   
}

#divRightBottomText {   
    height: 215px;
    width: 753px;
}

#divRightBottomTextWhere {   
    height: 180px;
    width: 753px;
}


#divRightBottomButton {
    height: 23px;
    margin: 5px;
    overflow: auto;
    text-align: right;
    width: 755px;
}

#divBottom {
	border: 1px solid #c4d6e5;
	min-height: 400px;
	padding-top: 13px;
	overflow: auto;
}

#divTop {
	BACKGROUND: #0962AD;
}

#ul_tblList {
	font-size: 10px;
	font-family: Verdana, Arial, sans serif;
	white-space: nowrap;
}

#ul_tblList li {
	font-size: 11px;
	margin: 0px;
	padding-bottom: 0px;
}

.box-clone {
	font-size: 10px;
	border: 1px solid #dcdcdc;
	width: 160px;
	padding: 3px;
	position: relative;
	!
	important;
}

.box-clone div {
	
}

.DivFixed {
    cursor: move;
    float: left;
    margin: 0;
    padding: 10px 0 10px 4px;
    width: 140px;
}

.spanClose {
    color: blue;
    cursor: pointer;
    font-size: 13px;
    font-weight: bold;
    padding-top: 6px;
    position: absolute;
}

.box-clone .spanData {
	overflow-x: hidden;
	overflow-y: auto;
	height: 150px;
}

.tblBox tr {
	cursor: auto;
}

.tblBox .od {
	background-color: #FFF;
}

.tblBox .ev {
	background-color: #dcdcdc;
}

.hoverDiv {
	background-color: #4592D4;
}

.
.divDataType {
	font-size: 9px;
	color: blue;
}

.tblBox td span
{
font-size: 10px;
}
-->
</style>
<script type="text/javascript">
<!--
var t = '<%=session.getAttribute("user_id")%>';
if (t == null)
{
	window.location.href= 'Login.jsp';
}
if (t == "")
{
	window.location.href= 'Login.jsp';
}
if (t == 'null')
{
	window.location.href= 'Login.jsp';
}
//-->
</script>

<script type="text/javascript" src="Include/jquery-1.9.1.js"></script>
<link rel="stylesheet" href="Include/jquery.treeview_css.css" />
<link rel="stylesheet" href="Include/jquery-ui-1.10.3.custom.css" />
<script type="text/javascript" src="Include/jquery.treeview.js"></script>
<script type="text/javascript" src="Include/jquery.treeview.async.js"></script>
<script type="text/javascript" src="Include/jquery.ui.core.js"></script>
<script type="text/javascript" src="Include/jquery.ui.widget.js"></script>
<script type="text/javascript" src="Include/jquery.ui.mouse.js"></script>
<script type="text/javascript" src="Include/jquery.ui.draggable.js"></script>
<script type="text/javascript" src="Include/jquery.ui.droppable.js"></script>
<script type="text/javascript" src="Include/jquery.ui.resizable.js"></script>
<script type="text/javascript" src="Include/pathCreator.js"></script>

<script type="text/javascript" src="Include/grid.locale-en.js"></script>
<script type="text/javascript" src="Include/jquery.jqGrid.js"></script>
<link rel="stylesheet" type="text/css" href="Include/ui.jqgrid.css">




<script type="text/javascript">

function ExportExcel(data)
{
	alert(data);
}

//<![CDATA[
var mCounter = false;
var set = null;
var DivNameMove = "";
var svg = null;
var myLines = [];
var Broswer_name;
var Broswer_version;
jQuery(document).ready(function ($) {
	 svg = Raphael("divRightTop", $("#divRightTop").width() - 30, $("#divRightTop").height() - 30);
	
    var N = navigator.appName,
        ua = navigator.userAgent,
        tem;
    var M = ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
    if (M && (tem = ua.match(/version\/([\.\d]+)/i)) != null) M[2] = tem[1];
    Broswer_name = M[1];
    Broswer_version = M[2];

    jQuery("#btnRUN").click(function () {

    	$("#divBottom").html("<table id='tblOutput'></table><div id='pager2'></div>");

    	var strQuery="";
    	if($("#divRightBottomText").val().length > 0)
    	{
         strQuery =$("#divRightBottomText").val().replace(new RegExp("<br>","g"),'').replace(new RegExp("<BR>","g"),'');

         if( $.trim($("#divRightBottomTextWhere").val()).length > 0)
         {
	         if (strQuery.toUpperCase().indexOf('WHERE')!=-1 )
	            	 strQuery+=(($("#divRightBottomTextWhere").val().toUpperCase().indexOf('AND')>=0 && $("#divRightBottomTextWhere").val().toUpperCase().indexOf('AND')<3) ?$("#divRightBottomTextWhere").val():" AND "+$("#divRightBottomTextWhere").val());
	         else if($("#divRightBottomTextWhere").val().toUpperCase().indexOf('GROUP BY')>=0 && $("#divRightBottomTextWhere").val().toUpperCase().indexOf('GROUP BY')<3)
	         {
	        	 strQuery+=$("#divRightBottomTextWhere").val();
		     }
	         else
	             strQuery+=(($("#divRightBottomTextWhere").val().toUpperCase().indexOf('WHERE')>=0 && $("#divRightBottomTextWhere").val().toUpperCase().indexOf('WHERE')<3)?$("#divRightBottomTextWhere").val():" WHERE "+$("#divRightBottomTextWhere").val());         
	         }	
    	}
    	
    	 strQuery=strQuery.toUpperCase();
    	

    	 $.ajax({
             type: "POST",
             url: 'Query_servlet',             
             data: 'param=getQueryColumn&query=' + strQuery,
             dataType: 'json',
             success: function (data) {
             
    		 jQuery("#tblOutput").jqGrid({
	                url: "Query_servlet?param=getQueryOutput",
	                    datatype: "json",
	                    type: "POST",
		                colNames: data.colNames,
		                colModel: data.colModel,
		                postData: {
									query: strQuery
						          },
		                rowNum:20,
	                    rowList:[20,40,80],
	                    pager: '#pager2',	                  
	                    viewrecords: true,	                                      
	                    rownumbers: true,
	                    sortorder: "asc",
	                    height: 400,
	                    width: 900,
	                    shrinkToFit:false,
	                    autowidth:true,
	                                  
		                caption: "Output"});

    		 jQuery("#tblOutput").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false,refresh:true})
    		 .navButtonAdd('#pager2',{ caption : "Export To Excel",
 	 			title: 'Export To Excel',
	 			buttonicon : 'ui-icon-document',
	 			onClickButton: function(){
    			 $("#txtQuery").val(strQuery);
    			 $('#txtTypeOutPut').val("excel");
    		    	$('#txtReportName').val("Query Output");
    		    	
    		    			 
  	 			 $("#frmPdfOutPut").submit();},
	 			position : "last",
	 			cursor : 'pointer'}).navButtonAdd('#pager2',{ caption : "Export To PDF",
	 	 			title: 'Export To PDF',
		 			buttonicon : 'ui-icon-document',
		 			onClickButton: function(){
	    			 $("#txtQuery").val(strQuery);
	    			 $('#txtTypeOutPut').val("pdf");
	    			 $('#txtWidth').val($('.ui-jqgrid-htable')[0].scrollWidth+20);
	    			 
	    			 
	    		     $('#txtReportName').val("Query Output");
	  	 			 $("#frmPdfOutPut").submit();},
		 			position : "last",
		 			cursor : 'pointer'});
    		
    	 },
    	 error: function (request, status, error) {
    	        alert(request.responseText);
    	    }
    	 });
    });

    $("#divRightTop").droppable({
        hoverClass: "hoverDiv"
    });
    jQuery("#btnGO").click(function () {
    	 alert('cdsfjkasdhkfhy');
    	$("#divLeft").html("Please Wait Tables Are Loading....");
if( $("#selSchema").val()!="")
{
	varAppIDS=$("#selSchema").val();
        jQuery.ajax({
            type: "POST",
            url: 'Query_servlet',
            data: $("#frmQuery").serialize(),
            success: function (data) {
                $("#divLeft").html(data);
                $("#ul_tblList").treeview({
                    url: "Query_servlet?param=getTableCol",
                    ajax: {
                        data: {
                            "table": function () {
                                return "fff";
                            }
                        },
                        type: "post"
                    }

                });

                var RCounter;
                var RCounter2;
                $(".topLi span").draggable({
                    helper: 'clone',
                    revert: function (socketObj) {
                        if (socketObj === false) {
                            RCounter = false;
                        } else {
                            RCounter = true;
                        }
                    },
                    drag: function (event, ui) {
                    	ui.position.top-=$(window).parent().scrollTop();
                    }   ,

                    stop: function (event, ui) {
                        if (RCounter) {
                            var tempID = ui.helper[0].innerHTML;
                            $(ui.helper).clone(true)
                                .html("<div id='" + tempID + "_box'><div><p class='DivFixed'>" + tempID + "</p> <span class='spanClose' onclick='spnClose(\""+ $.trim(tempID) + "_box\")' >X</span></div><div class='divClv'>   </div><div class='spanData' style='width:auto'>Fatching Columns</div></div>")
                                .removeClass('ui-draggable ui-draggable-dragging')
                                .addClass('box-clone')
                                .appendTo('#divRightTop')
                                .resizable({
                                    maxWidth: 200,
                                    minWidth: 150,
                                    alsoResize: "#" + tempID + "_box .spanData"
                                })
                                .draggable({
                                    revert: 'invalid',
                                    handle: 'p',
                                  	containment:"parent" 
                                                              	
                                }
                                );

                            $.ajax({
                                type: "POST",
                                url: 'Query_servlet',
                                data: 'param=getColumnListForBox&id=' + tempID,
                                success: function (data) {
                                    $("#" + tempID + "_box .spanData").html(data);

                                    $("#" + tempID + "_box .spanData input").change(function () {
                                        addTableColumn(this.className, this.id, this.checked);
                                    });

                                    

                                    $("#" + tempID + "_box .spanData table tr").droppable({
                                            hoverClass: "ui-state-hover",
                                            helper: "clone",
                                            cursor: "move",
                                            drop: function (event, ui) {
                                                var sourceValue = $(ui.draggable).find("input:hidden").val();
                                                var targetValue = $(this).find("input:hidden").val();

                                                $(ui.draggable)
                                                    .find("input:hidden")
                                                    .val(sourceValue + "_" + targetValue);

                                                svgDrawLine($(this), $(ui.draggable));
                                            }

                                        }

                                    ).draggable({
                                        helper: 'clone',
                                        revert: function (socketObj) {
                                            if (socketObj === false) {
                                                RCounter2 = false;
                                            } else {
                                                RCounter2 = true;
                                            }
                                        },
                                        start: function (event, ui) {
                                        
                                        },
                                        drag: function (event, ui) {
                                        
                                        },
                                        stop: function (event, ui) {
                                            if (RCounter2) {

                                            }
                                        }
                                    });

                                }
                            });
                        }
                    }
                });

               

            }
        });
}
else
{
alert('Please Select Schema.');
	}

    });

});

function moveDiv(divName) {

}

var controls;

function svgClear() {
    svg.clear();
}

function svgDrawLine(eTarget, eSource) {

    // wait 1 sec before draw the lines, so we can get the position of the draggable
    setTimeout(function () {

        if($(eSource.parents()[4]).hasClass('ui-draggable'))
        	$(eSource.parents()[4]).draggable('destroy');
        if($(eTarget.parents()[4]).hasClass('ui-draggable'))
    		$($(eTarget.parents()[4])).draggable('destroy');

         var $source = eSource;
        var $target = eTarget;
        Broswer_name;
        Broswer_version;
        // origin -> ending ... from left to right
        // 10 + 10 (padding left + padding right) + 2 + 2 (border left + border right)
        var originX = $source.offset().left - 50;
        var originY = (Broswer_name == 'MSIE') ? ((Broswer_version == '7.0') ? $source.offset().top - 75 : $source.offset().top - 50) : $source.offset().top - 70;

        var endingX = (Broswer_name == 'MSIE') ? ($target.offset().left - 50 - $target[0].offsetWidth) : ($target.offset().left - 50 - $target[0].offsetWidth - 30);
        var endingY = (Broswer_name == 'MSIE') ? ((Broswer_version == '7.0') ? $target.offset().top -75 : $target.offset().top - 50) : $target.offset().top - 70;

        var space = 20;
        var color = 'blue';

        var a = "M" + originX + " " + originY + " L" + (originX + space) + " " + originY; // beginning
        var b = "M" + (originX + space) + " " + originY + " L" + (endingX - space) + " " + endingY; // diagonal line
        var c = "M" + (endingX - space) + " " + endingY + " L" + endingX + " " + endingY; // ending
        var all = a + " " + b + " " + c;

       varAppIDS="";
        /**/
var tempID;
        // write line
        myLines[myLines.length] = svg
            .path(all)
            .attr({
                "stroke": color,
                "stroke-width": 2,
                "stroke-linecap": "round",
                "ID": "uiuiu"
            }).dblclick(function () {
            	tempID=this.id;
                var tempArry=[];
                if (confirm("Are sure want to remove this Link ?")) {
                	$.each(LinqName, function(i,n) { 
                		if (n[4] != tempID) tempArry.push(n);
                		if (n[4] == tempID){
                			$("#"+n[0]+"_box").removeClass(n[2]+"_box~"+tempID);
                			$("#"+n[2]+"_box").removeClass(n[0]+"_box~"+tempID);	
                    	};
                    });
                	LinqName=tempArry;
                    this.remove();
                    $('#divRightBottomText').val(updateQuery());
                }
            }); 

        LinqName.push( [$(eSource)[0].firstChild.children[0].childNodes[0].className ,$(eSource)[0].firstChild.children[0].childNodes[0].id,$(eTarget)[0].firstChild.children[0].childNodes[0].className,$(eTarget)[0].firstChild.children[0].childNodes[0].id, myLines[myLines.length-1].id]) ;

        $($(eSource).parents()[3]).addClass($(eTarget).parents()[3].id+"~"+myLines[myLines.length-1].id.toString());
        $($(eTarget).parents()[3]).addClass($(eSource).parents()[3].id+"~"+myLines[myLines.length-1].id.toString());
        
        $('#divRightBottomText').val(updateQuery());
    }, 1000);

}

var discattr = {
    fill: "#000",
    stroke: "none"
};
var tableName = [];
var ColumnName = [];
var LinqName = [];
var query = "";

function updateQuery() {
    query = "SELECT  ";
    $.each(ColumnName, function (i, j) {
        query += j[0] + "." + j[1] + ", ";
    });

    query = query.substr(0, query.length - 2) + " ";
    query += "   FROM  ";
    $.each(tableName, function (i, j) {
        query += j + ", ";
    });
    query = query.substr(0, query.length - 2) + " ";
    if (LinqName.length != 0) {
        query += "   WHERE   ";
        $.each(LinqName, function (i, j) {
            query += j[0] + "."+j[1]+"="+j[2]+"."+j[3]+" AND ";
        });
        query = query.substr(0, query.length - 4) + " ";
    }
  
    return query;
}

function addTableColumn(tableName_1, columns_1, temp_checked) {
    if (temp_checked) {
        ColumnName[ColumnName.length] = [tableName_1,columns_1];
        tableName=distinct(ColumnName);
    }
    else if (!temp_checked) {
        ColumnName = REMOVEarry(ColumnName,columns_1,'C',tableName_1);
        tableName=distinct(ColumnName);
    }
    $('#divRightBottomText').val(updateQuery());
}


function ADDarry(arr,item)
{
		if(-1 == $.inArray(item,arr )) {
			arr.push(item);
		}		
	
	return arr;
};

function REMOVEarry(arr,item,action,tableName) {
	var tempArry=[];
if(action=='C')
{
	$.each(arr, function(i,n) { 
		if (n[1] != item || n[0]!=tableName) tempArry.push(n);
	});
}

if(action=='T')
{
	$.each(arr, function(i,n) { 
		if (n[0] != item)tempArry.push(n);
	});
}
return tempArry;
}






function distinct(anArray)
{
	var result = [];
	$.each(anArray, function(i,v){
	    if ($.inArray(v[0], result) == -1) result.push(v[0]);
	});
	return result;
}

function spnClose(id)
{
	
	id=$.trim(id);
	  if (confirm("Are sure want to remove this Table ?")) {
		  if($("#"+id).attr('Class')!=undefined && $("#"+id).attr('Class')!="")
		  {
         $.each($("#"+id).attr('Class').split(' '),function(i,j){
        	 svg.getById(j.split('~')[1]).remove();
        	 $(JqueryName("."+$.trim(id)+"~"+j.split('~')[1])).removeClass($.trim(id)+"~"+j.split('~')[1]);
        	
        	 var tempArry=[];
             $.each(LinqName, function(i,n) { 
         		if (n[4] != tableName) tempArry.push(n);
         	 });
         	LinqName=tempArry;
           
             
          });
		  }
		  $($("#"+id).parents()[0]).remove();
		  ColumnName = REMOVEarry(ColumnName,$.trim(id).split('_')[0]+"_"+$.trim(id).split('_')[1],'T','');
		  tableName=distinct(ColumnName);
         
         $('#divRightBottomText').val(updateQuery());
      }
}

function JqueryName(id)
{
	return id.replace(new RegExp("~","g"),"\\~");
}

</script>

</head>
<body>




<div id="divContainer">
<div id="divTop">
<form action="Query_servlet" method="Post" id='frmQuery' name='frmQuery'>

<input type="hidden" id='param' name='param' value='getTableList' ></input>


<%
 String res = "";
 Query_Service service = new Query_Service();
 res= service.GetAppSchema(session.getAttribute("appName").toString());
 %>
 <%=res %>

<input type="button" id='btnGO' Value='Go'><a style="color: WHITE;margin-left: 100px;">FINDART (ENTERPRISE ARCHIVAL SYSTEM) QUERY BUILDER<font size="1" style=color: WHITE> 1.1.5 </font></a></form>

<!-- <input type="button" id='btnPoint' Value='Go'>-->

</div> 

<div id="divMain">
<div>

<div id="divLeft"></div>


<div id="divRight">
<div id="divRightTop"></div>
<div id="divRightBottom"><p style="margin: 3px 10px;">Query:</p><textarea id="divRightBottomText"></textarea></div>
<div id="divRightBottom22"><p style="margin: 3px 10px;">Where Condition:</p><textarea id="divRightBottomTextWhere"></textarea></div>
<div id="divRightBottomButton" style="float"><input type="button" id='btnRUN' Value='RUN'></div>
</div>
<div class="divClv"></div>

<div id="divBottom">
<table id="tblOutput"></table>
<div id="pager2"></div>
</div>


 <form action="QueryExcel.jsp" method="post" style="display: none;" id="frmPdfOutPut">	
	<input type="hidden" id="txtTypeOutPut" name="txtTypeOutPut">
	<input type="hidden" id="txtReportName" name="txtReportName">
	<input type="hidden" id="txtHTMLOutPut" name="txtHTMLOutPut">
	<input type="hidden" id="txtWidth" name="txtWidth">
	
	
	<textarea id="txtQuery" name="txtQuery"><textarea>
</form>

</div>
</div>
</div>

</body>
</html>
