<jsp:include page="new_header.jsp"></jsp:include>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Aadhaar Access Logs</title>
<script type="text/javascript">

$(function() {
	getAppName();
});
function getAppName(){
	jQuery.ajax({
		type:"POST",
		url:"Activity_LogServlet?operation=getAppName" ,
		data:{}, 
		success:function(msg){
				jQuery("#txtAppName").html(msg);
			}
		});
}
	$(function() {
		$('#frmDate').calendarsPicker({
			calendar : $.calendars.instance('gregorian')
		});
	});
	$(function() {
		$('#toDate').calendarsPicker({
			calendar : $.calendars.instance('gregorian')
		});
	});
	jQuery(document).ready(function($) {
		resizeWindow();
		jQuery('#tt1').datagrid({
			
			url : 'Activity_LogServlet?operation=accessLog',
			columns : [ [
			{
				field : 'USER_ID',
				title : 'USER ID',
				width : 50
			},
			
			{
				field : 'APPNAME',
				title : 'APP NAME',
				width : 60
			},  
			{
				field : 'REFNO',
				title : 'REF NO',
				width : 60
			},  
			
			{
				field : 'DATE',
				title : 'DATE',
				width : 60
			}, {
				field : 'TIME',
				title : 'TIME',
				width : 60
			},
			{
				field : 'IP_ADDRESS',
				title : 'IP ADDRESS',
				width : 80
			}, {
				field : 'TASK_PERFORMED',
				title : 'TASK PERFORMED',
				width : 85
			}, {
				field : 'DESCPTION',
				title : 'DESCPTION',
				width : 500
			}

			] ],
			showTableToggleBtn : true,
			title : "Aadhaar access Log", //report Title
			height : 400, //Height of Grid
			width : 850,//Width of Grid
			pageSize : 50,
			pageNumber : 1

		});
		$("body").resize(function(e) {
			resizeWindow();
		});
	});
	
</script>

</head>

<body>
	<form id="form1" name="form1" method="post">
		<section id="content">
			<section class="main padder">
				<div class="row">

					<section class="panel">
						<header class="">
							<ul class="nav nav-tabs nav-justified">
								<li class="active"><a href="#search" data-toggle="tab">Search</a></li>
								<li><a href="#gdata" data-toggle="tab">Get Data</a></li>
							</ul>
						</header>
						<div class="panel-body" style="margin-left: 10%;">
							<div class="tab-content">
								<div class="tab-pane active" id="search">
									<div class="search_section">
										<div id="tb" class="tb_borders">
											<span>
												<h3>Aadhaar access  Log</h3> <br>
											</span> <input type="hidden" id="chkUserID" name="chkUserID"
												value="chkUserID"></input>
											<table class="TblMain">

												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">User
																ID:&nbsp;</label>
															<div class="col-lg-8">
																<input id="userID" type="text" autocomplete="false" name="userID"
																	class="bg-focus form-control" />
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>

												<!-- <tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">
																Aadhaar No.:&nbsp;</label>
															<div class="col-lg-4">
																<input id="uuid" name="uuid" autocomplete="false"
																	type="password"
																	class="bg-focus form-control" />
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr> -->
												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">
																Reference No.:&nbsp;</label>
															<div class="col-lg-4">
																<input id="uuid2" name="uuid2" autocomplete="false"
																	type="text"
																	class="bg-focus form-control" />
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">
																Date From:&nbsp;</label>
															<div class="col-lg-4">
																<input id="frmDate" name="frmDate"
																	size=10 onblur="checkdate(this,this.value)" type="text"
																	class="bg-focus form-control" />
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">
																Date To:&nbsp;</label>
															<div class="col-lg-4">
																<input id="toDate" name="toDate"
																	size=10 onblur="checkdate(this,this.value)" type="text"
																	class="bg-focus form-control" />
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												
												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">
																Application Name:</label>
															<div class="col-lg-4">
																<select id="txtAppName" class="input-text"   name="txtAppName" style="padding-left: 30%;">
																</select>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												<tr>
													<td colspan="3" style="padding-top: 15px;"><input
														class="btn_style btn btn-primary" onclick="doSearch()"
														plain="true" type="button" value="Search" id="serch_btn" />
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<!-- <input type="button"
														id="test" class="btn_style btn btn-primary" name="btn_Dwnload"
														value="Download" onclick="createOutput()" /> -->&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="reset"
														id="test" class="btn_style btn btn-white" name="btn_Clear"
														value="Clear" onclick="clrTxtEle()" /></td>
													<td></td>

												</tr>



											</table>

										</div>

									</div>

								</div>
								<div class="tab-pane" id="gdata">

									<div id="pdatagrid"
										style="width: 100%; overflow: auto; padding: 3%">
										<div id="tb" style="padding: 3px">

											<table id="tt1" class="easyui-datagrid" width="100%"
												iconCls="icon-search" rownumbers="true" pagination="true">
											</table>
										</div>
									</div>


								</div>
							</div>
						</div>
					</section>




				</div>
			</section>
		</section>

	</form>

	<script type="text/javascript">
		function createOutput() {
			$('#hdn_userID').val($('#userID').val());
			$('#hdn_refNo').val($('#uuid2').val());
			$('#hdn_adhNo').val($('#uuid').val());
			$('#hdn_frmDate').val($('#frmDate').val());
			$('#hdn_toDate').val($('#toDate').val());
			$('#hdn_AppName').val($('#txtAppName').val());
			$('#txtfilename_output').val("fName");
			$('#hdn_accessLog').val("Y");
			$('#txtTypeOutPut').val("pdf");
			$('#txtHTMLOutPut').val("");
			$('#frmPdfOutPut').submit();

		}
		function doSearch() { 
			
			$("a[href*=#gdata]").click();
				$('#tt1').datagrid('load', {
				msg : 'No Record Available For Selected criteria... ',
				hdn_accessLog:$('#hdn_accessLog').val(),
				hdn_userID : $('#userID').val(),
				hdn_frmDate : $('#frmDate').val(),
				hdn_toDate : $('#toDate').val(),
				hdn_adhNo : $('#uuid').val(),
				hdn_refNo : $('#uuid2').val(),
				hdn_AppName : $('#txtAppName').val(),
				flg:'Y'

			});

		}
	</script>
	<form action="ActivityLogPDF.jsp" method="post" style="display: none;"
		id="frmPdfOutPut">
		<input type="hidden" id="hdn_userID" name="hdn_userID">
		<input type="hidden" id="hdn_frmDate" name="hdn_frmDate">
		<input type="hidden" id="hdn_toDate" name="hdn_toDate">
		<input type="hidden" id="hdn_refNo" name="hdn_refNo">
		<input type="hidden" id="hdn_adhNo" name="hdn_adhNo">
		<input type="hidden" id="hdn_AppName" name="hdn_AppName">
		<input type="hidden" id="hdn_accessLog" name="hdn_accessLog">
		<input type="hidden" id="txtHTMLOutPut" name="txtHTMLOutPut">
	</form>
	<div id="divAdd" style="display: none;"></div>

</body>


</html>
<jsp:include page="Footer.jsp"></jsp:include>
