<jsp:include page="new_header.jsp"></jsp:include>
<%@ page import="com.dto.UserMasterDTO"%>
<%@ page import="com.service.UserMaster_service"%>
<%@ page import="com.connection.*"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@page import="java.net.URL"%><html>
<title>Add New Application</title>


<style type="text/css">
.panel-body {
	min-height: 500px; !important;
}
</style>

<script type="text/javascript">

//<![CDATA[
           
jQuery(document).ready(function($) {
	//Getdata When Load Form using

resizeWindow();	
	
	
	
	
	
	$("body").resize(function(e){
		resizeWindow();
	});
	
});


function doSearch() { //Function use Search The Data Using user paramater
	 $("a[href*=#gdata]").click();	 
	
	
}


function checkSave()
{
	var st = 0;
	
	if ($("#txtAPP_NAME").val() == '')
	{
		alert('APP NAME cannot be blank');
		$("#txtAPP_NAME").focus();
		return false;
	}
	if ($("#txtAPP_VERSION").val() == '')
	{
		alert('APP VERSION cannot be blank');
		$("#txtAPP_VERSION").focus(); 
		return false;
	}
	if ($("#txtAPP_SCHMA_NAME").val() == '')
	{
		alert('APP SCHMA NAME cannot be blank');
		$("#txtAPP_SCHMA_NAME").focus(); 
		return false;
	}

	if ($("#txtBANK_NAME").val() == '')
	{
		alert('BANK NAME cannot be blank');
		$("#txtBANK_NAME").focus(); 
		return false;
	}
	if ($("#txtAPP_PATH").val() == '')
	{
		alert('APP PATH cannot be blank');
		$("#txtAPP_PATH").focus(); 
		return false;
	}
	if ($("#selAPP_FLAG").val() == '')
	{
		alert('APP FLAG cannot be blank');
		$("#selAPP_FLAG").focus();
		return false;
	}
	
	if (cdate($("#txtMIGRATION_DATE").val()).toString() == "")
	{
		alert('MIGRATION DATE cannot be blank');
		return false;
	}
	
	else
	{
				document.form1.action = 'App_servlet?operation=AddNew';
				document.form1.submit();
	}
}



function GetName()
{
	$("#chkStatus").html('<img src=images/loading.gif>');
	var qstring=$("#txtUserID").val();
	
	if (qstring != '')
	{
		$.get('UserAuthentication_servlet?opr=GetName&userId='+ qstring,function(responseText){
			if (responseText == 'NO')
			{
				$("#chkStatus").html('<font color=red>Invalid ID</font>');
				$("#txtUserID").focus();		
			}
			else if(responseText == 'N') 
			{
				$("#chkStatus").html('<font color=red>ID already added</font>');
				$("#txtUserID").focus();
				//$("#txtUserName").val(responseText);
			}
			else if(responseText == 'E')
			{
				$("#chkStatus").html('<font color=red>Error Occured</font>');
			}
			else if(responseText != '')
			{
				$("#chkStatus").html('<font color=green>ID Validated</font>');
				$("#txtUserName").val(responseText);
			}
		});
				
	}
	else
	{
		$("#chkStatus").html('');
	}
	
}






	//]]>
	</script>

</head>

<body>
<%
 	UserMasterDTO dto = new UserMasterDTO();
 	UserMaster_service srv = new UserMaster_service();
 	if (request.getParameter("action") != null)
 		if (request.getParameter("action").equals("EditUser")
 				&& request.getParameter("UID") != null) {
 			dto = srv.GetUserDetails(request.getParameter("UID"));
 		}
 %>
	<form id="form1" name="form1" method="post">
		<section id="content">
			<section class="main padder">
				<div class="row">


					<section class="panel">
						<header class="panel-heading">
							<ul class="nav nav-tabs nav-justified">
								<li class="active"><a href="#search" data-toggle="tab"></a></li>
							
							</ul>
						</header>
						<div class="panel-body">
							<div class="tab-content">
								<div class="tab-pane active" id="search">
									<div class="search_section">
										<div id="tb" class="tb_borders">
											<span>
												<h3>Add New Application</h3> <br>
											</span> <input type="hidden" id="chkUserID" name="chkUserID"
												value="chkUserID"></input>
											<table class="TblMain">
												
												
												<tr><td><div class="form-group">  <label class="col-lg-3 control-label">Application Name</label>  <div class="col-lg-8"> &nbsp;<input type="text" id="txtAPP_NAME" value=""
			name="txtAPP_NAME"  class="bg-focus form-control" ></input>     <div class="line line-dashed m-t-large"></div>  </div> </div></td></tr> 
<tr><td><div class="form-group">  <label class="col-lg-3 control-label">Application Version</label>  <div class="col-lg-8"> &nbsp;<input type="text" id="txtAPP_VERSION" value=""
			name="txtAPP_VERSION"  class="bg-focus form-control" ></input>     <div class="line line-dashed m-t-large"></div>  </div> </div></td></tr> 
<tr><td><div class="form-group">  <label class="col-lg-3 control-label">Schema Name</label>  <div class="col-lg-8"> &nbsp;<input type="text" id="txtAPP_SCHMA_NAME" value=""
			name="txtAPP_SCHMA_NAME"  class="bg-focus form-control" ></input>     <div class="line line-dashed m-t-large"></div>  </div> </div></td></tr> 
<tr><td><div class="form-group">  <label class="col-lg-3 control-label">Migration Date</label>  <div class="col-lg-8"> &nbsp;<input  class="bg-focus form-control"  type="text"
			id="txtMIGRATION_DATE" value="" onblur="checkdate(this,this.value)"
			name="txtMIGRATION_DATE"></input>     <div class="line line-dashed m-t-large"></div>  </div> </div></td></tr> 
<tr><td><div class="form-group">  <label class="col-lg-3 control-label">Bank Name</label>  <div class="col-lg-8"> &nbsp;<input type="text" id="txtBANK_NAME" value=""
			name="txtBANK_NAME"  class="bg-focus form-control" ></input>     <div class="line line-dashed m-t-large"></div>  </div> </div></td></tr> 
<tr><td><div class="form-group">  <label class="col-lg-3 control-label">Application URL</label>  <div class="col-lg-8"> &nbsp;<input type="text" id="txtAPP_PATH" value=""
			name="txtAPP_PATH"  class="bg-focus form-control" ></input>     <div class="line line-dashed m-t-large"></div>  </div> </div></td></tr> 
<tr><td><div class="form-group">  <label class="col-lg-3 control-label">Application Status</label>  <div class="col-lg-8"> <select  class="bg-focus form-control"  id="selAPP_FLAG"
			name="selAPP_FLAG">
			<option value="Y" selected="true">Enable</option>
			<option value="N">Disable</option>
		</select>
		     <div class="line line-dashed m-t-large"></div>  </div> </div></td></tr> 

												
												<tr>
												
													<td colspan="3" style="padding-top: 15px;"><input
														class="btn_style btn btn-primary" onclick="return checkSave()"
														plain="true" type="button" value="Save" id="serch_btn">
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="button"
														id="test"  onclick="clr()"
														class="btn_style btn btn-white" name="btn_Clear"
														value="Clear">
														<input	type="hidden" id="hidAction" name="hidAction">
														</td>
													<td></td>

												</tr>
												

												<tr>
													<td colspan="3" class="char_srch_style"
														style="text-align: center;">Use * for wild card
														search Example : <B>BOM*</B>
													</td>
												</tr>
											</table>

										</div>

									</div>

								</div>
								<div class="tab-pane" id="gdata">
									<div id="pdatagrid"
										style="width: 100%; overflow: auto; padding: 3%">
										<table id="tt1" class="easyui-datagrid" width="100%"
											iconCls="icon-search" rownumbers="true" pagination="true">
										</table>
									</div>
								</div>

							</div>
						</div>

					</section>




				</div>
			</section>
		</section>

	</form>

<script type="text/javascript">
	$("#reg_box_wrapper").hide();

	$(function() {
		$('#txtMIGRATION_DATE').calendarsPicker( {
			calendar : $.calendars.instance('gregorian')
		});
	});
</script>


</body>
</html>
