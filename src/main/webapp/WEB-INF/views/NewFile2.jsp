<jsp:include page="new_header.jsp"></jsp:include>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@page import="java.net.URL"%><html>
<title>Drawing Power Inquiry</title>


<style type="text/css">
.panel-body {
	min-height: 500px; !important;
}
</style>

<script type="text/javascript">

//<![CDATA[
           
jQuery(document).ready(function($) {
	//Getdata When Load Form using

resizeWindow();	
	
		check_links(); 
jQuery('#tt1').datagrid({  
	    url:'Fin7_Drawing_Power_Inquiry_Servlet', 
	    columns:[[  
		 {field:'account_no',title:'Account No',width:100},
 {field:'account_name',title:'Account Name',width:100},
 {field:'account_currency',title:' Account Currency',width:100},
 {field:'account_solid',title:' Account Branch Code',width:100},
 {field:'applicable_date',title:'Applicable Date',width:100},
 {field:'event_type',title:'Event Type',width:100},
 {field:'dp_indicator',title:'DP Indicator',width:100},
 {field:'drawing_power_per',title:' Drawing Power %',width:100},
 {field:'drawing_power',title:'Drawing Power',width:100},
 {field:'status',title:'Status',width:100},
 {field:'sanction_limit',title:'Sanction Limit',width:100} , 
 {field:'remarks',title:'Remarks',width:150}        
	    ]] ,
	     showTableToggleBtn: true ,
	    title:"Drawing Power Inquiry", //report Title
	    height:400, //Height of Grid
	    width:1420,//Width of Grid
	    pageSize:50
	    
	}); 
	
	
	
	$("body").resize(function(e){
		resizeWindow();
	});
	
});


function doSearch() { //Function use Search The Data Using user paramater
	 $("a[href*=#gdata]").click();	 
	
	$('#tt1').datagrid('load', {
			msg:'No Drawing Power Details Available For This Account ',
			//msg:'details does not exists on the database',
			inpaccount_no :$('#inpaccount_no').val(),
			 flg:"Y"
		});
}

function check_links()
	{
		var acc_no='<%=request.getParameter("inpaccountno")%>';
		
		
		if(acc_no!='null')
				{
					$('#menus').hide(); 
					
					
					$('#inpaccount_no').val(acc_no); 
					$('#inpaccount_no').attr("disabled","disabled");
					
					//$('#Report_type').attr("disabled","disabled");
				}
	}
		
	
	function createOutput(typeOutPut)
	{
		$('#txtaccount_noOutPut').val($('#inpaccount_no').val()); 		$('#txtTypeOutPut').val(typeOutPut);
		$('#txtReportName').val("Drawing Power Inquiry");		
		$('#txtHTMLOutPut').val("");	
		$('#frmPdfOutPut').submit();
		
	}





	//]]>
	</script>

</head>

<body>
	<form id="form1" name="form1" method="post">
		<section id="content">
			<section class="main padder">
				<div class="row">


					<section class="panel">
						<header class="panel-heading">
							<ul class="nav nav-tabs nav-justified">
								<li class="active"><a href="#search" data-toggle="tab">Search</a></li>
								<li><a href="#gdata" data-toggle="tab">Get Data</a></li>
							</ul>
						</header>
						<div class="panel-body">
							<div class="tab-content">
								<div class="tab-pane active" id="search">
									<div class="search_section">
										<div id="tb" class="tb_borders">
											<span>
												<h3>Drawing Power Inquiry</h3> <br>
											</span> <input type="hidden" id="chkUserID" name="chkUserID"
												value="chkUserID"></input>
											<table class="TblMain">
												
												
												<tr><td><div class="form-group">  <label class="col-lg-3 control-label">Account No :&nbsp;</label>  <div class="col-lg-8"> 
												<input id="inpaccount_no" type="text"  class="bg-focus form-control" />
	     <div class="line line-dashed m-t-large"></div>  </div> </div></td></tr> 
												
												
												
												<tr>
													<td colspan="3" style="padding-top: 15px;"><input
														class="btn_style btn btn-primary" onclick="conditions()"
														plain="true" type="button" value="Search" id="serch_btn">
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="button"
														id="test" disabled="disabled" onclick="clr()"
														class="btn_style btn btn-white" name="btn_Clear"
														value="Clear"></td>
													<td></td>

												</tr>
												

												<tr>
													<td colspan="3" class="char_srch_style"
														style="text-align: center;">Use * for wild card
														search Example : <B>BOM*</B>
													</td>
												</tr>
											</table>

										</div>

									</div>

								</div>
								<div class="tab-pane" id="gdata">
									<div id="pdatagrid"
										style="width: 100%; overflow: auto; padding: 3%">
										<table id="tt1" class="easyui-datagrid" width="100%"
											iconCls="icon-search" rownumbers="true" pagination="true">
										</table>
									</div>
								</div>

							</div>
						</div>

					</section>




				</div>
			</section>
		</section>

	</form>
	
	
<form action="Fin7_Drawing_Power_Inquiry_PDF.jsp" method="post" style="display: none;" id="frmPdfOutPut">
	<input type="hidden" id="txtaccount_noOutPut" name="txtaccount_noOutPut"> 	<input type="hidden" id="txtTypeOutPut" name="txtTypeOutPut">
	<input type="hidden" id="txtHTMLOutPut" name="txtHTMLOutPut">
	<input type="hidden" id="txtReportName" name="txtReportName">	
</form>

<script type="text/javascript">
function conditions()
{

	if($('#inpaccount_no').val()==''){ alert('Please Enter Account No.'); return false; } 
	if($('#inpaccount_no').val()!=''){
		Fin7_Get_Input_Info($('#inpaccount_no').val(),'','','','','','','','','','');
	} 
	else{
	doSearch();
	}
	
} 
</script>


</body>
</html>
