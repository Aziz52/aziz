<html>
<jsp:include page="new_header.jsp"></jsp:include>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@page import="java.net.URL"%><html>
<title></title>


<style type="text/css">
.panel-body {
	min-height: 500px; !important;
}
</style>

<script type="text/javascript">

//<![CDATA[
           
jQuery(document).ready(function($) {
	//Getdata When Load Form using

resizeWindow();	
	
	jQuery('#tt1').datagrid({  
	    url:'AddTempateServlet?operation=GetData',  
	    
	    columns:[[  
	        
	        {field:'tempalte_name',title:'Template Name',width:250}, 
	        {field:'searchField',title:'Edit',width:170}, 
	      
	       
	    ]] ,
	     showTableToggleBtn: true ,
	    title:"Template Details", //report Title
	    height: 150, //Height of Grid
	    width:500 //Width of Grid
	    
	}); 


	//$(".pagination table tbody tr ").append('<td><a onclick="newSave();"  class="easy-linkbutton" href="javascript:void(0)">Save</a></td>');

	
	
	
	$("body").resize(function(e){
		resizeWindow();
	});
	
});


function doSearch() { //Function use Search The Data Using user paramater
	 $("a[href*=#gdata]").click();	 
	
	$('#tt1').datagrid('load', {
		UserID :$('#UserID').val(),
		UserName :$('#UserName').val()
	//	acc_no1:'0002BC0001151'
	});
}

function chkReset(userID)
{
	var st = '<%=session.getAttribute("user_id") %>';
	if (st != userID)
	{
		$("#chkUserID").val(userID);
		document.form1.action = 'UserAuthentication_servlet?operation=ResetLogin';
		document.form1.submit();
	}
	else
	{	
		alert('Please Log out to perform the action');
		return false;
	}
	
}

function newSave()
{
	var tempCheked="";
	var tempNotCheked="";
	$.each($("input[name^='chk~']"),function(i,j)
		{
		if(j.checked)
		{
			tempCheked+="'"+j.id.split("~")[1]+"',";
		}
		else
		{
			tempNotCheked+="'"+j.id.split("~")[1]+"',";
		}
		});
	var str="";
	if(tempCheked.length > 0)
	{
		str="tempCheked="+tempCheked.substr(0,tempCheked.length-1)+"&";
	}
	if(tempNotCheked.length > 0)
	{
		str+="tempNotCheked="+tempNotCheked.substr(0,tempNotCheked.length-1);
	}

	 jQuery.ajax({
         type: "POST",
         url: 'App_servlet?operation=saveData',
         data: str,
         success: function (data) {         	
         		if (data="y")
    			{
    				alert('Data Updated successfully');window.location.href='App_master.jsp';	
    			}
    			else
    			{
    				alert('Action Failed');window.location.href='App_master.jsp';
    			}
         }
     });
	
}



function EditData(id,metaid)
{    
	
	
	//alert(metaid);
	
	///window.open('Fin7_Show_Report.jsp?rTyp=TD&acc_type='+Acc_type+'&accno='+ Account_Number +'&sol_id='+ sol_id,'winname','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=900,height=500,top=100,left=70');
	
	   window.location.href='CAddTemplate.jsp?operation=Edit&id='+id+'&metaid='+metaid;
	
}




	//]]>
	</script>

</head>

<body>
	<form id="form1" name="form1" method="post">
		<section id="content">
			<section class="main padder">
				<div class="row">


					<section class="panel">
						<header class="panel-heading">
							<ul class="nav nav-tabs nav-justified">
								
								<li class="active"><a href="#gdata" data-toggle="tab"></a></li>
							</ul>
						</header>
						<div class="panel-body"  style="padding-left: 46px;">
							<div class="tab-content">
								<a href="CAddTemplate.jsp">Add New Template</a>
								<div class="tab-pane active" id="gdata">
									<div id="pdatagrid"
										style="width: 100%; overflow: auto; padding: 3%">
										<table id="tt1" class="easyui-datagrid" width="100%"
											iconCls="icon-search" rownumbers="true" pagination="true">
										</table>
									</div>
								</div>

							</div>
						</div>

					</section>




				</div>
			</section>
		</section>

	</form>



</body>
	<jsp:include page="Footer.jsp"></jsp:include>

</html>
