<jsp:include page="new_header.jsp"></jsp:include>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SwiftDAR_search</title>

<script type="text/javascript">
	//         

	jQuery(document).ready(function($) {
		//Getdata When Load Form using

		resizeWindow();
		
		$.ajax({
			type : "POST",
			url : "Template_commanDetails_servlet?Detail=Select_Zone",
			data : "",
			success : function(msg) {
				$("#Zone_Name").html(msg);

			}
		});

		jQuery('#tt1').datagrid({
			
			url : 'TemplateSearch_Master_Servlet?Report=master',
			columns : [ [
			{
				field : 'zone_Name',
				title : 'Zone Name',
				width : 100
			},
			{
				field : 'report_Name',
				title : 'Report Name',
				width : 200
			}, 
			
			{
				field : 'report_version',
				title : 'Version',
				width : 20
			}, {
				field : 'report_Date',
				title : 'Report Date',
				width : 80
			}, {
				field : 'file_name',
				title : 'File Name',
				width : 160
			},
			/* 		  	     {field:'keywords', title:'KeyWords',width:280},  
			 */{
				field : 'view',
				title : 'View',
				width : 50
			}, {
				field : 'pdf',
				title : 'PDF',
				width : 50
			}, {
				field : 'download',
				title : 'Download',
				width : 70
			}

			] ],
			showTableToggleBtn : true,
			title : "Search Report", //report Title
			height : 400, //Height of Grid
			width : 850,//Width of Grid
			pageSize : 50,
			pageNumber : 1

		});
		$("body").resize(function(e) {
			resizeWindow();
		});

		/* 	doSearch() ;
		 */
	});

	function File_details(Value) {

		try {
			jQuery.ajax({
				type : "POST",
				url : "TemplateSearch_Master_Servlet",
				data : "Report=getdetails&Value=" + Value,
				success : function(msg) {
					$("#divAdd").html(msg);
					$("#divAdd").dialog('open');
				}
			});

			$(function() {

				$('#divAdd').dialog({
					autoOpen : false,
					title : "File Details",
					width : 700,
					height : 'auto',
					modal : true
				});

			});

		} catch (e) {
			alert(e);
		}
	}

	$(function() {
		$('#Report_Date').calendarsPicker({
			calendar : $.calendars.instance('gregorian')
		});
	});

	$(function() {
		$('#Report_Date2').calendarsPicker({
			calendar : $.calendars.instance('gregorian')
		});
	});

	function doSearch() { //Function use Search The Data Using user paramater
		
		$("a[href*=#gdata]").click();
			$('#tt1').datagrid('load', {
			msg : 'No Record Available For Selected criteria... ',
			file_name : $('#file_name').val(),
			keywords : $('#Report_Date').val(),
			content : $('#KeyWord').val(),
			Report_Date2 : $('#Report_Date2').val(),
			zone : $('#Zone_Name').val()

		});

	}

	//
</script>

</head>

<body>
	<form id="form1" name="form1" method="post">
		<section id="content">
			<section class="main padder">
				<div class="row">

					<section class="panel">
						<header class="">
							<ul class="nav nav-tabs nav-justified">
								<li class="active"><a href="#search" data-toggle="tab">Search</a></li>
								<li><a href="#gdata" data-toggle="tab">Get Data</a></li>
							</ul>
						</header>
						<div class="panel-body" style="margin-left: 10%;">
							<div class="tab-content">
								<div class="tab-pane active" id="search">
									<div class="search_section">
										<div id="tb" class="tb_borders">
											<span>
												<h3>Report Search</h3> <br>
											</span> <input type="hidden" id="chkUserID" name="chkUserID"
												value="chkUserID"></input>
											<table class="TblMain">

												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Zone Name
																:&nbsp;</label>
															<div class="col-lg-8">
																<Select id="Zone_Name" class="bg-focus form-control">
																</Select>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>

												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Report
																Name:&nbsp;</label>
															<div class="col-lg-8">
																<input id="file_name" type="text"
																	class="bg-focus form-control" />
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>


												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Report
																Date From:&nbsp;</label>
															<div class="col-lg-4">
																<input id="Report_Date"
																	size=10 onblur="checkdate(this,this.value)" type="text"
																	class="bg-focus form-control" />
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Report
																Date To:&nbsp;</label>
															<div class="col-lg-4">
																<input id="Report_Date2"
																	size=10 onblur="checkdate(this,this.value)" type="text"
																	class="bg-focus form-control" />
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>

												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Key Word:&nbsp;</label>
															<div class="col-lg-8">
																<input id="KeyWord"
																	 type="text"
																	class="bg-focus form-control" />
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												

												<tr>
													<td colspan="3" style="padding-top: 15px;"><input
														class="btn_style btn btn-primary" onclick="doSearch()"
														plain="true" type="button" value="Search" id="serch_btn" />
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="reset"
														id="test" class="btn_style btn btn-white" name="btn_Clear"
														value="Clear" /></td>
													<td></td>

												</tr>



											</table>

										</div>

									</div>

								</div>
								<div class="tab-pane" id="gdata">

									<div id="pdatagrid"
										style="width: 100%; overflow: auto; padding: 3%">
										<div id="tb" style="padding: 3px">

											<table id="tt1" class="easyui-datagrid" width="100%"
												iconCls="icon-search" rownumbers="true" pagination="true">
											</table>
										</div>
									</div>


								</div>
							</div>
						</div>
					</section>




				</div>
			</section>
		</section>

	</form>

	<script type="text/javascript">
		function DispalyFile(docid, filename,rptName,zoneName,rptDate,folderId) {
			createLog('View',filename,rptName,zoneName,rptDate,folderId);
			var e = $("input[type=search]").val();

			 window
					.open(
							'Template_view.jsp?docide=' + docid + '&filename='
									+ filename + '&content=' + e,
							'winname',
							'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=900,height=500,top=100,left=70'); 

		}
		function createOutput(docid, filename,rptName,zoneName,rptDate,folderId) {

			//var kvpairs = [];
			//var form = // get the form somehow
			createLog('PDF',filename,rptName,zoneName,rptDate,folderId);
			$('#txtdocid_output').val(docid);
			$('#txtfilename_output').val(filename);
			$('#txtTypeOutPut').val("pdf");
			$('#txtHTMLOutPut').val("");
			$('#frmPdfOutPut').submit();

		}
	</script>
	<form action="DownloadTemplatePDF" method="post" style="display: none;"
		id="frmPdfOutPut">
		<input type="hidden" id="txtdocid_output" name="txtdocid_output">
		<input type="hidden" id="txtfilename_output" name="txtfilename_output">
		<input type="hidden" id="txtTypeOutPut" name="txtTypeOutPut">
		<input type="hidden" id="txtHTMLOutPut" name="txtHTMLOutPut">
	</form>
	<div id="divAdd" style="display: none;"></div>

</body>


</html>
<jsp:include page="Footer.jsp"></jsp:include>
