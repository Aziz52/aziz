<jsp:include page="new_header.jsp"></jsp:include>
<html><head><title>SwiftDB Vault</title>
<script type="text/javascript">
jQuery(document).ready(function($) {
	jQuery('#tt1').datagrid({
	    url:'DBVault_Servlet?operation=GetVerifyUser', 
	    columns:[[
			//{field:'ACTION',title:'ACTION',width:50}, 
	        {field:'USER_ID',title:'USER ID',width:80},  
	        {field:'IP_ADDTRESS',title:'IP ADDRESS',width:80},
	        {field:'APPLICATION_NAME',title:'APPLICATION NAME',width:100},
	        {field:'USER_STATUS',title:'USER STATUS',width:80}, 
	        {field:'LICENCE_KEY',title:'LICENCE KEY',width:400}
			]] ,
	    queryParams:{
	    		msg : 'No Records...',
	    		vr_flg:'Y'
		    		},
	     showTableToggleBtn: true ,
	    title:"SwiftDB Vault", //report Title
	    height: 350, //Height of Grid
	    width:850 //Width of Grid    
	}); 
});
function clr() 
{
	$('#UserID').val(''); 
	$('#UserName').val(''); 
}
function doSearch() {
	$('#tt1').datagrid('load', {
		msg : 'No Records...',
		UserID :$('#UserID').val(),
		vr_flg:'Y',
		UserName :$('#UserName').val()
	});
	showreg();
}
</script>
</head>
<body style="width: 90%;">
<form id="form1" name="form1" method="post">
<div   class="tab_divs">
<center>
<div class="form_inner_wrap">
<div id="reg_box" class="form_section" style="padding-bottom: 4%;"><br />
<br />

<div id="get_details">
<center>
<table id="tt1" style="diplay: none !important;" class="easyui-datagrid"
	iconCls="icon-search" rownumbers="true" width="90%" pagination="true">
</table>
<br> 
</center>
</div>
</div>
</div>
<div id="search_box" class="search_section" style="display: none;padding-top: 12%;padding-bottom: 16%;">

<div id="tb" class="tb_borders">
<span>
<h3>Swift DB Vault</h3>

</span>

<input type="hidden" id="chkUserID" name="chkUserID" value="chkUserID"></input>
<table>
	<tr>
		<td><label class="cm-required">User ID :&nbsp;</label></td>
		<td><input id="UserID" type="text" class="input-text" /></td>
		<td>*</td>
	</tr><tr>
		<td><label class="cm-required">User Name:&nbsp;</label></td>
		<td><input id="UserName" type="text" class="input-text" /></td>
		<td>*</td>
	</tr>
	

	
	<tr>
		<td  colspan="3" style="padding-top: 15px;">
		
		<input class="btn_style" onclick="doSearch()"  plain="true" type="button" value="Search">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" onclick="clr()" class="btn_style" name="btn_Clear" value="Clear"></td>
		<td></td>
	</tr>
</table>

</div>
</div>
<div style="display:none;">
<div id="verify" style="padding:8px;">

<table class="mTable" >
	<tr>
		<td>User Id</td>
		<td>&nbsp;<input type="text" id="txt_User_id" value=""
			name="txt_User_id" disabled="disabled" class="input-text"></input></td>
	</tr>
	<tr>
		<td>IP ADDTRESS</td>
		<td>&nbsp;<input type="text" id="txt_User_Name" value=""
			name="txt_User_Name"  class="input-text" onblur="ValidateIPaddress(this.value)"></input></td>
	</tr>
	<tr>
		<td>APPLICATION NAME</td>
		<td>&nbsp;<input type="text" id="txt_UserRole" value=""
			name="txt_UserRole" class="input-text"></input></td>
	</tr><tr>
		<tr>
		<td>PASSWORD</td>
		<td>&nbsp; <input type="password" id="txt_Password" class="input-text" value="" name="txt_Password" ></input></td>
	</tr><tr>
		<td>User Status</td>
		<td> 
		<select id="txt_UserStatus" class="input-text"   name="txt_UserStatus">
			<option value="E">Enable</option>
			<option value="D">Disable</option>
			<option value="L">Lock</option>
			<option value="X">Delete</option>
		</select>
		</td>
	</tr>
	
	<tr>
	<td colspan="2" style="text-align: center;">
		<br>
	<input class="btn_style" plain="true" id="edit" onclick="doUpdate()" type="button" value="Update"> 
	<input	type="hidden" id="userID" name="userID"></input> 
	</td>
	</tr>
</table>
</div>
</div>

<div id="showDiv" style="display:none;">
</div>
<jsp:include page="Footer.jsp"></jsp:include>
</center>
</div>
<script type="text/javascript">
function doUpdate(){
	var txtUserID=$('#txt_User_id').val();
	var txtAppName= $("#txt_UserRole").val();
	var txtIPadd= $("#txt_User_Name").val();
	var txt_password= jQuery("#txt_Password").val();
	var txt_UserStatus= $("#txt_UserStatus").val();
	
	//alert($("#txt_User_id").val());
	//alert(jQuery("#txt_Password").val());
	
	if (txt_User_id == '') {
		alert('User id cannot be blank');
		$("#txt_User_id").focus();
		return false;
	}
	
	if (txtAppName == '') {
		alert('Application Name cannot be blank');
		$("#txt_UserRole").focus();
		return false;
	}
	 if (txtIPadd == '') {
		alert('IP Address cannot be blank');
		$("#txt_User_Name").focus();
		return false;
	}
	 if (txt_password == '') {
		alert('Password cannot be blank');
		$("#txt_password").focus();
		return false;
	}
	 
	jQuery.ajax({
		type:"POST",
		url:"DBVault_Servlet?operation=UpdateAppUser" ,
		data:{
			txtUserID: txtUserID,
			txtAppName: txtAppName,
			txtIPadd: txtIPadd,
			txt_password: txt_password,
			txt_UserStatus: txt_UserStatus
	    	}, 
		success:function(msg){
				alert(msg);
				window.location = "dbvault.jsp";
		//$('#check1').val(msg);
		}
		});
}
function doDelete(uid){
	var result = confirm("Want to delete?");
	if (result) {
		jQuery.ajax({
			type:"POST",
			url:"DBVault_Servlet?operation=DeleteAppUser&uid="+uid ,
			data:"", 
			success:function(msg){
					alert(msg);
					window.location = "dbvault.jsp";
			}
			});
	}
}
function display_user(userID)
{	
	jQuery("#userID").val('');
	var edit_txt=new Array();
		jQuery.ajax({
		type:"POST",
		url:'DBVault_Servlet?operation=selUpdateAppUser&uid=' + userID,
		data:"",
		success:function(msg){
			edit_txt=msg.split('~');
			jQuery("#userID").val(edit_txt[0]);
			jQuery("#txt_User_id").val(edit_txt[0]);
			jQuery("#txt_User_Name").val(edit_txt[1]);
			jQuery("#txt_UserRole").val(edit_txt[2]);
			jQuery("#txt_UserStatus").val(edit_txt[3]);
			jQuery("#txt_Password").val(edit_txt[4]);
		}
		});
		display();
}
function display()
{	
	var divTag = jQuery("#verify");
	divTag.dialog({
  		modal: true,
  		autoOpen: false,
  		title: "Update",
  		show: "blind",
        hide: "explode"
  	});
	divTag.dialog('open');
	return false;
}
function ValidateIPaddress(ipaddress)   
{  
	var txtIPadd= $("#txt_User_Name").val();
	if (ipaddress == ""){
		
	}
else {
 if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(txtIPadd))  
  { 
	return (true)  ;
  } 
 else{
	 alert("You have entered an invalid IP address!")  ;
	 $("#txt_User_Name").focus();
	 return (false)  ;
 }
}
}
</script>
</form></body></html>