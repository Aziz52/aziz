<%@page import="java.sql.ResultSet"%>
<%@page import="com.connection.DBUtil"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<jsp:include page="Header.jsp"></jsp:include>

<head>
<link rel="stylesheet" type="text/css" href="AS.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Document Upload</title>
</head>
<body>
<div id="fulldiv" class="wrapper" align="center">
        <div class="content" id="content">
            <div align="left" class="contentPanel" id="contentPanel">
                <script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ss1', 'aspnetForm', ['tUpdatePanel1',''], ['btnSave',''], [], 90, '');
//]]>
</script>

                <div id="UpdatePanel1">
	
                        <div id="jscript1">
                        </div>
                        <input type="hidden" name="showFileName" id="showFileName">
                        <table id="tblData" style="height: 490.2px;">
                            <tbody><tr>
                            
                                <td colspan="2" style="height: 10%;">
                                    <table style="width: 100%;">
                                        <tbody><tr>
                                            <td style="width: 90%;">
                                                <div id="SingleFile">
		<input type="hidden" name="SingleFile$ctl00" id="SingleFile_ctl00"><div id="SingleFile_ctl01"><input name="SingleFile$ctl02" type="file" id="SingleFile_ctl02" style="width:95%;"></div>
	</div>
                                            </td>
                                            <td>
                                                <span id="myThrobber" style="float: left; display: none;"><img align="left" alt="" src="images/uploading.gif"></span>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>
                                
                            </tr>
                            <tr>
                                <td id="td_data" style="height: 100%; width: 80%;">
                                    <div id="datafile" style="width: 100%; height: 100%;">
                                        <h3 style="color: #716D6D;">
                                            <center>
                                                Click on Browse to load the file.
                                            </center>
                                        </h3>
                                    </div>
                                </td>
                                <td valign="top">
                                    <table>
                                        <tbody><tr>
                                            <td colspan="2">
                                                <div id="indexing"><table align="left" valign="center" cellspacing="5" cellpadding="5"><tbody><tr><td align="right" class="txtheading" width="30%">Security</td><td align="left"><select name="DocSecurity" id="DocSecurity"><option selected="selected" value="G">General</option><option value="S">Secured</option></select></td></tr><tr><td align="right" class="txtheading">Customer Name</td><td align="left">
<input type="text" class="txtdata" name="customer_name" id="customer_name" maxlength="100" size="30" onkeyup="fn(this.form,this)"><font color="red">*</font>
</td></tr>
<tr><td align="right" class="txtheading">Date</td><td align="left">
<input type="text" id="ddate" name="ddate" size="10" class="hasDatepicker">
<script>$(function() {$( "#ddate" ).datepicker({dateFormat:'d M yy',changeMonth: true,changeYear: true});});</script></td></tr>
<tr><td colspan="3" class="heading" width="100%" align="center"></td></tr></tbody></table><script language="javascript">

</script>
</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:right;">
                                                <div id="btns"><input type="submit" name="btnSave" value="Save" onclick="return CheckUpload();" id="btnSave" class="Cbutton"></div>
                                                </td>
                                                <td style="text-align:left;">
                                                <input type="button" value="Exit" onclick="CheckExit()" class="Cbutton ">
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>
                            </tr>
                        </tbody></table>
                        <input type="hidden" id="UploadFilePath" name="UploadFilePath">
                        <input type="hidden" id="doc_id" name="doc_id" value="">
                        <input type="hidden" id="doaction" name="doaction" value="FileUpload">
                        <span id="lblcount" class="disText"></span>
                        <span id="Label2" style="color:#0000C0;font-size:Smaller;font-weight:normal;"></span>
                    
</div>
            </div>
        </div>
    </div>
</body>
</html>