<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="com.service.Chart"%>

<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<html>
    
    <head> 
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>amCharts examples</title>
          <link rel="stylesheet" href="amcharts/style.css" type="text/css">
        <script src="amcharts/amcharts.js" type="text/javascript"></script>
      <script src="amcharts/serial.js" type="text/javascript"></script>
             <script src="amcharts/pie.js" type="text/javascript"></script>
       
       
		 <!--       <script type="text/javascript" src="http://www.amcharts.com/lib/3/amcharts.js"></script>
		<script type="text/javascript" src="http://www.amcharts.com/lib/3/serial.js"></script>
	<script type="text/javascript" src="http://www.amcharts.com/lib/3/themes/none.js"></script>
        -->
       <script type="text/javascript">
            var chart;
            var legend;
            var title = [{ text: "aaa"  }];
			var chartData = [
                {
                	
                    "country": "Lithuania",
                    "value": 260
                },
                {
                    "country": "Ireland",
                    "value": 201
                },
                {
                    "country": "Germany",
                    "value": 65
                },
                {
                    "country": "Australia",
                    "value": 39
                },
                {
                    "country": "UK",
                    "value": 19
                },
                {
                    "country": "Latvia",
                    "value": 10
                }
            ];

            AmCharts.ready(function () {
                // PIE CHART
                chart = new AmCharts.AmPieChart();
                
                
                chart.dataProvider = chartData;
                chart.titles = title;
                chart.titleField = "country";
                chart.valueField = "value";
                chart.outlineColor = "#FFFFFF";
                chart.labelsEnabled = true;
                chart.outlineAlpha = 0.8;
                chart.outlineThickness = 2;
                chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
                // this makes the chart 3D
                chart.depth3D = 15;
                chart.angle = 30;

                // WRITE
                chart.write("chartdiv");
                
            });
        </script>
       
    </head>
  
    <body>
        <div id="chartdiv" style="width: 90%; height: 300px; font-size	: 12px;"></div>
    </body>

</html>