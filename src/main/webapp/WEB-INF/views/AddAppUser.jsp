<jsp:include page="new_header.jsp"></jsp:include>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<script type="text/javascript">
	jQuery(document).ready(function($) {
			resizeWindow();
		$("body").resize(function(e) {
			resizeWindow();
		});
	});
	function checkSave() {
		var txtUserID=$('#txtUserID').val();
		var txtAppName= $("#txtAppName").val();
		var txtIPadd= $("#txtIPadd").val();
		var txtPassword= $("#txtPassword").val();
		if ($("#txtUserID").val() == '') {
			alert('User ID cannot be blank');
			$("#txtUserID").focus();
			return false;
		}
		else if ($("#txtAppName").val() == '') {
			alert('Application Name cannot be blank');
			$("#txtAppName").focus();
			return false;
		}
		else if ($("#txtIPadd").val() == '') {
			alert('IP Address cannot be blank');
			$("#txtIPadd").focus();
			return false;
		}
		else if ($("#txtPassword").val() == '') {
			alert('Password cannot be blank');
			$("#txtPassword").focus();
			return false;
		}
		else if ($("#hidAction").val() == 'N') {
			alert('User ID already exists');
			$("#txtUserID").focus();
			return false;
		}
		else {
			jQuery.ajax({
				type:"POST",
				url:"DBVault_Servlet?operation=AddAppUser" ,
				data:{
					txtUserID: txtUserID,
					txtAppName: txtAppName,
					txtPassword:txtPassword,
					txtIPadd: txtIPadd
			    	}, 
				success:function(msg){
						alert(msg);
						window.location = "AddAppUser.jsp";
				//$('#check1').val(msg);
				}
				});
		}
	}
	
	function ValidateIPaddress(ipaddress)   
	{  
		if (ipaddress == ""){
			
		}
	else {
	 if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(form1.txtIPadd.value))  
	  {
		 /* jQuery.ajax({
				type:"POST",
				url:"AddDeviceServlet?Action=checkIPadd&ipAddress="+ipaddress ,
				data:"",
				success:function(msg){
					//alert(msg);
				$('#check1').val(msg);
				}
				}); */
	    return (true)  ;
	  } 
	 else{
		 alert("You have entered an invalid IP address!")  ;
		 $("#txtIPadd").focus();
		 return (false)  ;
	 }
	}
	}
	function isUserIDavail(uid){
			jQuery.ajax({
				type:"POST",
				url:"DBVault_Servlet?operation=chkUserID&uid="+uid ,
				data:"", 
				success:function(msg){
					if(msg=='User ID available..'){
						jQuery("#hidAction").val('Y');
					}else{
						jQuery("#hidAction").val('N');
					}
				}
				});
	}
	function clr(){
		$('.form-group input[type="text"]').val('');
}
</script>
<center>
	<form id="form1" name="form1" method="post">
		<section id="content">
			<section class="main padder">
				<div class="row">
					<section class="panel">
						<div class="panel-body">
							<div class="tab-content">
								<div class="tab-pane active" id="search">
									<div class="search_section">
										<div id="tb" class="tb_borders">
											<span>
												<h3>Add Application</h3> <br>
											</span>
											<table class="TblMain">
												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Enter User
																ID</label>
															<div class="col-lg-8">
																<input type="text" onfocus="" id="txtUserID" value=""
																	class="bg-focus form-control" name="txtUserID" 
																	onblur="isUserIDavail(this.value)"></input>
																<font color="Black">*</font>
																<div id="chkStatus" style="float: right;"></div>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Enter Application Name</label>
															<div class="col-lg-8">
																<input type="text" id="txtAppName" value=""
																	name="txtAppName" class="bg-focus form-control"></input>
																<font color="Black">*</font>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Enter Password</label>
															<div class="col-lg-8">
																<input type="password" id="txtPassword" value=""
																	name="txtPassword" class="bg-focus form-control"></input>
																<font color="Black">*</font>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">IP Address</label>
															<div class="col-lg-8">
																<input type="text" id="txtIPadd" value="<%=request.getRemoteAddr() %>"
																	name="txtIPadd" class="bg-focus form-control"
																	onblur="ValidateIPaddress(this.value)"></input>
																<font color="Black">*</font>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												<!-- <tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Enter
																Expiry Date</label>
															<div class="col-lg-8">
																<input class="bg-focus form-control" type="text"
																	id="txtExpiryDate" value=""
																	name="txtExpiryDate"></input> <font color="Black">*</font>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr> -->
												<!-- <tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Enter User
																Status</label>
															<div class="col-lg-8">
																<select class="bg-focus form-control" id="selUserStatus"
																	name="selUserStatus">
																	<option value="E" selected="true">Enable</option>
																	<option value="D">Disable</option>
																</select> <font color="Black">*</font>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr> -->
												<tr>
													<td colspan="3" style="padding-top: 15px;"><input
														class="btn_style btn btn-primary"
														onclick="checkSave()" plain="true" type="button"
														value="Save" id="serch_btn">
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="button"
														id="test" onclick="clr()" class="btn_style btn btn-white"
														name="btn_Clear" value="Clear"> <input
														type="hidden" id="hidAction" name="hidAction"></input></td>
													<td></td>
												</tr>
											</table>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="gdata">
									<div id="pdatagrid"
										style="width: 100%; overflow: auto; padding: 3%">
										<table id="tt1" class="easyui-datagrid" width="100%"
											iconCls="icon-search" rownumbers="true" pagination="true">
										</table>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</section>
		</section>
		<div id="showDiv" style="display: none;"></div>
	</form>
</center>
<script>
//$("#reg_box_wrapper").hide();
//$(function() {$('#txtExpiryDate').calendarsPicker({calendar: $.calendars.instance('gregorian')});});
</SCRIPT>
<jsp:include page="Footer.jsp"></jsp:include>