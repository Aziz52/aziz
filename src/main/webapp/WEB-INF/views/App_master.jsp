<jsp:include page="new_header.jsp"></jsp:include>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@page import="java.net.URL"%><html>
<title></title>


<style type="text/css">
.panel-body {
	min-height: 500px; !important;
}
</style>

<script type="text/javascript">

//<![CDATA[
           
jQuery(document).ready(function($) {
	//Getdata When Load Form using

resizeWindow();	
	
	jQuery('#tt1').datagrid({  
	    url:'App_servlet?operation=getData',  
	    
	    columns:[[  
	        {field:'APP_ID',title:'Application <br/> Id',width:70},  
	        {field:'APP_NAME',title:'Application <br/> Name',width:120},
	        {field:'APP_VERSION',title:'Application <br/> Version',width:70},  
	        {field:'APP_SCHMA_NAME',title:'Schema Name',width:150},  
	        {field:'BANK_NAME',title:'Bank Name',width:150},
	        {field:'APP_PATH',title:'Application <br/> URL',width:100},
	        {field:'APP_FLAG',title:'Application <br/> Status',width:75},  
	        {field:'MIGRATION_DATE',title:'Migration <br/> Date',width:85}
	    ]] ,
	     showTableToggleBtn: true ,
	    title:"Application  Details", //report Title
	    height: 350, //Height of Grid
	    width:900 //Width of Grid
	    
	}); 


	$(".pagination table tbody tr ").append('<td><a onclick="newSave();"  class="easy-linkbutton" href="javascript:void(0)">Save</a></td>');

	
	
	
	$("body").resize(function(e){
		resizeWindow();
	});
	
});


function doSearch() { //Function use Search The Data Using user paramater
	 $("a[href*=#gdata]").click();	 
	
	$('#tt1').datagrid('load', {
		UserID :$('#UserID').val(),
		UserName :$('#UserName').val()
	//	acc_no1:'0002BC0001151'
	});
}

function chkReset(userID)
{
	var st = '<%=session.getAttribute("user_id") %>';
	if (st != userID)
	{
		$("#chkUserID").val(userID);
		document.form1.action = 'UserAuthentication_servlet?operation=ResetLogin';
		document.form1.submit();
	}
	else
	{	
		alert('Please Log out to perform the action');
		return false;
	}
	
}

function newSave()
{
	var tempCheked="";
	var tempNotCheked="";
	$.each($("input[name^='chk~']"),function(i,j)
		{
		if(j.checked)
		{
			tempCheked+="'"+j.id.split("~")[1]+"',";
		}
		else
		{
			tempNotCheked+="'"+j.id.split("~")[1]+"',";
		}
		});
	var str="";
	if(tempCheked.length > 0)
	{
		str="tempCheked="+tempCheked.substr(0,tempCheked.length-1)+"&";
	}
	if(tempNotCheked.length > 0)
	{
		str+="tempNotCheked="+tempNotCheked.substr(0,tempNotCheked.length-1);
	}

	 jQuery.ajax({
         type: "POST",
         url: 'App_servlet?operation=saveData',
         data: str,
         success: function (data) {         	
         		if (data="y")
    			{
    				alert('Data Updated successfully');window.location.href='App_master.jsp';	
    			}
    			else
    			{
    				alert('Action Failed');window.location.href='App_master.jsp';
    			}
         }
     });
	
}





	//]]>
	</script>

</head>

<body>
	<form id="form1" name="form1" method="post">
		<section id="content">
			<section class="main padder">
				<div class="row">


					<section class="panel">
						<header class="panel-heading">
							<ul class="nav nav-tabs nav-justified">
								
								<li class="active"><a href="#gdata" data-toggle="tab"></a></li>
							</ul>
						</header>
						<div class="panel-body">
							<div class="tab-content">
								<a href="AddApp.jsp">Add New Application</a>
								<div class="tab-pane active" id="gdata">
									<div id="pdatagrid"
										style="width: 100%; overflow: auto; padding: 3%">
										<table id="tt1" class="easyui-datagrid" width="100%"
											iconCls="icon-search" rownumbers="true" pagination="true">
										</table>
									</div>
								</div>

							</div>
						</div>

					</section>




				</div>
			</section>
		</section>

	</form>

<script type="text/javascript">

</script>


</body>
</html>
