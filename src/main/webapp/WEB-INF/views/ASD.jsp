
<jsp:include page="new_header.jsp"></jsp:include>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

 
<%@ page import="com.dto.UserMasterDTO" %>
<%@ page import="com.service.UserMaster_service" %>
<%@ page import="com.connection.*" %>




<script type="text/javascript">
	//         
         
	
	
	           
jQuery(document).ready(function($) {
	//Getdata When Load Form using

resizeWindow();	
	
	
	
	
	
	$("body").resize(function(e){
		resizeWindow();
	});
	
});

function checkSave()
{ 
	
	var st = 0;
    var dt=new Date();
    var get_select =$("#selUserRole").val();
	if ($("#txtUserID").val() == '')
	{
		alert('User ID cannot be blank');
		$("#txtUserID").focus();
		return false;
	}
	
	

	if ($("#txtUserName").val() == '')
	{
		alert('User Name cannot be blank');
		$("#txtUserName").focus(); 
		return false;
	}
	
	
	
	
	if ($("#txtExpiryDate").val() == '')
	{
		alert('Expiry Date cannot be blank');
		$("#txtExpiryDate").focus(); 
		return false;
	}
	if ((cdate($("#txtExpiryDate").val())<= dt))
	{
		alert('Expiry Date should be greater than today');
		return false;
	}	
	if(get_select!='User')
	{
	if (document.form1.chkApp.length == null)
	{
		if (document.form1.chkApp.checked==true)
		{
			st = 1;
		}
	}
	else
	{	
	for (var i=0;i<document.form1.chkApp.length;i++)
	{
		
		if (document.form1.chkApp[i].checked == true)
		{
			st = 1;
			//document.form1.submit();
		}
	}
	}
	}
	else
	{
		st =1;
	}
	if (st ==0)
	{
		alert('Please select atleast one aplication');
		return false;
	}
	else
	{
		
		$.get('UserAuthentication_servlet?opr=GetName&userId='+ $("#txtUserID").val(),function(responseText){
			
			
			if (responseText == 'NO')
			{
				document.form1.action = 'UserAuthentication_servlet?operation=AddNew';
				document.form1.submit();		
			}
			else if(responseText == 'YES') 
			{
				alert('User already added');
				return false;
			}
			else if (responseText == "E")
			{
				alert('Error Occured');
				return false;
			}
			
		});
	}
	
}


function che_his()
{
	var get_select =$("#selUserRole").val();
	
	if(get_select=='User')
	{
		$("#appList").hide();
		
	}
	else
	{
		$("#appList").show();
	}
}
function GetApp()
{
	$.get('UserAuthentication_servlet?opr=GetAppList&typ=check',function(responseText){
		if (responseText != "")
		{
			$("#appList").html(responseText);		
		}
	});

	
}

function GetName()
{
	
	$("#chkStatus").html('<img src=images/loading.gif>');
	var qstring=$("#txtUserID").val();
	
	if (qstring != '')
	{
		$.get('UserAuthentication_servlet?opr=GetName&userId='+ qstring,function(responseText){
			if (responseText == 'NO')
			{
				$("#chkStatus").html('<font color=Green>ID Available</font>');
					
			}
			else if(responseText == 'YES') 
			{
				$("#chkStatus").html('<font color=red>ID already added</font>');
				$("#txtUserID").focus();
				//$("#txtUserName").val(responseText);
			}
			else if(responseText == 'E')
			{
				$("#chkStatus").html('<font color=red>Error Occured</font>');
			}
			else if(responseText != '')
			{
				$("#chkStatus").html('<font color=green>ID Validated</font>');
				
			}
		});
				
	}
	else
	{
		$("#chkStatus").html('');
	}
	
}
//]]>
</script>

<%
UserMasterDTO dto = new UserMasterDTO();
UserMaster_service srv = new UserMaster_service();
if (request.getParameter("action") != null)
if (request.getParameter("action").equals("EditUser") && request.getParameter("UID") != null)
{
	dto = srv.GetUserDetails(request.getParameter("UID"));	
}

%>

 <center>
<form id="form1" name="form1" method="post">
		<section id="content">
			<section class="main padder">
				<div class="row">


					<section class="panel">
						<header class="panel-heading">
							<ul class="nav nav-tabs nav-justified">
								<li class="active"><a href="#search" data-toggle="tab"></a></li>
								
							</ul>
						</header>
						<div class="panel-body">
							<div class="tab-content">
								<div class="tab-pane active" id="search">
									<div class="search_section">
										<div id="tb" class="tb_borders">
											<span>
												<h3>Add New User</h3> <br>
											</span> 
											<table class="TblMain">
												
												
												<tr><td><div class="form-group">  <label class="col-lg-3 control-label">Enter User ID</label>  <div class="col-lg-8"> 
<input type="text" onfocus="" id="txtUserID" onBlur="GetName()" value=""  class="bg-focus form-control"  name="txtUserID"></input>
<font color="Black">*</font>
<div id="chkStatus" style=" float:right;"></div>
     <div class="line line-dashed m-t-large"></div>  </div> </div></td></tr> 
<tr><td><div class="form-group">  <label class="col-lg-3 control-label">Enter User Name</label>  <div class="col-lg-8"> 
<input type="text" id="txtUserName" value="" name="txtUserName"  class="bg-focus form-control" ></input>
<font color="Black">*</font>
     <div class="line line-dashed m-t-large"></div>  </div> </div></td></tr> 
     
     
     <tr><td><div class="form-group">  <label class="col-lg-3 control-label">PF Number</label>  <div class="col-lg-8"> 
<input type="text" id="txtP_F_Number" value="" name="txtP_F_Number"  class="bg-focus form-control" ></input>
<font color="Black">*</font>
     <div class="line line-dashed m-t-large"></div>  </div> </div></td></tr> 
     
     
       <tr><td><div class="form-group">  <label class="col-lg-3 control-label">Sol Id</label>  <div class="col-lg-8"> 
<input type="text" id="txtSolID" value="" name="txtSolID"  class="bg-focus form-control" ></input>
<font color="Black">*</font>
     <div class="line line-dashed m-t-large"></div>  </div> </div></td></tr> 
<tr><td><div class="form-group">  <label class="col-lg-3 control-label">Enter User Status</label>  <div class="col-lg-8"> 
<select  class="bg-focus form-control"  id="selUserStatus" name="selUserStatus">
<option value="E" selected="true">Enable</option>
<option value="D">Disable</option>
</select>
<font color="Black">*</font>
     <div class="line line-dashed m-t-large"></div>  </div> </div></td></tr> 
<tr><td><div class="form-group">  <label class="col-lg-3 control-label">Enter Expiry Date</label>  <div class="col-lg-8"> 
<input  class="bg-focus form-control"  type="text" id="txtExpiryDate" value="31/12/2099" onblur="checkdate(this,this.value)" name="txtExpiryDate"></input>

<font color="Black">*</font>
     <div class="line line-dashed m-t-large"></div>  </div> </div></td></tr> 
<tr><td><div class="form-group">  <label class="col-lg-3 control-label">Enter User Role</label>  <div class="col-lg-8"> 
<select  class="bg-focus form-control"  id="selUserRole" onchange="che_his()" name="selUserRole">
<option value="Data" selected="true">Data</option>
<option value="Admin">Admin+Data</option>
<option value="User">Admin</option>
</select>
<font color="Black">*</font>
     <div class="line line-dashed m-t-large"></div>  </div> </div></td></tr> 


<tr id="appList">
</tr>
									



												<tr>
													<td colspan="3" style="padding-top: 15px;"><input
														class="btn_style btn btn-primary" onclick="return checkSave()"
														plain="true" type="button" value="Save" id="serch_btn">
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="button"
														id="test"  onclick="clr()"
														class="btn_style btn btn-white" name="btn_Clear"
														value="Clear">
														<input type="hidden" id="hidAction" name="hidAction"></input>
														</td>
													<td></td>

												</tr>
												

												<tr>
													<td colspan="3" class="char_srch_style"
														style="text-align: center;">Use default password 'M1' (without single quotes)
													</td>
												</tr>
											</table>

										</div>

									</div>

								</div>
								<div class="tab-pane" id="gdata">
									<div id="pdatagrid"
										style="width: 100%; overflow: auto; padding: 3%">
										<table id="tt1" class="easyui-datagrid" width="100%"
											iconCls="icon-search" rownumbers="true" pagination="true">
										</table>
									</div>
								</div>

							</div>
						</div>

					</section>




				</div>
			</section>
		</section>
		
		<script>
function conditions(){
if (document.form1["ftxt_account_open_date"].value != '' && document.form1["ttxt_account_open_date"].value == ''){
alert(' To Date Cannot Be blank   ');
document.form1["ttxt_account_open_date"].focus();
return false; 
}
if (document.form1["ttxt_account_open_date"].value != '' && document.form1["ftxt_account_open_date"].value == ''){
alert(' From Date Cannot Be blank   ');
document.form1["ftxt_account_open_date"].focus();
return false; 
}
if (document.form1["ftxt_account_closed_date"].value != '' && document.form1["ttxt_account_closed_date"].value == ''){
alert(' To Date Cannot Be blank   ');
document.form1["ttxt_account_closed_date"].focus();
return false; 
}
if (document.form1["ttxt_account_closed_date"].value != '' && document.form1["ftxt_account_closed_date"].value == ''){
alert(' From Date Cannot Be blank   ');
document.form1["ftxt_account_closed_date"].focus();
return false; 
}
 document.form1.submit();
}
</script>
		
		
		<div id="showDiv" style="display:none;">
</div>
	</form>


	 </center>
	 <script>
$("#reg_box_wrapper").hide();
GetApp();
$(function() {$('#txtExpiryDate').calendarsPicker({calendar: $.calendars.instance('gregorian')});});
</SCRIPT>
	<jsp:include page="Footer.jsp"></jsp:include>


