
<jsp:include page="new_header.jsp"></jsp:include>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%@ page import="com.dto.UserMasterDTO"%>
<%@ page import="com.service.UserMaster_service"%>
<%@ page import="com.connection.*"%>




<script type="text/javascript">
	//          
	jQuery(document).ready(function($) {
		//Getdata When Load Form using

		resizeWindow();

		$("body").resize(function(e) {
			resizeWindow();
		});

	});

	function checkSave() {
		p1=$("#txtPass1").val();
		p2=$("#txtPass2").val();
		if (p1=="" || p2==""){
			alert('Enter both passwords.');
			return false;
		}
		if(!chkPasswordStrenth($("#txtPass1").val(),'New Password')){
			return false;
		}
		/* if(!specialcharecter($("#newPassW").val(),'New Password')){
			return false;
		} */
		if(p1!=p2){
			alert('Passwords does not match');
			return false;
		}
	
		/* if(p1.length < 6)
		{
			alert('Passwords must be atleast six(6) characters');
			return false;
		} */
		
		$.ajax({
			type : "POST",
			url : "UserAuthentication_servlet?operation=ResetPassword",
			data : {
				txtPass1:$("#txtPass1").val(),
				pUser:$("#pUser").val(),
				lUser:$("#lUser").val()
			},
			success : function(msg) {
				var msg1=msg.split("~");
				alert(msg1[1]);
				if(msg1[0]=="S"){
					window.location.href='User_master.jsp';
				}else if(msg1[0]=="E"){
					window.location.href='User_master.jsp';
				}else if(msg1[0]=="M"){
					
				}
			}
		});
		//document.form1.action = 'UserAuthentication_servlet?operation=ResetPassword';
		//document.form1.submit();
	
	}
	function chkPasswordStrenth(iptxt,type){
		var lowerCaseLetters = /[a-z]/g;
		  if(iptxt.match(lowerCaseLetters)) { 
			  //alert("valid");
		  } else {
			  alert(type+" should contain atleast one lower case alphabet.");
			  return false;
		}

		  // Validate capital letters
		  var upperCaseLetters = /[A-Z]/g;
		  if(iptxt.match(upperCaseLetters)) { 
			  //alert("valid");
		  } else {
			  alert(type+" should contain atleast one upper case alphabet.");
			  return false;
		  }

		  // Validate numbers
		  var numbers = /[0-9]/g;
		  if(iptxt.match(numbers)) { 
			  //alert("valid");
		  } else {
			  alert(type+" should contain atleast one number.");
			  return false;
		  }

		  // Validate length
		  if(iptxt.length >= 7) {
			  //alert("valid");
		  } else {
			  alert(type+" should be more than 7 characters in length.");
			  return false;
		  }
		  
		  /* var passw = '(.*[,~,!,@,#,$,%,^,&,*,(,),-,_,=,+,[,{,],},|,;,:,<,>,/,?].*$)';
		  if(iptxt.match(numbers)) { 
			  //alert("valid");
		  } else {
			  alert(type+" should contain atleast one special character.");
			  return false;
		  } */
		  return true;
	}

	function specialcharecter(data,type)
	{
	    var iChars = "!`@#$%^&*()+=-[]\\\';,./{}|\":<>?~_";   
	    for (var i = 0; i < data.length; i++)
	    {      
	        if (iChars.indexOf(data.charAt(i)) != -1)
	        {    
	        
	        } 
	        else{
	        	alert ("New password should contain atleast one special character..");
	            document.getElementById("txtCallSign").value = "";
	            return false; 
	        }
	    }
	    return true; 
	}

	
</script>



<center>
	<form id="form1" name="form1" method="post">
	<input type="hidden" id="pUser" name="pUser" value="<%=request.getParameter("uid")%>">
	<input type="hidden" id="pAction" name="pAction" value="<%=request.getParameter("action")%>">
	<input type="hidden" id="lUser" name="lUser" value="<%=session.getAttribute("user_id")%>">
	
		<section id="content">
			<section class="main padder">
				<div class="row">


					<section class="panel">

						<div class="panel-body">
							<div class="tab-content">
								<div class="tab-pane active" id="search">
									<div class="search_section">
										<div id="tb" class="tb_borders">
											<span>
												<h3>Change Password</h3> <br>
											</span>
											<table class="TblMain">


												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Password</label>
															<div class="col-lg-8">
																<input type="password" onfocus="" id="txtPass1" value=""
																	class="bg-focus form-control" name="txtPass1"></input>
																<font color="Black">*</font>
																<div id="chkStatus" style="float: right;"></div>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
													<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Repeat Password</label>
															<div class="col-lg-8">
																<input type="password" onfocus="" id="txtPass2" value=""
																	class="bg-focus form-control" name="txtPass2"></input>
																<font color="Black">*</font>
																<div id="chkStatus" style="float: right;"></div>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
											


												<tr>
													<td colspan="3" style="padding-top: 15px;"><input
														class="btn_style btn btn-primary"
														onclick="return checkSave()" plain="true" type="button"
														value="Save" id="serch_btn">
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="button"
														id="test" onclick="clr()" class="btn_style btn btn-white"
														name="btn_Clear" value="Clear"> <input
														type="hidden" id="hidAction" name="hidAction"></input></td>
													<td></td>

												</tr>


											
											</table>

										</div>

									</div>

								</div>
								

							</div>
						</div>

					</section>




				</div>
			</section>
		</section>

		<script>
			function conditions() {
				if (document.form1["ftxt_account_open_date"].value != ''
						&& document.form1["ttxt_account_open_date"].value == '') {
					alert(' To Date Cannot Be blank   ');
					document.form1["ttxt_account_open_date"].focus();
					return false;
				}
				if (document.form1["ttxt_account_open_date"].value != ''
						&& document.form1["ftxt_account_open_date"].value == '') {
					alert(' From Date Cannot Be blank   ');
					document.form1["ftxt_account_open_date"].focus();
					return false;
				}
				if (document.form1["ftxt_account_closed_date"].value != ''
						&& document.form1["ttxt_account_closed_date"].value == '') {
					alert(' To Date Cannot Be blank   ');
					document.form1["ttxt_account_closed_date"].focus();
					return false;
				}
				if (document.form1["ttxt_account_closed_date"].value != ''
						&& document.form1["ftxt_account_closed_date"].value == '') {
					alert(' From Date Cannot Be blank   ');
					document.form1["ftxt_account_closed_date"].focus();
					return false;
				}
				document.form1.submit();
			}
		</script>


		<div id="showDiv" style="display: none;"></div>
	</form>


</center>
<script>
$("#reg_box_wrapper").hide();
GetApp();
GetFolderList();
$(function() {$('#txtExpiryDate').calendarsPicker({calendar: $.calendars.instance('gregorian')});});
</SCRIPT>
<jsp:include page="Footer.jsp"></jsp:include>


