<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="js/jquery.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
		jQuery.ajax({
			type:"POST",
			url:"http://192.168.1.99:8082/DbVaultWebService/rest/PostExampleService/empInfo",
			data:{
				res_status : 200,
			    res : 'Arun' 
			}, 
			success:function(msg){
				if(msg=='User ID available..'){
					jQuery("#hidAction").val('Y');
				}else{
					jQuery("#hidAction").val('N');
				}
			}
			});
});
</script>
</head>
<body style="width: 90%;">

<form>
<div  class="tab_divs">

<center>
<div class="form_inner_wrap">
<div id="reg_box" class="form_section" style="padding-bottom: 3%;"><br />
<br />
<br />
<div id="get_details">
<table id="tt1" style="diplay: none !important;" class="easyui-datagrid"
	iconCls="icon-search" rownumbers="true" pagination="true">
</table>
</div>
<table>
	<tr>
		<td><input type="button"  class="btn_style"  onclick="report('Excel')"
			name="operation" value="Excel"></td>
		<td><input type="button"  class="btn_style"  onclick="report('Word')"
			name="operation" value="Word"></td>
		<td><input type="submit"  class="btn_style"  name="operation" value="PDF"></td>
		<%-- <td> <input type="submit" name="operation" value="Cancel"></td> --%>
	</tr>
</table>

</div>
<div id="search_box" class="search_section" style="display: none;padding-top: 12%;padding-bottom: 11%;">
<div id="tb" class="tb_borders">

<h3>RBI Cash Report</h3>

<table class="myTable">
	<tr>
		<td><label class="cm-required">Account branch :&nbsp;</label></td>
		<td><input id="Account_branch" type="text" class="input-text" /></td>
		
	</tr>
	<tr>
		<td><label class="cm-required">Amount :&nbsp;</label></td>
		<td><input id="amount" type="text" style="width:100px; " class="input-text" />
		<select id="amount_opr" onchange="select_ch(this.value)" >
		<option value="=" >Select</option>
		<option value="&lt;">&lt;</option>
		<option value="&gt;">&gt;</option>
		<option value="=&gt;">=&gt;</option>
		<option value="&lt;=">&lt;=</option>
		<option value="5" >Between</option>
		<option value="&lt;&gt;">&lt;&gt;</option>
		
		</select>
		<input id="between_amount" type="text" style="width:100px; " class="input-text" />
		
		</td>
		
	</tr>
	<tr>
		<td><label class="cm-required">From Date:&nbsp;</label></td>
		<td><input id="frm_date" onblur="checkdate(this,this.value)" type="text" class="input-text" /></td>
		
	</tr>
	<tr>
		<td><label class="cm-required">To Date:&nbsp;</label></td>
		<td><input id="to_date" onblur="checkdate(this,this.value)" type="text" class="input-text" /></td>
		
	</tr>



	<tr >
		<td colspan="3" style="padding-top: 15px;text-align: center;" ><input
			class="btn_style" onclick="doSearch()" plain="true" type="button"
			value="Search"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
			type="button" onclick="clr()" class="btn_style" name="btn_Clear"
			value="Clear"></td>
		<td></td>
	</tr>
</table>

</div>
</div>
</div>
</center>
</div>
</form>


</body>
</html>