<%@page import="java.sql.ResultSet"%>
<%@page import="com.connection.DBUtil"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<jsp:include page="new_header.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="AS.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Document Upload</title>
</head>
<body>
          
<form id="form1" name="form1" method="post">
<div  id="divbody">
<div id="CentrePart" ">
                    

    <script type="text/javascript">
        function UploadError()
        {
            alert("Upload Error");
        }
function UploadComplete()
{
    //alert("UC");
    
        //alert(document.getElementById('docSec').value);
        var tmp;
        tmp = loadXMLDoc('commondata.aspx?action=BulkUpload&docsec=G');
        //alert(tmp);
        if(tmp == 'Yes')
        {
            alert('Process Completed');
            $('#ClientUpload').empty();
            var d = new Date();
            $('#ClientUpload').load('tdesign.aspx?action=BulkUploadReport&t='+ d.getTime());
        
        }
        else{
            alert('Error Occured');
            $('#report').html(tmp);
        }

    

 }
    </script><div class="content" id="content" >
        <div id="UpdatePanel1">
	
                <div align="left" class="contentPanel" id="contentPanel" style="vertical-align: top; min-height: 407.33px;">
                    <div class="divHeading" style="padding-left: 70px;">
                        <table >
                            <tbody><tr>
                                <td valign="middle" style="padding-bottom: 5px;">
                                    <img src="images/file.png" height="35" width="35">
                                </td>
                                <td style="padding-bottom: 20px;" class="disText">
                                    <h5>
                                        Document Upload</h5>
                                </td>
                                <td valign="middle" style="width: 83%; text-align: right;">
                                    <a href="DMS_User_Manual_v4.0.pdf#page=22" target="_blank">
                                        <img src="images/help.png" alt="Document Upload Help" height="35" width="35"></a>
                                </td>
                            </tr>
                        </tbody></table>
                    </div>
                    <div style="text-align: center;">
                     
                        <div style="float: left; position: relative; width: 2%;">
                        </div>
                        <div style="float: left; text-align: left; width: 70%; margin-left: 80px;" id="MainDiv">
                            <br>
                            <br>
                            <table>
                                <tbody><tr>
                                    <td align="left" class="disText">
                                        Select Template&nbsp;&nbsp;
                                    </td>
                                    <td align="left" class="txtheading">
                                        <select name="selectTemplate"  id="selectTemplate" style="width:170px;">
													<option selected="selected"  value="">--Select--</option>


	</select></td>
	
 <td align="left" class="disText">
                                       
                                </tr>
                            </tbody></table>
                            <br>
                            <br>
                            <div style="width: 95%; overflow: auto;" id="container">
                                <center>
                                    <div class="Bigcontrols" style="float: none; width: 50%; text-align: center;">
                                        <span onclick="Popup('AddSingleFile','','')">
                                            <img src="images/fileupload.png" alt="Single document upload"><br>
                                            Single Document Upload</span> <span onclick="Popup('BulkUpload','ClientUpload','container')">
                                                <img src="images/bulkupload.png" alt="Bulk document upload"><br>
                                                Bulk Document Uplaod</span>
                                    </div>
                                </center>
                            </div>
                            <div id="ClientUpload" class="Hide">
                                
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="hid_template_id" id="hid_template_id">
                    <input type="hidden" name="hid_folder_id" id="hid_folder_id">
                    <input type="hidden" name="hid_DuplicateDoc" id="hid_DuplicateDoc">
                    <input type="hidden" name="Hid_errorDoc" id="Hid_errorDoc">
                    <span id="lblDuplicateDoc"></span>
                </div>
            
</div>
        <div class="Hide">
        <div id="divClUpload" style="float: left; width:95%; text-align: center; padding-left: 20px;">
                                    <center>
                                        <table>
                                            <tbody><tr>
                                                <td>
                                                    Document Security
                                                </td>
                                                <td>
                                                    <select name="docsec" id="docsec">
	<option selected="selected" value="G">General</option>
	<option value="S">Secured</option>

</select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span disabled="disabled"><input id="indexbyfilename" type="checkbox" name="indexbyfilename" disabled="disabled"><label for="indexbyfilename">Index By File Name</label></span>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </tbody></table>
                                         <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" width="575" height="375" id="fileUpload" align="middle"><param name="allowScriptAccess" value="sameDomain"><param name="movie" value="/SwiftDocs Advanced/WebResource.axd?d=RRsuxWU7C60HE6CA7ehfPSvAuroNUkMDK2AOgWIwLgc-_qSQyxnMVaPCwKd8eAZqsVSoCMU5yzl6zzpCEXwQxsfcs7cqRQJkrCa8KKJ7klcDjL0_X_RcI5wQ8r1ym0104oN_avs38jUBpRbbdXCjvA2&amp;t=635078636612182335"><param name="quality" value="high"><param name="wmode" value="transparent"><param name="FlashVars" value="&amp;completeFunction=UploadComplete()&amp;uploadPage=/SwiftDocs Advanced/Upload.axd?Page%3dDocumentUpload"><embed src="/SwiftDocs Advanced/WebResource.axd?d=RRsuxWU7C60HE6CA7ehfPSvAuroNUkMDK2AOgWIwLgc-_qSQyxnMVaPCwKd8eAZqsVSoCMU5yzl6zzpCEXwQxsfcs7cqRQJkrCa8KKJ7klcDjL0_X_RcI5wQ8r1ym0104oN_avs38jUBpRbbdXCjvA2&amp;t=635078636612182335" flashvars="&amp;completeFunction=UploadComplete()&amp;uploadPage=/SwiftDocs Advanced/Upload.axd?Page%3dDocumentUpload" quality="high" wmode="transparent" width="575" height="375" name="fileUpload" align="middle" allowscriptaccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"></object>
                                            <br>
                                       
                                        <input type="button" value="Exit" class="Cbutton" onclick="Popup('BulkUpload','container','ClientUpload');">
                                    </center>
                                    <br>
                                </div>
        </div>
    </div>

    <script   type="text/javascript">

    
    jQuery(document).ready(function($) {
    	
    	
    	
    	$.ajax({
			type:"POST",
			url:"Template_commanDetails_servlet?Detail=CHEK_Select",
					data:"",
			success:function(msg){
				
			
				$("#selectTemplate").html(msg);
				
				
			}
			});
    
    }
    );
    
    
   
    
    
 $(window).bind("load", function() {
 Adjust();
   
});

function Adjust()
{
    SrchHeight = parseInt((68/100) *  getWindowHeight())
    jQuery('#MyTreeDiv').height(SrchHeight);
}

function Popup(fnction, showDiv, HidDiv)
{
  
            if (fnction == 'AddSingleFile')
            {
                 var bvalid = true;
                 
                /*  if ($("#hid_folder_id").val() == '')
                 {
                    alert('Please select the target folder')
                    return false 
                 } */
                 if ($("#selectTemplate").val() == '')
                 {
                    alert('Please select the template')
                    return false; 
                 }
                 if (bvalid)
                 {
                    var d = new Date();
                  theURL='SingleDocUpload.jsp?doaction=FileUpload&template_id='+$("#selectTemplate").val();

                    window.open(theURL, '', 'fullscreen=yes, scrollbars=auto');
                 }
            }
            if (fnction == 'BulkUpload')
            {
                 var bvalid = true;
                 
                 if ($("#hid_folder_id").val() == '')
                 {
                    alert('Please select the target folder')
                    return false 
                 }
                 if ($("#hid_template_id").val() == '')
                 {
                    alert('Please select the template')
                    return false 
                 }
                 if (bvalid)
                 {
                    $("#ClientUpload").empty();
                    $("#ClientUpload").html($('#divClUpload').html());
                    $("#" + HidDiv).hide('slow');
                    $("#" + showDiv).show('slow');
                 }
                               
            }
           
            if (fnction == 'Index')
            {
                var def_temp=document.getElementById("ddl_temp").value;
                if (def_temp==0)
                {
                    alert('Please Select a template');
                    return false;
                }
                str='addtosession.aspx?docid='+status+'&temp='+def_temp
               window.open(str, '', 'fullscreen=yes, scrollbars=auto');
               
            }
             return false;
         }
 
			
          
    </script>


                </div>            
</div>
      
                

    </form>
                    
</body>
</html>