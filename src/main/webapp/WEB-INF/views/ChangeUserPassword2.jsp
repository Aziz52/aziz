<jsp:include page="HeaderForReport.jsp"></jsp:include>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="com.dto.UserMasterDTO" %>
<%@ page import="com.service.UserMaster_service" %>
<%@ page import="com.connection.*" %>
<script type="text/javascript">
jQuery(document).ready(function($) {
resizeWindow();	
	$("body").resize(function(e){
		resizeWindow();
	});
	
});

function checkSave()
{ 
	if ($("#userID").val() == '')
	{
		alert('User Name cannot be blank');
		$("#userID").focus();
		return false;
	}
	if ($("#oldPassW").val() == '')
	{
		alert('Old Password cannot be blank');
		$("#txtUserID").focus();
		return false;
	}
	if ($("#newPassW").val() == '')
	{
		alert('New Password cannot be blank');
		$("#txtUserID").focus();
		return false;
	}
	if(!chkPasswordStrenth($("#newPassW").val(),'New Password')){
		return false;
	}
	if ($("#newPassW2").val() == '')
	{
		alert('Re New Password cannot be blank');
		$("#txtUserID").focus();
		return false;
	}
	if ($("#newPassW2").val() != $("#newPassW").val())
	{
		alert('Password Not Matched.');
		$("#txtUserID").focus();
		return false;
	}
	
	$.ajax({
		type : "POST",
		url : "UserAuthentication_servlet?operation=changePass",
		data : {
			oldPassW:$("#oldPassW").val(),
			newPassW:$("#newPassW").val(),
			newPassW2:$("#newPassW2").val()
		},
		success : function(msg) {
			var msg1=msg.split("~");
			alert(msg1[1]);
			if(msg1[0]=="S"){
				window.location.href='Home.jsp';
			}else if(msg1[0]=="E"){
				window.location.href='ChangeUserPassword.jsp';
			}else if(msg1[0]=="M"){
				
			}
		}
	});
}

function chkPasswordStrenth(iptxt,type){
	var lowerCaseLetters = /[a-z]/g;
	  if(iptxt.match(lowerCaseLetters)) { 
	
	  } else {
		  alert(type+" should contain atleast one lower case alphabet.");
		  return false;
	}

	  var upperCaseLetters = /[A-Z]/g;
	  if(iptxt.match(upperCaseLetters)) { 
	  } else {
		  alert(type+" should contain atleast one upper case alphabet.");
		  return false;
	  }
	  var numbers = /[0-9]/g;
	  if(iptxt.match(numbers)) { 
	  } else {
		  alert(type+" should contain atleast one number.");
		  return false;
	  }
	  if(iptxt.length >= 8) {
	  } else {
		  alert(type+" should be more than 8 characters in length.");
		  return false;
	  }
	  return true;
}
function ClearLI()
{
   var collection = document.getElementsByTagName('li');
     // alert("CL" + collection.length);
     
   for (i=0;i<collection.length;i++)
   {
       
   	
     collection[i].innerHTML = '<a href=#>&nbsp;</a>';
     $(collection[i]).removeClass();
     if (i == collection.length-1)
    	 {
    	 $(collection[i]).css('width','57.5%');
    	 }
   }
  $('.menu').css('width','80%');
}
	
$(document).ready(function() {
	
	var appName='<%=session.getAttribute("AppName") %>';
	if(appName=="" || appName=='null')
		{
		ClearLI();
		}
	
	
});
</script>
<%
UserMasterDTO dto = new UserMasterDTO();
UserMaster_service srv = new UserMaster_service();
if (request.getParameter("action") != null)
if (request.getParameter("action").equals("EditUser") && request.getParameter("UID") != null)
{
	dto = srv.GetUserDetails(request.getParameter("UID"));	
}
%>
 <center>
<form id="form1" name="form1" >
		<section id="content">
			<section class="main padder">
				<div class="row">
					<section class="panel">
						<header class="panel-heading">
							<ul class="nav nav-tabs nav-justified">
								<li class="active"><a href="#search" data-toggle="tab"></a></li>						
							</ul>
						</header>
						<div class="panel-body">
							<div class="tab-content">
								<div class="tab-pane active" id="search">
									<div class="search_section">
										<div id="tb" class="tb_borders">
											<span>
												<h3>Change Password</h3> <br>
											</span>
											<table class="TblMain">
												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">User Name</label>
															<div class="col-lg-8">
																<input type="text" onfocus="" id="userID" value=""
																	class="bg-focus form-control" name="userID"></input>
																<font color="Black">*</font>
																<div id="chkStatus" style="float: right;"></div>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Old Password</label>
															<div class="col-lg-8">
																<input type="password" onfocus="" id="oldPassW" value=""
																	class="bg-focus form-control" name="oldPassW"></input>
																<font color="Black">*</font>
																<div id="chkStatus" style="float: right;"></div>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">New Password</label>
															<div class="col-lg-8">
																<input type="password" id="newPassW" value=""
																	name="newPassW" class="bg-focus form-control"></input>
																<font color="Black">*</font>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Re New Password </label>
															<div class="col-lg-8">
																<input type="password" id="newPassW2" value=""
																	name="newPassW2" class="bg-focus form-control"></input>
																<font color="Black">*</font>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												<tr>
													<td colspan="3"
														style="padding-top: 15px; padding-left: 22%"><input
														class="btn_style btn btn-primary"
														onclick="checkSave()" plain="true" type="button"
														value="Save" id="serch_btn">
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="button"
														id="test" onclick="clr()" class="btn_style btn btn-white"
														name="btn_Clear" value="Clear"> <input
														type="hidden" id="hidAction" name="hidAction"></input></td>
													<td></td>

												</tr>
											</table>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="gdata">
									<div id="pdatagrid"
										style="width: 100%; overflow: auto; padding: 3%">
										<table id="tt1" class="easyui-datagrid" width="100%"
											iconCls="icon-search" rownumbers="true" pagination="true">
										</table>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</section>
		</section>
</div>
	</form>
	 </center>
	<jsp:include page="Footer.jsp"></jsp:include>


