<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="com.swiftDoc.service.MasterDaetilsService" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Template Setting</title>
<jsp:include page="new_header.jsp"></jsp:include>

<script type="text/javascript">
//<![CDATA[
          
jQuery(document).ready(function($) {
	
	//
	//Getdata When Load Form using -------SETTING_TYPE,PARAM_NAME,PARAM_VALUE,DEL_FLAG,CREATED_USER,CREATED_TIME,MODIFIED_USER,MODIFIED_TIME

	
	jQuery('#tt1').datagrid({  
	    url:'MasterDaetilsServlet', 
	    columns:[[  
	        {field:'FILE_NAME',title:'FILE_NAME',width:400},  
	        {field:'KEYWORDS',title:'KEYWORDS',width:300},
	       
	        
	    ]] ,
	     showTableToggleBtn: true ,
	   //report Title
	    height: 380, //Height of Grid
	    width:800 //Width of Grid
	    
	}); 
});
	
//]]>


function clr_txt()
{
	jQuery("#txtApp_Setting_Type").val('');
	jQuery("#txtParam_Name").val('');
	
}



function doSearch() { //Function use Search The Data Using user paramater

	
	 
	$('#tt1').datagrid('load', { 
		msg:'No Record Available For Selected criteria... ', 
		File_Name :$('#File_Name').val(),
		KEYWORDS :$('#KEYWORDS').val(),
	
		
		//Currency :$('#Currency').val(),
		 flg:"Y"
	});

}


</script>

</head>

<body style="width: 90%;">

<form>
<div   class="tab_divs">

<center>
<div class="form_inner_wrap">
<div id="reg_box" class="form_section" style="padding-bottom: 6%;"><br />
<br /><div>

<table>

<%-- <% 	
	MasterDaetilsService ser= new MasterDaetilsService();
	
   String as=  ser.GetTableStr("aaa");

  <%=as%>
 --%>
	<tr><td>File Name</td> <td >	<input type="text" id="File_Name" value="" name="File_Name" class="input-text" style=" width: 270px;" ></input>
 </td> <td>KEYWORDS</td> <td>	<input type="text" id="KEYWORDS" value="" name="KEYWORDS" class="input-text"  style=" width: 270px;" ></input>
	</td>
	
	<td>
	<input class="btn_style" style="float: right;" plain="true" id="adds" onclick="Search()" type="button" value="Search">
	
	
	</td>
	</tr>
	
</table>
</div>
<div id="get_details" style=" margin-left: 97px">
<table id="tt1" style="diplay: none !important;" class="easyui-datagrid"
	iconCls="icon-search" rownumbers="true" pagination="true">

	
</table>
</div>


</div>
</div>

<div id="search_box" class="search_section">

<div id="tb" class="tb_borders">


<div id="AddUserContent" class="tb_borders"  style="display:none;">

<table class="mTable"  style="align:center ; margin-left: 52px;margin-top:20px;">
	<tr>
		<td>Field Name</td>
		<td>&nbsp;<input type="text" id="txtApp_Setting_Type" value=""
			name="txtApp_Setting_Type" class="input-text"></input></td>
	</tr>
	<tr>
		<td>Field Code</td>
		<td>&nbsp;<input type="text" id="txtParam_Name" value=""
			name="txtParam_Name" class="input-text"></input></td>
	</tr>
	<tr>
	<td colspan="2" style="text-align: center;">
		<br>
	<input class="btn_style" plain="true" id="edit" onclick="editadd_user('edit')" type="button" value="Save"> 
	<input class="btn_style" plain="true" id="Add" onclick="editadd_user('Add')" type="button" value="Save"> 
	<input	class="btn_style" plain="true" type="reset" onclick="clr_txt();" value="Clear">
	<input	type="hidden" id="param_id" name="param_id"></input> 
	</td>
	</tr>
</table>
</div>

</div>
</div>

</center>


</div>
</form>

<jsp:include page="Footer.jsp"></jsp:include>


</body>

</html>