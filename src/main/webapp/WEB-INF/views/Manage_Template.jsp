<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Template Setting</title>
<jsp:include page="new_header.jsp"></jsp:include>

<script type="text/javascript">
//<![CDATA[
          
jQuery(document).ready(function($) {
	
	//
	//Getdata When Load Form using -------SETTING_TYPE,PARAM_NAME,PARAM_VALUE,DEL_FLAG,CREATED_USER,CREATED_TIME,MODIFIED_USER,MODIFIED_TIME

	
	jQuery('#tt1').datagrid({  
	    url:'ManageTempateServlet', 
	    columns:[[  
	        {field:'field_Name',title:'Field Name',width:170},  
	        {field:'field_Code',title:'Field Code',width:170},
	        {field:'edit',title:'Edit',width:100} ,
	        {field:'delete',title:'Delete',width:100} 
	        
	    ]] ,
	     showTableToggleBtn: true ,
	    title:"Template Field", //report Title
	    height: 380, //Height of Grid
	    width:800 //Width of Grid
	    
	}); 
});
	
//]]>

var x;
function display_user(field_Name)
{
	
	
	x=field_Name;

	
	
	
	jQuery("#param_id").val(x);
	
	var edit_txt=new Array();
		jQuery.ajax({
		type:"POST",
		url:"ManageTempateServlet?Report=Edit&details=get&id="+ field_Name,
		data:"",
		success:function(msg){
			edit_txt=msg.split('~');
			jQuery("#txtApp_Setting_Type").val(edit_txt[0]);
			jQuery("#txtParam_Name").val(edit_txt[1]);
			
		}
		});
		display('Edit Setting','edit');
}

function clr_txt()
{
	jQuery("#txtApp_Setting_Type").val('');
	jQuery("#txtParam_Name").val('');
	
}


function delete_user(ID)
{	var no=4;
	

var retVal = confirm("Do you want to continue ?");
if( retVal == true ){
   	jQuery.ajax({
	type:"POST",
	url:"ManageTempateServlet?Report=Edit&details=Delete&id="+ID ,
	data:"",
	success:function(msg){	
		if(msg=='E')
		{
			alert("Error Occured");
			window.parent.location='Manage_Template.jsp';
		}
		else if (msg=='T') {
			
			
			alert("Field use  in Template");
			 return false;
	
		}
		else {
			window.parent.location='Manage_Template.jsp';
			alert(" Field Delete Sucessfully");
		}		
	}
	});
	
   
	  return true;
}else{
   
	  return false;
}



}
function editadd_user(Edit)
{	var no=4;
	
	
	var edit_txt=new Array();
		jQuery.ajax({
		type:"POST",
		url:"ManageTempateServlet?Report=Edit&details="+Edit+"&setting="+ jQuery("#txtApp_Setting_Type").val() +"&name="+ jQuery("#txtParam_Name").val()+"&id="+ x,
		data:"",
		success:function(msg){	
			if(msg=='E')
			{
				alert("Error Occured");
				window.parent.location='Manage_Template.jsp';
			}
			else
			{
				window.parent.location='Manage_Template.jsp';
				alert("Field "+Edit+" Sucessfully");
			}		
		}
		});
		
}
function display(head,button)
{
	
	if(button=='edit')
	{
		$("#Add").hide();
		$("#edit").show();
	}
	else{
		$("#Add").show();
		$("#edit").hide();
	}
	jQuery("#AddUserContent").show();
	var divTag = jQuery("#AddUserContent");
	divTag.dialog({
  		modal: true,
  		autoOpen: false,
  		title: head,
  		show: "blind",
  		height: 310, //Height of Grid
	    width:400, 
        hide: "explode"
  	});
	divTag.dialog('open');
	return false;
}
$("#reg_box_wrapper").hide();

</script>

</head>

<body>

<form>
<div   class="tab_divs">

<center>
<div class="form_inner_wrap">

<div id="reg_box" class="form_section" style="padding-bottom: 6%;"><br />
<br />

<div style="padding-left: 772px">
<table>
<tr>
<td>
<a   style="float: right;" plain="true" id="adds"  href="#" onclick="display('Add Setting','Edit')" >Add New Field</a>
<!-- <input class="btn_style" style="float: right;" plain="true" id="adds" onclick="display('Add Setting','Edit')" type="button" value="Add New Field" />
 --><td>
</tr>
</table>

</div>
<div id="get_details" style=" margin-left: 97px">

<table id="tt1" style="diplay: none !important;" class="easyui-datagrid"
	iconCls="icon-search" rownumbers="true" pagination="true">
	
	
</table>
</div>


</div>
</div>

<div id="search_box" class="search_section">

<div id="tb" class="tb_borders">


<div id="AddUserContent" class="tb_borders"  style="display:none;">

<table class="mTable"  style="align:center ; margin-left: 52px;margin-top:20px;">
	<tr>
		<td>Field Name</td>
		<td>&nbsp;<input type="text" id="txtApp_Setting_Type" value=""
			name="txtApp_Setting_Type" class="input-text"></input></td>
	</tr>
	<tr>
		<td>Field Code</td>
		<td>&nbsp;<input type="text" id="txtParam_Name" value=""
			name="txtParam_Name" class="input-text"></input></td>
	</tr>
	<tr>
	<td colspan="2" style="text-align: center;">
		<br>
	<input class="btn_style" plain="true" id="edit" onclick="editadd_user('edit')" type="button" value="Save"> 
	<input class="btn_style" plain="true" id="Add" onclick="editadd_user('Add')" type="button" value="Save"> 
	<input	class="btn_style" plain="true" type="reset" onclick="clr_txt();" value="Clear">
	<input	type="hidden" id="param_id" name="param_id"></input> 
	</td>
	</tr>
</table>
</div>

</div>
</div>

</center>


</div>
</form>

<jsp:include page="Footer.jsp"></jsp:include>


</body>

</html>