<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="com.service.Chart"%>

<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>amCharts examples</title>
          <link rel="stylesheet" href="amcharts/style.css" type="text/css">
        <script src="amcharts/amcharts.js" type="text/javascript"></script>
      <script src="amcharts/serial.js" type="text/javascript"></script>
       
       
		 <!--       <script type="text/javascript" src="http://www.amcharts.com/lib/3/amcharts.js"></script>
		<script type="text/javascript" src="http://www.amcharts.com/lib/3/serial.js"></script>
	<script type="text/javascript" src="http://www.amcharts.com/lib/3/themes/none.js"></script>
        -->
       <script type="text/javascript">   
       
       
       var chart = AmCharts.makeChart("chartdiv", {"type": "serial","theme": "none", "legend": {"horizontalGap": 10,   "maxColumns": 1,  "position": "right",  "useGraphSettings": true,"markerSize": 10 },  "dataProvider": [{"Year": "2005-06", "Belgium": 0, "Cambodia": 2},{"Year": "2006-07", "Belgium": 724, "Cambodia": 70},{"Year": "2007-08", "Belgium": 20305, "Cambodia": 0},{"Year": "2008-09", "Belgium": 25466, "Cambodia": 152},{"Year": "2009-10", "Belgium": 27627, "Cambodia": 21020},{"Year": "2010-11", "Belgium": 30300, "Cambodia": 24232},{"Year": "2011-12", "Belgium": 34661, "Cambodia": 23149},{"Year": "2012-13", "Belgium": 8436, "Cambodia": 8383} ],  "valueAxes": [{ "stackType": "regular", "axisAlpha": 0.3,  "gridAlpha": 0, "title": "Total Number Of Report" }], "graphs": [ { "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",  "fillAlphas": 0.8,  "labelText": "[[value]]",  "lineAlpha": 0.3,  "title": "Belgium",  "type": "column",  "color": "#000000", "valueField": "Belgium"}, { "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",  "fillAlphas": 0.8,  "labelText": "[[value]]",  "lineAlpha": 0.3,  "title": "Cambodia",  "type": "column",  "color": "#000000", "valueField": "Cambodia"},], "categoryField": 'Year', "categoryAxis": {"gridPosition": "start",  "axisAlpha": 0.6,  "gridAlpha": 0, "title": "Financial Year"   }});


/* 
       var chart = AmCharts.makeChart("chartdiv", {
    	   
    	   
    	    "type": "serial",
    		"theme": "none",
    	
    	    "legend": {
    	        "horizontalGap": 10,
    	        "maxColumns": 1,
    	        "position": "right",
    			"useGraphSettings": true,
    			"markerSize": 10
    			 
    	    },
    	    "dataProvider": [{
    	        "year": '2003',
    	        "SBA": 700,
    	        "TDA": 100,
    	        "LAA": 500,
    	        "ODA": 200,
    	        "CAA": 100,
    	        "CCA": 200
    	    }, {
    	        "year": '2004',
    	        "SBA": 700,
    	        "TDA": 100,
    	        "LAA": 500,
    	        "ODA": 200,
    	        "CAA": 100,
    	        "CCA": 200
    	    },
    	    {
    	        "year": 2004,
    	        "SBA": 700,
    	        "TDA": 100,
    	        "LAA": 500,
    	        "ODA": 200,
    	        "CAA": 100,
    	        "CCA": 200
    	    },{
    	        "year": 2004,
    	        "SBA": 700,
    	        "TDA": 100,
    	        "LAA": 500,
    	        "ODA": 200,
    	        "CAA": 100,
    	        "CCA": 200
    	    },{
    	        "year": 2004,
    	        "SBA": 700,
    	        "TDA": 100,
    	        "LAA": 500,
    	        "ODA": 200,
    	        "CAA": 100,
    	        "CCA": 200
    	    },{
    	        "year": 2004,
    	        "SBA": 700,
    	        "TDA": 100,
    	        "LAA": 500,
    	        "ODA": 200,
    	        "CAA": 100,
    	        "CCA": 200
    	    },{
    	        "year": 2004,
    	        "SBA": 700,
    	        "TDA": 100,
    	        "LAA": 500,
    	        "ODA": 200,
    	        "CAA": 100,
    	        "CCA": 200
    	    },{
    	        "year": 2004,
    	        "SBA": 700,
    	        "TDA": 100,
    	        "LAA": 500,
    	        "ODA": 200,
    	        "CAA": 100,
    	        "CCA": 200
    	    },{
    	        "year": 2004,
    	        "SBA": 700,
    	        "TDA": 100,
    	        "LAA": 500,
    	        "ODA": 1000,
    	        "CAA": 100,
    	        "CCA": 200
    	    },{
    	        "year": 2004,
    	        "SBA": 700,
    	        "TDA": 300,
    	        "LAA": 500,
    	        "ODA": 200,
    	        "CAA": 100,
    	        "CCA": 200
    	    },{
    	        "year": 2005,
    	        "SBA": 700,
    	        "TDA": 100,
    	        "LAA": 500,
    	        "ODA": 200,
    	        "CAA": 100,
    	        "CCA": 200
    	    }],
    	    "valueAxes": [{
    	        "stackType": "regular",
    	        "axisAlpha": 0.3,
    	        "gridAlpha": 0,
    	        "title": "Total Account"
    	    }],
    	    
    	    
    	    "graphs": [{
    	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
    	        "fillAlphas": 0.8,
    	        "labelText": "[[value]]",
    	        "lineAlpha": 0.3,
    	        "title": "SBA",
    	        "type": "column",
    			"color": "#000000",
    	        "valueField": "SBA"
    	    }, {
    	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
    	        "fillAlphas": 0.8,
    	        "labelText": "[[value]]",
    	        "lineAlpha": 0.3,
    	        "title": "TDA",
    	        "type": "column",
    			"color": "#000000",
    	        "valueField": "TDA"
    	    }, {
    	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
    	        "fillAlphas": 0.8,
    	        "labelText": "[[value]]",
    	        "lineAlpha": 0.3,
    	        "title": "LAA",
    	        "type": "column",
    			"color": "#000000",
    	        "valueField": "LAA"
    	    }, {
    	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
    	        "fillAlphas": 0.8,
    	        "labelText": "[[value]]",
    	        "lineAlpha": 0.3,
    	        "title": "ODA",
    	        "type": "column",
    			"color": "#000000",
    	        "valueField": "ODA"
    	    }, {
    	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
    	        "fillAlphas": 0.8,
    	        "labelText": "[[value]]",
    	        "lineAlpha": 0.3,
    	        "title": "CAA",
    	        "type": "column",
    			"color": "#000000",
    	        "valueField": "CAA"
    	    }, {
    	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
    	        "fillAlphas": 0.8,
    	        "labelText": "[[value]]",
    	        "lineAlpha": 0.3,
    	        "title": "CCA",
    	        "type": "column",
    			"color": "#000000",
    	        "valueField": "CCA"
    	    }],
    	    "categoryField": "year",
    	    "categoryAxis": {
    	        "gridPosition": "start",
    	        "axisAlpha": 0.6,
    	        "gridAlpha": 0,
    	        "title": "Year"
    	    }
    		
    	});
	
		 */
	
	</script>
       
    </head>
  
    <body>
        <div id="chartdiv" style="width: 90%; height: 300px; font-size	: 12px;"></div>
    </body>

</html>