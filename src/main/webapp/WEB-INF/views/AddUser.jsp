
<%-- <%@page import="com.connection.Common"%> --%>
<jsp:include page="new_header.jsp"></jsp:include>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<script type="text/javascript">
	//          
	jQuery(document).ready(function($) {
		getExpiryDate();

	});
	function getExpiryDate(){
		jQuery.ajax({
			type:"POST",
			url:"UserAuthentication_servlet?operation=getExpiryDate" ,
			data:{}, 
			success:function(msg){
				jQuery("#txtExpiryDate").val(msg);
				}
			});	
	}
	
	jQuery(document).ready(function($) {
		//Getdata When Load Form using

		resizeWindow();

		$("body").resize(function(e) {
			resizeWindow();
		});

	});

	function checkSave() {

		var st = 1;
		var st1 = 0;
		var dt = new Date();
		var get_select = $("#selUserRole").val();
		if ($("#txtUserID").val() == '') {
			alert('User ID cannot be blank');
			$("#txtUserID").focus();
			return false;
		}

		if ($("#txtUserName").val() == '') {
			alert('User Name cannot be blank');
			$("#txtUserName").focus();
			return false;
		}

		if ($("#txtExpiryDate").val() == '') {
			alert('Expiry Date cannot be blank');
			$("#txtExpiryDate").focus();
			return false;
		}
		if ((cdate($("#txtExpiryDate").val()) <= dt)) {
			alert('Expiry Date should be greater than today');
			return false;
		}
		$("#txtUserName").removeAttr("disabled");
		document.form1.action = 'UserAuthentication_servlet?operation=AddNew';
		document.form1.submit();	


	}

	function che_his() {
		/* var get_select = $("#selUserRole").val();

		if (get_select == 'User') {
			$("#appList").hide();

		} else {
			$("#appList").show();
		} */
	}
	

	function GetFolderList() {

		/* $.get('UserAuthentication_servlet?opr=GetFolderList&typ=check',
				function(responseText) {
					if (responseText != "") {
						$("#FolderList").html(responseText);
					}
				}); */

	}

	function checkad() {

		$("#chkStatus").html('<img src=images/loading.gif>');
		var qstring = $("#txtUserID").val();

		if (qstring != '') {
			$.get('UserAuthentication_servlet?opr=GetName&userId=' + qstring,
					function(responseText) 
					{
				
						if (responseText == 'NO') {
							$("#chkStatus").html(
									'<font color=Green>User Id does not exist</font>');

						} else if (responseText == 'YES') {
							$("#chkStatus").html(
									'<font color=red>ID already added</font>');
							$("#txtUserID").focus();
							//$("#txtUserName").val(responseText);
						} else if (responseText == 'E') {
							$("#chkStatus").html(
									'<font color=red>Error Occured</font>');
						} else if (responseText != '') {
							$("#chkStatus").html(
									'<font color=green>ID Validated</font>');
							$("#txtUserName").val(responseText);

						}
					});

		} else {
			$("#chkStatus").html('');
		}

	}
	//
</script>

<center>
	<form id="form1" name="form1" method="post">
		<section id="content">
			<section class="main padder">
				<div class="row">


					<section class="panel">

						<div class="panel-body">
							<div class="tab-content">
								<div class="tab-pane active" id="search">
									<div class="search_section">
										<div id="tb" class="tb_borders">
											<span>
												<h3>Add New User</h3> <br>
											</span>
											<table class="TblMain">


												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Enter User
																ID</label>
															<div class="col-lg-8">
																<input type="text" onfocus="" id="txtUserID" value=""
																	class="bg-focus form-control" name="txtUserID"></input>
																<font color="Black">*</font>
																  <%-- <%
   if (Common.getPropValue("isadauth").equals("Yes")) {
			%>
																
																<input
														class="btn btn-info btn-xs"
														onclick="checkad()" plain="true" type="button"
														value="Check" id="serch_btn"> 
														 <% } %>  --%>
														
														<div id="chkStatus" style="float: right;"></div>
																
																
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label"> User
																Name</label>
															<div class="col-lg-8">
															<%-- <% 
															 if (Common.getPropValue("isadauth").equals("Yes")) {
			%>
																<input type="text" id="txtUserName" value=""
																	name="txtUserName" class="bg-focus form-control" disabled="disabled"></input>
																	<% 
															 }
																	else  {
			%><input type="text" id="txtUserName" value=""
																	name="txtUserName" class="bg-focus form-control" ></input>
																	<% 
																	}
																	%> --%>
																<font color="Black">*</font>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>


												<div class="line line-dashed m-t-large"></div>
												</div>
												</div>
												</td>
												</tr>
												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Enter User
																Status</label>
															<div class="col-lg-8">
																<select class="bg-focus form-control" id="selUserStatus"
																	name="selUserStatus">
																	<option value="E" selected="true">Enable</option>
																	<option value="D">Disable</option>
																</select> <font color="Black">*</font>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Enter
																Expiry Date</label>
															<div class="col-lg-8">
																<input class="bg-focus form-control" type="text"
																	id="txtExpiryDate" value=""
																	onblur="checkdate(this,this.value)"
																	name="txtExpiryDate"></input> <font color="Black">*</font>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Enter User
																Role</label>
															<div class="col-lg-8">
																<select class="bg-focus form-control" id="selUserRole"
																	onchange="che_his()" name="selUserRole">
																
																	<option value="Admin">Admin</option>
																	<option value="User">User</option>
																</select> <font color="Black">*</font>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>


												<!-- <tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Select Zone</label>
															<div id="appList" class="col-lg-8">
																<font color="Black">*</font>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr> -->
												



												<tr>
													<td colspan="3" style="padding-top: 15px;"><input
														class="btn_style btn btn-primary" plain="true" type="button"
														onclick="return checkSave();" 
														value="Save" id="serch_btn">
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="button"
														id="test" onclick="clr()" class="btn_style btn btn-white"
														name="btn_Clear" value="Clear"> <input
														type="hidden" id="hidAction" name="hidAction"></input></td>
													<td></td>

												</tr>


												<tr>
													<td colspan="3" class="char_srch_style"
														style="text-align: center;">User will be created with
														default password M1</td>
												</tr>
											</table>

										</div>

									</div>

								</div>
								<div class="tab-pane" id="gdata">
									<div id="pdatagrid"
										style="width: 100%; overflow: auto; padding: 3%">
										<table id="tt1" class="easyui-datagrid" width="100%"
											iconCls="icon-search" rownumbers="true" pagination="true">
										</table>
									</div>
								</div>

							</div>
						</div>

					</section>




				</div>
			</section>
		</section>

		<script>
			function conditions() {
				if (document.form1["ftxt_account_open_date"].value != ''
						&& document.form1["ttxt_account_open_date"].value == '') {
					alert(' To Date Cannot Be blank   ');
					document.form1["ttxt_account_open_date"].focus();
					return false;
				}
				if (document.form1["ttxt_account_open_date"].value != ''
						&& document.form1["ftxt_account_open_date"].value == '') {
					alert(' From Date Cannot Be blank   ');
					document.form1["ftxt_account_open_date"].focus();
					return false;
				}
				if (document.form1["ftxt_account_closed_date"].value != ''
						&& document.form1["ttxt_account_closed_date"].value == '') {
					alert(' To Date Cannot Be blank   ');
					document.form1["ttxt_account_closed_date"].focus();
					return false;
				}
				if (document.form1["ttxt_account_closed_date"].value != ''
						&& document.form1["ftxt_account_closed_date"].value == '') {
					alert(' From Date Cannot Be blank   ');
					document.form1["ftxt_account_closed_date"].focus();
					return false;
				}
				document.form1.submit();
			}
		</script>


		<div id="showDiv" style="display: none;"></div>
	</form>


</center>
<script>
$("#reg_box_wrapper").hide();
//GetApp();
//GetFolderList();
$(function() {$('#txtExpiryDate').calendarsPicker({calendar: $.calendars.instance('gregorian')});});
</SCRIPT>
<jsp:include page="Footer.jsp"></jsp:include>


