<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-Compatible" content="IE=9">
<title>Miscot - SwiftDB</title>
<script type="text/javascript" src="Include/jquery-1.4.4.min.js"></script>
</head>


<%@ page import="com.connection.DBUtil"%>
<body onload="resizeWindow();">


<script type="text/javascript">


function resizeWindow()
{

    var windowHeight = getWindowHeight();
    var windowWidth = getWindowWidth();
  //  $("#divtitle").css("height",(6/100)*windowHeight + "px");
    //$("#divwrapper").css("height",(90/100)*windowHeight + "px");
    //$("#divbody").css("height",(84/100)*windowHeight + "px");
    $("#divQuickLinks").css("height",(83/100)*windowHeight + "px");
  //  $("#divfooter").css("height",(6/100)*windowHeight + "px");
   
   $("#content").css("height",(84/100)*windowHeight + "px");
   $("#contentPanel").css("height",(83/100)*windowHeight + "px");
    
}

function getWindowHeight()
{
    var windowHeight=0;
    if (typeof(window.innerHeight)=='number')
    {
      windowHeight = window.innerHeight;
    }
    else 
    {
      if (document.documentElement && document.documentElement.clientHeight)
      {
          windowHeight = document.documentElement.clientHeight;
      }
      else
      {
         if (document.body && document.body.clientHeight)
        {
          windowHeight = document.body.clientHeight;
        }
      }
    }
    return windowHeight;
}

function getWindowWidth()
{
var windowWidth=0;
if (typeof(window.innerWidth)=='number')
{
windowWidth = window.innerWidth;
}
else {
if (document.documentElement && document.documentElement.clientWidth)
{
windowWidth = document.documentElement.clientWidth;
}
else
{
if (document.body && document.body.clientWidth)
{
windowWidth = document.body.clientWidth;
}
}
}
return windowWidth;
}

function isSplChar(field)
{
var iChars = "`~!@#$%^&*()+=-_:[]\;,./{}|\":<>?'";
var count=field.value.length;
for (var i = 0; i < count; i++) 
      { 
          if (iChars.indexOf(field.value.charAt(i)) != -1) 
          { 
          alert("Containts special characters. \n These are not allowed.\n Please remove them and try again.");
          field.value = ""
          field.focus()
          return false;
          }

   	 }
   	 return true ;
  }      
  
    
 function fn(form1,field)
      {

          if (event.keyCode==13) {
              if (field.type!='button'){
                  event.keyCode=9;
                  } 
               return event.keyCode; 
          }

      }
</script>

	<style type="text/css">
body {
	background-color: #ffffff;
	-webkit-font-smoothing: antialiased;
	line-height: 1.53846154;
}
/************************************ Login Page CSS Start ********************************************/
#loginDiv {
	width: 30%;
	height: 50%;
	margin-top: 50px;
	margin-bottom: 100px;
	background-color: #f2f4f8;
	box-shadow: 0 5px 5px rgba(0, 0, 0, 0.05), 0 5px 5px rgba(0, 0, 0, 0.05);
}

.panel.panel-default {
	border-color: #eaeef1;
}

.wrapper {
	padding: 15px;
}

.text-center {
	text-align: center;
}

.btn {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 14px;
	font-weight: normal;
	line-height: 1.42857143;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

#divtitle {
	width: 100%;
	/* background-color: #ffffff; */
}

html {
	background-color: #f2f4f8;
	overflow-x: hidden;
}

html {
	-webkit-tap-highlight-color: rgba(0, 0, 0, 0);
}

.bg-white {
	background-color: #fff;
	color: #788288;
}

.i {
	display: inline-block;
	font-family: 'icon';
	font-style: normal;
	font-weight: normal;
	line-height: 1;
	vertical-align: -5%;
	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;
}

.panel.panel-default>.panel-heading,.panel.panel-default>.panel-footer {
	border-color: #eaeef1;
}

.panel-default .panel-heading {
	background-color: #f9fafc;
}

.panel-default>.panel-heading {
	color: #333;
	background-color: #f5f5f5;
	border-color: #ddd;
}

.panel-heading {
	border-radius: 2px 2px 0 0;
	background-color: #f9fafc;
}

.panel-heading {
	padding: 10px 15px;
	border-bottom: 1px solid transparent;
	border-top-left-radius: 3px;
	border-top-right-radius: 3px;
}

.form-control {
	display: block;
	width: 100%;
	padding: 6px 12px;
	font-size: 16px;
	line-height: 1.42857143;
	color: #555;
	background-color: #fff;
	background-image: none;
	border: 1px solid #ccc;
	border-radius: 1px;
	-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
	box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
	-webkit-transition: border-color ease-in-out .15s, box-shadow
		ease-in-out .15s;
	transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}

.panel table tr td div {
	text-align: left;
	padding: 5px;
}

.text-black {
	text-align: left;
	color: #000;
	color: rgba(0, 0, 0, 0.8);
}

.panel {
	margin-bottom: 20px;
	background-color: #fff;
	border: 1px solid transparent;
	border-radius: 4px;
	-webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
	box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
}

#divfooter {
	margin: 0px;
	width: 100%;
	background-color: #ffffff !important;
	border-top: 1px solid #ccc;
}

#divfooter {
	margin: 0px;
	width: 100%;
	background-color: #ffffff !important;
	border-top: 1px solid #ccc;
}

#divfooter table {
	width: 100%;
	background-color: transparent;
}

#divfooter table td {
	width: 33%;
	text-align: center;
	background-color: transparent;
}

#divfooter table tr td:first-child {
	padding-left: 10px;
	text-align: left;
}

#divfooter table tr td:last-child {
	padding-right: 10px;
	text-align: right;
}

#divfooter table tr td:last-child img {
	height: 40px;
	width: 160px;
}

#divfooter table tr td:first-child+td+td
	/*last child doesnt work in ie8 so this is the hack if fixed columns are there*/
	{
	padding-right: 10px;
	text-align: right;
}

#divfooter table tr td:first-child+td+td img {
	height: 40px;
	width: 160px;
}

.text-lg {
	font-size: 16px;
}

.text-primary {
	color: #177bbb;
}
</style>

	<form name="loginform" method="post"
		action="UserAuthentication_servlet?operation=authenticate">
		<%
session.setAttribute("User_IP",request.getRemoteAddr());
if (request.getParameter("logout") != null && session.getAttribute("user_id")!= null)
{ 
	DBUtil ds = new DBUtil();
	//ds.Qexecute("update tbl_user_master set isActive = 'N' where user_id = '" + session.getAttribute("user_id").toString() +"'");
	//ds.Qexecute("insert into tbl_audit_login (user_id,login_date,login_status,LOGIN_IP) values('" + session.getAttribute("user_id").toString() +"',sysdate,'O','"+request.getRemoteAddr()+"')");
	
	session.invalidate();
}
%>
<div id="divtitle" class="bg-white" style="">
<table>
	<tbody>
		<tr>
			
			<td></td>
			<td>
			<div class="list-logo" >
					<div style="padding-top: 10">

						<img src="./images/SIDBIProd.png" height="60" width="200">

					</div>
				</div>
			</td>
			<td  style="width: 100%; text-align: right;">
				<div class="list-logo">
					<div style="padding-top: 10">

						<img src="logo.png" height="60" width="200">

					</div>
				</div>
			</td>
			<!-- <td style="width: 100%; text-align: right;"><img src="logo.png" height="30px"/> -->
			<td></td>

			</tr>
		</tbody>
	</table>



</div>

<div id="divbody">
	<div id="dmenu" style="width: 100%; display: none;">

	<div class="navdd"></div>
</div>
<div id="CentrePart" style="width: 100%;">
	<div class="content" id="content" style="height: 523.32px;">
		<div class="contentPanel" id="contentPanel"
			style="overflow: auto; height: 517.09px;">
			<div id="ctl00_ContentPlaceHolder1_UpdatePanel1">

				<center>
					<div id="loginDiv" class="animated fadeInUp">
						<header class="wrapper text-center"
							style="padding-bottom: 20px;">

							<div>
								<span class="text-primary dker text-lg" style="font-family:arial;"> SwiftDB</span>
							</div>

						</header>
						<section class="panel panel-default" style="background-color: #f2f4f8;">
							<div class="panel-heading" style="font-family:arial;background-color: #f2f4f8;">User Authentication</div>

							<div class="panel-body text-center" style="background-color: #f2f4f8;">
								<center>
									<table style="width: 70%;" align="center">

										<tbody>
											<tr>
												<td>
													<div class="text-black"  style="font-family:arial;">User ID</div> <input type="text"
													name="userName" id="userName" class="form-control"
													value="">

													<div>
														<span
															id="ctl00_ContentPlaceHolder1_RequiredFieldValidator3"
															style="color: red; display: none;">(Required)</span>
													</div>
												</td>
											</tr>


											<tr>
												<td>
													<div class="text-black"  style="font-family:arial;">Password</div> <input
													type="password" class="form-control" name="password"
													value="">
													<div>
														<span
															id="ctl00_ContentPlaceHolder1_RequiredFieldValidator4"
															style="color: red; display: none;"  style="font-family:arial;">(Required)</span>
													</div>
												</td>
											</tr>


											<tr>
												<td><input
													class="btn btn-sm btn-defalut .dker pull-right text-uc m-t-n-xs"
													type="submit" name="Submit" value="Login">

													
												</td>
											</tr>

										</tbody>
									</table>
								</center>


							</div>
							<div id="ctl00_ContentPlaceHolder1_Errlbl" style="background-color: #f2f4f8;"
								
								style="margin: 20px; padding: 10px;"  style="background-color: #f2f4f8;"></div>

							<span id="msgUser" style="background-color: #f2f4f8;"> <%
  if (request.getParameter("err")!= null )
  {
  	if (request.getParameter("err").equals("N"))
  	{
  	%> User Name and Password do not match. <% 
  	}
  	else if (request.getParameter("err").equals("A"))
  	{
   	%> Already Logged in. <% 
   	}
  	else if (request.getParameter("err").equals("D"))
  	{
   	%> User Disabled. Contact Administrator. <% 
   	}
  	else if (request.getParameter("err").equals("I"))
  	{
   	%> User Inactive. Contact Administrator. <% 
   	}
  	else if (request.getParameter("err").equals("L"))
  	{
   	%> Account Locked. 3 consecutive login attempts failed <% 
   	}
  	else if (request.getParameter("err").equals("UL"))
  	{
   	%> User has been unlocked.Please Try Again. <% 
   	}
  	else if (request.getParameter("err").equals("PX"))
  	{
   	%> Password Expired Re-Login to Change Password.</a> <% 
   	}
  	else if (request.getParameter("err").equals("V"))
  	{
   	%> Account not yet verified. <% 
   		}
  	
  	else if (request.getParameter("err").equals("X"))
  	{
   	%> User Expired. Contact Administrator. <% 
		    	}
	    }else if(request.getParameter("logout")!=null)
	    {
if(request.getParameter("logout").equals("Y"))
{
	    	%> You are successfully logout. <% }}
  else
  {
  	%> &nbsp; <% 
  	
  }
  %>


								</span>

							</section>
					</center>
				</div>

			</div>
		</div>

	</div>

</div>
<div style="width: 100%; bottom: 0;position:fixed; " id="divfooter">
			<table>
				<tbody>
					<tr>
						<td><span  style="font-family:arial;">Copyright � 2018 Miscot Systems</span></td>
						<td><span  style="font-family:arial;"> Version(v1.0.0)</span></td>
						<td><img id="myLogo" src="images/miscot_logo_new.png" height=30px
							width=150px></td>
					</tr>
				</tbody>
			</table>
		</div>
	</form>
</body>
</html>