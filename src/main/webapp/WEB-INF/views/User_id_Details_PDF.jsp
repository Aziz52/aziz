<%-- <%@ taglib uri="/WEB-INF/lib/tlds/pd4ml.tld" prefix="pd4ml"%>
<%@page import="com.dto.Get_Query"%>
<%@page import="java.sql.*"%>
<%@page import="com.connection.DBUtil"%>
<%@page contentType="text/html; charset=ISO8859_1"%>
<%@page import="org.zefer.pd4ml.PD4ML"%>

<%
	String txtTypeOutPut = "";
	txtTypeOutPut = request.getParameter("txtTypeOutPut");
	if (txtTypeOutPut.equals("pdf")) {
%>
<pd4ml:transform screenWidth="1500" pageOrientation="landscape"
	pageInsets="1,1,1,1,point" enableImageSplit="false"
	fileName="User_id_Details.pdf" inline="false">



	<html>
	<head>
	<title>pd4ml test</title>
	<style type="text/css">
body {
	color: #000000;
	font-family: Tahoma, "Sans-Serif";
	font-size: 10pt;
}

#tblHeader td {
	color: #fff;
	vertical-align: top;
}

#tblMain tr td {
	margin: 2px;
}
</style>
	</head>
	<body>



<% 
	if(txtTypeOutPut.equalsIgnoreCase("pdf"))
	{ 
	%>
		<jsp:include page="pdfHeader.jsp"></jsp:include>
	<%} %>


	<table width="1480px" border="1" id="tblMain">
		<tr>
			<td width=50px;>User Id</td>
			<td width=290px;>User Full Name</td>
			<td width=70px;>Branch Id</td>
			<td width=125px;>Branch Name</td>
			<td width=65px;>User Class</td>
			<td width=70px;>User Id <br/> Open Date</td>
			<td width=75px;>General Text</td>
			<td width=75px;>Auth <br/>Any Branch</td>
			<td width=65px;>User <br />	Branch No</td>
		</tr>
		<%
			PreparedStatement prepStmt = null;
					ResultSet rs = null;
					Connection objConnection = null;
					DBUtil ds = new DBUtil();
					objConnection = ds.getConnect();
					Get_Query g_query = new Get_Query();

					String User_id=request.getParameter("txtUser_id_output");
					String User_Branch_id=request.getParameter("txtUser_Branch_id_output");
					String User_Name=request.getParameter("txtUser_Name_output");

					String strSQL=g_query.Get_User_id_Query(0, 99999999,User_id,User_Name,User_Branch_id);
					prepStmt = objConnection
							.prepareStatement(strSQL.split("~")[0]);
					rs = prepStmt.executeQuery();
		%>
		<%
			while (rs.next()) {
		%><%="<tr>"%>
		<%
			for (int j = 1; j <= rs.getMetaData().getColumnCount()-3; j++) {
//				System.out.println(rs.getMetaData().getColumnName(j));
				if(rs.getMetaData().getColumnName(j).equalsIgnoreCase("user_branch"))
				{
		%><%="<td>"+ (rs.getString(j).toString().trim().length() <= 0 ?
				"&nbsp;" 
				: rs.getString(j).toString().trim().split("/")[0])+ "</td>"%>
				
				<%="<td>"+ (rs.getString(j).toString().trim().length() <= 0 ?
				"&nbsp;" 
				: rs.getString(j).toString().trim().split("/")[1])+ "</td>"%>
		<%
			
		}			
		else
		{
			%><%="<td>"+ (rs.getString(j).toString().trim().length() <= 0 ? 
					"&nbsp;" 
					: rs.getString(j).toString().trim())+ "</td>"%>
			<%
				}}
				
		%><%="</tr>"%>
		<%
			}
		%>
		<%
			rs.close();
					prepStmt.close();
		%>
	</table>



	</body>
	</html>


</pd4ml:transform>
<%
	} else {
		response.setContentType("application/ms-excel");
		response.setHeader("Content-Disposition", "inline; filename="
				+ "User_id_Details.xls");
%>

<table width="1480px" border="1" id="tblMain">
	<tr>
	<td width=50px;>User Id</td>
			<td width=290px;>User Full Name</td>
			<td width=70px;>Branch Id</td>
			<td width=125px;>Branch Name</td>
			<td width=65px;>User Class</td>
			<td width=70px;>User Id <br/> Open Date</td>
			<td width=75px;>General Text</td>
			<td width=75px;>Auth <br/>Any Branch</td>
			<td width=65px;>User <br />	Branch No</td>
	</tr>
	<%
			PreparedStatement prepStmt = null;
					ResultSet rs = null;
					Connection objConnection = null;
					DBUtil ds = new DBUtil();
					objConnection = ds.getConnect();
					Get_Query g_query = new Get_Query();

					String User_id=request.getParameter("txtUser_id_output");
					String User_Branch_id=request.getParameter("txtUser_Branch_id_output");
					String User_Name=request.getParameter("txtUser_Name_output");

					String strSQL=g_query.Get_User_id_Query(0, 99999999,User_id,User_Name,User_Branch_id);
					prepStmt = objConnection
							.prepareStatement(strSQL.split("~")[0]);
					rs = prepStmt.executeQuery();
		%>
		<%
			while (rs.next()) {
		%><%="<tr>"%>
		<%
			for (int j = 1; j <= rs.getMetaData().getColumnCount()-3; j++) {
//				System.out.println(rs.getMetaData().getColumnName(j));
				if(rs.getMetaData().getColumnName(j).equalsIgnoreCase("user_branch"))
				{
		%><%="<td>"+ (rs.getString(j).toString().trim().length() <= 0 ?
				"&nbsp;" 
				: rs.getString(j).toString().trim().split("/")[0])+ "</td>"%>
				
				<%="<td>"+ (rs.getString(j).toString().trim().length() <= 0 ?
				"&nbsp;" 
				: rs.getString(j).toString().trim().split("/")[1])+ "</td>"%>
		<%
			
		}			
		else
		{
			%><%="<td>"+ (rs.getString(j).toString().trim().length() <= 0 ? 
					"&nbsp;" 
					: rs.getString(j).toString().trim())+ "</td>"%>
			<%
				}}
				
		%><%="</tr>"%>
		<%
			}
		%>
		<%
			rs.close();
					prepStmt.close();
		%>
</table>



</body>
</html>
<%
	}
%>

 --%>