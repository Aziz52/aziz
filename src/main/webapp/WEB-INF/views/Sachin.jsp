<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
  <meta name="description" content="mobile first, app, web app, responsive, admin dashboard, flat, flat ui">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  
  

  <script src="js/jquery.min.js"></script>
  <script type="text/javascript">
<!--


var t = '<%=session.getAttribute("user_id")%>';

if (t == null)
{
	window.location.href= 'Login.jsp';
}
if (t == "")
{
	window.location.href= 'Login.jsp';
}
if (t == 'null')
{
	window.location.href= 'Login.jsp';
}

//-->
</script>
  
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/plugin.css">
  <link rel="stylesheet" href="css/font.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="easyui.css">
  <script type="text/javascript" src="Include/Fin7_JavaScript_Function.js"></script>
   <script type="text/javascript" src="Include/Isbs_JavaScript_Function.js"></script>
   
   <link rel="stylesheet" type="text/css" href="jquery.calendars.picker.css">
<script type="text/javascript" src="jquery.calendars.all.js"></script>
  <!--[if lt IE 9]>
    <script src="js/ie/respond.min.js"></script>
    <script src="js/ie/html5.js"></script>
  <![endif]-->
</head>
<body>
  <div style="float: left; width: 98%; padding: 0px 0px 0px 15px;" id="container">
                    <div id="toolstrip" class="toolstrip" style="width: 95%;">
                        <div id="ctl00_ContentPlaceHolder1_dynaStrip"><div class="smallIco"><img onclick="downloadFile('Folder');" alt="Toggle Folder" title="Toggle Folder" src="images/sfolder.png"></div><div class="smallIco"><img onclick="downloadFile('download');" alt="Download" title="Download" src="images/download.png"></div><div class="smallIco"><img onclick="downloadFile('preview');" alt="Preview" title="Preview" src="images/preview.png"></div><div class="smallIco"><img onclick="downloadFile('zipfiles');" alt="Compress Files" title="Compress Files" src="images/szip.png"></div><div class="smallIco"><img onclick="downloadFile('FileEdit');" alt="Edit" title="Edit" src="images/edit.png"></div><div class="smallIco"><img onclick="downloadFile('versioncontrol')" alt="Version control" title="Version control" src="images/versioncontrol.png"></div><div class="smallIco"><img onclick="downloadFile('delete')" alt="Delete" title="Delete" src="images/delete.png"></div><div class="smallIco"><img onclick="downloadFile('relateddocs')" alt="Add Related Document" title="Add Related Document" src="images/relateddocument.png"></div><div class="smallIco"><img onclick="downloadFile('routefile');" alt="Route Files" title="Route Files" src="images/routing.png"></div><div class="smallIco"><img onclick="downloadFile('email');" alt="Send Email" title="Send Email" src="images/email.png"></div><div class="smallIco"><img onclick="downloadFile('print');" alt="Print File" title="Print File" src="images/print.png"></div><div class="smallIco"><img onclick="downloadFile('ManageDoc');" alt="Manage Document" title="Manage Document" src="images/modify.png"></div></div>
                        <div class="smallIco">
                            <input type="image" name="ctl00$ContentPlaceHolder1$imgToggleButton" id="ctl00_ContentPlaceHolder1_imgToggleButton" src="images/toggle.png" style="border-width:0px;"></div>
                        <div id="ctl00_ContentPlaceHolder1_dDownloadAll" class="smallIco">
                            <input type="image" name="ctl00$ContentPlaceHolder1$btnDownloadAll" id="ctl00_ContentPlaceHolder1_btnDownloadAll" src="images/downloadall.png" alt="Download All Retrieved Records" style="border-width:0px;"></div>
                        <div class="smallIco">
                            <img style="height: auto; width: auto;" alt="Display" src="images/show.gif">
                            <input name="ctl00$ContentPlaceHolder1$txtDisplay" type="text" value="15" id="ctl00_ContentPlaceHolder1_txtDisplay" class="bigBox" size="2">
                            <input type="image" name="ctl00$ContentPlaceHolder1$btnDisplay" id="ctl00_ContentPlaceHolder1_btnDisplay" src="images/go.png" alt="Show entered number of records" style="border-width:0px;">
                        </div>
                       
                        <div class="smallIco">
                            <img onclick="downloadFile('searchadvance');" title="Advanced Search " src="images/advancedsearch.png"></div>
                    <div class="disText" style="float: right; padding-top: 5px;">
                    Total Records : <span id="ctl00_ContentPlaceHolder1_totrec">12</span>
                    </div>
                    </div>
                   
                      
                            <br>
                            <div id="Rowcontainer" style="text-align: center; width: 95%; height: 487px; overflow: auto; padding-top: 0px;">
                                <center>
                                    <table class="myTable nopadding" id="myTable1" style="white-space: nowrap;" width="98%">
                                        <tbody><tr class="header">
                                            <th valign="top" style="width: 15px;">
                                            </th>
                                            <th valign="top" style="width: auto;">
                                            </th>
                                            <th valign="top" style="width: auto;">
                                            </th>
                                            <th id="Th3">
                                            </th>
                                            <th id="ffName">
                                                Name
                                            </th>
                                            <th id="ffKey" style="text-align: center;">
                                                Keywords
                                            </th>
                                        </tr>
                                        <tr>
                                            <th valign="top" style="width: 15px;">
                                                <input type="image" name="ctl00$ContentPlaceHolder1$fltButt" id="ctl00_ContentPlaceHolder1_fltButt" src="images/filter.png" style="height:30px;width:30px;border-width:0px;">
                                            </th>
                                            <th valign="top" style="width: 1%;">
                                            </th>
                                            <th valign="top" style="width: 1%;">
                                            </th>
                                            <th valign="top" style="width: 1%;">
                                            </th>
                                            <th id="Th1">
                                                <input type="text" class="bigBox" style="width: 100%;" name="fltName">
                                            </th>
                                            <th id="Th2" style="text-align: center; width: auto;">
                                                <input type="text" class="bigBox" name="fltKey" style="width: 100%;">
                                            </th>
                                        </tr>
                                        <tr id="Srtr_13" class=""><td> <input type="checkbox" id="chkbx" name="cb1" value="13|64"></td> <td onclick="ShowDetails('13')"><img src="images/bullet_green.png" alt="No" related="" document="" height="20" width="20"> </td> <td onclick="ShowDetails('13')">&nbsp;</td> <td onclick="ShowDetails('13')"><img src="images/srar.png" height="18" width="18"> </td> <td style="text-align:left;white-space:nowrap;" onclick="ShowDetails('13')">SwiftDAR_CD.rar</td> <td style="text-align:left;white-space:nowrap;" id="td4_13" onclick="ShowDetails('13')">Malwa Gramin Bank SwiftDAR CD </td></tr><tr id="trHid_13" class="Hide noHover" style="height: 300px; display: none;"><td></td><td></td><td></td><td colspan="3" id="td_13" valign="top"></td></tr><tr id="Srtr_12" class="RowClick"><td> <input type="checkbox" id="chkbx" name="cb1" value="12|64"></td> <td onclick="ShowDetails('12')"><img src="images/bullet_green.png" alt="No" related="" document="" height="20" width="20"> </td> <td onclick="ShowDetails('12')"><img src="images/lock.png" alt="Secured" height="20" width="20"> </td> <td onclick="ShowDetails('12')"><img src="images/srar.png" height="18" width="18"> </td> <td style="text-align:left;white-space:nowrap;" onclick="ShowDetails('12')">HDFC Trade Finance.rar</td> <td style="text-align:left;white-space:nowrap;" id="td4_12" onclick="ShowDetails('12')">HDFC Report Creation HDFC TF 1</td></tr><tr id="trHid_12" class="Hide noHover" style="height: 300px; display: none;"><td></td><td></td><td></td><td colspan="3" id="td_12" valign="top"></td></tr><tr id="Srtr_11"><td> <input type="checkbox" id="chkbx" name="cb1" value="11|64"></td> <td onclick="ShowDetails('11')"><img src="images/bullet_green.png" alt="No" related="" document="" height="20" width="20"> </td> <td onclick="ShowDetails('11')"><img src="images/lock.png" alt="Secured" height="20" width="20"> </td> <td onclick="ShowDetails('11')"><img src="images/srar.png" height="18" width="18"> </td> <td style="text-align:left;white-space:nowrap;" onclick="ShowDetails('11')">BOB_MERGER.rar</td> <td style="text-align:left;white-space:nowrap;" id="td4_11" onclick="ShowDetails('11')">BOB Data Migration BOB Merger </td></tr><tr id="trHid_11" class="Hide noHover" style="height: 300px;"><td></td><td></td><td></td><td colspan="3" id="td_11" valign="top"> </td></tr><tr id="Srtr_10"><td> <input type="checkbox" id="chkbx" name="cb1" value="10|64"></td> <td onclick="ShowDetails('10')"><img src="images/bullet_green.png" alt="No" related="" document="" height="20" width="20"> </td> <td onclick="ShowDetails('10')"><img src="images/lock.png" alt="Secured" height="20" width="20"> </td> <td onclick="ShowDetails('10')"><img src="images/srar.png" height="18" width="18"> </td> <td style="text-align:left;white-space:nowrap;" onclick="ShowDetails('10')">SwiftForm_Bob_Merger_Jogeshwari .rar</td> <td style="text-align:left;white-space:nowrap;" id="td4_10" onclick="ShowDetails('10')">Bank of Baroda Data Migration </td></tr><tr id="trHid_10" class="Hide noHover" style="height: 300px;"><td></td><td></td><td></td><td colspan="3" id="td_10" valign="top"> </td></tr><tr id="Srtr_9"><td> <input type="checkbox" id="chkbx" name="cb1" value="9|64"></td> <td onclick="ShowDetails('9')"><img src="images/bullet_green.png" alt="No" related="" document="" height="20" width="20"> </td> <td onclick="ShowDetails('9')"><img src="images/lock.png" alt="Secured" height="20" width="20"> </td> <td onclick="ShowDetails('9')"><img src="images/srar.png" height="18" width="18"> </td> <td style="text-align:left;white-space:nowrap;" onclick="ShowDetails('9')">Brusel.rar</td> <td style="text-align:left;white-space:nowrap;" id="td4_9" onclick="ShowDetails('9')">BOB Brusel Data Migration BOB </td></tr><tr id="trHid_9" class="Hide noHover" style="height: 300px;"><td></td><td></td><td></td><td colspan="3" id="td_9" valign="top"> </td></tr><tr id="Srtr_8"><td> <input type="checkbox" id="chkbx" name="cb1" value="8|64"></td> <td onclick="ShowDetails('8')"><img src="images/bullet_green.png" alt="No" related="" document="" height="20" width="20"> </td> <td onclick="ShowDetails('8')"><img src="images/lock.png" alt="Secured" height="20" width="20"> </td> <td onclick="ShowDetails('8')"><img src="images/srar.png" height="18" width="18"> </td> <td style="text-align:left;white-space:nowrap;" onclick="ShowDetails('8')">USA.rar</td> <td style="text-align:left;white-space:nowrap;" id="td4_8" onclick="ShowDetails('8')">BOB USA Data Migration BOB USA</td></tr><tr id="trHid_8" class="Hide noHover" style="height: 300px;"><td></td><td></td><td></td><td colspan="3" id="td_8" valign="top"> </td></tr><tr id="Srtr_7"><td> <input type="checkbox" id="chkbx" name="cb1" value="7|64"></td> <td onclick="ShowDetails('7')"><img src="images/bullet_green.png" alt="No" related="" document="" height="20" width="20"> </td> <td onclick="ShowDetails('7')"><img src="images/lock.png" alt="Secured" height="20" width="20"> </td> <td onclick="ShowDetails('7')"><img src="images/srar.png" height="18" width="18"> </td> <td style="text-align:left;white-space:nowrap;" onclick="ShowDetails('7')">UCO.rar</td> <td style="text-align:left;white-space:nowrap;" id="td4_7" onclick="ShowDetails('7')">UCO Singapur Data Arichval UCO</td></tr><tr id="trHid_7" class="Hide noHover" style="height: 300px;"><td></td><td></td><td></td><td colspan="3" id="td_7" valign="top"> </td></tr><tr id="Srtr_6"><td> <input type="checkbox" id="chkbx" name="cb1" value="6|64"></td> <td onclick="ShowDetails('6')"><img src="images/bullet_green.png" alt="No" related="" document="" height="20" width="20"> </td> <td onclick="ShowDetails('6')"><img src="images/lock.png" alt="Secured" height="20" width="20"> </td> <td onclick="ShowDetails('6')"><img src="images/srar.png" height="18" width="18"> </td> <td style="text-align:left;white-space:nowrap;" onclick="ShowDetails('6')">DCB BANK.rar</td> <td style="text-align:left;white-space:nowrap;" id="td4_6" onclick="ShowDetails('6')">DCB BANK Data Arichval DCB 1.0</td></tr><tr id="trHid_6" class="Hide noHover" style="height: 300px;"><td></td><td></td><td></td><td colspan="3" id="td_6" valign="top"> </td></tr><tr id="Srtr_5" class=""><td> <input type="checkbox" id="chkbx" name="cb1" value="5|64"></td> <td onclick="ShowDetails('5')"><img src="images/bullet_green.png" alt="No" related="" document="" height="20" width="20"> </td> <td onclick="ShowDetails('5')">&nbsp;</td> <td onclick="ShowDetails('5')"><img src="images/srar.png" height="18" width="18"> </td> <td style="text-align:left;white-space:nowrap;" onclick="ShowDetails('5')">mainpuri.rar</td> <td style="text-align:left;white-space:nowrap;" id="td4_5" onclick="ShowDetails('5')">Mainpuri Mainpuri 1.0 .Net 3.5</td></tr><tr id="trHid_5" class="Hide noHover" style="height: 300px; display: none;"><td></td><td></td><td></td><td colspan="3" id="td_5" valign="top"></td></tr><tr id="Srtr_4"><td> <input type="checkbox" id="chkbx" name="cb1" value="4|64"></td> <td onclick="ShowDetails('4')"><img src="images/bullet_green.png" alt="No" related="" document="" height="20" width="20"> </td> <td onclick="ShowDetails('4')">&nbsp;</td> <td onclick="ShowDetails('4')"><img src="images/srar.png" height="18" width="18"> </td> <td style="text-align:left;white-space:nowrap;" onclick="ShowDetails('4')">sbm_signature_software.rar</td> <td style="text-align:left;white-space:nowrap;" id="td4_4" onclick="ShowDetails('4')">SBM SBM Mode of Operation to i</td></tr><tr id="trHid_4" class="Hide noHover" style="height: 300px;"><td></td><td></td><td></td><td colspan="3" id="td_4" valign="top"> </td></tr><tr id="Srtr_3"><td> <input type="checkbox" id="chkbx" name="cb1" value="3|64"></td> <td onclick="ShowDetails('3')"><img src="images/bullet_green.png" alt="No" related="" document="" height="20" width="20"> </td> <td onclick="ShowDetails('3')">&nbsp;</td> <td onclick="ShowDetails('3')"><img src="images/srar.png" height="18" width="18"> </td> <td style="text-align:left;white-space:nowrap;" onclick="ShowDetails('3')">Sambhav_Motors.rar</td> <td style="text-align:left;white-space:nowrap;" id="td4_3" onclick="ShowDetails('3')">Sandeep Doshi Na Sambhav Motor</td></tr><tr id="trHid_3" class="Hide noHover" style="height: 300px;"><td></td><td></td><td></td><td colspan="3" id="td_3" valign="top"> </td></tr><tr id="Srtr_2"><td> <input type="checkbox" id="chkbx" name="cb1" value="2|64"></td> <td onclick="ShowDetails('2')"><img src="images/bullet_green.png" alt="No" related="" document="" height="20" width="20"> </td> <td onclick="ShowDetails('2')"><img src="images/lock.png" alt="Secured" height="20" width="20"> </td> <td onclick="ShowDetails('2')"><img src="images/srar.png" height="18" width="18"> </td> <td style="text-align:left;white-space:nowrap;" onclick="ShowDetails('2')">SWIFT_ETL.rar</td> <td style="text-align:left;white-space:nowrap;" id="td4_2" onclick="ShowDetails('2')">BOB,CEDGE,RBS,WIPRO,HP SwiftET</td></tr><tr id="trHid_2" class="Hide noHover" style="height: 300px;"><td></td><td></td><td></td><td colspan="3" id="td_2" valign="top"> </td></tr>
                                    </tbody></table>
                                </center>
                            </div>
                            <br>
                            <div id="Div1" style="width: 95%;" class="grdDiv">
                                <table width="100%">
                                    <tbody><tr>
                                        <td style="width: 33%; text-align: left;">
                                            <table>
                                                <tbody><tr>
                                                    <td style="text-align: center;">
                                                        </td></tr><tr>
                                                            <td style="text-align: left;" class="disText">
                                                                Page No : &nbsp;&nbsp;
                                                            </td>
                                                            <td style="text-align: left;">
                                                                <input name="ctl00$ContentPlaceHolder1$gotopage" type="text" id="ctl00_ContentPlaceHolder1_gotopage" class="bigBox" style="width: 50px;">
                                                            </td>
                                                            <td>
                                                                <input type="image" name="ctl00$ContentPlaceHolder1$btnGoTo" id="ctl00_ContentPlaceHolder1_btnGoTo" text="Go" src="images/go.png" alt="Go to the page number entered" onclick="return ValidatePaging('goto');" style="border-width:0px;">
                                                            </td>
                                                        </tr>
                                                    
                                                    
                                                    
                                                    
                                                
                                            </tbody></table>
                                        </td>
                                        <td style="width: 33%; text-align: center;" class="disText">
                                            Page  <span id="ctl00_ContentPlaceHolder1_current_page" style="width: 20px;">1</span> Of <span id="ctl00_ContentPlaceHolder1_total_pages" style="width: 20px;">1</span> Pgs
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                    <td colspan="3" style="text-align:left;">
                                    
                                    
                                    </td>
                                    </tr>
                                </tbody></table>
                            </div>
                            <!-- Hidden Fields to preserve the advanced Searched Field value in paging and additional filtering -->
                            <input type="hidden" name="ctl00$ContentPlaceHolder1$advSrFld" id="ctl00_ContentPlaceHolder1_advSrFld">
                            <input type="hidden" name="ctl00$ContentPlaceHolder1$hidSearchStr" id="ctl00_ContentPlaceHolder1_hidSearchStr">
                            <input type="hidden" name="ctl00$ContentPlaceHolder1$advSrVal" id="ctl00_ContentPlaceHolder1_advSrVal">
                            <input type="hidden" name="ctl00$ContentPlaceHolder1$hidSearchCondtion" id="ctl00_ContentPlaceHolder1_hidSearchCondtion">
                            <input type="hidden" name="ctl00$ContentPlaceHolder1$hidDeleteStr" id="ctl00_ContentPlaceHolder1_hidDeleteStr">
                    <input type="hidden" name="ctl00$ContentPlaceHolder1$hidDigSign" id="ctl00_ContentPlaceHolder1_hidDigSign">
                    <input type="hidden" name="ctl00$ContentPlaceHolder1$hidVersionControl" id="ctl00_ContentPlaceHolder1_hidVersionControl">
                            <!-- Hidden Fields to preserve the Basic Search Field value in paging -->
                            <input type="hidden" name="ctl00$ContentPlaceHolder1$hidFileName" id="ctl00_ContentPlaceHolder1_hidFileName">
                            <input type="hidden" name="ctl00$ContentPlaceHolder1$hidKeywords" id="ctl00_ContentPlaceHolder1_hidKeywords">
                            <input type="hidden" name="ctl00$ContentPlaceHolder1$tempid" id="ctl00_ContentPlaceHolder1_tempid">
                            <input type="hidden" name="doaction">
                            <input type="hidden" name="pgAction" id="pgAction">
                            <input type="hidden" name="routefileid" id="routefileid">
                            <input type="hidden" id="routefoldid" name="routefoldid">
                       
                </div>















</body>
</html>