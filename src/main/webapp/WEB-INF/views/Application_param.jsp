<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Application Setting</title>
<jsp:include page="new_header.jsp"></jsp:include>

<script type="text/javascript">
//<![CDATA[
          
jQuery(document).ready(function($) {
	
	//
	//Getdata When Load Form using -------SETTING_TYPE,PARAM_NAME,PARAM_VALUE,DEL_FLAG,CREATED_USER,CREATED_TIME,MODIFIED_USER,MODIFIED_TIME

	
	jQuery('#tt1').datagrid({  
	    url:'Application_param_servlet', 
	    columns:[[  
	        {field:'setting_type',title:'Setting Type',width:80},  
	        {field:'param_name',title:'Parameter Name',width:170},
	        {field:'param_value',title:'Parameter Value',width:100},  
	        {field:'del_flag',title:'Status',width:50},  
	        {field:'created_user',title:'Created User',width:80},  
	        {field:'created_time',title:'Created Time',width:120},
	        {field:'modified_user',title:'Last Modified User',width:90},  
	        {field:'modified_time',title:'Last Modified Time',width:120},
	        {field:'edit',title:'Edit',width:50}  
	    ]] ,
	     showTableToggleBtn: true ,
	    title:"Application Setting", //report Title
	    height: 380, //Height of Grid
	    width:900 //Width of Grid
	    
	}); 
});
	
//]]>
function display_user(setting_type,Param_name)
{

	var edit_txt=new Array();
		jQuery.ajax({
		type:"POST",
		url:"Application_param_servlet?Report=Edit&details=get&setting="+ setting_type +"&name="+ Param_name,
		data:"",
		success:function(msg){
			edit_txt=msg.split('~');
			jQuery("#txtApp_Setting_Type").val(edit_txt[0]);
			jQuery("#txtParam_Name").val(edit_txt[1]);
			jQuery("#txtParam_Value").val(edit_txt[2]);
			jQuery("#selParam_Flag").val(edit_txt[3]);
			jQuery("#param_id").val(edit_txt[4]);
		}
		});
		display('Edit Setting','edit');
}

function clr_txt()
{
	jQuery("#txtApp_Setting_Type").val('');
	jQuery("#txtParam_Name").val('');
	jQuery("#txtParam_Value").val('');
	jQuery("#selParam_Flag").val('');
	jQuery("#param_id").val('');
}
function editadd_user(Edit)
{	var no=4;
	

	var edit_txt=new Array();
		jQuery.ajax({
		type:"POST",
		url:"Application_param_servlet?Report=Edit&details="+Edit+"&setting="+ jQuery("#txtApp_Setting_Type").val() +"&name="+ jQuery("#txtParam_Name").val()+"&param_val="+ jQuery("#txtParam_Value").val()+"&status="+ jQuery("#selParam_Flag").val()+"&param_id="+  jQuery("#param_id").val(),
		data:"",
		success:function(msg){	
			if(msg=='E')
			{
				alert("Error Occured");
				window.parent.location='Application_param.jsp';
			}
			else
			{
				window.parent.location='Application_param.jsp';
				alert(Edit+"Sucessfully");
			}		
		}
		});
		
}
function display(head,button)
{
	
	if(button=='edit')
	{
		$("#Add").hide();
		$("#edit").show();
	}
	else{
		$("#Add").show();
		$("#edit").hide();
	}
	jQuery("#AddUserContent").show();
	var divTag = jQuery("#AddUserContent");
	divTag.dialog({
  		modal: true,
  		autoOpen: false,
  		title: head,
  		show: "blind",
  		height: 310, //Height of Grid
	    width:400, 
        hide: "explode"
  	});
	divTag.dialog('open');
	return false;
}
$("#reg_box_wrapper").hide();

</script>

</head>

<body style="width: 90%;">

<form>
<div   class="tab_divs">

<center>
<div class="form_inner_wrap">
<div id="reg_box" class="form_section" style="padding-bottom: 6%;"><br />
<br />
<input class="btn_style" style="float: right;" plain="true" id="adds" onclick="display('Add Setting','Edit')" type="button" value="Add New">
<div id="get_details" style=" margin-left: 49px">
<table id="tt1" style="diplay: none !important;" class="easyui-datagrid"
	iconCls="icon-search" rownumbers="true" pagination="true">
</table>
</div>


</div>
</div>

<div id="search_box" class="search_section">

<div id="tb" class="tb_borders">


<div id="AddUserContent" class="tb_borders"  style="display:none;">

<table class="mTable" >
	<tr>
		<td>Application Setting Type</td>
		<td>&nbsp;<input type="text" id="txtApp_Setting_Type" value=""
			name="txtApp_Setting_Type" class="input-text"></input></td>
	</tr>
	<tr>
		<td>Parameter Name</td>
		<td>&nbsp;<input type="text" id="txtParam_Name" value=""
			name="txtParam_Name" class="input-text"></input></td>
	</tr>
	<tr>
		<td>Parameter Value</td>
		<td>&nbsp;<input type="text" id="txtParam_Value" value=""
			name="txtParam_Value" class="input-text"></input></td>
	</tr>
	<tr>
		<td>Status</td>
		<td><select class="input-text" id="selParam_Flag"
			name="selParam_Flag">
			<option value="Y" selected="true">Yes</option>
			<option value="N">No</option>
		</select>
		</td>
	</tr>
	<tr>
	<td colspan="2" style="text-align: center;">
		<br>
	<input class="btn_style" plain="true" id="edit" onclick="editadd_user('edit')" type="button" value="Save"> 
	<input class="btn_style" plain="true" id="Add" onclick="editadd_user('Add')" type="button" value="Save"> 
	<input	class="btn_style" plain="true" type="reset" onclick="clr_txt();" value="Clear">
	<input	type="hidden" id="param_id" name="param_id"></input> 
	</td>
	</tr>
</table>
</div>

</div>
</div>

</center>

<script type="text/javascript">


	function conditions()
	{
		
	} 
</script>

</div>
</form>

<jsp:include page="Footer.jsp"></jsp:include>


</body>

</html>