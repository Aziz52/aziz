<%@page import="com.swiftDoc.service.ReadFilesFromFolder"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.connection.DBUtil"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<jsp:include page="new_header.jsp"></jsp:include>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Report Upload</title>

</head>
<body>

	<form id="form1" name="form1" method="post">
		<div id="divbody">
			<div id="CentrePart">


				<script type="text/javascript">
				
				function GetApp() {
					
					$.get('UserAuthentication_servlet?opr=GetAppList&typ=select', function(
							responseText) {
						if (responseText != "") {
							$("#appList").html(responseText);
						}
					});

				}
					function UploadError() {
						alert("Upload Error");
					}
					function UploadComplete() {
						//alert("UC");

						//alert(document.getElementById('docSec').value);
						var tmp;
						tmp = loadXMLDoc('commondata.aspx?action=BulkUpload&docsec=G');
						//alert(tmp);
						if (tmp == 'Yes') {
							alert('Process Completed');
							$('#ClientUpload').empty();
							var d = new Date();
							$('#ClientUpload').load(
									'tdesign.aspx?action=BulkUploadReport&t='
											+ d.getTime());

						} else {
							alert('Error Occured');
							$('#report').html(tmp);
						}

					}

					jQuery(document)
							.ready(
									function($) {

										$
												.ajax({
													type : "POST",
													url : "Template_commanDetails_servlet?Detail=CHEK_Select",
													data : "",
													success : function(msg) {

														$("#selectTemplate")
																.html(msg);

													}
												});

									});

					$(window).bind("load", function() {
						Adjust();

					});

					function savedata() {
						if ($("#inptrantype").val() == '') {
							alert('Select Zone');
					
							return false;
						}
						$( "#btnSave" ).prop( "disabled", true );
						$( "#upl" ).show();
						document.form1.action = 'InsertDatainTempateServlet?operation=Save&zone='+$("#inptrantype").val();
						document.form1.submit();
						
					}
				</script>
				<%
					ReadFilesFromFolder res = new ReadFilesFromFolder();
				%>

				<div class="content" id="content">
					<div id="UpdatePanel1" style="width:550px";>

						<div align="left" class="contentPanel" id="contentPanel"
							style="vertical-align: top; min-height: 407.33px;">
							<div class="divHeading" style="padding-left: 170px;">
								<table  with=300px>
									<tbody>
										<tr>
													<td><div class="form-group">
															
															<div id="appList" class="col-lg-8">
																
															</div>
															
																<div class="col-lg-3 control-label">
															
															<input type="button" id="btnSave"
												class="btn_style btn btn-white" name="Save"
												onclick="savedata()" value="Upload">
												<div id="upl">
												<img src="images/uploading.gif">
												</div>
												
															
																</div>
																
														</div></td>
												</tr>
										
									</tbody>
								</table>
							</div>

							

							<input type="hidden" name="hid_template_id" id="hid_template_id">
							<input type="hidden" name="hid_folder_id" id="hid_folder_id">
							<input type="hidden" name="hid_DuplicateDoc"
								id="hid_DuplicateDoc"> <input type="hidden"
								name="Hid_errorDoc" id="Hid_errorDoc"> <span
								id="lblDuplicateDoc"></span>
						</div>

					</div>

				</div>




			</div>

		</div>


		<script type="text/javascript">
			function Get_status_Details(id, Status) {

				jQuery('#status_Details').datagrid(
						{
							url : 'File_LogDetailsServlet?Status=' + Status
									+ '&Folder_id=' + id,
							columns : [ [ {
								field : 'FILE_NAME',
								title : 'FILE_NAME',
								width : 200
							}, {
								field : 'STATUS',
								title : 'STATUS',
								width : 80
							}, {
								field : 'FILE_PATH',
								title : 'FILE_PATH',
								width : 300
							}, {
								field : 'FILEUPLOAD_DATE',
								title : 'FILEUPLOAD_DATE',
								width : 120
							} ] ],
							showTableToggleBtn : true,
							//title:"Lien Details", //report Title
							height : 400, //Height of Grid
							width : 700, //Width of Grid
						// pagination:false  
						//1pageSize:tot_chq
						});
				var divTag = jQuery("#hidDIvdds");

				divTag.dialog({
					modal : true,
					autoOpen : false,
					title : "File Details ",
				//show: "blind",
				//hide: "explode"
				});
				divTag.dialog('open');
				return false;

			}
		</script>


	</form>
	<script>
GetApp();
$( "#upl" ).hide();
</SCRIPT>
	<jsp:include page="Footer.jsp"></jsp:include>

</body>
</html>