
<html>
<jsp:include page="new_header.jsp"></jsp:include>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SwiftDAR_ORF Details</title>


<link rel="stylesheet" href="TreeView/jquery.treeview.css" />

<link rel="stylesheet" href="TreeView/screen.css" />

<script src="TreeView/jquery.cookie.js" type="text/javascript"></script>
<script src="TreeView/jquery.treeview.js" type="text/javascript"></script>



<script type="text/javascript">
	//         
	var CurrentFolder;
	CurrentFolder="";
	jQuery(document).ready(function($) {
		//Getdata When Load Form using
		resizeWindow();

		$('#Report_Name').change(function() {
			check_links2();

		});
		$.ajax({
			type : "POST",
			url : "Template_commanDetails_servlet?Detail=Select_Zone",
			data : "",
			success : function(msg) {
				$("#Zone_Name").html(msg);

			}
		});

		jQuery('#tt1').datagrid({
			url : 'TemplateSearchBOI_Servlet',
			columns : [ [

			{
				field : 'zone_Name',
				title : 'Zone Name',
				width : 150
			}, {
				field : 'folder_Name',
				title : 'Folder Name',
				width : 150
			}, {
				field : 'report_Name',
				title : 'Report Name',
				width : 150
			}, {
				field : 'report_Date',
				title : 'Report Date',
				width : 80
			}, {
				field : 'report_Version',
				title : 'Report Version',
				width : 80
			}, {
				field : 'view',
				title : 'View',
				width : 50
			}, {
				field : 'pdf',
				title : 'PDF',
				width : 50
			}, {
				field : 'download',
				title : 'Download',
				width : 70
			}

			] ],
			showTableToggleBtn : true,
			title : "Report Details", //report Title
			height : 400, //Height of Grid
			width : 900,//Width of Grid
			pageSize : 50

		});
		$("body").resize(function(e) {
			resizeWindow();
		});

	});

	function Folder_id(Value) {
		CurrentFolder="";
		
		try {
			jQuery.ajax({
				type : "POST",
				url : "TemplateSearchBOI_Servlet",
				data : "operation=getfolder&Value=" + Value,
				success : function(msg) {
					$("#main").html(msg);
					$("#browser").treeview(
							{
								toggle : function() {
									console.log("%s was toggled.", $(this)
											.find(">span").text());
								}
							});

				}
			});

		} catch (e) {
			alert(e);
		}
	}

	function reportId(Value,FolderName) {
		CurrentFolder=Value;
		$("#selfolder").text($(FolderName).text());
		//$(selfolder).text=;
		//=$(FolderName).text();
		
		
		try {
			jQuery.ajax({
				type : "POST",
				url : "TemplateSearchBOI_Servlet",
				data : "operation=getrportName&Value=" + Value+"&zone_id=" +  $('#Zone_Name').val(),
				success : function(msg) {

					$("#Report_Name").html(
							"<option value=''>--Select--</option>" + msg);

				}
			});
		} catch (e) {
			alert(e);
		}
	}

	$(function() {
		$('#Report_Date').calendarsPicker({
			calendar : $.calendars.instance('gregorian')
		});
	});

	function check_links2() {

		if ($('#Report_Name').val() != "") {
			Boi_get_min_max_date($('#Report_Name').val());

		}
	}

	function doSearch() { //Function use Search The Data Using user paramater

		$("a[href*=#gdata]").click();
		$('#tt1').datagrid('load', {
			
			msg : 'No Record Available For Selected criteria... ',
			Zone_Name : $('#Zone_Name').val(),
			Folder_Name : CurrentFolder,
			Report_Name : $('#Report_Name').val(),
			Report_Date : $('#Report_Date').val(),
			//Currency :$('#Currency').val(),
			flg : "Y"
		});

	}

	//
</script>

</head>

<body>
	<form id="form1" name="form1" method="post">
		<section id="content">
			<section class="main padder">
				<div class="row">



					<section class="panel">
						<header class="panel-heading">
							<ul class="nav nav-tabs nav-justified">
								<li class="active"><a href="#search" data-toggle="tab">Search</a></li>
								<li><a href="#gdata" data-toggle="tab">Get Data</a></li>
							</ul>
						</header>
						<div class="panel-body" style="margin-left: 10%;">
							<div class="tab-content">
								<div class="tab-pane active" id="search">
									<div class="search_section">
										<div id="tb">
											<span>
												<h3>Advanced Search</h3> <br>
											</span> <input type="hidden" id="chkUserID" name="chkUserID"
												value="chkUserID"></input>
											<table class="TblMain" >


												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Zone Name
																:&nbsp;</label>
															<div class="col-lg-8">
																<Select id="Zone_Name" class="bg-focus form-control"
																	onchange="Folder_id(this.value);">
																</Select>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>


												<tr>
													<td colspan="2"><label class="col-lg-3 control-label">Folder
															Name :&nbsp;</label>
														<div id="main" style="overflow: auto; height: 200px;"">
														</div>
														<div class="line line-dashed m-t-large"></div></td>
												</tr>

	<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Selected Folder
																:&nbsp;</label>
															<div class="col-lg-8">
																<span id=selfolder><font color=green>None</font></span>
																<div class="line line-dashed m-t-large"></div>
															</div>
													

												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Report Name
																:&nbsp;</label>
															<div class="col-lg-8">
																<Select id="Report_Name" class="bg-focus form-control">
																</Select>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Report
																Date:&nbsp;</label>
															<div class="col-lg-8">
																<input id="Report_Date"
																	onblur="checkdate(this,this.value)" type="text"
																	class="bg-focus form-control" />
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												<tr>
													<td colspan="3" class="char_srch_style" id="msgs"
														style="text-align: center;"></td>
												</tr>
												<tr>
													<td colspan="3" align="center" "style="padding-top: 15px;"><input
														class="btn_style btn btn-primary" onclick="conditions()"
														plain="true" type="button" value="Search" id="serch_btn" />
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="reset"
														id="test" class="btn_style btn btn-white" name="btn_Clear"
														value="Clear" /></td>
													<td></td>

												</tr>



											</table>

										</div>

									</div>

								</div>
								<div class="tab-pane" id="gdata">

									<div id="pdatagrid" style="width: 100%; overflow: auto;">
										<div id="tb" style="padding: 3px">

											<table id="tt1" class="easyui-datagrid" width="100%"
												iconCls="icon-search" rownumbers="true" pagination="true">
											</table>
										</div>
									</div>


								</div>
							</div>
						</div>
					</section>




				</div>
			</section>
		</section>



	</form>

	<script type="text/javascript">
		function conditions() {
			if ($('#msgs').html() != "") {

				if ($('#Report_Date').val() != '') {

					if (fmDate1 != null && fmDate2 != null) {
						if (!(cdate(document.form1.Report_Date.value) <= cdate(fmDate2))
								|| !(cdate(document.form1.Report_Date.value) >= cdate(fmDate1))) {
							alert('Incorrect date parameter entered. Please Enter Report Date between From '+fmDate1 +' To '+fmDate2);
							return false;
						}
					}
				}

			}

			// Cust_id Account_Number  Account_Name  acc_type  Branch_Code
			if ($('#Report_Name').val() != '' && $('#Report_Date').val() != '') {
				//alert('Please Criteria ');
				check_ReportDate_exist($('#Report_Name').val(), $(
						'#Report_Date').val());

			}

			else {
				doSearch();
			}
		}

		function DispalyFile(docid, filename,rptName,zoneName,rptDate,folderId) {
			createLog('View',filename,rptName,zoneName,rptDate,folderId);
			var e = $("input[type=search]").val();

			window
					.open(
							'Template_view.jsp?docide=' + docid + '&filename='
									+ filename + '&content=' + e,
							'winname',
							'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=900,height=500,top=100,left=70');

		}
		function createOutput(docid, filename,rptName,zoneName,rptDate,folderId) {

			//var kvpairs = [];
			//var form = // get the form somehow
			createLog('PDF',filename,rptName,zoneName,rptDate,folderId);
			$('#txtdocid_output').val(docid);
			$('#txtfilename_output').val(filename);
			$('#txtTypeOutPut').val("pdf");
			$('#txtHTMLOutPut').val("");
			$('#frmPdfOutPut').submit();

		}
	</script>
	<form action="DownloadTemplatePDF" method="post" style="display: none;"
		id="frmPdfOutPut">
		<input type="hidden" id="txtdocid_output" name="txtdocid_output">
		<input type="hidden" id="txtfilename_output" name="txtfilename_output">
		<input type="hidden" id="txtTypeOutPut" name="txtTypeOutPut">
		<input type="hidden" id="txtHTMLOutPut" name="txtHTMLOutPut">
	</form>

</body>


</html>
<jsp:include page="Footer.jsp"></jsp:include>