<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 
 <jsp:include page="new_header.jsp"></jsp:include>
<%-- <%@page import="com.service.Chart"%> --%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<html>




<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Home</title>




<link rel="stylesheet" href="<c:url value='/assets/amcharts/style.css' />" type="text/css">
<link rel="stylesheet" href="<c:url value='/assets/amcharts/sDashboard.css' />" type="text/css">
<script src="<c:url value='/assets/amcharts/amcharts.js' />" type="text/javascript"></script>
<script src="<c:url value='/assets/amcharts/pie.js' />" type="text/javascript"></script>
<script src="<c:url value='/assets/amcharts/serial.js' />" type="text/javascript"></script>

</head>



<%-- <script type="text/javascript">   
	  <%
 	 Chart ss= new Chart();
	  String as="";
	  String ChartData= ss.Application_chart(String.valueOf(session.getAttribute("G_Zone_id")),String.valueOf(session.getAttribute("user_id")));
 	  
 	  %>
 	  <%=ChartData%>
</script>

<script type="text/javascript">   
    	  <%
    	  as= ss.Month_chart(String.valueOf(session.getAttribute("G_Zone_id")),String.valueOf(session.getAttribute("user_id")));
    	  %>
    	  <%=as%>
    	  
</script> --%>
<%-- 
<script type="text/javascript">   
	  <%
 	 String ChartData1= ss.Log_chart(String.valueOf(session.getAttribute("G_Zone_id")),String.valueOf(session.getAttribute("user_id")));
 	  
 	  %>
 	  <%=ChartData1%>
</script>

<script type="text/javascript">   
	  <%
 	 String ChartData2= ss.State_Wise_chart(String.valueOf(session.getAttribute("G_Zone_id")),String.valueOf(session.getAttribute("user_id")));
 	  
 	  %>
 	  <%=ChartData2%>
</script>
 --%>

<body>


	<div align="center" style="padding-left: 20%;">
		<ul id="myDashboard" class="sDashboard ui-sortable">

			<li id="id009">
				<div class="sDashboardWidget">
					<div class="sDashboardWidgetHeader sDashboard-clearfix">Application wise Aadhaar Number Stored in SwiftDB</div>
					<div id="divTemplateSummary" class="sDashboardWidgetContent"
						style="width: 100%; height: 400px;"></div>
				</div>
			</li>
			<li id="id009">
				<div class="sDashboardWidget">
					<div class="sDashboardWidgetHeader sDashboard-clearfix">Application wise Retrieval Request</div>
						<div id="chartdiv" class="sDashboardWidgetContent"
							style="width: 100%; height: 400px;"></div>
				</div>
			</li>
			<!-- 
			<li id="id009">
				<div class="sDashboardWidget">
					<div class="sDashboardWidgetHeader sDashboard-clearfix">Application wise Aadhaar Number Accessed through SwiftDB</div>
					 <div style="overflow: auto; height: 340px;">
						<div id="Actitvitydiv" class="sDashboardWidgetContent"
							style="width: 100%; height: 400px;"></div>
					 </div>
				</div>
			</li>
			
			<li id="id009">
				<div class="sDashboardWidget">
					<div class="sDashboardWidgetHeader sDashboard-clearfix">Application wise Access Status</div>
					 <div style="overflow: auto; height: 340px;">
						<div id="Statewisediv" class="sDashboardWidgetContent"
							style="width: 100%; height: 400px;"></div>
					 </div>
				</div>
			</li> -->

		</ul>

	</div>




</body>
<jsp:include page="Footer.jsp"></jsp:include>

<script>
$("#reg_box_wrapper").hide();
</script>
</html>