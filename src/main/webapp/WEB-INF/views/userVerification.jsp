<jsp:include page="new_header.jsp"></jsp:include>
<html>

<head>

<title>User Verification</title>

<script type="text/javascript">
//<![CDATA[
           
jQuery(document).ready(function($) {
	//Getdata When Load Form using 
	
	jQuery('#tt1').datagrid({  
	    url:'UserAuthentication_servlet?operation=GetVerifyUser', 
	    columns:[[  
	        {field:'USER_ID',title:'User ID',width:100},  
	        {field:'USER_NAME',title:'User Name',width:200},
	        {field:'USER_STATUS',title:'User Status',width:100},  
	        {field:'USER_ROLE',title:'User Role',width:100},  
	        {field:'LAST_LOGIN_DATE',title:'Last Login Date',width:150},
	        {field:'LAST_LOGIN_IP',title:'Last Login IP',width:100},  
	        {field:'CREATED_USER',title:'Created User',width:100},
	        {field:'CREATED_TIME',title:'Created Time',width:150},
	        {field:'MODIFIED_USER',title:'Modified User',width:100},
	        {field:'MODIFIED_TIME',title:'Modified Time',width:80}//,
	      //  {field:'USER_VERIFY',title:'Verify User',width:100}
	         
	    ]] ,
	    queryParams:{
	    		msg : 'No Records to Verify',
	    		vr_flg:'Y'
		    		},
	     showTableToggleBtn: true ,
	    title:"User Verification", //report Title
	    height: 350, //Height of Grid
	    width:850 //Width of Grid
	    
	}); 
});
function clr() //Function Use To Blank Text
{
	$('#UserID').val(''); 
	$('#UserName').val(''); 
	
}
function doSearch() { //Function use Search The Data Using user paramater
	
	$('#tt1').datagrid('load', {
		msg : 'No Records to Verify',
		UserID :$('#UserID').val(),
		vr_flg:'Y',
		UserName :$('#UserName').val()
	//	acc_no1:'0002BC0001151'
	});
	showreg();
}
function chkVerify()
{
	var userID=jQuery("#userID").val();
	//alert(userID);
	if (userID != '')
	{
		document.form1.action = 'UserAuthentication_servlet?operation=VerifyUser&uid=' + userID;
		document.form1.submit();
	}
	
	
}

//]]>
</script>

</head>

<body style="width: 90%;">
<form id="form1" name="form1" method="post">
<div   class="tab_divs">
<center>
<div class="form_inner_wrap">
<div id="reg_box" class="form_section" style="padding-bottom: 4%;"><br />
<br />

<div id="get_details">
<center>
<table id="tt1" style="diplay: none !important;" class="easyui-datagrid"
	iconCls="icon-search" rownumbers="true" width="90%" pagination="true">
</table>
<br> 
</center>
</div>
</div>
</div>
<div id="search_box" class="search_section" style="display: none;padding-top: 12%;padding-bottom: 16%;">

<div id="tb" class="tb_borders">
<span>
<h3>User Verification</h3>

</span>

<input type="hidden" id="chkUserID" name="chkUserID" value="chkUserID"></input>
<table>
	<tr>
		<td><label class="cm-required">User ID :&nbsp;</label></td>
		<td><input id="UserID" type="text" class="input-text" /></td>
		<td>*</td>
	</tr><tr>
		<td><label class="cm-required">User Name:&nbsp;</label></td>
		<td><input id="UserName" type="text" class="input-text" /></td>
		<td>*</td>
	</tr>
	

	
	<tr>
		<td  colspan="3" style="padding-top: 15px;">
		
		<input class="btn_style" onclick="doSearch()"  plain="true" type="button" value="Search">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" onclick="clr()" class="btn_style" name="btn_Clear" value="Clear"></td>
		<td></td>
	</tr>
</table>

</div>
</div>
<div style="display:none;">
<div id="verify" style="padding:8px;">

<table class="mTable" >
	<tr>
		<td>User Id</td>
		<td>&nbsp;<input type="text" id="txt_User_id" value=""
			name="txt_User_id" disabled="disabled" class="input-text"></input></td>
	</tr>
	<tr>
		<td>User Name</td>
		<td>&nbsp;<input type="text" id="txt_User_Name" value=""
			name="txt_User_Name" disabled="disabled" class="input-text"></input></td>
	</tr>
	<tr>
		<td>User Status</td>
		<td> 
		<select id="txt_UserStatus" class="input-text"  disabled="disabled"  name="txt_UserStatus">
			<option value="E">Enable</option>
			<option value="D">Disable</option>
		</select>
		</td>
	</tr>
	<tr>
		<td>User Role</td>
		<td>&nbsp;<input type="text" id="txt_UserRole" value=""
			name="txt_UserRole" disabled="disabled" class="input-text"></input></td>
	</tr>
	<tr>
		<td>User Expiry Date</td>
		<td>&nbsp;<input type="text" id="txt_UserExpDate" value=""
			name="txt_UserExpDate" disabled="disabled" class="input-text"></input></td>
	</tr>
	<tr>
		<td>Unlock Account </td>
		<td>
		<select id="txt_Unlock_Account" class="input-text"  disabled="disabled"  name="txt_Unlock_Account">
			<option value="N" selected="true">No</option>
			<option value="Y">Yes</option>
		</select>
		</td>
	</tr>
	<tr>
		<td>Selected Zone </td>
		<td id="lb_app" style="padding-left: 20px;">  
		</td>
	</tr>
	<tr>
	<td colspan="2" style="text-align: center;">
		<br>
	<input class="btn_style" plain="true" id="edit" onclick="chkVerify()" type="button" value="Verify"> 
	<input class="btn_style" plain="true" id="Add" onclick="Rejected()" type="button" value="Reject"> 
	
	<input	type="hidden" id="userID" name="userID"></input> 
	</td>
	</tr>
</table>
</div>
</div>

<div id="showDiv" style="display:none;">
</div>
<jsp:include page="Footer.jsp"></jsp:include>
</center>
</div>
<script type="text/javascript">

function Rejected()
{
	var userID =jQuery("#userID").val();
	
	jQuery.ajax({
	type:"POST",
	url:'UserAuthentication_servlet?operation=get_Veri_Reject&uid=' + userID,
	data:"",
	success:function(msg){
		window.parent.location='userVerification.jsp';
	}
	});
}

function display_user(userID)
{
	
	jQuery("#userID").val('');
	var edit_txt=new Array();
		jQuery.ajax({
		type:"POST",
		url:'UserAuthentication_servlet?operation=get_VeriUserReport&uid=' + userID,
		data:"",
		success:function(msg){
			edit_txt=msg.split('~');
			//txt_User_id,txt_User_Name,txt_UserStatus,txt_UserRole,txt_UserExpDate,txt_Unlock_Account,lb_app
			jQuery("#userID").val(edit_txt[0]);
			jQuery("#txt_User_id").val(edit_txt[0]);
			jQuery("#txt_User_Name").val(edit_txt[1]);
			jQuery("#txt_UserStatus").val(edit_txt[2]);
			jQuery("#txt_UserRole").val(edit_txt[3]);
			jQuery("#txt_UserExpDate").val(edit_txt[4]);
			jQuery("#txt_Unlock_Account").val(edit_txt[5]);
			jQuery("#lb_app").html(edit_txt[7]);
		}
		});
		display();
}

function display()
{	
	var divTag = jQuery("#verify");
	divTag.dialog({
  		modal: true,
  		autoOpen: false,
  		title: "UserAuthentication",
  		show: "blind",
        hide: "explode"
  	});
	divTag.dialog('open');
	return false;
}
</script>
</form>




</body>
	</html>
	