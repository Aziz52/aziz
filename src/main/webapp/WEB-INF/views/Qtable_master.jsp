<jsp:include page="Header_new.jsp"></jsp:include>
<html>

<head>


<title>User Master</title>
<script type="text/javascript">
//<![CDATA[
           
jQuery(document).ready(function($) {
	//Getdata When Load Form using 
	
	jQuery('#tt1').datagrid({  
	    url:'Qtable_servlet',  
	    columns:[[  
	        {field:'ID',title:'Table Id',width:100},  
	        {field:'SCHEMA_NAME',title:'Schema Name',width:200},
	        {field:'TABLE_NAME',title:'Table Name',width:300},	       
	        {field:'TABLE_STATUS',title:'Use For Query Builder',width:150}
	    ]] ,
	     showTableToggleBtn: true ,
	    title:"Query builder Table", //report Title
	    height: 350, //Height of Grid
	    width:850 //Width of Grid
	});

	
	
	showsearch();  
});


function newSave()
{
	var tempCheked="";
	var tempNotCheked="";
	$.each($("input[name^='chk~']"),function(i,j)
		{
		if(j.checked)
		{
			tempCheked+="'"+j.id.split("~")[1]+"',";
		}
		else
		{
			tempNotCheked+="'"+j.id.split("~")[1]+"',";
		}
		});
	var str="";
	if(tempCheked.length > 0)
	{
		str="tempCheked="+tempCheked.substr(0,tempCheked.length-1)+"&";
	}
	if(tempNotCheked.length > 0)
	{
		str+="tempNotCheked="+tempNotCheked.substr(0,tempNotCheked.length-1);
	}

	 jQuery.ajax({
         type: "POST",
         url: 'Qtable_servlet?operation=saveData',
         data: str,
         success: function (data) {         	
         		if (data="y")
    			{
    				alert('Data Updated successfully');window.location.href='Qtable_master.jsp';	
    			}
    			else
    			{
    				alert('Action Failed');window.location.href='Qtable_master.jsp';
    			}
         }
     });
	
}


function clr() //Function Use To Blank Text
{
	$('#txtSchemaName').val(''); 
	$('#txtTableName').val(''); 
	
}
function doSearch() { //Function use Search The Data Using user paramater
	
	$('#tt1').datagrid('load', {
		msg:'The record not found',
		SchemaName :$('#txtSchemaName').val(),
		TableName :$('#txtTableName').val()
	//	acc_no1:'0002BC0001151'
	});

	$(".pagination table tbody tr ").append('<td><a onclick="newSave();"  class="easy-linkbutton" href="javascript:void(0)">Save</a></td>');
	showreg();
}


//]]>
</script>

</head>

<body style="width: 90%;">
<form id="form1" name="form1" method="post" action="">
<div class="tab_divs">

<center>
<div class="form_inner_wrap">
<div id="reg_box" class="form_section" style="padding-bottom: 6%;"><br />
<br />
<br />
<div id="get_details">
<center>
<table id="tt1" style="diplay: none !important;" class="easyui-datagrid"
	iconcls="icon-search" rownumbers="true" width="90%" pagination="true">
</table>

</center>
</div>
</div>

<div id="search_box" class="search_section"
	style="display: none; padding-top: 12%; padding-bottom: 16%;">

<div id="tb" class="tb_borders"><span>
<h3>Query Builder Table</h3>

</span> <input type="hidden" id="chkUserID" name="chkUserID" value="chkUserID"></input>
<table>
	<tr>
		<td><label class="cm-required">Schema Name :&nbsp;</label></td>
		<td><input id="txtSchemaName" type="text" class="input-text" /></td>
		<td></td>
	</tr>
	<tr>
		<td><label class="cm-required">Table Name:&nbsp;</label></td>
		<td><input id="txtTableName" type="text" class="input-text" /></td>
		<td></td>
	</tr>

	<tr>
		<td colspan="3" style="padding-top: 15px;"><input
			class="btn_style" onclick="doSearch()" plain="true" type="button"
			value="Search"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
			type="button" onclick="clr()" class="btn_style" name="btn_Clear"
			value="Clear"></td>
		<td></td>
	</tr>
</table>

</div>
</div>

</div>
<div id="showDiv" style="display: none;"></div>
<jsp:include page="Footer.jsp"></jsp:include></center>
</div>
</form>




</body>
</html>
