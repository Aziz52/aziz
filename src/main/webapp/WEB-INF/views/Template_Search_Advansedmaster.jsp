<jsp:include page="new_header.jsp"></jsp:include>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SwiftDAR_search</title>

<script type="text/javascript">

//<![CDATA[
           
jQuery(document).ready(function($) {
	//Getdata When Load Form using

	resizeWindow();
	
	
	$.ajax({
		type:"POST",
		url:"Template_commanDetails_servlet?Detail=CHEK_SelectTemp",
				data:"",
		success:function(msg){
			
		
			$("#selectTemplate").html(msg);
			
			
			
		}
		});
	
	jQuery('#tt1').datagrid({  
	    url:'Template_advanced_serch?Report=master', 
	    columns:[[  
           
 
	              {field:'file_name', title:'File Name',width:250},  
		  	     {field:'keywords', title:'KeyWords',width:280},  
		  	  	  {field:'view', title:'View',width:70},  
		  	    {field:'pdf',title:'PDF',width:60},
		  	  {field:'download', title:'Download',width:70}
		  	   
		  	    

	  	    ]] ,
	     showTableToggleBtn: true ,
	    title:"Search Report", //report Title
	    height:400, //Height of Grid
	    width:850,//Width of Grid
	    pageSize:50
	    
	}); 
	$("body").resize(function(e){
		resizeWindow();
	});
	
 });



function File_details(Value)
{
		
	try{
		jQuery.ajax({
			type:"POST",
			url:"TemplateSearch_Master_Servlet",
			data:"Report=getdetails&Value="+Value,
			success:function(msg){ 
					$("#divAdd").html(msg);
					$("#divAdd").dialog('open');
			}
		});
		
		 $(function () {

	            $('#divAdd').dialog({
	                autoOpen: false,
	                title:"File Details",
	                width: 700,
	                height: 'auto',
	                modal: true
	            });

	        });
		
		
		
	}
catch(e){
	alert(e);		
}	
}





function doSearch() { //Function use Search The Data Using user paramater
	
	$("a[href*=#gdata]").click();	 
	
	 var columnname = $('#hidcolumnname').val();
	  
	   var keywords="";
	
	    var myarray = [];
	    myarray = columnname.split("|");
	  
	    for(var i=0; i< myarray.length-1  ;i++)
	    	{
	    	//alert(myarray[i]);
	    	
	    		//if($('#'+ myarray[i]).val() != ''){
	    			
	    			if (i==0) {
	    				keywords = keywords +""+ $('#'+ myarray[i]).val();
					}
	    			else{
	    				keywords = keywords +" "+ $('#'+ myarray[i]).val();
	    			}
	    			
	    		//}
	    	
	    	}
	    

	  $('#data').val(keywords);
	  
	$('#tt1').datagrid('load', { 
		msg:'No Record Available For Selected criteria... ', 
		//file_name :$('#file_name').val(),
		//keywords :$('#keywords').val(),
		data :$('#data').val(),
		
		 flg:"Y"
		
	});
	
}





//]]>
	</script>

</head>

<body>
	<form id="form1" name="form1" method="post">
		<section id="content">
			<section class="main padder">
				<div class="row">
		
						<section class="panel">
					<header class="">
							<ul class="nav nav-tabs nav-justified">
								<li class="active"><a href="#search" data-toggle="tab">Search</a></li>
								<li><a href="#gdata" data-toggle="tab">Get Data</a></li>
							</ul>
						</header>
						<div class="panel-body" style="margin-left: 10%;">
							<div class="tab-content">
								<div class="tab-pane active" id="search">
									<div class="search_section">
   <div class="content" id="content" >
        <div id="UpdatePanel1">
	
                <div align="left" class="contentPanel" id="contentPanel" style="vertical-align: top; min-height: 407.33px;">
                  
                   
                    <div style="text-align: center;">
                     
                        <div style="float: left; position: relative; width: 2%;">
                        </div>
                        <div style="float: left; text-align: left; width: 70%; margin-left: 162px;" id="MainDiv">
                            <br>
                            <h3> Advanced Search </h3> <br>
                            <br>
                            <table  class="myTable" width="100%">
                                <tbody><tr>
                                    <td align="left" class="disText">
                                        Select Template
                                    </td>
                                    <td align="left" class="txtheading">
                                        <select name="selectTemplate"  id="selectTemplate"  onchange="Fin7_chek_select(this.value)" style="width:170px;">
													<option selected="selected"  value="">--Select--</option>



	</select></td>
	</tr><tr>
           <td colspan="4" align="left" class="disText" id="data" name="data">
           
           
           </td>
                                       
                                </tr>
                                                                       
                       
                   
                                     
                            </tbody></table>
                            <br>
                            <br>
                            <div style="width: 95%; overflow: auto;" id="container">
                                <center>
                                
                             <input type="button" id="btnSave"    class="btn_style btn btn-white" name="Save"  onclick="doSearch()"	value="search"/>
                                
                                </center>
                            </div>
                            <div id="ClientUpload" class="Hide">
                                
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="hid_template_id" id="hid_template_id">
                    <input type="hidden" name="hid_folder_id" id="hid_folder_id">
                    <input type="hidden" name="hid_DuplicateDoc" id="hid_DuplicateDoc">
                    <input type="hidden" name="Hid_errorDoc" id="Hid_errorDoc">
                                     <input type="hidden" name="z" id="z">
                 
                   
                   
                    <span id="lblDuplicateDoc"></span>
                </div>
            
</div>
     
    </div>

									</div>

								</div>
								<div class="tab-pane" id="gdata">
								
									<div id="pdatagrid"
										style="width: 100%; overflow: auto; padding: 3%">
											<div id="tb" style="padding:3px">
  	
										<table id="tt1" class="easyui-datagrid" width="100%"
											iconCls="icon-search" rownumbers="true" pagination="true">
											</table>
									</div>
								</div>

<%-- <%
	String data=request.getParameter("data");
	
	%>
	<input id="data" type="hidden" class="input-text"
		value=<%=data%>></input> --%>
							</div>
						</div>
	</div>
					</section>




				</div>
			</section>
		</section>

	</form>

<script type="text/javascript">

function DispalyFile(docid,filename) {
	
	

	var e = $("input[type=search]").val();
	  
	window.open('Template_view.jsp?docide='+docid+'&filename='+ filename+'&content='+ e,'winname','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=900,height=500,top=100,left=70');

    	
    }
function checkdate(Value)

{
	$(function() {$(Value).calendarsPicker({calendar: $.calendars.instance('gregorian')});});

	
	}
function Fin7_chek_select(val)
{
	 
		$.ajax({
			type:"POST",
			url:"Template_commanDetails_servlet?Detail=CHEK_Data&val="+val,
					data:"",
			success:function(msg){
				$("#data").html(msg);
				 
				
			}
			});
	
}


    
function createOutput(docid,filename)
{ 
	
	//var kvpairs = [];
	//var form = // get the form somehow
	
	$('#txtdocid_output').val(docid);
	$('#txtfilename_output').val(filename); 
	$('#txtTypeOutPut').val("pdf");
	$('#txtHTMLOutPut').val("");	
	$('#frmPdfOutPut').submit();
	
}


    
</script>
<form action="TemplatePDF.jsp" method="post" style="display: none;" id="frmPdfOutPut">
	<input type="hidden" id="txtdocid_output" name="txtdocid_output">
	<input type="hidden" id="txtfilename_output" name="txtfilename_output"> 
	<input type="hidden" id="txtTypeOutPut" name="txtTypeOutPut" >
	<input type="hidden" id="txtHTMLOutPut" name="txtHTMLOutPut">
</form>
<div id="divAdd" style="display: none;">
</div>

</body>
    

</html>
