<jsp:include page="new_header.jsp"></jsp:include>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@page import="java.net.URL"%>
<%@ page import="com.dto.UserMasterDTO"%>
<%@ page import="com.service.UserMaster_service"%>
<%@ page import="com.connection.*"%>
<html>

<title>Edit User Details</title>
<%
	UserMasterDTO dto = new UserMasterDTO();
	UserMaster_service srv = new UserMaster_service();
	if (request.getParameter("action") != null)
		if (request.getParameter("action").equals("Edit")
				&& request.getParameter("uid") != null) {
			dto = srv.GetUserDetails(request.getParameter("uid"));

		}
%>

<style type="text/css">
.panel-body {
	min-height: 500px;
	!
	important;
}
</style>

<script type="text/javascript">

//<![CDATA[
           
jQuery(document).ready(function($) {
	//Getdata When Load Form using

resizeWindow();	
	
	
	
	
	
	$("body").resize(function(e){
		resizeWindow();
	});
	
});


function doSearch() { //Function use Search The Data Using user paramater
	 $("a[href*=#gdata]").click();	 
	
	
}


function checkSave()
{
	
	var st = 0;
	var dt = new Date();
	var get_select =$("#selUserRole").val();

	
	
	if (($("#selUserStatus").val() !='D') && ($("#selUserStatus").val() !='E'))
	{
		alert('User status can be changed to Enable or Disable');
		$("#selUserStatus").focus(); 
		return false;
	}
	if ($("#txtExpiryDate").val() == '')
	{
		alert('Expiry Date cannot be blank');
		$("#txtExpiryDate").focus(); 
		return false;
	}
	
	
	if ((cdate($("#txtExpiryDate").val())<= dt))
	{
		alert('Expiry Date should be greater than today');
		return false;
	}
	
	
	document.form1.action = 'UserAuthentication_servlet?operation=EditUserSave';
	document.form1.submit();
}

function che_his()
{
	var get_select =$("#selUserRole").val();
	
	if(get_select=='User')
	{
		$("#appList").hide();
		
	}
	else
	{
		$("#appList").show();
	}
}

function GetApp() {
	<%-- var uid = '<%=dto.getUSER_ID()%>';
	
	$.get('UserAuthentication_servlet?opr=GetAppList&typ=check&userId='+ uid, function(
			responseText) {
		if (responseText != "") {
			$("#appList").html('<td><div class="form-group">  <label class="col-lg-3 control-label">Select Zone</label>  <div class="col-lg-8"> '+ responseText+ '  </div> </div></td>');
		}
	}); --%>

}

</script>

</head>

<body>
	<form id="form1" name="form1" method="post"
		action="UserAuthentication_servlet?operation=EditUserSave">
		<section id="content">
			<section class="main padder">
				<div class="row">


					<section class="panel">
						<header class="panel-heading">
							<ul class="nav nav-tabs nav-justified">
								<li class="active"><a href="#search" data-toggle="tab"></a></li>

							</ul>
						</header>
						<div class="panel-body">
							<div class="tab-content">
								<div class="tab-pane active" id="search">
									<div class="search_section">
										<div id="tb" class="tb_borders">
											<span>
												<h3>Edit User Details</h3> <br>
											</span> <input type="hidden" id="chkUserID" name="chkUserID"
												value="chkUserID"></input>
											<table class="TblMain">


												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">User ID</label>
															<div class="col-lg-8">
																<input type="text" onfocus="clear();" id="txtUserID"
																	readonly="readonly" value="<%=dto.getUSER_ID()%>"
																	name="txtUserID" class="bg-focus form-control"></input>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">User Name</label>
															<div class="col-lg-8">
																<input type="text" id="txtUserName"
																	value="<%=dto.getUSER_NAME()%>" name="txtUserName"
																	class="bg-focus form-control"></input>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">User Status</label>
															<div class="col-lg-8">
																<select id="selUserStatus" name="selUserStatus"
																	class="bg-focus form-control">
																	<option value="E">Enable</option>
																	<option value="D">Disable</option>
																	<option value="L">Locked</option>
																	<option value="I">Inactive</option>
																	<option value="X">Expired</option>
																</select>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Enter User
																Role</label>
															<div class="col-lg-8">
																<select id="selUserRole" onchange="che_his()"
																	class="bg-focus form-control" name="selUserRole">
																	
																	<option	<%if(dto.getUSER_ROLE().equalsIgnoreCase("Admin"))out.print(" selected ");%>value="Admin">Admin</option>
																	<option <%if(dto.getUSER_ROLE().equalsIgnoreCase("User"))out.print(" selected ");%>value="User">User</option>
																</select>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>


												<tr>
													<td><div class="form-group">
															<label class="col-lg-3 control-label">Enter
																Expiry Date</label>
															<div class="col-lg-8">
																<input type="text" id="txtExpiryDate"
																	value="<%=dto.getEXPIRY_DATE()%>"
																	onblur="checkdate(this,this.value)"
																	name="txtExpiryDate" class="bg-focus form-control"></input>
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div></td>
												</tr>
												<tr id="appList">
												</tr>

												<tr>
													<td colspan="3" style="padding-top: 15px;"><input
														class="btn_style btn btn-primary"
														onclick="return checkSave();" plain="true" type="button"
														value="Save" id="serch_btn">
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="button"
														id="test" onclick="clr()" class="btn_style btn btn-white"
														name="btn_Clear" value="Clear"> <input
														type="hidden" id="hidAction" name="hidAction"></input></td>
													<td></td>

												</tr>


												
											</table>

										</div>

									</div>

								</div>
								<div class="tab-pane" id="gdata">
									<div id="pdatagrid"
										style="width: 100%; overflow: auto; padding: 3%">
										<table id="tt1" class="easyui-datagrid" width="100%"
											iconCls="icon-search" rownumbers="true" pagination="true">
										</table>
									</div>
								</div>

							</div>
						</div>

					</section>




				</div>
			</section>
		</section>

	</form>

	<script type="text/javascript">
		
		GetApp();
		$("#reg_box_wrapper").hide();
		$(function() {
			$('#txtExpiryDate').calendarsPicker({
				calendar : $.calendars.instance('gregorian')
			});
		});
	</script>


</body>
</html>
