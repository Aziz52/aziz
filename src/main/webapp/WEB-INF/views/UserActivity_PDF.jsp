<%@ taglib uri="/WEB-INF/lib/tlds/pd4ml.tld" prefix="pd4ml"%>
<%@page import="com.dto.Get_Query"%>
<%@page import="java.sql.*"%>
<%@page import="com.connection.DBUtil"%>
<%@page contentType="text/html; charset=ISO8859_1"%>
<%@page import="org.zefer.pd4ml.PD4ML"%>
<%@page import="com.dto.Get_pdf_herder"%>
<%

String txtTypeOutPut = "";
txtTypeOutPut=request.getParameter("txtTypeOutPut"); 
if(txtTypeOutPut.equals("pdf"))
{

%>
<pd4ml:transform screenWidth="1500" pageOrientation="landscape"
	pageInsets="1,1,1,1,point" enableImageSplit="false" 
	fileName="User_Activity_Report.pdf" inline="false">



	<html>
	<head>
	<title>pd4ml test</title>
	<style type="text/css">
body {
	color: #000000;
	font-family: Tahoma, "Sans-Serif";
	font-size: 10pt;
}

#tblHeader td {
	color: #fff;
	vertical-align: top;
}

#tblMain tr td {
	margin: 2px;
}
</style>
	</head>
	<body>


	<% 
	if(txtTypeOutPut.equalsIgnoreCase("pdf"))
	{ 
	%>
		<%= Get_pdf_herder.getPdfHeader("User Activity Report","",session.getAttribute("bankName").toString()) %>
	<%} %>



	<table width="1480px" border="1" id="tblMain">
		<tr>
		<td width=94px;>User ID</td>
		<td width=202px;>Action By</td>
		<td width=86px;>Target User ID</td>
		<td width=202px;>Target User Name</td>
		<td width=131px;>Activity Time</td>
		<td width=82px;>Activity Type</td>
		<td width=82px;>IP_Address</td>
		<td width=158px;>Activity Description</td>

		</tr>
		<%
			PreparedStatement prepStmt = null;
				ResultSet rs = null;
				Connection objConnection = null;
				DBUtil ds = new DBUtil();
				objConnection = ds.getConnect();
				Get_Query g_query = new Get_Query();
				
				String txtUserNameOutPut = request.getParameter("txtUserNameOutPut"); 
				
				String txtUserIDoutPut = request.getParameter("txtUserIDoutPut"); 
				
//				System.out.println("1111111"+ txtUserIDoutPut);

				String txtfromdtOutPut = request.getParameter("txtfromdtOutPut"); 
				String txttodateOutPut = request.getParameter("txttodateOutPut"); 
				String txtuA_ActivityOutPut = request.getParameter("txtuA_ActivityOutPut"); 
				String txtHTMLOutPut = request.getParameter("txtHTMLOutPut"); 
				String Temp34;
				
				String strSQL = g_query.User_Activity_Report(0, 99999999, txtUserIDoutPut, txtuA_ActivityOutPut,txtfromdtOutPut,txttodateOutPut);
//				System.out.println("444444444"+strSQL);
		
				prepStmt = objConnection.prepareStatement(strSQL.split("~")[0]);
				rs = prepStmt.executeQuery();
		%>
		<%
			while (rs.next()) {
		%><%="<tr>"%>
		<%
		
			for (int j = 1; j <= rs.getMetaData().getColumnCount()-1; j++) {
			if(rs.getMetaData().getColumnName(j).equalsIgnoreCase("ACTIVITY_TYPE"))
			{
				%><%="<td>" + ds.RemoveNull(rs.getString("ACTIVITY_TYPE_DESC"))+ "</td>"%>
		<%
			
			}
			else if(rs.getMetaData().getColumnName(j).equalsIgnoreCase("ACTIVITY_TYPE_DESC")) {
				%><%=" "%>
				<%
					
					}
			else
			{
				
				%><%="<td>" + (ds.RemoveNull(rs.getString(j)).toString().trim().length()<=0?"&nbsp;":ds.RemoveNull(rs.getString(j)).toString().trim())+ "</td>"%>
				<%
			
			}
		
			}%>
		<%="</tr>"%><%		
			}
		%>
		<%
			rs.close();
				prepStmt.close();
		%>
	</table>



	</body>
	</html>
	

</pd4ml:transform>
<%}
else 
{
	response.setContentType("application/ms-excel");
	response.setHeader("Content-Disposition", "inline; filename="
			+"User_Activity_Report.xls");	
	%>

	<table width="1480px" border="1" id="tblMain">
		<tr>
		<td width=94px;>User ID</td>
		<td width=202px;>Action By</td>
		<td width=86px;>Target User ID</td>
		<td width=202px;>Target User Name</td>
		<td width=131px;>Activity Time</td>
		<td width=82px;>Activity Type</td>
		<td width=82px;>IP_Address</td>
		<td width=158px;>Activity Description</td>

		</tr>
		<%
			PreparedStatement prepStmt = null;
				ResultSet rs = null;
				Connection objConnection = null;
				DBUtil ds = new DBUtil();
				objConnection = ds.getConnect();
				Get_Query g_query = new Get_Query();
				
            String txtUserIDoutPut = request.getParameter("txtUserIDoutPut"); 
				
//				System.out.println("1111111"+ txtUserIDoutPut); 
				
				
				String txtUserNameOutPut = request.getParameter("txtUserNameOutPut"); 
				String txtfromdtOutPut = request.getParameter("txtfromdtOutPut"); 
				String txttodateOutPut = request.getParameter("txttodateOutPut"); 
				String txtuA_ActivityOutPut = request.getParameter("txtuA_ActivityOutPut"); 
				String txtHTMLOutPut = request.getParameter("txtHTMLOutPut"); 
				String Temp34;
				
				String strSQL = g_query.User_Activity_Report(0, 99999999, txtUserIDoutPut, txtuA_ActivityOutPut,txtfromdtOutPut,txttodateOutPut);
//				System.out.println(strSQL);
		
				prepStmt = objConnection.prepareStatement(strSQL.split("~")[0]);
				rs = prepStmt.executeQuery();
		%>
		<%
			while (rs.next()) {
		%><%="<tr>"%>
		<%
		
			for (int j = 1; j <= rs.getMetaData().getColumnCount()-1; j++) {
			if(rs.getMetaData().getColumnName(j).equalsIgnoreCase("ACTIVITY_TYPE"))
			{
				%><%="<td>" + ds.RemoveNull(rs.getString("ACTIVITY_TYPE_DESC"))+ "</td>"%>
		<%
			
			}
			else if(rs.getMetaData().getColumnName(j).equalsIgnoreCase("ACTIVITY_TYPE_DESC")) {
				%><%=" "%>
				<%
					
					}
			else
			{
				
				%><%="<td>" + (ds.RemoveNull(rs.getString(j)).toString().trim().length()<=0?"&nbsp;":ds.RemoveNull(rs.getString(j)).toString().trim())+ "</td>"%>
				<%
			
			}
		
			}%>
		<%="</tr>"%><%		
			}
		%>
		<%
			rs.close();
				prepStmt.close();
		%>
	</table>



	</body>
	
</html>
<% } %>

