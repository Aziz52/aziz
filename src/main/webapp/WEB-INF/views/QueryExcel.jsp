<%@ taglib uri="/WEB-INF/lib/tlds/pd4ml.tld" prefix="pd4ml"%>
<%@page import="com.dto.Get_Query"%>
<%@page import="java.sql.*"%>
<%@page import="com.connection.DBUtil"%>
<%@page contentType="text/html; charset=ISO8859_1"%>
<%@page import="org.zefer.pd4ml.PD4ML"%>
<%@page import="com.dto.Get_pdf_herder "%>

<%

String txtTypeOutPut = "";
String txtReportName = "";
String txtBranchName = "";
String txtFileName = "";
txtTypeOutPut=request.getParameter("txtTypeOutPut"); 
txtReportName=request.getParameter("txtReportName");
txtBranchName=request.getParameter("txtBranchName");
txtFileName=request.getParameter("txtFileName");

String txtHeight =request.getParameter("txtHeight");
String txtWidth = request.getParameter("txtWidth");

if(txtWidth!=null)
{
if(txtWidth.length()==0)
	txtWidth="1024";
}
else
	txtWidth="1024";

if(txtHeight!=null)
{
if(txtHeight.length()==0)
	txtHeight="landscape";
}
else
	txtHeight="landscape";

if(txtFileName!=null)
{
if(txtFileName.length()==0)
	txtFileName="Report";
}
else
	txtFileName="Report";


if(txtBranchName!=null)
{
if(txtBranchName.length()==0)
	txtBranchName="";
}
else
	txtBranchName="";


if(txtReportName!=null)
{
if(txtReportName.length()==0)
	txtReportName="Deatils";
}
else
	txtReportName="Deatils";

if(txtTypeOutPut.equalsIgnoreCase("pdf"))
{
%>
<pd4ml:transform screenWidth="<%=txtWidth %>"   pageOrientation="<%=txtHeight %>"
	pageInsets="1,1,1,1,point" enableImageSplit="false"
	fileName="output.pdf" inline="false">



	<html>
	<head>
	<title>pd4ml test</title>
	<style type="text/css">
body {
	color: #000000;
	font-family: Tahoma, "Sans-Serif";
	font-size: 10pt;
}

#tblHeader td {
	color: #fff;
	vertical-align: top;
}

#tblMain tr td {
	margin: 2px;
}

.myTable {
	font: 13px/ 1.9em Tahoma, Helvetica, sans-serif;
}

.myTable tr {
	font: 13px/ 1.9em Tahoma, Helvetica, sans-serif;
	width: auto;
}

.myTable tr td {
	font: 14px/ 1.9em Tahoma, Helvetica, sans-serif;
	padding-left: 12px;
	text-align: left;
	width: auto;
}

.myTable tr td .right {
	font: 13px/ 1.9em Tahoma, Helvetica, sans-serif;
	text-align: right;
	width: auto;
}

.myTable th {
	font: bold 13px/ 1.9em Tahoma, Helvetica, sans-serif;
	padding-left: 12px;
	text-align: left;
	width: auto;
}

.myTable .header th {
	color: white;
	font: bold 13px/ 1.8em Tahoma, Helvetica, sans-serif;
	height: auto;
	padding-left: 10px;
	text-align: left;
}

.mTable {
	color: #000000;
	font: 13px/ 1.9em Tahoma, Helvetica, sans-serif;
}

.mTable .header th {
	font: bold 13px/ 1.8em Tahoma, Helvetica, sans-serif;
	height: auto;
	text-align: center;
}

.mTable th {
	font: bold 13px/ 1.8em Tahoma, Helvetica, sans-serif;
	height: auto;
	text-align: left;
}

.mTable tr td {
	text-align: left;
	width: auto;
}


</style>
	</head>
	<body>




	<% 
	if(txtTypeOutPut.equalsIgnoreCase("pdf"))
	{ 
	%>
		<%= Get_pdf_herder.getPdfHeader(txtReportName,txtBranchName,session.getAttribute("bankName").toString()) %>
	<%} %>


<%

StringBuilder Res = new StringBuilder();
Connection objConnection = null;
PreparedStatement prepStmt = null;
String strSQL = "";
ResultSet rs = null;
DBUtil ds = new DBUtil();
objConnection = ds.getConnect();
strSQL = request.getParameter("txtQuery").toString();
prepStmt = objConnection.prepareStatement(strSQL);
rs = prepStmt.executeQuery();
int ii = 1;
int ij = 1;
response.setContentType("application/vnd.ms-excel");
response.setHeader("Content-Disposition", "inline; filename=Output.xls");
ResultSetMetaData rsmd = rs.getMetaData();
%><%= "<table border=1><thead><tr>" %>
<%
while (rsmd.getColumnCount() >= ii) {
	%><%= "<th>"+rsmd.getColumnName(ii)+"</th>" %><%
	ii++;
}
%>
<%= "</tr></thead><tbody>" %>
<%
while (rs.next()) {
	%><%= "<tr>" %><%
	for (int j = 1; j <= rs.getMetaData().getColumnCount(); j++) {		
		%><%="<td width=150px;>" + (ds.RemoveNull(rs.getString(j)).toString().trim().length()<=0?"&nbsp;":ds.RemoveNull(rs.getString(j)).toString().trim())+ "</td>"%><%	
	}
	
	
	%><%= "</tr>" %><%
//	System.out.println("Count:-"+ ij++);
}
%><%= "</tbody></table>"%><%
rs.close();
prepStmt.close();
%>

	
</pd4ml:transform>
<%}
else 
{
%>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<%

StringBuilder Res = new StringBuilder();
Connection objConnection = null;
PreparedStatement prepStmt = null;
String strSQL = "";
ResultSet rs = null;
DBUtil ds = new DBUtil();
objConnection = ds.getConnect();
strSQL = request.getParameter("txtQuery").toString();
prepStmt = objConnection.prepareStatement(strSQL);
rs = prepStmt.executeQuery();
int ii = 1;
int ij = 1;
response.setContentType("application/vnd.ms-excel");
response.setHeader("Content-Disposition", "inline; filename=Output.xls");
ResultSetMetaData rsmd = rs.getMetaData();
%><%= "<table border=1><thead><tr>" %>
<%
while (rsmd.getColumnCount() >= ii) {
	%><%= "<th>"+rsmd.getColumnName(ii)+"</th>" %><%
	ii++;
}
%>
<%= "</tr></thead><tbody>" %>
<%
while (rs.next()) {
	%><%= "<tr>" %><%
	for (int j = 1; j <= rs.getMetaData().getColumnCount(); j++) {		
		%><%="<td>"+ ds.RemoveNull(rs.getString(j))+"</td>"%><%		
	}
	%><%= "</tr>" %><%
//	System.out.println("Count:-"+ ij++);
}
%><%= "</tbody></table>"%><%
rs.close();
prepStmt.close();
}%>

</body>
</html>