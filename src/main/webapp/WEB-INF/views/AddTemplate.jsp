<%@page import="java.sql.ResultSet"%>
<%@page import="com.connection.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<jsp:include page="Header.jsp"></jsp:include>

<head>
<link rel="stylesheet" type="text/css" href="mSILVERSTYLE.CSS">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Template</title>
</head>
<body>
             <form id="form1" name="form1" method="post">
              <div class="divHeading">
            
                    <table>
                        <tr>
                            <td valign="middle" style="padding-bottom: 5px;">
                                <img src="images/template.png" height="35" width="35" />
                            </td>
                            <td style="padding-bottom: 20px;" class="disText">
                                <h5>
                                    <span  id="pgTitle">Add Template</span></h5>
                            </td>
                             <td valign="middle" style=" width:81%; text-align:right;">
                                    <a href="DMS_User_Manual_v4.0.pdf#page=48" target="_blank"><img src="images/help.png" alt="Template Creation Help" height="35" width="35" /></a>
                                </td>
                        </tr>
                    </table>
                </div>
                
                <input id="id" type="hidden" class="input-text" value="id"></input>
                <div id="metaAdd" class="Hide" >
                        <div style="width: 90%; padding: 5px;">
                            <div id="criteria" style="overflow: auto; height: 100%; width: 100%;">
                             
                
                 
<table class="myTable" width="100%">
                                    <tbody><tr>
                                        <td>
                                            Template Name
                                        </td>
                                        <td>
                                            <input name="metaid" type="text" id="metaid" class="txtdata" onblur="checkName(this);" onkeyup="fn(this.form,this)" style="width:166px;">
                                            <font size="1" color="red">*</font>
                                        </td>
                                         <td>
                                           Index By File Name
                                        </td>
                                        <td>
                                             <select name="IndexByFileName" id="IndexByFileName" onchange="ShowFileNameMatch(this.value)" onkeyup="fn(this.form,this)">
	<option selected="selected" value="No">No</option>
	<option value="Yes">Yes</option>

</select>
                                          </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Display Field Name
                                        </td>
                                        <td colspan="3">
                                            <div id="FieldDrop" style="float: left;">
                                                <select name="prname" id="prname" >
	<option selected="selected" value="--Select--">--Select--</option>
	
	
	
	<!-- <option value="70">Account No</option>
	<option value="64">Account Name</option>
	<option value="63">Account Type</option>
	<option value="73">Account From Date</option>
	<option value="61">Account to Date</option>
	<option value="68">Branch Name</option>
	<option value="66">Branch Code</option>
	<option value="48">Customer Name</option>
	<option value="56">Date</option>
	<option value="64">Description</option>
	<option value="72">Tran Id</option>
	<option value="51">Instrument No</option>
	<option value="69">Account Holder Add</option> -->
	

</select>
                                            </div>
                                            <div style="float: left;">
                                                <font size="1" color="red">*</font>
                                                <img id="imgFieldRefresh" src="images/loader.gif" style="cursor: pointer;" onclick="RefreshList('FieldDrop','prname','MetaField','imgFieldRefresh')" alt="refresh">
                                            </div>
                                            
                                        </td>
                                    </tr>
                                    <tr id="addnewfield" style="display:none;">
                                        <td>
                                            Field Name
                                        </td>
                                        <td>
                                            <input name="displayname" type="text" id="displayname" class="txtdata">
                                        </td>
                                        <td>
                                            Field ID
                                        </td>
                                        <td>
                                            <input name="fieldid" type="text" maxlength="25" id="fieldid" class="txtdata" onkeyup="fn(this.form,this)">
                                            <font size="1" color="red">*</font>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Field Type
                                        </td>
                                        <td>
                                            <select name="fieldtype" id="fieldtype" onchange="setSize(this)" onkeyup="fn(this.form,this)">
	<option value="date">Date</option>
	<option selected="selected" value="varchar">Text</option>
	<option value="numeric">Number</option>
	<option value="nvarchar(max)">Memo</option>

</select>
                                            <font size="1" color="red">*</font>
                                        </td>
                                        <td>
                                            Field Size
                                        </td>
                                        <td>
                                            <input name="fieldsize" type="text" maxlength="25" id="fieldsize" class="txtdata" onkeyup="fn(this.form,this)">
                                            <font size="1" color="red">*</font>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            DecimalPlaces
                                        </td>
                                        <td>
                                            <input name="decimalplaces" type="text" value="0" maxlength="10" id="decimalplaces" class="txtdata" onkeyup="fn(this.form,this)">
                                        </td>
                                        <td>
                                            Fixed Value
                                        </td>
                                        <td>
                                            <input name="fixedvalue" type="text" maxlength="50" id="fixedvalue" class="txtdata" onkeyup="fn(this.form,this)">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Default Value
                                        </td>
                                        <td>
                                            <input name="defaultvalue" type="text" maxlength="50" id="defaultvalue" class="txtdata" onkeyup="fn(this.form,this)">
                                        </td>
                                        <td>
                                            Mandatory Flag
                                        </td>
                                        <td>
                                            <select name="mandatoryflag" id="mandatoryflag" onkeyup="fn(this.form,this)">
	<option value="No">No</option>
	<option selected="selected" value="Yes">Yes</option>

</select>
                                            <font size="1" color="red">*</font>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Display Flag
                                        </td>
                                        <td>
                                            <select name="displayflag" id="displayflag" onkeyup="fn(this.form,this)">
	<option value="No">No</option>
	<option selected="selected" value="Yes">Yes</option>

</select>
                                            <font size="1" color="red">*</font>&nbsp;
                                        </td>
                                        <td>
                                            Display Type
                                        </td>
                                        <td>
                                            <select name="displaytype" id="displaytype" onchange="EnableLoad(this)" onkeyup="fn(this.form,this)">
	<option value="--Select--">--Select--</option>
	<option value="Combo">Drop Down</option>
	<option selected="selected" value="Text">Text</option>
	<option value="Radio">Radio</option>
	<option value="CheckBox">CheckBox</option>

</select>
                                            <font size="1" color="red">*</font>
                                            <span id="displaytypeValidator" style="color: red; display: none;">(Required)</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Select List
                                        </td>
                                        <td>
                                            <input name="selectlist" type="text" maxlength="80" id="selectlist" disabled="disabled" class="txtdata" onkeyup="fn(this.form,this)">&nbsp; OR
                                        </td>
                                        <td>
                                            From Masters
                                        </td>
                                        <td>
                                            <div id="FromMaster" style="float: left;">
                                                <select name="datasource11" id="datasource11" disabled="disabled" onkeyup="fn(this.form,this)">
	<option selected="selected" value="">--Select--</option>
	<option value="select description from tbl_ref_type where ref_code = 'RT'">Register Type</option>

</select>
                                            </div>
                                            <div id="imgdiv" style="float: left;" style="display:none;">
                                                <img id="imgFromMaster" src="images/icon-refresh.gif" style="cursor: pointer;" onclick="RefreshList('FromMaster','datasource11','MasterDrop','imgFromMaster')" alt="refresh">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="display:none;">
                                        <td>
                                            Unique Field
                                        </td>
                                        <td>
                                            <select name="uniquefield" id="uniquefield" onkeyup="fn(this.form,this)">
	<option selected="selected" value="No">No</option>
	<option value="Yes">Yes</option>

</select>
                                            <font size="1" color="red">*</font>&nbsp;
                                        </td>
                                        <td>
                                            Index Field
                                        </td>
                                        <td>
                                            <select name="indexedfield" id="indexedfield" onkeyup="fn(this.form,this)">
	<option selected="selected" value="No">No</option>
	<option value="Yes">Yes</option>

</select>
                                            <font size="1" color="red">*</font>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Predictive Search
                                        </td>
                                        <td>
                                            <select name="searchfield" id="searchfield" onkeyup="fn(this.form,this)">
	<option value="No">No</option>
	<option value="Yes">Yes</option>

</select>
                                            <font size="1" color="red">*</font>&nbsp;
                                        </td>
                                        <td>
                                            Display Index
                                        </td>
                                        <td>
                                            <input name="disp_index" type="text" maxlength="80" id="disp_index" class="txtdata" onkeyup="fn(this.form,this)">&nbsp;<font size="1" color="red">*</font>&nbsp;
                                        </td>
                                    </tr>
                                    <tr style="display:none;">
                                        <td>
                                            Conditional LoadField
                                        </td>
                                        <td>
                                            <input name="conloadfld" type="text" maxlength="80" id="conloadfld" class="txtdata" onkeyup="fn(this.form,this)">&nbsp;
                                        </td>
                                        <td>
                                            Conditional LoadSql
                                        </td>
                                        <td>
                                            <textarea name="conloadsql" rows="2" cols="20" id="conloadsql" class="txtdata" onkeyup="fn(this.form,this)"></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Display In Grid
                                        </td>
                                        <td>
                                            <select name="displayingrid" id="displayingrid" onkeyup="fn(this.form,this)">
	<option value="No">No</option>
	<option selected="selected" value="Yes">Yes</option>

</select>
                                            <font size="1" color="red">*</font>&nbsp;
                                        </td>
                                        <td>
                                            Key Field
                                        </td>
                                        <td>
                                            <select name="ddlqa" id="ddlqa" onkeyup="fn(this.form,this)" style="z-index: 100; left: 210px;
                                                top: 534px">
	<option selected="selected" value="No">No</option>
	<option value="Yes">Yes</option>

</select>
                                            &nbsp; <font color="red" size="1">*</font>
                                        </td>
                                    </tr>
                                    <tr id="tr_idxByFileName" style="display:none;">
                                        <td>
                                            Delimiter
                                        </td>
                                        <td>
                                             <input name="delimiter" type="text" maxlength="50" id="delimiter" class="txtdata" onkeyup="fn(this.form,this)">
                                        </td>
                                        <td>
                                            Field Position
                                        </td>
                                        <td>
                                             <input name="fieldPosition" type="text" maxlength="50" id="fieldPosition" class="txtdata" onkeyup="fn(this.form,this)">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                        <div style="width:97%;" class="Theading">
                                            <div id="Div1"  style="float: right; ">
                                                <img src="images/cancel.png" height="30px" width="30px"  onclick="check()" alt="Remove Selected Field">
                                            
                                           </div>
                                            <div id="btnAdd" style="float: right;">
                                                <img src="images/as.png" height="30px" width="30px"  onclick="addfield()" alt="Add Fields">
                                            </div>
                                             </div>
                                        </td>
                                    </tr>
                                </tbody></table>
                            </div>
                        </div>
                        <div class="Theading"     color="#716D6D;" font="bold 11px Verdana, Arial, Helvetica, sans-serif;" style="width: 90%; height: auto; overflow: auto; padding: 5px;">
                            <table  width="100%">
                                <tr>
                                    <td style="width: 20%; text-align: center;">
                                        Selected Fields
                                    </td>
                                    <td valign="top" style="width: 80%;">
                                        <select id="ItemList" multiple="multiple" tabindex="-1" name="ItemList" style="width: 98%;
                                            height: 150px">
                                           <%  
                                            DBUtil ds= new DBUtil();
                                            
                                            String  id="2";
                                            if(id.equalsIgnoreCase("2"))
                                            		{  String NVALSTRING="";
                                                    String NTXTSTRING;
                                                     Integer kk;
                                                   String getstr11 = " select * from tbl_metadata_detail where metaid = " +request.getParameter("ID") + "" ;
                                                  
                                                  
                                                   // Dim pvalueds As System.Data.DataSet = GetData(getstr11)
                                              		ResultSet rs = null;
                                                   rs = ds.get_ResultSet(getstr11);
                                                   Integer jj = 0;
                                                	   
                                                   while (rs.next()) {
                                                       String getFldName = "";
                                                        getFldName = ds.GetValue("select RefName from tbl_refType where id =  " + rs.getString("FieldID")+ "" );
                                                        String txtstring= getFldName + "->" ;
                                                        String valstring = "";
                                                        txtstring = txtstring + "(" + rs.getString("FIELDTYPE") + ")" ;
                                                        valstring = rs.getString("FieldID") + "|" + rs.getString("FIELDTYPE") + "|" +  rs.getString("FIELDSIZE") + "|" + rs.getString("DECIMALPLACES")  + "|" + rs.getString("FIXEDVALUE")+ "|" +rs.getString("DEFAULTVALUE") + "|" +  rs.getString("MANDATORYFLAG").toString() + "|" + rs.getString("DISPLAYFLAG").toString() + "|" + rs.getString("DISPLAYTYPE").toString() + "|" +rs.getString("SELECTLIST").toString().replace(",", "#") + "|" + rs.getString("DATASOURCE11").toString().replace(",", "#") + "|" + rs.getString("UNIQUEFIELD").toString() + "|" + rs.getString("INDEXEDFIELD").toString() + "|" +rs.getString("SEARCHFIELD").toString() + "|" +rs.getString("DISPLAYINGRID").toString() + "|" + rs.getString("ConLoadFld").toString().replace(",", "#") + "|" + rs.getString("ConLoadSql").toString().replace(",", "#") + "|" + rs.getString("QA").toString() + "|" + rs.getString("displayindex").toString() + "|" + ds.RemoveNull(rs.getString("delimiter")) + "|" + ds.RemoveNull(rs.getString("fieldposition"));


                                            %>
                                            <option value="<%=valstring.toString() %>">
                                                <%=txtstring.toString()%>
                                            </option>
                                            <%
                                                NVALSTRING = "";
                                                NTXTSTRING = "";

                                           
                                                   }
                                            %>
                                            <% }%> 
                                        </select>
                                    </td>
                                    
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="pgButt"  style="width: 90%; height: auto; overflow: auto;">
                        <table width="100%">
                            <tr>
                                <td colspan="4" align="center">
                                
                                 <input type="button" id="btnSave"   class="btn_style btn btn-white" name="Save"  onclick="savedata()"	value="Save">
                                   <input type="button" class="btn_style btn btn-white" value="Cancel" />
                               
                                </td>
                            </tr>
                        </table>
                         <input id="svalue" name= "svalue" style="width: 1px" type="hidden" />
                        <input id="stext" style="width: 1px" type="hidden" />
                        <input id="prlength" name="prlength" style="width: 20px" type="hidden" />
                    </div>
                    <br />
                    
                    </form>
                    
                    <script type="text/javascript">
                    
                    
                    
                    jQuery(document).ready(function($) {
                    	
                    	//alert("hello");
                    	
                    	$.ajax({
                			type:"POST",
                			url:"Template_commanDetails_servlet?Detail=Select_Field",
                					data:"",
                			success:function(msg){
                				
                			
                				$("#prname").html(msg);
                				
                				
                			}
                			});
                    
                    }
                    );
                    
                    
                /*     
                    function addfield()
                    {
                    	
                    	if ($('#prname').val()=="" || $('#prname').val()=="--Select--") 
                    	{
                    		alert('Please Select Field Name');
                    		
                    		return false;
                    	}


                    if ($('#fieldsize').val()=="")
                    	{
                    		alert('Please Select Field Size');
                    		
                    		return false;
                    	}
                    	
                    	vl=  $('#prname :selected').text() +"->" +  "(" +  $('#fieldtype').val()  + ")";
                    	$('#ItemList').append('<option value="'+vl+'">'+vl+'</option>');
                    	return false;
                    }
 */
                 
                    
 function fn(id, name){}
 function check(){
	
     var x = $('#ItemList').val();
     
     
      $("#ItemList option[value="+x+"]").remove();
     
 }
 

 
 function addfield()
 {
 	var name = $('#prname').text();
     //var val = $('#prname').val();
     
    
     
 	if ($('#prname').val()=="" || $('#prname').val()=="--Select--") 
 	{
 		alert('Please Select Field Name');
 		
 		return false;
 	}


 if ($('#fieldsize').val()=="")
 	{
 		alert('Please Select Field Size');
 		
 		return false;
 	}
 	
 
 
 	var name=  $('#prname :selected').text() +"->" +  "(" +  $('#fieldtype').val()  + ")";
    var  value = $('#prname').val();
     

     
     var exists = false; 
    
     $('#ItemList  option').each(function(){    	 
    	
         if (this.value.split('|')[0] == value) {
             exists = true;
         }
     });
     if (exists == true ){
         alert("This field already exists..");
 	}
     
     
     else{
    	 
       
        
         var dlist1="";
         var dlist2="";
         var cdlist1="";
         var cdlist2="";

          var svalstr = $('#prname').val() + '|' + $('#fieldtype').val() + '|' + $('#fieldsize').val() + '|' 
          +  $('#decimalplaces').val()  + '|' + $('#fixedvalue').val()  
          + '|' + $('#defaultvalue').val() + '|' + $('#mandatoryflag').val()  
          + '|' + $('#displayflag').val()  + '|'  +  $('#displaytype').val() 
          + '|' + dlist1   +  '|' +  dlist2 + '|' 
          + $('#uniquefield').val() + '|' + $('#indexedfield').val()  + '|'  +  $('#searchfield').val() + '|' + $('#displayingrid').val()  +
          '|' + cdlist1   +  '|' +  cdlist2 + '|' +   $('#ddlqa').val() + '|' +   $('#disp_index').val()  + '|' + $('#delimiter').val() + '|' + ''  ;
 
          
         alert(svalstr);
          
          
          
          $('#ItemList').append('<option value="'+svalstr+'">'+ name+' </option>');
         // alert(svalstr);
          
          var x= document.getElementById("ItemList").length;  
          $('#prlength').val(x);
          
         
     }
 	
 }
       
 function savedata()
 {
 	
	 alert($('#ItemList').val());
 		document.form1.action = 'AddTempateServlet?operation=Save&data='+$('#ItemList').val();
 	      document.form1.submit();
 		//alert($('#ItemList').val());
 	
 }  
 
 
 function editadd_user(Edit)
 {	jQuery.ajax({
 		type:"POST",
 		url:"AddTempateServlet?operation=Save&data="+ jQuery("#ItemList").val(),
 		data:"",
 		success:function(msg){	
 			if(msg=='E')
 			{
 				alert("Error Occured");
 				window.parent.location='Manage_Template.jsp';
 			}
 			else
 			{
 				window.parent.location='Manage_Template.jsp';
 				alert(Edit+"Sucessfully");
 			}		
 		}
 		});
 		
 }
 
 
 
                    
                    </script>

</body>
</html>