<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>SwiftDar</title>
  <meta name="description" content="mobile first, app, web app, responsive, admin dashboard, flat, flat ui">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/font.css">
  <link rel="stylesheet" href="css/style.css">
  <!--[if lt IE 9]>
    <script src="js/ie/respond.min.js"></script>
    <script src="js/ie/html5.js"></script>
  <![endif]-->
</head>
<body>
  <!-- header -->
  <header id="header" >
    <div id="headccer" style=" width: 95%; ">
	
	<table width="105%"  >
		<tr style="left: 0;" ><td>
		<span style='font-size:26.0pt;line-height:115%;'>&nbsp;&nbsp;&nbsp;<img src="logo.png" /><o:p></o:p></span>
		<!-- 		<td style="color: white;  "><img src="images/companyLogo.jpg" height="50px;">  --> 
				 </td>
			
			<td style="text-align: right;"> 
			
			
		&nbsp;<br>
		
		 </td>
			
		</tr>
	</table>
</div>

  </header>
  <!-- header end -->
  <section id="content">
    <div class="main padder">
      <div class="row">
          <div class="col-lg-4 col-lg-offset-4">
            <div class="text-center m-b-large">
              <h1 class="h text-white">404</h1>
            </div>
            <div class="list-group m-b-small">
              <a href="Home.jsp" class="list-group-item">
                <i class="fa fa-chevron-right"></i>
                <i class="fa fa-fw fa-home"></i> Goto homepage
              </a>
              
             
            </div>
          </div>
      </div>
    </div>
  </section>
  <!-- footer -->
  <footer id="footer">
    <div class="text-center padder clearfix">
      <p>
        <small>&copy; Copyright � 2014 Miscot Systems, All rights reserved.</small><br><br>
        <a href="#" class="btn btn-xs btn-circle btn-twitter"><i class="fa fa-twitter"></i></a>
        <a href="#" class="btn btn-xs btn-circle btn-facebook"><i class="fa fa-facebook"></i></a>
        <a href="#" class="btn btn-xs btn-circle btn-gplus"><i class="fa fa-google-plus"></i></a>
      </p>
    </div>
  </footer>
  <!-- / footer -->
	<script src="js/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="js/bootstrap.js"></script>
  <!-- app -->
  <script src="js/app.js"></script>
  <script src="js/app.plugin.js"></script>
  <script src="js/app.data.js"></script>
</body>
</html>