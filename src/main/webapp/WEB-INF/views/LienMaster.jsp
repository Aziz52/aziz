<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Lien Master</title>
<jsp:include page="Header_new.jsp"></jsp:include>
<script type="text/javascript">
jQuery(document).ready(function($) {
	
	//Getdata When Load Form using 
	jQuery('#tt1').datagrid({  
	    url:'LianMaster_Servlet?Report=Master',  
	    columns:[[  
	        {field:'ABC',title:'Account Number',width:100},  
	        {field:'JUHNOstr',title:'Hold number',width:100},
	        {field:'JUSDTE',title:'Lien Start Date',width:100},
	        {field:'JUHED',title:'Lien Expiry Date',width:100}  
	        //If Columns is amount aligh right
	        //If Columns is Date,String Data aligh Left
	    ]] ,
	     showTableToggleBtn: true ,
	    title:"Lien Details", //report Title
	    height: 350, //Height of Grid
	    width:920 //Width of Grid
	    
	}); 
	showsearch();
});
function clr() //Function Use To Blank Text
{
	$('#AccountNumber').val('');
	 
}
	function doSearch() { //Function use Search The Data Using user paramater
		
		$('#tt1').datagrid('load', {
			
			msg:'Lien Does Not Exist On The Database',
			acc_no :$('#AccountNumber').val(), //pass textbox value
			flg :'Y' 
		//	acc_no1:'0002BC0001151'
		});
		showreg(); //display Dataview Tab
	}

//]]>
</script>

<script type="text/javascript">
function conditions()
{
	if ($('#AccountNumber').val()=='')
	{
		alert('Invalid Account Number');
		 return false;
	}
	if ( $('#AccountNumber').val().length < 13)
	{
		 alert('Invalid Account Number');
		 return false;
	}
	
	
	check_customer_exist($('#AccountNumber').val(),'');
	//doSearch();
}

function createOutputHTML(typeOutPut,htmlid)
{

	$('#txtTypeOutPut').val(typeOutPut);
	$('#txtReportName').val("Lien Details");
	$('#txtHTMLOutPut').val($("#"+htmlid).html());	
	$('#frmPdfOutPut').submit();
	
}
    </script>
    
    
    

</head>
<body style="width: 90%;">

<center>
<div  class="tab_divs">

<div class="form_inner_wrap" >
<div id="reg_box" class="form_section" style="padding-bottom: 6%;"><br />
<br />
<br />


<div id="get_details">
<table id="tt1" class="easyui-datagrid" iconCls="icon-search"
	rownumbers="true" pagination="true">
</table>
</div>
<!--<table>
	<tr>
		<td ><input type="button" onclick="report('Excel')"
			name="operation" value="Excel" class="btn_style"></td>
		<td><input type="button"  class="btn_style"  onclick="report('Word')"
			name="operation" value="Word"></td>
		<td><input type="submit" name="operation" value="PDF"
			class="btn_style"></td>
		<%-- <td> <input type="submit" name="operation" value="Cancel"></td> --%>
	</tr>
</table>-->

</div>
</div>
<div id="search_box" class="search_section" style="display: none;padding-top: 12%;padding-bottom: 18%;">

<div id="tb" class="tb_borders" >

<h3>Lien Details</h3>

<table  class="myTable">
	<tr>
		<td><label class="cm-required">Account Number :&nbsp;</label></td>
		<td><input id="AccountNumber" type="text" class="input-text" /></td>
		
	</tr>
	<tr>
		<td colspan="3" style="padding-top: 15px;text-align: center;"><input
			class="btn_style" onclick="conditions()" plain="true" type="button"
			value="Search"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
			type="button" onclick="clr()" class="btn_style" name="btn_Clear"
			value="Clear"></td>
		<td></td>
	</tr>
</table>
<BR/>


</div>

<BR/>
</div>


<jsp:include page="Footer.jsp"></jsp:include>

<div id="hidDIvdd1" style="display: none;overflow: scroll;">
<div id="hidDIvdd" style="overflow: scroll;">

</div>
</div>
</div>

<script type="text/javascript">

function GetDetails(hold_no,acc_no){	

			var output="";
			jQuery("#hidDIvdd").html('');
			jQuery.ajax({
			type:"POST",
			url:'LianMaster_Servlet?Report=Details&acc_no='+acc_no+'&hold_no='+hold_no,
			data:"",
			success:function(msg){
				jQuery("#hidDIvdd").html(msg);
			}
			});
		var divTag = jQuery("#hidDIvdd");
		
		divTag.dialog({
	  		modal: true,
	  		autoOpen: false,
	  		title: "Lien Details",
	  		show: "blind",
	  		height: 400, //Height of Grid
	 	  	width:880, 
	        hide: "explode"
	  	});
		divTag.dialog('open');
		//return false;
			
	}

</script>
</center>

<form action="OutPutOnlyHtml.jsp" method="post" style="display: none;" id="frmPdfOutPut">	
	<input type="hidden" id="txtTypeOutPut" name="txtTypeOutPut">
	<input type="hidden" id="txtReportName" name="txtReportName">
	<input type="hidden" id="txtHTMLOutPut" name="txtHTMLOutPut">
</form>
</body>
</html>
