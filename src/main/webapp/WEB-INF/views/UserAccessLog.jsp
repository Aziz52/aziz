<%-- <%@page import="com.service.DateConversion"%> --%>
<jsp:include page="new_header.jsp"></jsp:include>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@page import="java.net.URL"%>
<title>User Access Log Search</title>


<style type="text/css">

.datagrid-pager{
width: 100%; !important;
}	
.datagrid-wrap{
width: 97%; !important;
}

</style>

<script type="text/javascript">
<%-- <% DateConversion ds= new DateConversion();%> --%>
$(function() {$('#fromdate').calendarsPicker({maxDate: 0 },{calendar: $.calendars.instance('gregorian')});});
$(function() {$('#fromdate').calendarsPicker({calendar: $.calendars.instance('gregorian')});});

$(function() {$('#todate').calendarsPicker({maxDate: 0 },{calendar: $.calendars.instance('gregorian')});});
$(function() {$('#todate').calendarsPicker({calendar: $.calendars.instance('gregorian')});});

jQuery(document).ready(function($) {
	getStatus();
});   
function getStatus(){
	jQuery.ajax({
		type:"POST",
		url:"Common_Servlet?operation=getStatus" ,
		data:{}, 
		success:function(msg){
				jQuery("#status").html(msg);
			}
		});
}
jQuery(document).ready(function($) {
	jQuery('#tt1').datagrid({  
	    url:'UserAccessLogServlet', 
	    columns:[[
			{field:'USER_ID',title:'User ID',width:100},
			{field:'USER_NAME',title:'User name',width:100},
	        {field:'LOGIN_DATE',title:'Login date',width:160},
	        {field:'STATUS_DES',title:'Status ',width:160} ,
	        {field:'IP_ADDRESS',title:'IP Address ',width:160}
	    ]] ,
	     showTableToggleBtn: true ,
	     queryParams:{
            	msg : 'No Record Available For Selected Citeria... ',    			
                flg:"Y"
                },
	    title:"User Access Log Search", //report Title
	    height: 350, //Height of Grid
	    width:1350, //Width of Grid
	    pageSize:10
	});
	
	resizeWindow();
	$("body").resize(function(e){
		resizeWindow();
	});
	
});
function doSearch() { 
	
	var startDate =(document.getElementById("fromdate").value);
    $("a[href*=#gdata]").click();	 
	$('#tt1').datagrid('load', {
		userID :$('#userID').val(),
		rrn :$('#rrn').val(),
		fromdate :$('#fromdate').val(),
		todate :$('#todate').val(),
		status :$('#status').val(),
		flg:'Y',
	});
}
function generateOutput(typeOutPut,name){
	
	
	var refNo=$('#refNo').val();
	$('#ref_No').val(refNo);
	
	var adh_No=$('#branchcode').val();
	$('#adh_No').val(adh_No);
	
	var From_date=$('#fromdate').val();
	$('#From_date').val(From_date);
	
	$('#txtTypeOutPut').val(typeOutPut);
    $('#txtReportName').val("NPCI Log");
    $('#txtHTMLOutPut').val("");
    $('#frmPdfOutPut').submit();
}




function resizeWindow()
{
	var pg = window.location.pathname;
	
	//jAlert(window.location.pathname);
    var windowHeight = getWindowHeight();
    var windowWidth = getWindowWidth();
    $("#body").css("height",(10/100)*windowHeight + "px");
    //$("#divwrapper").css("height",(90/100)*windowHeight + "px");
   
   
  
}

function getWindowHeight()
{
    var windowHeight=0;
    if (typeof(window.innerHeight)=='number')
    {
      windowHeight = window.innerHeight;
    }
    else 
    {
      if (document.documentElement && document.documentElement.clientHeight)
      {
          windowHeight = document.documentElement.clientHeight;
      }
      else
      {
         if (document.body && document.body.clientHeight)
        {
          windowHeight = document.body.clientHeight;
        }
      }
    }
    return windowHeight;
}

function getWindowWidth()
{
var windowWidth=0;
if (typeof(window.innerWidth)=='number')
{
windowWidth = window.innerWidth;
}
else {
if (document.documentElement && document.documentElement.clientWidth)
{
windowWidth = document.documentElement.clientWidth;
}
else
{
if (document.body && document.body.clientWidth)
{
windowWidth = document.body.clientWidth;
}
}
}
return windowWidth;
}  

</script>

</head>

<body>
	<form action="ShowReport" method="post" class="body"   id="frmPdfOutPut"  style="padding-left: 8%;">
		<section  style="padding-left: 5%;padding-rigth: 17%;" >
 
           
            <div class="box-body" style="display: block;">
             <div class=" box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <div class="form-horizontal" style="padding-right:5%">

					<section class="panel">
						<header class="panel-heading">
							<ul class="nav nav-tabs nav-justified">
								<li class="active"><a href="#search" data-toggle="tab">Search</a></li>
								<li><a href="#gdata" data-toggle="tab">Get Data</a></li>
							</ul>
						</header>
						<div >
								<div class="tab-content" style="height:500px;">
								<div class="tab-pane active" id="search">
									<div class="search_section">
											<div id="tb" class="tb_borders"  style="height:300px;padding-top:2%;">
																		<div class="box-header with-border tab-pane">
             <span><h3>User Access Log Search</h3></span>
               
            </div>
											  <input type="hidden" id="chkUserID" name="chkUserID"
												value="chkUserID"></input>
				<table class="TblMain " style="width: 80%;height:250px;padding-top:2%;padding-left:10%">
				<tr> 
                 	<td colspan="1" align="right" >
												 <span >User ID : </span> 
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												</td>
												<td style="paddign-left:5%">
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="text" name="userID" id="userID" class="bg-focus form-control"  >
												    </td>
												    </tr>
				<tr> 
                 	<td colspan="1" align="right" >
												 <span >Enter Date : </span> 
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												</td>
												<td colspan="2"  style="paddign-left:5%">
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<input type="text" name="fromdate" id="fromdate" class="bg-focus form-control"  onblur="checkdate(this,this.value)">
												   To : 
												   </td>
												   <td>
												     <input type="text" name="todate" id="todate" class="bg-focus form-control"  onblur="checkdate(this,this.value)">
												    </td>
												    </tr> 	
						<tr> 
                 	<td colspan="1" align="right" >
												 <span > Status : </span> 
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												</td>
												<td style="paddign-left:5%">
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<select id="status"   name="status"  class="bg-focus form-control">
																</select>
													<!-- <input type="text" name="status" id="status" class="bg-focus form-control"  onblur="validateAdnNo(this.value)"> -->
												    </td>
												    </tr> 	
												    
												    	<tr>
													<td colspan="6" style="padding-top: 15px;" align="center">
													 <input class="btn_style btn btn-primary" onclick="doSearch()"
														plain="true" type="button" value="Search" id="serch_btn">
														&nbsp;&nbsp; 
														<input type="reset"
														id="test"  onclick="clr()"
														class="btn_style btn btn-primary" name="btn_Clear"
														value="Clear">
													</td>
											</tr>
											<tr>
											
											<td colspan="3" class="char_srch_style"
														style="text-align: center;">
														Use * for wild card search Example : <B>BOM*</B>	
											</td>
											</tr>		
				</table>
                 	
           
										
											 
<div class="center-block"></div>
										</div>

									</div>

								</div>
								<div class="tab-pane" id="gdata">
									<div id="pdatagrid"
										style="width: 100%; overflow: auto; padding: 3%">
										<table id="tt1" class="easyui-datagrid" width="100%"
											iconCls="icon-search" rownumbers="true" pagination="true">
										</table>
										
									<center>
										<input class="btn btn-info" onclick="generateOutput('excel','OutPutInExcel')"
														plain="true" type="submit" value="Excel" id="serch_btn" />
											</center>	
												
									</div>
								</div>

							</div>
						</div>

					</section>
</div>
</div>
</div>



				
			</section>
		 
<div id="showDiv" style="display:none;">
<input type="hidden" id="branch_code" name="branch_code" value=" "></input>
<input type="hidden" id="check1" name="check1">
<input type="hidden" id="adh_No" name="adh_No">
<input type="hidden" id="ref_No" name="ref_No">
<input type="hidden" id="From_date" name="From_date">

</div>
</form>
</body>
<%-- <jsp:include page="Footer.jsp"></jsp:include> --%>
 <jsp:include page="Footer.jsp"></jsp:include> 