<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<%-- <%@ page import="com.connection.DBUtil" %> --%>
<head id="ctl00_Head1">
<!-- <link rel="shortcut icon" type="image/x-icon"	href="images/Miscot_grey.png"> -->
<meta http-equiv="X-UA-Compatible" content="IE=10">
<title>Miscot - SIDBI</title>
 <link type="text/css" href="<c:url value='/assets/css/bootstrap.css' />" rel="stylesheet" />
 <link type="text/css" href="<c:url value='/assets/css/style.css' />" rel="stylesheet" />
 <script src="<c:url value='/assets/Include/jquery-1.4.4.min.js' />" type="text/javascript"></script>
</head>
<body   style="font-family: 'Open Sans',"HelveticaNeue", "Helvetica Neue", Helvetica, Arial,sans-serif;">


	<script type="text/javascript">
	
	$( document ).ready(function() {
		var err="${err}";
		if(err!=""&&err!=null)
			{
			$("#msgUser").text(err);
			}
	});

	
var xmlhttp

function getremotedata(url)
{
var dd =new Date();
 
url=url+'&time1='+dd.getTime();

  xmlhttp=null
  // code for Mozilla, etc.
  if (window.XMLHttpRequest)
  {
  xmlhttp=new XMLHttpRequest()
  }
  // code for IE
  else if (window.ActiveXObject)
  {
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP")
  }
  if (xmlhttp!=null)
  {

  xmlhttp.open("GET",url,false)
  xmlhttp.send(null)
  xmldata=xmlhttp.responseText
  return xmldata

  }
  else
  {
  alert("Your browser does not support XMLHTTP.")
  }
  }
  
  
  function loadXMLDoc(dname)
  {
    if (window.XMLHttpRequest)
    {
      xhttp=new window.XMLHttpRequest();
    }
    else
    {
       xhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
      xhttp.open("GET",dname,false);
      xhttp.send("");
      return xhttp.responseText
  }


  function resizeWindow()
  {

      var windowHeight = getWindowHeight();
      var windowWidth = getWindowWidth();
      $("#divtitle").css("height",(6/100)*windowHeight + "px");
      //$("#divwrapper").css("height",(90/100)*windowHeight + "px");
      $("#divbody").css("height",(84/100)*windowHeight + "px");
      $("#divQuickLinks").css("height",(83/100)*windowHeight + "px");
      $("#divfooter").css("height",(6/100)*windowHeight + "px");
     
     $("#content").css("height",(84/100)*windowHeight + "px");
     $("#contentPanel").css("height",(83/100)*windowHeight + "px");
      
  }
  
  function getWindowHeight()
  {
      var windowHeight=0;
      if (typeof(window.innerHeight)=='number')
      {
        windowHeight = window.innerHeight;
      }
      else 
      {
        if (document.documentElement && document.documentElement.clientHeight)
        {
            windowHeight = document.documentElement.clientHeight;
        }
        else
        {
           if (document.body && document.body.clientHeight)
          {
            windowHeight = document.body.clientHeight;
          }
        }
      }
      return windowHeight;
  }

  function getWindowWidth()
  {
  var windowWidth=0;
  if (typeof(window.innerWidth)=='number')
  {
  windowWidth = window.innerWidth;
  }
  else {
  if (document.documentElement && document.documentElement.clientWidth)
  {
  windowWidth = document.documentElement.clientWidth;
  }
  else
  {
  if (document.body && document.body.clientWidth)
  {
  windowWidth = document.body.clientWidth;
  }
  }
  }
  return windowWidth;
  }

  function isSplChar(field)
  {
  var iChars = "`~!@#$%^&*()+=-_:[]\;,./{}|\":<>?'";
  var count=field.value.length;
  for (var i = 0; i < count; i++) 
        { 
            if (iChars.indexOf(field.value.charAt(i)) != -1) 
            { 
            alert("Containts special characters. \n These are not allowed.\n Please remove them and try again.");
            field.value = ""
            field.focus()
            return false;
            }
 
     	 }
     	 return true ;
    }      
    
     function loadDoc(url)
    {
        if (window.XMLHttpRequest)
        {
          xmlHttp=new window.XMLHttpRequest();
        }
        else
        {
          xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
//        var xmlHttp=new ActiveXObject("Microsoft.XMLHTTP")
        xmlHttp.open("GET", url, false)
        xmlHttp.send()
        xmlDoc=xmlHttp.responseText
        return xmlDoc
    }
      
      
   function fn(form1,field)
        {

            if (event.keyCode==13) {
                if (field.type!='button'){
                    event.keyCode=9;
                    } 
                 return event.keyCode; 
            }

        }
</script>

	<style type="text/css">
body {
	font-family: 'Open Sans',"HelveticaNeue", "Helvetica Neue", Helvetica, Arial,sans-serif;
	background-color: transparent;
    color: #656565;
    font-size: 13px;
    overflow-x: hidden;
	background-color: transparent !important;
	-webkit-font-smoothing: antialiased;
	line-height: 1.53846154;
}
/************************************ Login Page CSS Start ********************************************/
#loginDiv {
	width: 30%;
	height: 50%;
	margin-top: 50px;
	margin-bottom: 100px;
	background-color: #ffffff;
	box-shadow: 0 5px 5px rgba(0, 0, 0, 0.05), 0 5px 5px rgba(0, 0, 0, 0.05);
}


.panel.panel-default {
	border-color: #eaeef1;
}

.wrapper {
	padding: 15px;
}

.text-center {
	text-align: center;
}

.btn {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 14px;
	font-weight: normal;
	line-height: 1.42857143;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

#divtitle {
	width: 100%;
	/* background-color: #ffffff; */
}

html {
	background-color: #f2f4f8;
	overflow-x: hidden;
}

html {
	-webkit-tap-highlight-color: rgba(0, 0, 0, 0);
}

.bg-white {
	background-color: #fff;
	color: #788288;
}

.i {
	display: inline-block;
	font-family: 'icon';
	font-style: normal;
	font-weight: normal;
	line-height: 1;
	vertical-align: -5%;
	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;
}
.panel.panel-default > .panel-heading, .panel.panel-default > .panel-footer {
border-color: #eaeef1;
}
.panel-default .panel-heading {
background-color: #f9fafc;
}
.panel-default > .panel-heading {
color: #333;
background-color: #f5f5f5;
border-color: #ddd;
}
.panel-heading {
border-radius: 2px 2px 0 0;

background-color: #f9fafc;
}
.panel-heading {
padding: 10px 15px;
border-bottom: 1px solid transparent;
border-top-left-radius: 3px;
border-top-right-radius: 3px;
}
.form-control {
display: block;
width: 100%;
padding: 6px 12px;
font-size: 16px;
line-height: 1.42857143;
color: #555;
background-color: #fff;
background-image: none;
border: 1px solid #ccc;
border-radius: 1px;
-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
-webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
.panel table tr td div {
text-align: left;
padding: 5px;
}
.text-black {
text-align: left;
color: #000;
color: rgba(0,0,0,0.8);
}
.panel {
margin-bottom: 20px;
background-color: #fff;
border: 1px solid transparent;
border-radius: 4px;
-webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
}
#divfooter {
margin: 0px;
width: 100%;
background-color: #ffffff !important;
border-top: 1px solid #ccc;
}


#divfooter {
    margin: 0px;
    width: 100%;
    background-color: #ffffff !important;
    border-top: 1px solid #ccc;
}

    #divfooter table {
        width: 100%;
        background-color: transparent;
    }

        #divfooter table td {
            width: 33%;
            text-align: center;
            background-color: transparent;
        }

        #divfooter table tr td:first-child {
            padding-left: 10px;
            text-align: left;
        }

        #divfooter table tr td:last-child {
            padding-right: 10px;
            text-align: right;
        }

            #divfooter table tr td:last-child img {
                height: 40px;
                width: 160px;
            }

        #divfooter table tr td:first-child + td + td /*last child doesnt work in ie8 so this is the hack if fixed columns are there*/ {
            padding-right: 10px;
            text-align: right;
        }

            #divfooter table tr td:first-child + td + td img {
                height: 40px;
                width: 160px;
            }
            .text-lg {
font-size: 16px;
}
.text-primary {
color: #177bbb;
}
#footer {
    background-color: #F3F3F3;
    padding-top: 10px;
    padding-bottom: 0px;
    position:fixed;
    bottom:0;
    width:100%;
}
</style>
  
	<form name="loginform" method="post" style="overflow: hidden;" action="UserAuthenticationController?operation=authenticate">
		<%-- <%
session.setAttribute("User_IP",request.getRemoteAddr());
if (request.getParameter("logout") != null && session.getAttribute("user_id")!= null)
{ 
	DBUtil ds = new DBUtil();
	//java.util.Date date = new java.util.Date();
	//java.sql.Timestamp timestamp = new java.sql.Timestamp(date.getTime());
	ds.Qexecute("update tbl_user_master set isActive = 'N' where user_id = '" + session.getAttribute("user_id").toString() +"'");
	//ds.Qexecute("insert into tbl_audit_login (user_id,login_date,login_status,LOGIN_IP) values('" + session.getAttribute("user_id").toString() +"',sysdate,'O','"+request.getRemoteAddr()+"')");
	
	session.invalidate();
}
%>
	 --%>	<!-- <header id="header" class="navbar" style="width: 100%;">
    <ul class="nav navbar-nav navbar-avatar pull-right">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <span class="hidden-xs-only"> </span>
          <span class="thumb-small avatar inline"><img src="images/user.jpg"   class="img-circle"></span>
          <b class="caret hidden-xs-only"></b>
        </a>
        <ul class="dropdown-menu"> 
          <li><a href="Error_page.jsp">Help</a></li>
        
        </ul>
      </li>
    </ul>
     <a class="navbar-brand" href="#"><img src="images/Miscot_grey.png" height="40" width="140" style="padding-top: 1%;">
    <button type="button" class="btn btn-link pull-left nav-toggle visible-xs" data-toggle="class:slide-nav slide-nav-left" data-target="body">
      <i class="fa fa-bars fa-lg text-default"></i>
    </button>
    </header> -->
    
    <header id="header" class="navbar" style="width: 100%;height: 70px;padding-top: 0px;">
		<!-- <img src="./images/SIDBIProd.png" width="200" height="60">
 -->

		<div class="nav navbar-nav navbar-avatar pull-right">
			<span class="nav navbar-nav navbar-avatar "> </br> <img
				src="<c:url value='/assets/images/logo.png' />" height="40px" style="padding-right: 10px;" />
			</span> 
		</div>
		</header>
    
    
		<div id="divbody" >
			<div id="dmenu" style="width: 100%; display: none;">

				<div class="navdd"></div>
			</div>
			<div id="CentrePart" style="width: 100%;">
				<div class="content" id="content" >
					<div class="contentPanel" id="contentPanel"
						 >
						<div id="ctl00_ContentPlaceHolder1_UpdatePanel1">

							<center>
								<div id="loginDiv" class="animated fadeInUp">
									<header class="wrapper text-center"
										style="padding-bottom: 20px;">
										
										<div>
											<span class="text-primary dker text-lg"> SIDBI</span>
										</div>

									</header>
									<section class="panel panel-default">
									<div class="panel-heading">User Authentication</div>
										
										<div class="panel-body text-center">
											<center>
												<table style="width: 70%;" align="center">

													<tbody>
														<tr>
															<td >
																<div >User ID</div> 
																<input type="text" name="userName"  id ="userName"  class="form-control" value="">	

																<div>
																	<span
																		id="ctl00_ContentPlaceHolder1_RequiredFieldValidator3"
																		style="color: red; display: none;">(Required)</span>
																</div>
															</td>
														</tr>


														<tr>
															<td>
																<div >Password</div> 
																
															
																 <input type="password"  class="form-control"  name="password" value="">
																<div>
																	<span
																		id="ctl00_ContentPlaceHolder1_RequiredFieldValidator4"
																		style="color: red; display: none;">(Required)</span>
																</div>
															</td>
														</tr>


														<tr>
															<td>

																<div style="float: left">
																	<label></label>
																</div> &nbsp;
																<div style="text-align: center;">
																   <input class="btn_style btn-primary btn-sm btn-defalut center-block text-uc m-t-n-xs" type="submit" name="Submit"  value="Login">
  																</div>
															</td>
														</tr>

													</tbody>
												</table>
											</center>


										</div>
										<div id="ctl00_ContentPlaceHolder1_Errlbl"
											class="ui-state-error ui-corner-all Hide"
											style="margin: 20px; padding: 10px;"></div>
											
										    <span id="msgUser" style="color:red;" value=""> </span>	
											
									</section>
							</center>
						</div>

					</div>
				</div>
				
			</div>
			
		</div>
		<div style="width: 100%; height: 37.38px;padding-bottom: 10px;position:fixed;
    bottom:0;" id="divfooter">
                <table>
                    <tbody><tr>
                        <td align="left"
				style="font-family: Verdana, Geneva, sans-serif; font-size: 12px;">
                            <span>Copyright � 2018 Miscot Systems Pvt. Ltd.</span>

                        </td>
                       
                        <td>
                            <img id="myLogo" src="<c:url value='/assets/images/miscot_transperant.png' />">
                        </td>
                    </tr>
                   
                </tbody></table>
               
            </div>
     </form>        
</body>
</html>