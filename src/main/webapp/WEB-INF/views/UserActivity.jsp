<jsp:include page="Header_new.jsp"></jsp:include>
<html>
<head>
<script type="text/javascript">
//<![CDATA[
jQuery(document).ready(function($) {
	//Getdata When Load Form using 
	
	jQuery('#tt1').datagrid({  
	    url:'UserAuthentication_servlet?operation=UserActivityReport',  

	    
	    columns:[[  
	        {field:'USER_ID',title:'User ID',width:94},  
	        {field:'ACTEDBY',title:'Action By',width:152},
	        {field:'USER_ID_ACTED_ON',title:'Target User ID',width:86},  
	        {field:'ACTEDON',title:'Target User Name',width:155},
	        {field:'STATUS_DATE',title:'Activity Time',width:131},
	        {field:'ACTIVITY_TYPE_DESC',title:'Activity Type',width:82},
	        {field:'IP_ADDRESS',title:'IP_Address',width:82},
	        {field:'ACTIVITY_DESC',title:'Activity Description',width:210}
	         
	    ]] ,
	     showTableToggleBtn: true ,
	    title:"User Activity Report", //report Title
	    height: 350, //Height of Grid
	    width:880 //Width of Grid
	    
	});
	showsearch();  
	
});



function doSearch() { //Function use Search The Data Using user paramater

	if ($('#fromdt').val() != '' && $('#todate').val()!='')
	{			
		if (!(cdate($('#fromdt').val())<= cdate($('#todate').val())))
		{
			alert('Date range is not valid');
			return false;
		}
	 }


	if ($('#UserID').val()=='' && $('#UserName').val() == '' && $('#fromdt').val()=='' && $('#todate').val()==''  )
	{
		alert('Please Enter Atleast Single Criteria');
		return false;
	}
	var msgr=" No Record Found ";
if($('#UserID').val()!='')
{
	msgr="No Activity Exists For This User";
}
	$('#tt1').datagrid('load', {
		msg:msgr,
		UserID :$('#UserID').val(),
		flg :'Y',
		UserName :$('#UserName').val(),
		fromdt : $('#fromdt').val(),
		todate : $('#todate').val(),
		uA_Activity : $('#uA_Activity').val()		
		
	//	acc_no1:'0002BC0001151'
	});
	
	showreg();
}


function createOutput(typeOutPut)
{

	//alert($('#txtUserIDoutPut').val());
	$('#txtUserNameOutPut').val($('#UserName').val());
	$('#txtfromdtOutPut').val($('#fromdt').val());
	$('#txttodateOutPut').val($('#todate').val());
	$('#txtuA_ActivityOutPut').val($('#uA_Activity').val());
	$('#txtUserIDoutPut').val($('#UserID').val());
	$('#txtTypeOutPut').val(typeOutPut);
	$('#txtReportName').val("User Activity Report");	
	$('#txtHTMLOutPut').val("");	
	$('#frmPdfOutPut').submit();
	
}
function clr() //Function Use To Blank Text
{
	$('#fromdt').val(''); 
	$('#todate').val(''); 
	$('#UserID').val(''); 
	$('#UserName').val(''); 
	
	
}


</script>

</head>

<body style="width: 90%;">
<form id="form1" name="form1" method="post" >
<div   class="tab_divs">

<center>
<div class="form_inner_wrap">
<div id="reg_box" class="form_section"  style="padding-bottom: 8%;"><br />
<br />
<br />
<br />
<div id="get_details">
<center>
<table id="tt1" style="diplay: none !important;" class="easyui-datagrid"
	iconCls="icon-search" rownumbers="true" width="90%" pagination="true">
</table>
<table>
	<tr>
		<td><input type="button"  class="btn_style"  onclick="createOutput('excel')"
			name="operation" value="Excel"></td>
		<!--<td><input type="button"  class="btn_style"  onclick="report('Word')"
			name="operation" value="Word"></td>-->
		<td><input type="button"  class="btn_style" onclick="createOutput('pdf')"  name="operation" value="PDF"></td>
		<%-- <td> <input type="submit" name="operation" value="Cancel"></td> --%>
	</tr>
	
</table>
</center>
</div>
<br />

</div>

<div id="search_box" class="search_section" style="display: none;padding-top: 11%;padding-bottom: 14%;">

<div id="tb" class="tb_borders"><span>
<h3>User Activity Report</h3>

</span>

<input type="hidden" id="chkUserID" name="chkUserID" value="chkUserID"></input>
<table class="myTable">
	<tr>
		<td><label class="cm-required">User ID :&nbsp;</label></td>
		<td><input id="UserID" type="text" class="input-text" /></td>
		<td>*</td>
	</tr>
	
	<tr>
	
	
	<td>User Activity</td>
	<td style="text-align: left;">
		<select name="uA_Activity" id="uA_Activity" >
			<option value="all">All</option>
			<option value="R">Reset</option>
			<option value="V">Verified</option>
			<option value="M">Modified</option>
			<option value="C">Created</option>
			</select></td>
	</tr>
	<tr>
		<td><label class="cm-required">From Date:&nbsp;</label></td>
		<td><input class="input-text" type="text" id="fromdt" value="" onblur="checkdate(this,this.value)" name="fromdt"></input></td>
		<td>*</td>
	</tr>
	<tr>
		<td><label class="cm-required">To Date:&nbsp;</label></td>
		<td><input class="input-text" type="text" id="todate" value="" onblur="checkdate(this,this.value)" name="todate"></input></td>
		<td>*</td>
	</tr>
	
	<tr>
		<td  colspan="3" style="padding-top: 15px;text-align: center;">
		
		<input class="btn_style" onclick="doSearch()"  plain="true" type="button" value="Search">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" onclick="clr()" class="btn_style" name="btn_Clear" value="Clear"></td>
		<td></td>
	</tr>
</table>

</div>
</div>


<div id="showDiv" style="display:none;">
</div>
<jsp:include page="Footer.jsp"></jsp:include>
</center>
</div>
</div>
</form>

<form action="UserActivity_PDF.jsp" method="post" style="display: none;" id="frmPdfOutPut">
	<input type="hidden" id="txtUserNameOutPut" name="txtUserNameOutPut">
	<input type="hidden" id="txtUserIDoutPut" name="txtUserIDoutPut">
	<input type="hidden" id="txtfromdtOutPut" name="txtfromdtOutPut">
	<input type="hidden" id="txttodateOutPut" name="txttodateOutPut">
	<input type="hidden" id="txtuA_ActivityOutPut" name="txtuA_ActivityOutPut">
	<input type="hidden" id="txtTypeOutPut" name="txtTypeOutPut">
	<input type="hidden" id="txtHTMLOutPut" name="txtHTMLOutPut">
	<input type="hidden" id="txtReportName" name="txtReportName">	
</form>


<script>
$(function() {$('#fromdt').calendarsPicker({calendar: $.calendars.instance('gregorian')});});
$(function() {$('#todate').calendarsPicker({calendar: $.calendars.instance('gregorian')});});
</SCRIPT>

</body>
	</html>
	