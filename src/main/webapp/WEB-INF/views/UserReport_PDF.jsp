<%@ taglib uri="/WEB-INF/lib/tlds/pd4ml.tld" prefix="pd4ml"%>
<%@page import="com.dto.Get_Query"%>
<%@page import="java.sql.*"%>
<%@page import="com.connection.DBUtil"%>
<%@page contentType="text/html; charset=ISO8859_1"%>
<%@page import="org.zefer.pd4ml.PD4ML"%>
<%@page import="com.dto.Get_pdf_herder"%>
<%

String txtTypeOutPut = "";
txtTypeOutPut=request.getParameter("txtTypeOutPut"); 
String txtHistypeOutPut = request.getParameter("txtHistypeOutPut"); 
if(txtTypeOutPut.equals("pdf"))
{

%>
<pd4ml:transform screenWidth="1500" pageOrientation="landscape"
	pageInsets="1,1,1,1,point" enableImageSplit="false" 
	fileName="User_Maintenance_Report.pdf" inline="false">



	<html>
	<head>
	<title>pd4ml test</title>
	<style type="text/css">
body {
	color: #000000;
	font-family: Tahoma, "Sans-Serif";
	font-size: 10pt;
}

#tblHeader td {
	color: #fff;
	vertical-align: top;
}

#tblMain tr td {
	margin: 2px;
}
</style>
	</head>
	<body>


	<% 
	if(txtTypeOutPut.equalsIgnoreCase("pdf"))
	{ 
	%>
		<%= Get_pdf_herder.getPdfHeader("User Maintenance Report","",session.getAttribute("bankName").toString()) %>
	<%} %>

	<table width="1480px" border="1" id="tblMain">
		<tr>
		
		<td width=80px;>User ID</td>
		<td width=150px;>User Name</td>
		<%if (txtHistypeOutPut.equalsIgnoreCase("N"))
				{%>
		<td width=70px;>User Status</td>
		<td width=60px;>User Role</td>
		<td width=120px;>Created Time</td>
		<td width=80px;>Created User</td>
		<td width=85px;>Modified User</td>
		<td width=125px;>Modified Time</td>
		<td width=80px;>Verified User</td>
		<td width=120px;>Verified Date</td>
		<td width=120px;>Expiry Date</td>
		<%}else{%>
		<td width=100px;>Last Login IP</td>		
		<td width=120px;>Login Date</td>
		<td width=105px;>Status Description</td>
		<%}%>

		</tr>
		<%
			PreparedStatement prepStmt = null;
				ResultSet rs = null;
				Connection objConnection = null;
				DBUtil ds = new DBUtil();
				objConnection = ds.getConnect();
				Get_Query g_query = new Get_Query();
				String txtFromdtOutPut = request.getParameter("txtFromdtOutPut"); 
				String txtUserIDOutPut = request.getParameter("txtUserIDOutPut"); 
				String txtUserNameOutPut = request.getParameter("txtUserNameOutPut"); 
				String txtTodateOutPut = request.getParameter("txtTodateOutPut"); 
				
				String txtReptypeOutPut = request.getParameter("txtReptypeOutPut"); 
				
				String strSQL = g_query.User_Maintenance_Report_pdf(0, 99999999, txtUserIDOutPut, txtUserNameOutPut,txtReptypeOutPut,txtFromdtOutPut,txtTodateOutPut,txtHistypeOutPut);
//				System.out.println(strSQL);
		
				prepStmt = objConnection.prepareStatement(strSQL.split("~")[0]);
				rs = prepStmt.executeQuery();
		%>
		<%
			while (rs.next()) {
		%><%="<tr>"%>
		<%
		
			for (int j = 1; j <= rs.getMetaData().getColumnCount()-1; j++) {
			
				%><%="<td>" + (ds.RemoveNull(rs.getString(j)).toString().trim().length()<=0?"&nbsp;":ds.RemoveNull(rs.getString(j)).toString().trim())+ "</td>"%>
				<%
			
			
		
			}%>
		<%="</tr>"%><%		
			}
		%>
		<%
			rs.close();
				prepStmt.close();
		%>
	</table>



	</body>
	</html>
	

</pd4ml:transform>
<%}
else 
{
	response.setContentType("application/ms-excel");
	response.setHeader("Content-Disposition", "inline; filename="
			+"User_Maintenance_Report.xls");	
	%>

	<table width="1480px" border="1" id="tblMain">
		<tr>
		
		<td width=80px;>User ID</td>
		<td width=150px;>User Name</td>
		<%if (txtHistypeOutPut.equalsIgnoreCase("N"))
				{%>
		<td width=70px;>User Status</td>
		<td width=60px;>User Role</td>
		<td width=120px;>Created Time</td>
		<td width=80px;>Created User</td>
		<td width=85px;>Modified User</td>
		<td width=125px;>Modified Time</td>
		<td width=80px;>Verified User</td>
		<td width=120px;>Verified Date</td>
		<td width=120px;>Expiry Date</td>
		<%}else{%>
		<td width=100px;>Last Login IP</td>		
		<td width=120px;>Login Date</td>
		<td width=105px;>Status Description</td>
		<%}%>

		</tr>
		<%
			PreparedStatement prepStmt = null;
				ResultSet rs = null;
				Connection objConnection = null;
				DBUtil ds = new DBUtil();
				objConnection = ds.getConnect();
				Get_Query g_query = new Get_Query();
				String txtFromdtOutPut = request.getParameter("txtFromdtOutPut"); 
				String txtUserIDOutPut = request.getParameter("txtUserIDOutPut"); 
				String txtUserNameOutPut = request.getParameter("txtUserNameOutPut"); 
				String txtTodateOutPut = request.getParameter("txtTodateOutPut"); 
				
				String txtReptypeOutPut = request.getParameter("txtReptypeOutPut"); 
				
				String strSQL = g_query.User_Maintenance_Report_pdf(0, 99999999, txtUserIDOutPut, txtUserNameOutPut,txtReptypeOutPut,txtFromdtOutPut,txtTodateOutPut,txtHistypeOutPut);
//				System.out.println(strSQL);
		
				prepStmt = objConnection.prepareStatement(strSQL.split("~")[0]);
				rs = prepStmt.executeQuery();
		%>
		<%
			while (rs.next()) {
		%><%="<tr>"%>
		<%
		
			for (int j = 1; j <= rs.getMetaData().getColumnCount()-1; j++) {
			
				%><%="<td>" + (ds.RemoveNull(rs.getString(j)).toString().trim().length()<=0?"&nbsp;":ds.RemoveNull(rs.getString(j)).toString().trim())+ "</td>"%>
				<%
			
			
		
			}%>
		<%="</tr>"%><%		
			}
		%>
		<%
			rs.close();
				prepStmt.close();
		%>
	</table>



	</body>
	
</html>
<% } %>

