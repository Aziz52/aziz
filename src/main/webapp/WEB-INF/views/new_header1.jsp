<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
  <meta name="description" content="mobile first, app, web app, responsive, admin dashboard, flat, flat ui">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  
  

  <script src="js/jquery.min.js"></script>
  <script type="text/javascript">



var t = '<%=session.getAttribute("user_id")%>';

if (t == null)
{
	window.location.href= 'Login.jsp';
}
if (t == "")
{
	window.location.href= 'Login.jsp';
}
if (t == 'null')
{
	window.location.href= 'Login.jsp';
}


</script>
  
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/plugin.css">
  <link rel="stylesheet" href="css/font.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="easyui.css">
  <script type="text/javascript" src="Include/Fin7_JavaScript_Function.js"></script>
   <script type="text/javascript" src="Include/Isbs_JavaScript_Function.js"></script>
   
   <link rel="stylesheet" type="text/css" href="jquery.calendars.picker.css">
<script type="text/javascript" src="jquery.calendars.all.js"></script>
  <!--[if lt IE 9]>
    <script src="js/ie/respond.min.js"></script>
    <script src="js/ie/html5.js"></script>
  <![endif]-->
</head>
<body style="width: 100%;">
  <!-- header -->
	<header id="header" class="navbar" style="width: 100%;" >
    <ul class="nav navbar-nav navbar-avatar pull-right">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <span class="hidden-xs-only"><%=session.getAttribute("user_name")%>(<%=session.getAttribute("user_id")%>)</span>
          <span class="thumb-small avatar inline"><img src="images/user.jpg" alt="<%=session.getAttribute("user_name")%>" class="img-circle"></span>
          <b class="caret hidden-xs-only"></b>
        </a>
        <ul class="dropdown-menu">
          <li><a href="Login.jsp?logout=Y">Logout</a></li>
          <li class="divider"></li>
          <li><a href="Error_page.jsp">Help</a></li>
        
        </ul>
      </li>
    </ul>
    <!--  <a class="navbar-brand" href="#">--><img src="logo.png" height="45" width="140" style="padding-top: 1%;" />
    <button type="button" class="btn btn-link pull-left nav-toggle visible-xs" data-toggle="class:slide-nav slide-nav-left" data-target="body">
      <i class="fa fa-bars fa-lg text-default"></i>
    </button>
    
    <!-- 
    <ul class="nav navbar-nav hidden-xs"> 
      
      <li class="dropdown shift" data-toggle="shift:appendTo" data-target=".nav-primary .nav">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog fa-lg visible-xs visible-xs-inline"></i>Settings <b class="caret hidden-sm-only"></b></a>
        <ul class="dropdown-menu">
          <li>
            <a href="#" data-toggle="class:navbar-fixed" data-target='body'>Navbar
              <span class="text-active">auto</span>
              <span class="text">fixed</span>
            </a>
          </li>
          <li class="hidden-xs">
            <a href="#" data-toggle="class:nav-vertical" data-target="#nav">Nav
              <span class="text-active">vertical</span>
              <span class="text">horizontal</span>
            </a>
          </li>
          <li class="divider hidden-xs"></li>
          <li class="dropdown-header">Colors</li>
          <li>
            <a href="#" data-toggle="class:bg bg-black" data-target='.navbar'>Navbar
              <span class="text-active">white</span>
              <span class="text">inverse</span>
            </a>
          </li>
          <li>
            <a href="#" data-toggle="class:bg-light" data-target='#nav'>Nav
              <span class="text-active">inverse</span>
              <span class="text">light</span>
            </a>
          </li>
        </ul>
      </li>
    </ul> 
     -->
	</header>
  <!-- / header -->
  <!-- nav -->
  <nav id="nav" class="nav-primary hidden-xs nav-vertical"  style="width: 9%; font: ">
    <ul class="nav" data-spy="affix" data-offset-top="50">
      <!-- <li><a href="Home.jsp"><i class="fa fa-dashboard fa-lg"></i><span>Home</span></a></li>-->
      <li class="dropdown-submenu">
        <a href="#"><i class="fa fa-dashboard fa-lg"></i><span>Account Inquiry</span></a>
        <ul class="dropdown-menu">
        	 <li><a href="Fin7_AccountGried.jsp">	Account Master			    </a></li>
			<li><a href="Fin7_Customer_Master.jsp">	Customer Master Inquiry		    </a></li>
			<li><a href="Fin7_Drawing_Power_Inquiry.jsp">	Drawing Power History Inquiry	    </a></li>
			<li><a href="Fin7_LienEnquiry.jsp">	Lien / Hold Inquiry		    </a></li>
			<li><a href="Fin7_LoanAccountMasterInquiry.jsp">	Loan Account Master Inquiry	    </a></li>
			<li><a href="Fin7_Repayment_Details.jsp">	Loan Repayment Details		    </a></li>
			<li><a href="Fin7_Sanction_Limit_Inquiry.jsp">	Sanction Limit History Inquiry	    </a></li>
			<li><a href="fin7_Security_Detail_Inquiry.jsp">	Security Details & Limit Nodes	    </a></li>
			
			<li><a href="Fin7_Standing_Instructions_Enquiry.jsp">	Standing Instructions Inquiry	    </a></li>
			<li><a href="Fin7_TD_Acct_Masr_Enquiry.jsp">	TD Account Master Inquiry	    </a></li>
			<li><a href="Fin7_Term_Deposit_Renew_Inquiry.jsp">	TD Renewal History		    </a></li>
			<li><a href="Fin7_TODInquiry.jsp">	Temporary Overdraft Inquiry	    </a></li>
			<li><a href="Fin7_Any_day_bal.jsp">Any Day Balance	    </a></li>
			<li><a href="fin7_PanNo_New_Report.jsp">Pan Card   </a></li>
			<li><a href="Fin7_NPA_Report.jsp">NPA Details  </a></li>
			
			
			
        </ul>
      </li>
      <li class="dropdown-submenu">
        <a href="#"><i class="fa fa-list fa-lg"></i><span>Audit And Clearing Inquriy</span></a>
        <ul class="dropdown-menu">
	        <li><a href="Fin7_Audit_File_Inquiry.jsp">	 Audit File Inquiry. (AFI)		    </a></li>
			<li><a href="Fin7_BranchMaster.jsp">	 Branch Master Inquiry	    </a></li> 
	        <li><a href="Fin7_ChequeBookEnquiry.jsp">	Cheque Book Inquiry  </a></li>
		  	<li><a href="Fin7_DD_MC_Instrument.jsp">	      DD / MC Instrument Details     </a></li>
		  	
		  	<li><a href="Fin7_InwardClearingEnquiry.jsp">	      Inward Clearing Inquiry	     </a></li>
		  	<li><a href="Fin7_OutwardClearingInquiry.jsp">	      Outward Clearing Inquiry	     </a></li>
		  	<li><a href="Fin7_StopPayment.jsp">	      Stop Cheque Inquiry	     </a></li>	 
			<li><a href="Fin7_User_Details_Enquiry.jsp">	User Details Inquiry	    </a></li> 
        </ul>
      </li>
      
       <li class="dropdown-submenu">
        <a href="#"><i class="fa fa-list fa-lg"></i><span>Gl Master</span></a>
        <ul class="dropdown-menu">
	        <li><a href="Fin7_GL_Statement.jsp">GL / Internal Account Statement - Normal		 </a></li>
		  	<li><a href="Fin7_GL_Partitioned_Statement.jsp">GL / Internal Account Statement - Partitioned	 </a></li>
		  	<li><a href="Fin7_GL_Pointing_Statement.jsp">GL / Internal Account Statement - Pointing		 </a></li>
		  	<li><a href="Fin7_Trial_Balance.jsp">Trial Balance Inquiry				 </a></li>
        </ul>
      </li>
      
      <li class="dropdown-submenu">
        <a href="#"><i class="fa fa-edit fa-lg"></i><span>TDS Inquiry</span></a>
        <ul class="dropdown-menu">
	        <li><a href="Fin7_Interest_Adjustment_Inquiry.jsp">TD Interest Adjustment Inquiry	       </a></li>
		  	<li><a href="Fin7_Interest_Rate_Inquiry.jsp">Interest Rate History at Account Level    </a></li>
		  	<li><a href="Fin7_Interest_Report.jsp">Interest Report			       </a></li>
		  	<li><a href="Fin7_Interest_Report_Master.jsp">Interest Report Master		       </a></li>
		  	<li><a href="Fin7_TDS_Summary_Report.jsp">TDS Summary Report			       </a></li>
        </ul>
      </li> 
      
      
      <li class="dropdown-submenu">
        <a href="#"><i class="fa fa-signal fa-lg"></i><span>Transaction Inquiry</span></a>
        <ul class="dropdown-menu">
		    <li><a href="Fin7_Statement.jsp">	Statement of Account	    </a></li>
		  	<li><a href="Fin7_TransactionEnquiry.jsp">	Transaction Inquiry (FTI)   </a></li>
  	 	</ul>
      </li> 
      
      	<li class="dropdown-submenu">
        <a href="#"><i class="fa fa-list fa-lg"></i><span>Report Archival </span></a>
        <ul class="dropdown-menu">
        
                  <li><a href="TemplateSearch.jsp">Search</a></li>
                <li><a href="TemplateAdvansedSearch.jsp"> Advanced Search </a></li>
              <li><a href="CAddTemplate.jsp">Add Indexing Template</a></li>
			     <li><a href="Edit_Template.jsp">Edit Indexing Template</a></li>
				<li><a href="Manage_Template.jsp">Add Indexing Fields</a></li>
			    <li><a href="DocumentUpload.jsp">Report Upload</a></li>
				
			</ul>
      </li> 
    
    <%
   if (session.getAttribute("Role").equals("Admin")	|| session.getAttribute("Role").equals("User")) {
			%>
			<li class="dropdown-submenu">
		        <a href="#"><i class="fa fa-link fa-lg"></i><span>User Administration</span></a>
		        <ul class="dropdown-menu">
                 
				<li><a href="AddUser.jsp">Add User</a></li>
				<li><a href="User_master.jsp">Search User</a></li>
				<li><a href="AddApp.jsp">Add Application</a></li>
				<li><a href="App_master.jsp">Search Application</a></li>
				
				<!-- <li><a href="userVerification.jsp">User Verification</a></li>
				<li><a href="UserReport.jsp">User Maintenance Report</a></li>
				<li><a href="UserActivity.jsp">User Activity Report</a></li> -->
				<%if (session.getAttribute("Role").equals("Admin")){ %>
				<li><a href="Application_param.jsp">Application Settings</a></li>
				<%} %>
				
			</ul>
			</li>
			
		
			<%
				}
				 
			%>
    </ul>
  </nav>
  <!-- / nav --> 
<section style="padding-left:10%">
<div class="row">
 <div class="col-lg-12">
          <section class="toolbar clearfix m-t-large m-b">
            <a href="Home.jsp" class="btn btn-circle btn-sm"><i class="fa fa-home"></i>Home <b class=""> </b></a>
            <a href="Fin7_AccountGried.jsp" class="btn btn-primary btn-circle btn-sm"><i class="fa fa-group"></i>Account</a>
            <a href="Fin7_Customer_Master.jsp" class="btn btn-danger btn-circle btn-sm"><i class="fa fa-male"></i>Customer</a>
            
            <a href="Fin7_LoanAccountMasterInquiry.jsp" class="btn btn-info btn-circle btn-sm"><i class="fa fa-tasks"></i>Loan Details<b><i class="fa"></i></b></a>
            <a href="Fin7_Statement.jsp" class="btn btn-inverse btn-circle btn-sm"><i class="fa fa-bar-chart-o"></i>Statement</a>
            <a href="Fin7_TransactionEnquiry.jsp" class="btn btn-warning btn-circle btn-sm"><i class="fa fa-calendar-o"></i>Transaction</a>
            <a href="Fin7_ChequeBookEnquiry.jsp" class="btn btn-success btn-circle btn-sm"><i class="fa fa-check"></i>Cheque Book</a>
            
             
          </section>
        </div>
        </div>
 </section>

  <!-- / footer -->
	
  <!-- Bootstrap -->
  <script src="js/bootstrap.js"></script>
  
  <!-- app -->
  <script src="js/app.js"></script>
  
  
 
 
<script type="text/javascript" src="Include/jquery.easyui.min.js"></script> 
  
  <SCRIPT LANGUAGE="JavaScript">


var odate;
var dtCh= "/";
var minYear=1900;
var maxYear=2100;
var msg;
function checkdate(objName,st) {

if (odate!=null){
	return true;
}	

var datefield = objName;
odate="OK";

if(st==false){
	if(datefield.value==""){
		odate=null;
		return true;
	}
}

	if(datefield.value==""){
		odate=null;
		return true;
	}

	if (isDate(datefield.value)==false){
		datefield.select();
		alert(msg)
		datefield.focus();
		odate=null;
		return false;
	}else{
		odate=null;		
		return true;
	   }

}

function CheckKey()
{

	if (window.event.ctrlKey) {
		if (window.event.keyCode == 25){
			validate();
		} 
	}
}

function isInteger(objName,s){
	//alert(s);
	var i;
	if(s!=null && s!='undefined')
	{
	var datefield = objName;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
      
        if (((c < "0") || (c > "9")))
        { 
            if(c=="."||c=='-')
            {
            	
            }
            else{
        	datefield.select();
        	alert("Enter Valid Number");
        	datefield.focus();
            return false;
            }
        }
    }
	}
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dtStr){
	msg=""
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strDay=dtStr.substring(0,pos1)
	var strMonth=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		msg="The date format should be : dd/mm/yyyy"
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		msg="Please enter a valid month"
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		msg="Please enter a valid day"
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		msg="Please enter a valid 4 digit year between "+minYear+" and "+maxYear
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		msg="Please enter a valid date"
		return false
	}
return true
}


function cdate(txtDate){
	if (txtDate==""){
		return "";
	}else{
		arrDt=txtDate.split("/");
		if (arrDt.length<3){
			arrDt=txtDate.split("-");
				if (arrDt.length<3){
					return "";
				}
		}	
		newdt=new Date(arrDt[1]+"/"+arrDt[0]+"/"+arrDt[2]);
		return  newdt;
	}	
}


function datediff(date1,date2)
{
	
		firstdate=new Date(date1)
		seconddate=new Date(date2)
		var one_day=1000*60*60*24
		return Math.ceil((seconddate.getTime()-firstdate.getTime())/(one_day))
	
}

function fn(form,field)
{
var next=0, found=false
var f=form

if (window.event.ctrlKey) {
 	if (window.event.keyCode == 89){
 		validate();
 		return;
 	} 
 }
	
if(event.keyCode!=13) return;

   
	
for(var i=0;i<f.length;i++)
	{
	if(field.name==f.item(i).name) 
	{
		next=i+1;
		found=true
		break;
	}
	}
while(found)  //Infinite loop 
	{
	if( f.item(next).disabled==false &&  f.item(next).type!='hidden')
		{
		f.item(next).focus();
		if( f.item(next+1).disabled==false &&  f.item(next+1).type!='hidden'){
			f.item(next+1).focus();
			f.item(next).focus();
		}
		break;
		}
	else
		{
		if(next<f.length-1)
			next=next+1;
		else
			break;
			
		}
	}
}

function delrec()
{

	var answer = confirm ("Are you sure?")
	if (answer)
	{
		document.form1.uaction.value="delete";
		document.form1.saveform.value="";
		document.form1.submit();
	}
	
	
}


function dateAdd(intval, numb, base){
/*intval is YYYY, M, D, H, N, S as in VBscript; numb is amount +/-; base is javascript date object*/
switch(intval){
case "M":
base.setMonth(base.getMonth() + numb);
break;
case "YYYY":
base.setFullYear(base.getFullYear() + numb);
break;
case "D":
base.setDate(base.getDate() + numb);
break;
case "H":
base.setHours(base.getHours() + numb);
break;
case "N":
base.setMinutes(base.getMinutes() + numb);
break;
case "S":
base.setSeconds(base.getSeconds() + numb);
break;
default:
}
return base
}


function Left(st,ln)
{
	try{
		return st.slice(0,ln)
	}catch(e)
		{
		return "";
	}
	
}


</SCRIPT> 
  
</body>
</html>