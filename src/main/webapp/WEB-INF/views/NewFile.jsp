<jsp:include page="new_header.jsp"></jsp:include>

<html>
<head>

<title>User Master</title>


<style type="text/css">
.panel-body {
	min-height: 500px;
	!
	important
}
</style>

<script type="text/javascript" src="Include/jquery.easyui.min.js"></script>
<script type="text/javascript">
//<![CDATA[
           
jQuery(document).ready(function($) {
	//Getdata When Load Form using 

	resizeWindow();
	jQuery('#tt1').datagrid({  
	    url:'UserAuthentication_servlet',  
	    
	    columns:[[  
	        {field:'USER_ID',title:'User ID', sortable: true, resizable: true},  
	        {field:'USER_NAME',title:'User Name'},
	        {field:'USER_STATUS',title:'User Status'},  
	        {field:'USER_ROLE_DESC',title:'User Role'},  
	        {field:'LAST_LOGIN_DATE',title:'Last Login Date'},
	        {field:'LAST_LOGIN_IP',title:'Last Login IP'},
	        {field:'ISVERIFIED',title:'User Verification'},  
	        {field:'CREATED_USER',title:'Created User'},  
	        {field:'CREATED_TIME',title:'Created Time'},  
	        {field:'LOGIN_STATUS',title:'Login Status'},
	        {field:'LOGIN_RESET',title:'Reset'}
	    ]] ,
	     showTableToggleBtn: true ,
	     title:"Application User Details", //report Title
	     height: 350, //Height of Grid
		    width:1010 //Width of Grid
	  
	});
	
	$("body").resize(function(e){
		resizeWindow();
	});
	 
});
function clr() //Function Use To Blank Text
{
	$('#UserID').val(''); 
	$('#UserName').val(''); 
	
}
function doSearch() { //Function use Search The Data Using user paramater
	
	 $("#serch_btn").attr("disabled", "disabled");
		$("a[href*=#gdata]").click();
       
	
	$('#tt1').datagrid('load', {
		UserID :$('#UserID').val(),
		flg:'Y',
		UserName :$('#UserName').val()
	//	acc_no1:'0002BC0001151'
		
	});
}

function chkReset(userID)
{
	var st = '<%=session.getAttribute("user_id")%>';
		if (st != userID) {
			$("#chkUserID").val(userID);
			document.form1.action = 'UserAuthentication_servlet?operation=ResetLogin';
			document.form1.submit();
		} else {
			alert('Please Log out to perform the action');
			return false;
		}

	}

	//
</script>

</head>

<body>
	<form id="form1" name="form1" method="post">
		<section id="content">
			<section class="main padder">
				<div class="row">


					<section class="panel">
						<header class="panel-heading">
							<ul class="nav nav-tabs nav-justified">
								<li class="active"><a href="#search" data-toggle="tab">Search</a></li>
								<li><a href="#gdata" data-toggle="tab">Get Data</a></li>
							</ul>
						</header>
						<div class="panel-body">
							<div class="tab-content">
								<div class="tab-pane active" id="search">
									<div class="search_section">
										<div id="tb" class="tb_borders">
											<span>
												<h3>Application User Details</h3> <br>
											</span> <input type="hidden" id="chkUserID" name="chkUserID"
												value="chkUserID"></input>
											<table class="TblMain">
												<tr>
													<td>
														<div class="form-group">
															<label class="col-lg-3 control-label">User ID:</label>
															<div class="col-lg-8">

																<input id="UserID" type="text"
																	class="bg-focus form-control" />
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div>
													</td>
												</tr>
												<tr>

													<td>
														<div class="form-group">
															<label class="col-lg-3 control-label">User Name:</label>
															<div class="col-lg-8">
																<input id="UserName" type="text"
																	class="bg-focus form-control" />
																<div class="line line-dashed m-t-large"></div>
															</div>
														</div>
													</td>


												</tr>



												<tr>
													<td colspan="3" style="padding-top: 15px;"><input
														class="btn_style btn btn-primary" onclick="doSearch()"
														plain="true" type="button" value="Search" id="serch_btn">
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="button"
														id="test" disabled="disabled" onclick="clr()"
														class="btn_style btn btn-white" name="btn_Clear"
														value="Clear"></td>
													<td></td>

												</tr>
												<tr>
													<td colspan="3" class="char_srch_style"
														style="text-align: center;">Use * for wild card
														search Example : <B>BOM*</B>
													</td>
												</tr>
											</table>

										</div>

									</div>

								</div>
								<div class="tab-pane" id="gdata">
									<div id="pdatagrid"
										style="width: 100%; overflow: auto; padding: 3%">
										<table id="tt1" class="easyui-datagrid" width="100%"
											iconCls="icon-search" rownumbers="true" pagination="true">
										</table>
									</div>
								</div>

							</div>
						</div>

					</section>




				</div>
			</section>
		</section>

	</form>




</body>
</html>
