<%@ taglib uri="/WEB-INF/lib/tlds/pd4ml.tld" prefix="pd4ml"%>
<%@page import="com.dto.Get_Query"%>
<%@page import="java.sql.*"%>
<%@page import="com.connection.DBUtil"%>
<%@page contentType="text/html; charset=ISO8859_1"%>
<%@page import="org.zefer.pd4ml.PD4ML"%>

<%

String txtTypeOutPut = "";
String txtReportName = "";
String txtBranchName = "";
String txtFileName = "";
String txtReportName1 = "";
txtTypeOutPut=request.getParameter("txtTypeOutPut"); 
txtReportName=request.getParameter("txtReportName");
txtReportName1=request.getParameter("txtReportName1");
txtBranchName=request.getParameter("txtBranchName");
txtFileName=request.getParameter("txtFileName");


%>

<%
String txtHeight =request.getParameter("txtHeight");
String txtWidth = request.getParameter("txtWidth");

if(txtWidth!=null)
{
if(txtWidth.length()==0)
	txtWidth="1024";
}
else
	txtWidth="1024";

if(txtHeight!=null)
{
if(txtHeight.length()==0)
	txtHeight="landscape";
}
else
	txtHeight="landscape";

if(txtFileName!=null)
{
if(txtFileName.length()==0)
	txtFileName="Output.pdf";
else
	txtFileName=txtFileName+".pdf";
}
else
	txtFileName="Output.pdf";


if(txtBranchName!=null)
{
if(txtBranchName.length()==0)
	txtBranchName="";
}
else
	txtBranchName="";


if(txtReportName!=null)
{
if(txtReportName.length()==0)
	txtReportName=" Deatils";
}
else
	txtReportName="Deatils";
if(txtTypeOutPut.equals("pdf"))
{

%>


<pd4ml:transform screenWidth="<%=txtWidth %>"   pageOrientation="<%=txtHeight %>"
	pageInsets="1,1,1,1,point" enableImageSplit="false"
	fileName="<%=txtFileName%>" inline="false" >



	<html>
	<head>
	<title><%=txtFileName%></title>
	<style type="text/css">
body {
	color: #000000;
	font-family: Tahoma, "Sans-Serif";
	font-size: 10pt;
}

#tblHeader td {
	color: #fff;
	vertical-align: top;
}

#tblMain tr td {
	margin: 2px;
}

.myTable {
	font: 13px/ 1.9em Tahoma, Helvetica, sans-serif;
}

.myTable tr {
	font: 13px/ 1.9em Tahoma, Helvetica, sans-serif;
	width: auto;
}

.myTable tr td {
	font: 14px/ 1.9em Tahoma, Helvetica, sans-serif;
	padding-left: 12px;
	text-align: left;
	width: auto;
}

.myTable tr td .right {
	font: 13px/ 1.9em Tahoma, Helvetica, sans-serif;
	text-align: right;
	width: auto;
}

.myTable th {
	font: bold 13px/ 1.9em Tahoma, Helvetica, sans-serif;
	padding-left: 12px;
	text-align: left;
	width: auto;
}

.myTable .header th {
	color: white;
	font: bold 13px/ 1.8em Tahoma, Helvetica, sans-serif;
	height: auto;
	padding-left: 10px;
	text-align: left;
}

.mTable {
	color: #000000;
	font: 13px/ 1.9em Tahoma, Helvetica, sans-serif;
}

.mTable .header th {
	font: bold 13px/ 1.8em Tahoma, Helvetica, sans-serif;
	height: auto;
	text-align: center;
}

.mTable th {
	font: bold 13px/ 1.8em Tahoma, Helvetica, sans-serif;
	height: auto;
	text-align: left;
}

.mTable tr td {
	text-align: left;
	width: auto;
}

.datagrid-cell, .datagrid-cell-group, .datagrid-header-rownumber, .datagrid-cell-rownumber {
    font-size: 12px;
    font-weight: normal;
    height: 18px;
    line-height: 18px;
    margin: 0;   
    padding: 0 4px;
    white-space: nowrap;
    width: 300px;
    word-wrap: break-word;
    color: #000;
}

.datagrid-header td, .datagrid-body td, .datagrid-footer td {  
    border:1px solid ;
}


a{
color: #000;
text-decoration: none;
}



</style>
	</head>
	<body>




	<% 
	if(txtTypeOutPut.equalsIgnoreCase("pdf"))
	{ 
		
		%>
		<%= Get_pdf_herder.getPdfHeader(txtReportName,txtBranchName,session.getAttribute("bankName").toString()) %>
	<%}%>




	<%=request.getParameter("txtHTMLOutPut") %>
</pd4ml:transform>
<%}
else 
{
	txtFileName="";
	

	txtFileName=request.getParameter("txtFileName");
	
	if(txtFileName!=null)
	{
	if(txtFileName.length()==0)
		txtFileName="Output.xls";
	else
		txtFileName=txtFileName+".xls";
	}
	else
		txtFileName="Output.xls";
	
	
	response.setContentType("application/ms-excel");
	response.setHeader("Content-Disposition", "inline; filename="+txtFileName);	
	%>

<%@page import="com.dto.Get_pdf_herder"%><html>
<head>
<style type="text/css">
body {
	color: #000000;
	font-family: Tahoma, "Sans-Serif";
	font-size: 10pt;
}

#tblHeader td {
	color: #fff;
	vertical-align: top;
}

#tblMain tr td {
	margin: 2px;
}

.myTable {
	font: 13px/ 1.9em Tahoma, Helvetica, sans-serif;
}

.myTable tr {
	font: 13px/ 1.9em Tahoma, Helvetica, sans-serif;
	width: auto;
}

.myTable tr td {
	font: 14px/ 1.9em Tahoma, Helvetica, sans-serif;
	padding-left: 12px;
	text-align: left;
	width: auto;
}

.myTable tr td .right {
	font: 13px/ 1.9em Tahoma, Helvetica, sans-serif;
	text-align: right;
	width: auto;
}

.myTable th {
	font: bold 13px/ 1.9em Tahoma, Helvetica, sans-serif;
	padding-left: 12px;
	text-align: left;
	width: auto;
}

.myTable .header th {
	color: white;
	font: bold 13px/ 1.8em Tahoma, Helvetica, sans-serif;
	height: auto;
	padding-left: 10px;
	text-align: left;
}

.mTable {
	color: #000000;
	font: 13px/ 1.9em Tahoma, Helvetica, sans-serif;
}

.mTable .header th {
	font: bold 13px/ 1.8em Tahoma, Helvetica, sans-serif;
	height: auto;
	text-align: center;
}

.mTable th {
	font: bold 13px/ 1.8em Tahoma, Helvetica, sans-serif;
	height: auto;
	text-align: left;
}

.mTable tr td {
	text-align: left;
	width: auto;
}

.datagrid-cell, .datagrid-cell-group, .datagrid-header-rownumber, .datagrid-cell-rownumber {
    font-size: 12px;
    font-weight: normal;
    height: 18px;
    line-height: 18px;
    margin: 0;   
    padding: 0 4px;
    white-space: nowrap;
    width: 300px;
    word-wrap: break-word;
    color: #000;
}

.datagrid-header td, .datagrid-body td, .datagrid-footer td {  
    border:1px solid ;
}


a{
color: #000;
text-decoration: none;
}

</style>
</head>
<body>
<%=request.getParameter("txtHTMLOutPut") %>
</body>
</html>

<% } %>

