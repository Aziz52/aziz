<jsp:include page="new_header.jsp"></jsp:include>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@page import="java.net.URL"%><html>
<title>Application User Details</title>


<style type="text/css">
.panel-body {
	min-height: 500px; !important;
}
</style>

<script type="text/javascript">

//<![CDATA[
           
jQuery(document).ready(function($) {
	//Getdata When Load Form using

resizeWindow();	
	
	jQuery('#tt1').datagrid({  
	    url:'UserAuthentication_servlet',  
	    
	    columns:[[  
	        {field:'USER_ID',title:'User ID',width:80},  
	        {field:'USER_NAME',title:'User Name',width:150},
	        {field:'USER_STATUS',title:'User Status',width:60},  
	        {field:'USER_ROLE_DESC',title:'User Role',width:80},  
	        {field:'LAST_LOGIN_DATE',title:'Last Login Date',width:120},
	        {field:'ISVERIFIED',title:'Verification',width:80},  
	        {field:'CREATED_USER',title:'Created User',width:60},  
	        {field:'LOGIN_STATUS',title:'Login Status',width:60},
	        {field:'LOGIN_RESET',title:'Reset',width:50},
	        
	    ]] ,
	     showTableToggleBtn: true ,
	    title:"Application User Details", //report Title
	    height: 200, //Height of Grid
	    width:900 //Width of Grid
	});
	
	
	
	$("body").resize(function(e){
		resizeWindow();
	});
	
});


function doSearch() { //Function use Search The Data Using user paramater
	 $("a[href*=#gdata]").click();	 
	
	$('#tt1').datagrid('load', {
		UserID :$('#UserID').val(),
		flg:'Y',
		UserName :$('#UserName').val()
	//	acc_no1:'0002BC0001151'
	});
}

function chkReset(userID)
{
	var st = '<%=session.getAttribute("user_id") %>';
	if (st != userID)
	{
		$("#chkUserID").val(userID);
		document.form1.action = 'UserAuthentication_servlet?operation=ResetLogin';
		document.form1.submit();
	}
	else
	{	
		alert('Please Log out to perform the action');
		return false;
	}
	
}





	//]]>
	</script>

</head>

<body>
	<form id="form1" name="form1" method="post">
		<section id="content">
			<section class="main padder">
				<div class="row">


					<section class="panel">
						<header class="panel-heading">
							<ul class="nav nav-tabs nav-justified">
								<li class="active"><a href="#search" data-toggle="tab">Search</a></li>
								<li><a href="#gdata" data-toggle="tab">Get Data</a></li>
							</ul>
						</header>
						<div class="panel-body" style="margin-left: 10%;">
							<div class="tab-content">
								<div class="tab-pane active" id="search">
									<div class="search_section">
										<div id="tb" class="tb_borders">
											<span>
												<h3>Application User Details</h3> <br>
											</span> <input type="hidden" id="chkUserID" name="chkUserID"
												value="chkUserID"></input>
												
											
											<table class="TblMain">
												
												
												<tr><td><div class="form-group">  <label class="col-lg-3 control-label">User ID :&nbsp;</label>  <div class="col-lg-8"> <input id="UserID" type="text"  class="bg-focus form-control"  />     <div class="line line-dashed m-t-large"></div>  </div> </div></td></tr> 
<tr><td><div class="form-group">  <label class="col-lg-3 control-label">User Name:&nbsp;</label>  <div class="col-lg-8"> <input id="UserName" type="text"  class="bg-focus form-control"  />     <div class="line line-dashed m-t-large"></div>  </div> </div></td></tr> 

												
												<tr>
													<td colspan="3" style="padding-top: 15px;"><input
														class="btn_style btn btn-primary" onclick="doSearch()"
														plain="true" type="button" value="Search" id="serch_btn">
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="button"
														id="test"  onclick="clr()"
														class="btn_style btn btn-white" name="btn_Clear"
														value="Clear"></td>
													<td></td>

												</tr>
												

												<tr>
													<td colspan="3" class="char_srch_style"
														style="text-align: center;">Use * for wild card
														search Example : <B>BOM*</B>
													</td>
												</tr>
											</table>

										</div>

									</div>

								</div>
								<div class="tab-pane" id="gdata">
									<div id="pdatagrid"
										style="width: 100%; overflow: auto; padding: 3%">
										<table id="tt1" class="easyui-datagrid" width="100%"
											iconCls="icon-search" rownumbers="true" pagination="true">
										</table>
									</div>
								</div>

							</div>
						</div>

					</section>




				</div>
			</section>
		</section>
<div id="showDiv" style="display:none;">
</div>
	</form>

<script type="text/javascript">

</script>


</body>
<jsp:include page="Footer.jsp"></jsp:include>
</html>
