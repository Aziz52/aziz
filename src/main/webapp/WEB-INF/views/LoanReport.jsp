<jsp:include page="Header_new.jsp"></jsp:include>
<html>

<head>
<title>Loan Details </title>


<script type="text/javascript">
//<![CDATA[
           
jQuery(document).ready(function($) {
	//Getdata When Load Form using 
	
	jQuery('#tt1').datagrid({  
	    url:'LoanDetails_servlet?operation=Grid',  
	    
	    columns:[[  
	        {field:'ACNO',title:'Account Number',width:100},  
	        {field:'DEAL_REFERENCE',title:'Loan Reference Number',width:200},
	        {field:'OTDLR',title:'Account Name',width:200}  
	        
	         
	    ]] ,
	     showTableToggleBtn: true ,
	    title:"Loan Details", //report Title
	    height: 350, //Height of Grid
	    width:850 //Width of Grid
	    
	}); 
	showsearch();
});
function clr() //Function Use To Blank Text
{
	$('#txtscab').val(''); 
	$('#txtscan').val('');
	$('#txtscas').val(''); 
	$('#txtLoanDealNum').val('');  
	
}
function doSearch() { //Function use Search The Data Using user paramater

	//alert($('#txtLoanDealNum').val());
	$('#tt1').datagrid('load', {
		msg:'Loan Deal Does Not Exists On The Database',
		scab :$('#txtscab').val(),
		scan :$('#txtscan').val(),
		scas :$('#txtscas').val(),
		flg  :'Y',
		FDDeal :$('#txtLoanDealNum').val()
	//	acc_no1:'0002BC0001151'
	});
	showreg();
}
function ShowReport(fschema,lschema,scab,scan,scas,otbrnm,otdlp,otdlr)
{
	//alert(fschema+lschema+scab +scan+scas+otbrnm+otdlp+otdlr)
	window.open('ShowReport.jsp?rTyp=LN&fschema='+ fschema + '&lschema='+ lschema + "&scan="+scan+"&scab="+scab+"&scas="+scas+"&otbrnm="+otbrnm+"&otdlp="+otdlp + "&otdlr="+otdlr,'winname','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=900,height=500,top=100,left=70');
}

//]]>
</script>

</head>

<body style="width: 90%;">
<form id="form1" name="form1" method="post" >
<div   class="tab_divs">
<center>
<div class="form_inner_wrap">
<div id="reg_box" class="form_section" style="padding-bottom: 6%;"><br />
<br />
<br />
<div id="get_details">
<center>
<table id="tt1" style="diplay: none !important;" class="easyui-datagrid"
	iconCls="icon-search" rownumbers="true" width="90%" pagination="true">
</table>
</br>

</center>
</div>
</div>

<div id="search_box" class="search_section" style="display: none;padding-top: 12%;padding-bottom: 16%;">
<div id="tb" class="tb_borders"  >
<span>
<h3>Loan Details</h3>

</span>

<table class="mTable">
	<tr>
		<td><label  class="cm-required">Loan Number :&nbsp;</label></td>
		<td><input id="txtscab" type="text" class="input-text-small" /><input id="txtscan" type="text" class="input-text-small" /><input id="txtscas" type="text" class="input-text-small" /></td>
		
	</tr><tr>
		<td><label class="cm-required">Loan Reference Number:&nbsp;</label></td>
		<td><input id="txtLoanDealNum" type="text" class="input-text" /></td>
		
	</tr>
	

	
	<tr>
	<td colspan="3" style="padding-top: 15px;text-align: center;"><input class="btn_style" onclick="conditions()"  plain="true" type="button" value="Search">

		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" onclick="clr()" class="btn_style" name="btn_Clear" value="Clear"></td>
		<td></td>
	</tr>
	<tr>
	<td colspan="3" class="char_srch_style"  style="text-align: center;" >
	 Use * for wild card search  Example : <B>BOM*</B> 
	</td>
	</tr>
</table>

</div>
</div>
</div>
<jsp:include page="Footer.jsp"></jsp:include>
</center>


<script type="text/javascript">
function conditions()
{
	//alert($('#AccountNumber').val().length);
	if ($('#txtscab').val()==''&& $('#txtLoanDealNum').val()=='')
	{
		alert('Enter Loan Number Or Loan Reference Number');
		 return false;
	}

	
	if($('#txtLoanDealNum').val()!='')
	{
		var txtLoanDealNum=$('#txtLoanDealNum').val();
		txtLoanDealNum=txtLoanDealNum.replace('*','');
		if (txtLoanDealNum.length < 3)
		{
			 alert('Please Enter Minimum Three Characters in Loan Reference Number');
			 return false;
		}
	}
	if( $('#txtscab').val()!='' && $('#txtscan').val()!='' && $('#txtscas').val()!='')
	{
		check_account_exist($('#txtscab').val(),$('#txtscan').val(),$('#txtscas').val(),'Loan');
	}
	else
	{
		doSearch();
	}
	//doSearch();
} 


    </script><!--
  change By anurag

--><div id="divFDReport" style="display:none;z-index:10000000;">

</div>
</div>
</form>




</body>
	</html>
	