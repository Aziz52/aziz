<jsp:include page="Header_new.jsp"></jsp:include>
<html>
<head>
<title>New Number Details</title>
<script type="text/javascript">
//<![CDATA[
           
jQuery(document).ready(function($) {
	//Getdata When Load Form using 
	
	jQuery('#tt1').datagrid({  
	    url:'NewNumber_servlet',  
	    
	    columns:[[  
	        {field:'OLDCUST',title:'Old Cust id',width:150,align:'center'},  
	        {field:'NEWCUSTID',title:'New Cust id',width:150,align:'center'},
	        {field:'OLDACTID',title:'Old Account Number',width:150,align:'center'},  
	        {field:'NEWACTID',title:'New Account Number',width:150,align:'center'},
	        {field:'deal_ref_no',title:'Deal Reference No',width:150,align:'center'}  
	        
	         
	    ]] ,//Deal_ref_no
	     showTableToggleBtn: true ,
	    title:"Old New Account Number Search Details", //report Title
	    height:348, //Height of Grid
	    width:900//Width of Grid
	    
	}); 
	showsearch();
});
function clr() //Function Use To Blank Text
{
	$('#Account_No').val(''); 
	$('#Cust_id').val(''); 
}
	function doSearch() { //Function use Search The Data Using user paramater

		
		$('#tt1').datagrid('load', {

			msg:'Customer Does Not Exists On The Database',
			Account_No :$('#Account_No').val(),
			Cust_id :$('#Cust_id').val(),
			 flg:"Y"
		});
		showreg();
	}

	function createOutput(typeOutPut)
	{
		$('#txtAccount_NoOutPut').val($('#Account_No').val());
		$('#txtCust_idOutPut').val($('#Cust_id').val());
		$('#txtTypeOutPut').val(typeOutPut);
		$('#txtHTMLOutPut').val("");	
		$('#frmPdfOutPut').submit();
		
	}



	//]]>
	</script>

	</head>
	<body style="width: 90%;">

	<form>
	<div  class="tab_divs">

	<center>
	<div class="form_inner_wrap">
	<div id="reg_box" class="form_section" style="padding-bottom: 6%;"> 
	<br />
	<br /><br />
	
	<div id="get_details">
	<table id="tt1" style="diplay: none !important;" class="easyui-datagrid"
		iconCls="icon-search" rownumbers="true" pagination="true">
	</table>
	</div>
	<table>
	<tr>
		<td><input type="button" class="btn_style" onclick="createOutput('excel')"
			name="operation" value="Excel"></td>
		
        <td><input type="button"  class="btn_style" onclick="createOutput('pdf')"  name="operation" value="PDF">
</td>	
	
	</tr>
</table>

	</div>

	<div id="search_box" class="search_section" style="display: none;padding-top: 12%;padding-bottom: 15%;">
	
	<div id="tb" class="tb_borders" >

	<h3>Old New Account Number Search</h3>

	<table class="myTable">
	
	<tr>
		<td><label class="cm-required">Account Number &nbsp;</label></td>
		<td><input id="Account_No" type="text" class="input-text" /></td>
		<td></td>
	</tr><tr>
		<td><label class="cm-required">Customer Id&nbsp;</label></td>
		<td><input id="Cust_id" type="text" class="input-text" /></td>
		<td></td>
	</tr>

		<tr>
		<td  colspan="3" style="padding-top: 15px;text-align: center;">
		
		<input class="btn_style" onclick="conditions()"  plain="true" type="button" value="Search">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" onclick="clr()" class="btn_style" name="btn_Clear" value="Clear"></td>
		<td></td>
	</tr>
	<tr>
	<td colspan="3" class="char_srch_style"  style="text-align: center;" >
	 Use * for wild card search  Example : <B>BOM*</B> 
	</td>
	</tr>
</table>

	</div>
	</div>
	</div>

	</center>
	<script type="text/javascript">
function conditions()
{
	//alert($('#AccountNumber').val().length);
	if ($('#Account_No').val()==''&& $('#Cust_id').val()=='')
	{
		alert('Enter Account Number Or Customer Id');
		 return false;
	}

	if($('#Account_No').val()!='')
	{
		var Account_No=$('#Account_No').val();
		Account_No=Account_No.replace('*','');
		if (Account_No.length < 3)
		{
			 alert('Please Enter Minimum Three Characters in Account No');
			 return false;
		}
	}


	if($('#Cust_id').val()!='')
	{
		var Cust_id=$('#Cust_id').val();
		Cust_id=Cust_id.replace('*','');
		if (Cust_id.length < 3)
		{
			 alert('Please Enter Minimum Three Characters in Cust id');
			 return false;
		}
	}
	
	
	
	if ($('#Account_No').val()!='')
	{
		jQuery.ajax({
			type:"POST",
			url:"Get_CommanDetails?Detail=check_cust&rep_name='New_number'&accno="+$('#Account_No').val(),
			data:"",
			success:function(msg){	
				if(msg=='false')
				{
					alert('Sorry Session TimeOut ');
					window.location.href='Login.jsp';
					
				}else if(msg=='no' || msg =='0')	
							{
					alert('Account Does Not Exists On The Database');				
				}
				else
				{
					var ss= doSearch();	
					//alert($("#txt_cust_check").val()+'yes');
				}
				
			}
			
			});
	//check_customer_exist($('#Account_No').val());
	}
	else{
	doSearch();
	}
} 
    </script>

	</div>
	</form>
	
	
<form action="NewNumber_PDF.jsp" method="post" style="display: none;" id="frmPdfOutPut">
	<input type="hidden" id="txtAccount_NoOutPut" name="txtAccount_NoOutPut">
	<input type="hidden" id="txtCust_idOutPut" name="txtCust_idOutPut">
	<input type="hidden" id="txtTypeOutPut" name="txtTypeOutPut">
	<input type="hidden" id="txtHTMLOutPut" name="txtHTMLOutPut">
</form>
	
	
	
	
	

	<jsp:include page="Footer.jsp"></jsp:include>


	</body>
	</html>
	