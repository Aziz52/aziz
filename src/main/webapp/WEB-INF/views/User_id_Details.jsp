<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Id Details</title>
<script src="jquery.js" type="text/javascript"></script>
<jsp:include page="Header_new.jsp"></jsp:include>

	<script src="jquery-ui.js" type="text/javascript"></script>
    <script src="setup.js" type="text/javascript"></script>
    <link href="current-theme.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript">
//<![CDATA[
          
jQuery(document).ready(function($) {
	//Getdata When Load Form using 
	//showsearch();
	
	jQuery('#tt1').datagrid({  
	    url:'User_id_Details_Servlet?Report=Master',  
	    columns:[[  
	        {field:'OCUSID',title:'User Id',width:50},  
	        {field:'OCUNAM',title:'User Full Name',width:290},
	        {field:'ocbrnm',title:'Branch Id',width:70},
	        {field:'ocbrnm1',title:'Branch Name',width:125},  
	        {field:'occoa',title:'User Class',width:65},  
	        {field:'OCPWDM',title:'User Id <br/> Open Date',width:70},  
	        {field:'OCPWD',title:'General Text',width:75},
	        {field:'ocaab',title:'Auth <br/>Any Branch',width:75},  
	        {field:'OCBBN',title:'User <br/>Branch No',width:65}  
	         //If Columns is amount aligh right
	        //If Columns is Date,String Data aligh Left
	    ]] ,
	     showTableToggleBtn: true ,
	    title:"User Id Details", //report Title
	    height: 350, //Height of Grid
	    width:930 //Width of Grid
	});
	showsearch(); 
});
function clr() //Function Use To Blank Text//User_Branch_id,User_Name,User_id
{
	$('#User_id').val(''); 
	$('#User_Name').val(''); 
	$('#User_Branch_id').val(''); 
}
	function doSearch() { //Function use Search The Data Using user paramater

		$('#tt1').datagrid('load', {
			msg:'User Does Not Exists On The Database',
			User_id :$('#User_id').val(),
			User_id1 :$('#User_id').val(),
			User_Name :$('#User_Name').val(),
			flg :'Y',
			User_Branch_id :$('#User_Branch_id').val()
		//	acc_no1:'0002BC0001151'
		});
		showreg();
	}

	//*********************************************************
function get_details(schema)
    {
    	$("#divloader").html('<img src="images/loading.gif"/><br/>Processing Please Wait ...').show();
       	var User_id=jQuery('#user_id_H').val();
//    	alert(User_id);
    	jQuery.ajax({
    		type:"POST",
    		url:"User_id_Details_Servlet?Report=User_details&schema="+ schema +"&User_id="+User_id,
    		data:"",
    		success:function(msg){
    		$("#divloader").html('').show();
    			jQuery("#"+schema).html(msg);
    		}
    		});
    }

function ShowReport(User_id,first_schema,last_schema)
{
	window.open('User_schema_details.jsp?User_id='+ User_id + '&first_schema='+ first_schema + "&last_schema="+last_schema,'winname','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=900,height=500,top=100,left=70');
}

function createOutput(typeOutPut)
{
	
	$('#txtUser_id_output').val($('#User_id').val());
	$('#txtUser_Name_output').val($('#User_Name').val());
	$('#txtUser_Branch_id_output').val($('#User_Branch_id').val());
	$('#txtTypeOutPut').val(typeOutPut);
	$('#txtHTMLOutPut').val("");	
	$('#frmPdfOutPut').submit();
	
}

//]]>
</script>

</head>
<body style="width: 90%;">

<form >
<div  class="tab_divs">
<center>
<div class="form_inner_wrap">
<div id="reg_box" class="form_section" style="padding-bottom: 4%;"  ><br />
<br />
<br />
<div id="get_details">
<table id="tt1" style="diplay: none !important;" class="easyui-datagrid" iconCls="icon-search" rownumbers="true" pagination="true">
</table>
</div>
<table>
	<tr>
		<td><input type="button"  class="btn_style"  onclick="createOutput('excel')"
			name="operation" value="Excel"></td>
		<!--<td><input type="button"  class="btn_style"  onclick="report('Word')"
			name="operation" value="Word"></td>-->
		<td><input type="button"  class="btn_style" onclick="createOutput('pdf')"  name="operation" value="PDF"></td>
		<%-- <td> <input type="submit" name="operation" value="Cancel"></td> --%>
	</tr>
	
</table>

</div>
</div>
<div id="search_box" class="search_section" style="display: none;padding-top: 12%;padding-bottom: 13%;">

<div id="tb" class="tb_borders" >

<h3>User Id Details</h3>
<input id="user_id_H" type="hidden" class="input-text" value="">
<input id="First_schema_H" type="hidden" class="input-text" value="">
<input id="Last_schema_H" type="hidden" class="input-text" value="">
<table class="myTable">
	<tr>
		<td><label class="cm-required">User Id :&nbsp;</label></td>
		<td><input id="User_id" type="text" class="input-text" /></td>
		<td></td>
	</tr><tr>
		<td><label class="cm-required">User Name :&nbsp;</label></td>
		<td><input id="User_Name" type="text" class="input-text" /></td>
		<td></td>
	</tr><tr>
		<td><label class="cm-required">User Branch Id :&nbsp;</label></td>
		<td><input id="User_Branch_id" type="text" class="input-text" /></td>
		<td></td>
	</tr>
	

	
	<tr>
		<td  colspan="3" style="padding-top: 10px;text-align: center;">
		
		<input class="btn_style" onclick="conditions()"  plain="true" type="button" value="Search">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" onclick="clr()" class="btn_style" name="btn_Clear" value="Clear"></td>
		<td></td>
	</tr>
	<tr>
	<td colspan="3" class="char_srch_style"  style="text-align: center;" >
	 Use * for wild card search  Example : <B>BOM*</B> 
	</td>
	</tr>
</table>

</div>
</div>
</center>
</div>
</form>


<div style="display: none;">
<div id="hidDIvdds">
<div id="user_details_div" style="width: 90%;height:50%"></div>
</div>
 <div id="t13" style="width: 90%;height:50%; display: none;"></div>
 </div>

<script type="text/javascript">
function conditions()
{
	if ($('#User_id').val()=='' && $('#User_Name').val() == '' && $('#User_Branch_id').val()=='' )
	{
		alert('Enter User Id Or User Name Or Branch code');
		return false;
	}


	if($('#User_id').val()!='')
	{
		var User_id=$('#User_id').val();
		User_id=User_id.replace('*','');
		if (User_id.length < 3)
		{
			 alert('Please Enter Minimum Three Characters in User id');
			 return false;
		}
	}


	if($('#User_Name').val()!='')
	{
		var User_Name=$('#User_Name').val();
		User_Name=User_Name.replace('*','');
		if (User_Name.length < 3)
		{
			 alert('Please Enter Minimum Three Characters in User Name');
			 return false;
		}
	}


	if($('#User_Branch_id').val()!='')
	{
		var User_Branch_id=$('#User_Branch_id').val();
		User_Branch_id=User_Branch_id.replace('*','');
		if (User_Branch_id.length < 3)
		{
			 alert('Please Enter Minimum Three Characters in User Branch id ');
			 return false;
		}
	}

	
	if($('#User_Branch_id').val()!='')
	{
		check_branch_exist($('#User_Branch_id').val());
	}
	else{	 
			doSearch();
		}
} 
    </script>


<jsp:include page="Footer.jsp"></jsp:include>
<form action="User_id_Details_PDF.jsp" method="post" style="display: none;" id="frmPdfOutPut">
	<input type="hidden" id="txtUser_id_output" name="txtUser_id_output">
	<input type="hidden" id="txtUser_Name_output" name="txtUser_Name_output">
	<input type="hidden" id="txtUser_Branch_id_output" name="txtUser_Branch_id_output">
	<input type="hidden" id="txtTypeOutPut" name="txtTypeOutPut">
	<input type="hidden" id="txtHTMLOutPut" name="txtHTMLOutPut">
</form>

</body>
	</html>
	