<jsp:include page="Header_new.jsp"></jsp:include>
<html>
<head>
<title> User Maintenance Report
</title>
<script type="text/javascript">
//<![CDATA[
jQuery(document).ready(function($) {
	//Getdata When Load Form using 
	
	jQuery('#tt1').datagrid({  
	    url:'UserAuthentication_servlet?operation=UserReport',  
	     columns:[[  
	        {field:'USER_ID',title:'User ID',width:80},  
	        {field:'USER_NAME',title:'User Name',width:150},
	        {field:'USER_STATUS',title:'User Status',width:70},  
	        {field:'USER_ROLE_DESC',title:'User Role',width:60},
	        {field:'CREATED_TIME',title:'Created Time',width:120},
	        {field:'CREATED_USER',title:'Created User',width:80},  
	       // {field:'LAST_LOGIN_DATE',title:'Last Login Date',width:120},
	        {field:'LOGIN_IP',title:'Last Login IP',width:100},
	        //{field:'UNLOCK_DATE',title:'Unlock Date',width:120},
	         {field:'MODIFIED_USER',title:'Modified User',width:85},
	         {field:'MODIFIED_TIME',title:'Modified Time',width:125},
	        {field:'VERIFIED_USER',title:'Verified User',width:80},
	        {field:'VERIFIED_DATE',title:'Verified Date',width:120},
	        {field:'EXPIRY_DATE',title:'Expiry Date',width:120}, 
	        {field:'LOGIN_DATE',title:'Login Date',width:120},
	        {field:'STATUS_DESC',title:'Status Description',width:105}
	         
	    ]] ,
	     showTableToggleBtn: true ,
	    title:"User Maintenance Report", //report Title
	    height: 380, //Height of Grid
	    width:900, //Width of Grid
	    pageSize:20
	});
	showsearch();  
	che_his();
});



function clr() //Function Use To Blank Text
{
	$('#fromdt').val(''); 
	$('#todate').val(''); 
	$('#UserID').val(''); 
	$('#UserName').val(''); 
	$('#histype').val(''); 
	
	
}
function doSearch() { //Function use Search The Data Using user paramater

	if ($('#fromdt').val() != '' && $('#todate').val()!='')
	{			
		if (!(cdate($('#fromdt').val())<= cdate($('#todate').val())))
		{
			alert('Date range is not valid');
			return false;
		}
	 }


	if ($('#UserID').val()=='' && $('#UserName').val() == '' && $('#fromdt').val()=='' && $('#todate').val()=='' && $('#reptype').val()=='' )
	{
		alert('Please Enter Atleast Single Criteria');
		return false;
	}

	if($('#histype').val()=='N')
	{
		show_bal_cls();
	}
	if($('#histype').val()=='Y')
	{
		hide_bal_cls();
	}
	$('#tt1').datagrid('load', {
		UserID :$('#UserID').val(),
		flg :'Y',
		UserName :$('#UserName').val(),
		fromdt : $('#fromdt').val(),
		todate : $('#todate').val(),
		his_flg : $('#histype').val(),
		reptype : $('#reptype').val()
		
	//	acc_no1:'0002BC0001151'
	});
	
	showreg();
}


function show_bal_cls()
{
	$('#tt1').datagrid('showColumn','USER_STATUS');
	$('#tt1').datagrid('showColumn','MODIFIED_USER');
	$('#tt1').datagrid('showColumn','MODIFIED_TIME');
	$('#tt1').datagrid('showColumn','USER_ROLE_DESC');
	$('#tt1').datagrid('showColumn','CREATED_TIME');
	$('#tt1').datagrid('showColumn','CREATED_USER');
	$('#tt1').datagrid('showColumn','VERIFIED_USER');
	$('#tt1').datagrid('showColumn','VERIFIED_DATE');
	$('#tt1').datagrid('showColumn','EXPIRY_DATE');
	$('#tt1').datagrid('hideColumn','LOGIN_IP');
	$('#tt1').datagrid('hideColumn','LOGIN_DATE');
	$('#tt1').datagrid('hideColumn','STATUS_DESC');
	
}
function hide_bal_cls()
{
	
	$('#tt1').datagrid('hideColumn','USER_STATUS');
	$('#tt1').datagrid('hideColumn','USER_ROLE_DESC');
	$('#tt1').datagrid('hideColumn','CREATED_TIME');
	$('#tt1').datagrid('hideColumn','CREATED_USER');
	$('#tt1').datagrid('hideColumn','VERIFIED_USER');
	$('#tt1').datagrid('hideColumn','MODIFIED_USER');
	$('#tt1').datagrid('hideColumn','MODIFIED_TIME');
	//$('#tt1').datagrid('hideColumn','UNLOCK_DATE');
	$('#tt1').datagrid('hideColumn','VERIFIED_DATE');
	$('#tt1').datagrid('hideColumn','EXPIRY_DATE');
	$('#tt1').datagrid('showColumn','LOGIN_IP');
	$('#tt1').datagrid('showColumn','LOGIN_DATE');
	$('#tt1').datagrid('showColumn','STATUS_DESC');
	
	//$('#tt1').datagrid('reload');,
}
function che_his()
{
	//var his_type='<option value="">-Select-</option>';
	
	var gr=document.form1.reptype;
	if($('#histype').val()=='Y')
	{
		var newoption=new Option('-Select-','');
		gr.options[0]=newoption;
		var newoption=new Option('SUCCESS','S');
		gr.options[1]=newoption;
		var newoption=new Option('EXPIRED','X');
		gr.options[2]=newoption;
		var newoption=new Option('LOCKED','L');
		gr.options[3]=newoption;
		var newoption=new Option('AUTHETICATION FAILED','F');
		gr.options[4]=newoption;
		var newoption=new Option('LOGOUT','O');
		gr.options[5]=newoption;
				
	}
	if($('#histype').val()=='N')
	{

		var newoption=new Option("-Select-","");
		gr.options[0]=newoption;
		var newoption=new Option("ACTIVE USER","user_status_desc='Enabled'");
		gr.options[0]=newoption;
		var newoption=new Option("CURRENTLY LOGGED IN","ISACTIVE='Yes'");
		gr.options[1]=newoption;
		var newoption=new Option("EXPIRED","user_status_desc='Expired'");
		gr.options[2]=newoption;
		var newoption=new Option("LOCKED","user_status_desc='Locked'");
		gr.options[3]=newoption;
		var newoption=new Option("VERIFICATION PENDING","IS_VERIFIED='No'");
		gr.options[4]=newoption;
		var newoption=new Option("VERIFICATION REJECTED","IS_VERIFIED='Rejected'");
		gr.options[4]=newoption;
		var newoption=new Option("INACTIVE","USER_STATUS='I'");
		gr.options[5]=newoption;
		
	}
		//type+='<option value="S">SUCESS</option><option value="X">EXPIRED</option><option value="L">LOCKED</option><option value="F">AUTHETICATION FAILED</option>';
	//	$('#reptype').innerHTML(his_type);
		
}
function createOutput(typeOutPut)
{
	$('#txtFromdtOutPut').val($('#fromdt').val());
	$('#txtTodateOutPut').val($('#todate').val());
	$('#txtUserIDOutPut').val($('#UserID').val());
	$('#txtUserNameOutPut').val($('#UserName').val());
	$('#txtHistypeOutPut').val($('#histype').val());
	$('#txtReptypeOutPut').val($('#reptype').val());
	
	$('#txtTypeOutPut').val(typeOutPut);
	$('#txtReportName').val("Application User Details");
	
	$('#txtHTMLOutPut').val("");	
	$('#frmPdfOutPut').submit();
	
}



//]]>
</script>

</head>

<body style="width: 90%;">
<form id="form1" name="form1" method="post" >
<div   class="tab_divs">
<center>
<div class="form_inner_wrap">
<div id="reg_box" class="form_section"  style="padding-bottom: 6%;"><br />
<br />

<div id="get_details">
<center>
<table id="tt1" style="diplay: none !important;" class="easyui-datagrid"
	iconCls="icon-search" rownumbers="true" width="90%" pagination="true">
</table>
<table>
	<tr>
		<td><input type="button"  class="btn_style"  onclick="createOutput('excel')"
			name="operation" value="Excel"></td>
		<!--<td><input type="button"  class="btn_style"  onclick="report('Word')"
			name="operation" value="Word"></td>-->
		<td><input type="button"  class="btn_style" onclick="createOutput('pdf')"  name="operation" value="PDF"></td>
		<%-- <td> <input type="submit" name="operation" value="Cancel"></td> --%>
	</tr>
	
</table>

</center>
</div>
</div>

<div id="search_box" class="search_section" style="display: none;padding-top: 7%;padding-bottom: 6%;">

<div id="tb" class="tb_borders"><span>
<h3>Application User Details</h3>

</span>

<input type="hidden" id="chkUserID" name="chkUserID" value="chkUserID"></input>
<table class="myTable">
	<tr>
		<td><label class="cm-required">User ID :&nbsp;</label></td>
		<td><input id="UserID" type="text" class="input-text" /></td>
		<td></td>
	</tr>
	<tr>
		<td><label class="cm-required">User Name:&nbsp;</label></td>
		<td><input id="UserName" type="text" class="input-text" /></td>
		<td></td>
	</tr>
	<tr>
	<td>User History Report</td>
	<td style="text-align: left;"><select name="histype" id="histype" onchange="che_his()">
		<option value="N">NO</option>
		<option value="Y">YES</option>
		</select></td>
	</tr>
	<tr>
		<td><label class="cm-required">Report Type:&nbsp;</label></td>
		<td><select name="reptype" id="reptype" style="width: 220px;">
		</select></td>
		<td></td>
	</tr>
	<tr>
		<td><label class="cm-required">From Date:&nbsp;</label></td>
		<td><input class="input-text" type="text" id="fromdt" value="" onblur="checkdate(this,this.value)" name="fromdt"></input></td>
		<td></td>
	</tr>
	<tr>
		<td><label class="cm-required">To Date:&nbsp;</label></td>
		<td><input class="input-text" type="text" id="todate" value="" onblur="checkdate(this,this.value)" name="todate"></input></td>
		<td></td>
	</tr>

	
	<tr>
		<td  colspan="3" style="padding-top: 15px;text-align: center;">
		
		<input class="btn_style" onclick="doSearch()"  plain="true" type="button" value="Search">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" onclick="clr()" class="btn_style" name="btn_Clear" value="Clear"></td>
		<td></td>
	</tr>
</table>

</div>
</div>


<div id="showDiv" style="display:none;">
</div>
<jsp:include page="Footer.jsp"></jsp:include>
</center>
</div></div>
</form>



<form action="UserReport_PDF.jsp" method="post" style="display: none;" id="frmPdfOutPut">
	<input type="hidden" id="txtFromdtOutPut" name="txtFromdtOutPut">
	<input type="hidden" id="txtTodateOutPut" name="txtTodateOutPut">
	<input type="hidden" id="txtUserIDOutPut" name="txtUserIDOutPut">
	<input type="hidden" id="txtUserNameOutPut" name="txtUserNameOutPut">
	<input type="hidden" id="txtHistypeOutPut" name="txtHistypeOutPut">
	<input type="hidden" id="txtReptypeOutPut" name="txtReptypeOutPut">
	<input type="hidden" id="txtTypeOutPut" name="txtTypeOutPut">
	<input type="hidden" id="txtHTMLOutPut" name="txtHTMLOutPut">
	<input type="hidden" id="txtReportName" name="txtReportName">
	
</form>


<script>
$(function() {$('#fromdt').calendarsPicker({calendar: $.calendars.instance('gregorian')});});
$(function() {$('#todate').calendarsPicker({calendar: $.calendars.instance('gregorian')});});
</SCRIPT>

</body>
	</html>
	