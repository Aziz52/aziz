package com.miscot.springmvc.Listener;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.miscot.springmvc.repository.GetQueryImpl;

 
public class SessionListener implements HttpSessionListener {
	
	private int sessionCount = 0;
    @Override
    public void sessionCreated(HttpSessionEvent event) {
        System.out.println("==== Session is created ====");
        synchronized (this) {
			sessionCount++;
		}
		HttpSession ss=event.getSession();
		System.out.println("Session Created: " + event.getSession().getId());
		System.out.println("Total Sessions: " + sessionCount);
		event.getSession().setMaxInactiveInterval(15*60);
    }
 
    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
		synchronized (this) {
			sessionCount--;
		}
		WebApplicationContextUtils.getWebApplicationContext(event.getSession().getServletContext());
		try
		{
			if (event.getSession().getAttribute("user_id") != null)
			{
				String user_id=event.getSession().getAttribute("user_id").toString();
				System.out.println(user_id);
				
				
			}
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
	