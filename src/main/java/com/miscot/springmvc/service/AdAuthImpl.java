package com.miscot.springmvc.service;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.miscot.springmvc.configuration.PropertiesPath;
import com.miscot.springmvc.configuration.SpringConfiguration;
import com.miscot.springmvc.repository.GetQueryImpl;


@Service
public class AdAuthImpl implements AdAuthInterface {

	@Autowired 
	PropertiesPath  pp;
	@Autowired 
	GetQueryImpl getQuery;
	@Override
	public int ISauthenticate(String user, String pass) {
		//System.out.println("ADAuth Start");
	    String returnedAtts[] ={ "sn", "givenName", "mail" };

	    String searchFilter = "(&(objectClass=user)(sAMAccountName=" + user + "))";

	     int found=0;

	    //Create the search controls

	    SearchControls searchCtls = new SearchControls();

	    searchCtls.setReturningAttributes(returnedAtts);



	    //Specify the search scope

	    searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);



	    /*Hashtable env = new Hashtable();

	    env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");

	    env.put(Context.PROVIDER_URL, ldapHost);

	    env.put(Context.SECURITY_AUTHENTICATION, "simple");

	    env.put(Context.SECURITY_PRINCIPAL, user + "@" + domain);

	    env.put(Context.SECURITY_CREDENTIALS, pass);*/



	    LdapContext ctxGC = null;



	    try

	    {
	    	
	    	if(pp.getIsadauth().equals("No"))
	    	{
	    			
	    		String chkuser="";
					try {
						chkuser = getQuery.get_chkuser(user.toUpperCase(), pass);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	    		
	    			if(!chkuser.equals(""))
	    			{
	    				
	    				if(Integer.parseInt(chkuser)>0)
	    				{
	    					
	    					found=1;
	    				}
	    			}
	    		
	    		

	    		
	    	}
	    	
	    	else
	    	{/*
	    	// System.out.println("here");
	      ctxGC = new InitialLdapContext(env, null);
	    //  System.out.println("after");
	      //Search objects in GC using filters

	     
	      NamingEnumeration answer = ctxGC.search(searchBase, searchFilter, searchCtls);
	   //   System.out.println("answer");

	      while (answer.hasMoreElements())
	      {
	        SearchResult sr = (SearchResult) answer.next();
	        Attributes attrs = sr.getAttributes();
	        if (attrs != null)
	        {
	        	found=1;
	        }
	      }
	          return found;
	      */}
	    }
	    /*catch (NamingException ex)

	    {
	    	 ex.printStackTrace();
	    	return found;
	    }*/ catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    return found;
	  }

}
