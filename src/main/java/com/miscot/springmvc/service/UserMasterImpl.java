package com.miscot.springmvc.service;

import java.net.InetAddress;
import java.sql.ResultSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.miscot.springmvc.model.UserMaster;
import com.miscot.springmvc.repository.DBUtil;
import com.miscot.springmvc.repository.GetQueryImpl;

@Service
@Transactional
public class UserMasterImpl implements UserMasterInterface {
	@Autowired 
	GetQueryImpl getQuery;
	@Autowired 
	AdAuthImpl objAdAuth;
	public List GetUserLIst(String Qry) {
		// TODO Auto-generated method stub
		return null;
	}

	public List GetUserMaintenanceReportLIst(String Qry) {
		// TODO Auto-generated method stub
		return null;
	}

	public List GetUserActivityReportLIst(String Qry) {
		// TODO Auto-generated method stub
		return null;
	}

	public List GetVerifyUserLIst(String Qry) {
		// TODO Auto-generated method stub
		return null;
	}

	public UserMaster GetUserDetails(String userid) {
		// TODO Auto-generated method stub
		return null;
	}

	public String Get_Zone_List(String usr, String typ) {
		// TODO Auto-generated method stub
		return null;
	}

	public String GetFolderList(String usr, String typ) {
		// TODO Auto-generated method stub
		return null;
	}

	public String GetAppList1(String usr, String typ) {
		// TODO Auto-generated method stub
		return null;
	}

	public String Dar_Get_Branch_List() {
		// TODO Auto-generated method stub
		return null;
	}

	public String Dar_Get_Branch_List_fin7() {
		// TODO Auto-generated method stub
		return null;
	}

	public String CheckUserExistLogin(String User_id, UserMaster dto) {

		boolean res  = false;
		String sts = "";
		System.out.println("CheckUserExistLogin");
		String query = "";

		int chkINT = 0;

		try {


			if (User_id != null && User_id != "")
			{
				
				chkINT =Integer.parseInt( getQuery.User_Master2_Cnt(1, 1, User_id,""));
				System.out.println("CheckLoginCount : " + chkINT);
				if (chkINT > 0)
				{
					query = getQuery.User_Master2(1, 1, User_id,"");
					List<UserMaster> userMasterList=getQuery.listUserMaster(query);
					for(int i=0;i<userMasterList.size();i++)
					{
						if (userMasterList.get(i).getISVERIFIED().equals("N") ||userMasterList.get(i).getISVERIFIED().equals("R") )
						{
							res = false;
							sts = "V";
						}
						else if (userMasterList.get(i).getUSER_STATUS().equals("X"))
						{
							res = false;
							sts = "X";
						}
						else if (userMasterList.get(i).getISACTIVE().equals("Y"))
						{
							res = false;
							sts = "A";
						}
						else if (userMasterList.get(i).getUSER_STATUS().equals("D"))
						{
							res = false;
							sts = "D";
						}
						else if (userMasterList.get(i).getUSER_STATUS().equals("L"))
						{
							res = false;
							sts = "L";
						}
						else if (userMasterList.get(i).getUSER_STATUS().equals("I"))
						{
							res = false;
							sts = "I";
						}
						else
						{
							if (dto != null)
							{
							dto.setUSER_ID (userMasterList.get(i).getUSER_ID());
							dto.setUSER_NAME(userMasterList.get(i).getUSER_NAME());
							dto.setUSER_STATUS (userMasterList.get(i).getUSER_STATUS());
							dto.setUSER_ROLE (userMasterList.get(i).getUSER_ROLE());
							dto.setUSER_ROLE_DESC(userMasterList.get(i).getUSER_ROLE_DESC());
							dto.setCREATED_TIME (userMasterList.get(i).getCREATED_TIME());
							dto.setCREATED_USER (userMasterList.get(i).getCREATED_USER());
							dto.setLAST_LOGIN_DATE (userMasterList.get(i).getLAST_LOGIN_DATE());
							dto.setLAST_LOGIN_IP (userMasterList.get(i).getLAST_LOGIN_IP());
							}
							res = true;
						}
					}
				}
				else{
					//System.out.println("Not available in checkLogin");
					res = false;
					sts = "N";
				}


			}
			else{
				res = false;
				sts = "E";
			}




		if (res == true)
		{
			sts = "";
		}




		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			res = false;
		}


		finally {

		}
		return sts ;


	}

	public String CheckUserExistNew(String User_id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String Get_zone(String User_id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String chkPasswordExpiry(String userid) {
		// TODO Auto-generated method stub
		return null;
	}

	public String chkLockedUserTime(String userid) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String AuthenticateUser(String userid, String passWord, UserMaster dto, String ipAddr, String sts) {
		// TODO Auto-generated method stub
		System.out.println("Inside Authenticate UserImpl..........");

		boolean res = true;
		
		String chk = "";
		String qry = "";
		//DBUtil ds  = new DBUtil();
		try
		{

			if (userid == null || passWord==null)
			{
				res = false;
			}
			else
			{
			
				
			
					int auth =0;				
				  ipAddr=	InetAddress.getLocalHost().getHostAddress();
				  System.out.println(ipAddr);
					 //AdAuth objAdAuth = new AdAuth() ;
					 //objAdAuth.ISauthenticate(userid,ds.encrypt(passWord));
				  auth=objAdAuth.ISauthenticate(userid,passWord);
					 if (auth == 1)
					 {
						chk = CheckUserExistLogin(userid,dto);					
						if (chk.equals(""))
						{
							getQuery.update_tbl_user_master1(ipAddr, userid.toUpperCase());
							getQuery.insert_tbl_audit_login1(ipAddr, userid.toUpperCase());
							res = true;
						}
						else if ((chk.equalsIgnoreCase("N"))) {
							
					 
						//System.out.println("AD Authentication : " + auth);
					    }					
						else
						{
							getQuery.insert_tbl_audit_login1(ipAddr, userid, chk);
							res = false;					
						}
		
					 } 
					 
					 else
					 {
						 //ds.Qexecute("insert into tbl_audit_login values ('"	+ userid.toUpperCase() + "',current_timestamp,'F','"	+ ipAddr + "')");
							/*int FailCount = 0;
							int ValidFailCnt = 0;
							
							String tempFailCount=ds.GetValue("select param_value from tbl_system_settings where upper(param_name) = 'FAILCOUNT'");
							ValidFailCnt = (tempFailCount==""?0:Integer.parseInt(tempFailCount));
							
							if (ValidFailCnt == 0) {
								ValidFailCnt = 3;
							}
							FailCount = UserFailCount(userid);
							
							chk = "N";
							if (FailCount >= ValidFailCnt) {
								ds.updateOrInsertValues("update tbl_user_master set user_status = 'L',isunlocked='N',unlock_date=null where   UPPER(user_id) = '"
										+ userid.toUpperCase() + "'");
								chk = "L";
								ds.updateOrInsertValues("insert into tbl_audit_login values ('"+ userid.toUpperCase() + "',current_timestamp,'L','"+ ipAddr + "')");
							}
						res = false;*/
					 }
					 
			}

		}

		catch(Exception e )
		{
			//ds.CloseConnection();
			e.printStackTrace();
			res = false;
		}
		
		if(res == true)
		{
			chk = "";
		}
		return chk;
	}

	public String AuthenticateUserDB(String userid, String passWord, UserMaster dto, String ipAddr, String sts) {
		// TODO Auto-generated method stub
		return null;
	}

	public int UserFailCount(String userid) {
		// TODO Auto-generated method stub
		return 0;
	}

	public boolean ResetPassword(String UserId, String Password) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean ResetLogin(String userid, String CurrentUser, String ip_addrss) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean VerifyUser(String userid, String currentUser, String ip_addrss) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean AddNewUser(UserMaster dto, String[] allApp, String[] AllFolder, String createdUser, String sts,
			String ip_addrss) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean EditUserDetails(UserMaster dto, String[] allApp, String modifiedUser, String usrLock, String sts,
			String ip_addrss) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean ChangePassword(String oldPassW, String newPassW, String user_id) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean SAChangePassword(String newPassW, String user_id, String SaUserID) {
		// TODO Auto-generated method stub
		return false;
	}

	public List GetBulkAadharLIst(String Qry) {
		// TODO Auto-generated method stub
		return null;
	}

	public String ProcessBulkAadharLIst(String query) {
		// TODO Auto-generated method stub
		return null;
	}



}
