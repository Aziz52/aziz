package com.miscot.springmvc.service;

import java.util.List;

import com.miscot.springmvc.model.UserMaster;


public interface UserMasterInterface {
	List GetUserLIst(String Qry) ;
	List GetUserMaintenanceReportLIst(String Qry);
	List GetUserActivityReportLIst(String Qry);
	List GetVerifyUserLIst(String Qry);
	UserMaster GetUserDetails(String userid);
	String Get_Zone_List(String usr,String typ);
	String  GetFolderList(String usr,String typ);
	String GetAppList1(String usr,String typ);
	String Dar_Get_Branch_List();
	String Dar_Get_Branch_List_fin7();
	String CheckUserExistLogin(String User_id,UserMaster dto);
	String CheckUserExistNew(String User_id);
	String Get_zone(String User_id);
	String chkPasswordExpiry(String userid);
	String chkLockedUserTime(String userid);
	String AuthenticateUser(String userid,String passWord,UserMaster dto,String ipAddr,String sts);
	String AuthenticateUserDB(String userid,String passWord,UserMaster dto,String ipAddr,String sts) ;
	int UserFailCount(String userid);
	boolean ResetPassword(String UserId,String Password);
	boolean ResetLogin(String userid,String CurrentUser ,String ip_addrss);
	boolean VerifyUser(String userid,String currentUser,String ip_addrss);
	boolean AddNewUser(UserMaster dto,String[] allApp,String[] AllFolder,String createdUser,String sts,String ip_addrss);
	boolean EditUserDetails(UserMaster dto,String[] allApp,String modifiedUser,String usrLock,String sts,String ip_addrss);
	boolean ChangePassword(String oldPassW, String newPassW, String user_id);
	boolean SAChangePassword(String newPassW, String user_id,String SaUserID);
	List GetBulkAadharLIst(String Qry);
	String ProcessBulkAadharLIst(String query);
	
}
