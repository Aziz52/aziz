package com.miscot.springmvc.repository;

import java.sql.SQLException;
import java.util.List;

import com.miscot.springmvc.model.UserMaster;

public interface GetQueryInterface {
	int update_tbl_user_master1(String ipAddr,String userid);
	int insert_tbl_audit_login1(String ipAddr,String userid);
	int insert_tbl_audit_login1(String ipAddr,String userid,String chk);
	String get_chkuser(String user,String pass);
	int update_tbl_user_master(String Sessionid,String userid);
	String User_Master2(int startI, int endI, String User_id,String User_name) throws SQLException, Exception;
	String User_Master2_Cnt(int startI, int endI, String User_id,String User_name) throws SQLException, Exception;
	List<UserMaster> listUserMaster(String qry);
	int invalidate_user_session1(String user_id);
	int invalidate_user_session2(String user_id,String User_IP);
	
}
