package com.miscot.springmvc.repository;


public interface DBUtilInterface {
String encrypt(String Data);
String getSingleValues(String query);
String RemoveNull(String Val) throws Exception;
String decrypt(String encryptedData);
}
