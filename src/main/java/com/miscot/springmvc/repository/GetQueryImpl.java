package com.miscot.springmvc.repository;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import com.miscot.springmvc.model.UserMaster;

@Service
public class GetQueryImpl implements GetQueryInterface {

	@Autowired  DBUtil ds;
	
	@Override
	public int update_tbl_user_master1(String ipAddr,String userid) {
		int res=ds.updateOrInsertValues("update tbl_user_master set last_login_date = current_timestamp,login_check_date = CURRENT_TIMESTAMP,"
				+ "last_login_ip='" + ipAddr + "',isActive='Y',isUnlocked='N' where upper(user_id)='"+userid.toUpperCase()+"' "
				+ "and user_status='E'  ");
		return res;
	}

	@Override
	public int insert_tbl_audit_login1(String ipAddr,String userid) {
		int res=ds.updateOrInsertValues("insert into tbl_audit_login values ('" +userid.toUpperCase()+ "',CURRENT_TIMESTAMP,'S','" + ipAddr + "')");
		return res;
	}

	@Override
	public int insert_tbl_audit_login1(String ipAddr, String userid, String chk) {
		int res=ds.updateOrInsertValues("insert into tbl_audit_login values ('" +userid.toUpperCase()+ "',current_timestamp,'"
				+ chk.trim() +"','" + ipAddr + "')");
		return res;
	}

	@Override
	public String get_chkuser(String user, String pass) {
		String chkuser=ds.getSingleValues("select count(*) from TBL_USER_MASTER where upper(USER_ID) = '"+ user.toUpperCase() + "' and passwd='"+ ds.encrypt(pass) +"'");
		return chkuser;
	}

	@Override
	public int update_tbl_user_master(String Sessionid, String userid) {
		int res=ds.updateOrInsertValues("update tbl_user_master set last_session_id = '"+Sessionid+"'  where upper(user_id)= '"+userid.toUpperCase()+"' " );
		return res;
	}
	
	@Override
	public String User_Master2(int startI, int endI, String User_id,
			String User_name) throws SQLException, Exception {

		String res = "";
		String strSQL1 = "select /*+ parallel */ a.*,last_login_date as LoginDate, CASE when isactive = 'N' then 'Inactive' else 'Active' end as IsActiveS, "
				+ "case when isactive = 'Y' then '<a href=# onclick=chkReset(''' || User_ID || ''');>Reset</a>' else '' end as Reset , "
				+ "ROW_NUMBER() OVER ( ORDER BY User_id ) AS rn  from v_userDetails a where del_flag = 'N'  ";

		String where="";
		if (User_id != null && !(User_id.equalsIgnoreCase(""))) {
			// System.out.println(BranchCode.indexOf("*")
			// +"Linker--BranchCode");
			if (User_id.indexOf("*") > 0) {
				where += " and  upper(User_id) like '%"
						+ User_id.replace("*", "").toUpperCase() + "%'";

			} else {

				where += " and  upper(User_id) = '"
						+ User_id.replace("*", "").toUpperCase() + "'";
			}
		}

		if (User_name != null && !(User_name.equalsIgnoreCase(""))) {

			if (User_name.indexOf("*") > 0) {
				where += " and  upper(User_name) like '%"
						+ User_name.replace("*", "").toUpperCase() + "%'";

			} else {
				where += " and  upper(User_name) = '"
						+ User_name.replace("*", "").toUpperCase() + "'";
			}
		}

		strSQL1 += where;

		String strSQL = "Select * from (" + strSQL1 + ") b where rn between " + startI
				+ " and " + endI + "";
		res = strSQL ;//+ "~" + Count;

		System.out.println(strSQL + where);

		return res;
	}
	
	@Override
	public String User_Master2_Cnt(int startI, int endI, String User_id,
			String User_name) throws SQLException, Exception {

		String res = "";
		String strSQL1 = "select /*+ parallel */ a.*,last_login_date as LoginDate, CASE when isactive = 'N' then 'Inactive' else 'Active' end as IsActiveS, "
				+ "case when isactive = 'Y' then '<a href=# onclick=chkReset(''' || User_ID || ''');>Reset</a>' else '' end as Reset , "
				+ "ROW_NUMBER() OVER ( ORDER BY User_id ) AS rn  from v_userDetails a where del_flag = 'N'  ";

		String where="";
		if (User_id != null && !(User_id.equalsIgnoreCase(""))) {
			// System.out.println(BranchCode.indexOf("*")
			// +"Linker--BranchCode");
			if (User_id.indexOf("*") > 0) {
				where += " and  upper(User_id) like '%"
						+ User_id.replace("*", "").toUpperCase() + "%'";

			} else {

				where += " and  upper(User_id) = '"
						+ User_id.replace("*", "").toUpperCase() + "'";
			}
		}

		if (User_name != null && !(User_name.equalsIgnoreCase(""))) {

			// System.out.println(BranchName.indexOf("*")
			// +"Linker--BranchName");
			if (User_name.indexOf("*") > 0) {
				where += " and  upper(User_name) like '%"
						+ User_name.replace("*", "").toUpperCase() + "%'";

			} else {
				where += " and  upper(User_name) = '"
						+ User_name.replace("*", "").toUpperCase() + "'";
			}
		}

		strSQL1 += where;

		String strSQL = "Select * from (" + strSQL1 + ") b where rn between " + startI
				+ " and " + endI + "";
		//Count = dtcon.GetValue(" select count(*) from (" + strSQL1 + ") b ");

		// strSQL = strSQL + " order by user_name ";
		res = strSQL ;//+ "~" + Count;

		res=ds.getSingleValues("select count(*) from (" + strSQL1 + ") b");

		return res;
	}

	@Override
	public List<UserMaster> listUserMaster(String qry) {
		List<UserMaster> userMasters=ds.listUserMaster(qry);
		return userMasters;
	}

	@Override
	public int invalidate_user_session1(String user_id) {
		int res=ds.updateOrInsertValues("update tbl_user_master set isActive = 'N' where user_id = '" +user_id +"'");
		return res;
	}

	@Override
	public int invalidate_user_session2(String user_id, String User_IP) {
		int res=ds.updateOrInsertValues("insert into tbl_audit_login (user_id,login_date,login_status,LOGIN_IP) values('" + user_id  +"',current_date,'O','"+User_IP +"')");
		return res;
	}
	
}
