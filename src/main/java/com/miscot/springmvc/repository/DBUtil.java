package com.miscot.springmvc.repository;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.security.spec.AlgorithmParameterSpec;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Vector;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import org.springframework.stereotype.Repository;

import com.miscot.springmvc.model.UserMaster;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;


@Repository
public class DBUtil implements DBUtilInterface {
	public Connection conn = null;
	static String ConnectionStatus = "N";
	static String LastError = "";
	public static String ip_addr = "";
	@Autowired
    JdbcTemplate jdbcTemplate;

	public String GetValue(String sql) throws SQLException, Exception {
		System.out.println("GetValue " + sql);
		String Res = "";
		PreparedStatement prepStmt = null;
		ResultSet rs = null;

		try {

			prepStmt = conn.prepareStatement(sql);
			rs = prepStmt.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					Res = RemoveNull(rs.getString(1));
				}
			}
		}

		catch (SQLException ex) {
			ex.printStackTrace();
		}

		finally {
			prepStmt.close();
			rs.close();
		}

		return Res;
	}

	

	public String retrieveUid(String encAdhNo) {
		// TODO Auto-generated method stub
		String query = "select uuid from tbl_vault where encAdhNo='" + encAdhNo + "'";
		String result = getSingleValues(query);
		System.out.println("Result is" + result);
		return result;
	}

	public String getSingleValues(final String query) {
		// TODO Auto-generated method stub

		return (String) jdbcTemplate.query(query, new Object[] {}, new ResultSetExtractor<String>() {

			public String extractData(ResultSet rs) throws SQLException, DataAccessException {
				// TODO Auto-generated method stub
				String Status = null;
				String Res = "";
				System.out.println("Query" + query + "rs" + rs);
				
				if (rs != null) {
					while (rs.next()) {
						try {
							Res = RemoveNull(rs.getString(1));
							return Res;
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

				// set other properties

				return Res;
			}
		});

	}

	public int updateOrInsertValues(String query) {
		int i = jdbcTemplate.update(query);
		System.out.println("Records Updated!!");
		return i;
	}

	String algorithm = "DESede";
	String strPassword = "password12345678";
	public String encrypt(String Data){
		
		String encryptedValue = "";
		try {
		if (!Data.equalsIgnoreCase(null)) {

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			SecretKeySpec key = new SecretKeySpec(strPassword.getBytes(), "AES");
			AlgorithmParameterSpec paramSpec = new IvParameterSpec(
					strPassword.getBytes());
			cipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
			byte[] ecrypted = cipher.doFinal(Data.getBytes());
			encryptedValue = new BASE64Encoder().encode(ecrypted);
		}
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return encryptedValue;
	}

	public String decrypt(String encryptedData) {
		String decryptedValue = "";
		try {
		if (!encryptedData.equalsIgnoreCase(null)) {
			SecretKeySpec key = new SecretKeySpec(strPassword.getBytes(), "AES");
			AlgorithmParameterSpec paramSpec = new IvParameterSpec(
					strPassword.getBytes());
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
			byte[] output = new BASE64Decoder().decodeBuffer(encryptedData);
			byte[] decrypted = cipher.doFinal(output);
			decryptedValue = new String(decrypted);

		}
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return decryptedValue;
	}
	public String RemoveNull(String Val) throws Exception {
		String Res = "";
		if (Val == null) {
			Res = "";
		} else {
			Res = Val;
		}
		return Res;
	}
	
	public List<UserMaster> listUserMaster(String SQL) {
		   
		      List <UserMaster> userMaster = jdbcTemplate.query(SQL, 
		         new ResultSetExtractor<List<UserMaster>>(){
		         
		         public List<UserMaster> extractData(
		            ResultSet rs) throws SQLException, DataAccessException {
		            
		            List<UserMaster> list = new ArrayList<UserMaster>();  
		            while(rs.next()){  
		            	UserMaster userMaster = new UserMaster();
		            	userMaster.setUSER_ID (rs.getString("USER_ID"));
		            	userMaster.setUSER_NAME(rs.getString("USER_NAME"));
		            	userMaster.setUSER_STATUS (rs.getString("USER_STATUS"));
		            	userMaster.setUSER_ROLE (rs.getString("USER_ROLE"));
		            	userMaster.setUSER_ROLE_DESC(rs.getString("USER_ROLE_DESC"));
		            	userMaster.setCREATED_TIME (rs.getString("CREATED_TIME"));
		            	userMaster.setCREATED_USER (rs.getString("CREATED_USER"));
		            	userMaster.setLAST_LOGIN_DATE (rs.getString("LoginDate"));
		            	userMaster.setLAST_LOGIN_IP (rs.getString("LAST_LOGIN_IP"));
		            	userMaster.setISACTIVE (rs.getString("Isactive"));
		            	userMaster.setISVERIFIED(rs.getString("is_verified"));
		               list.add(userMaster);  
		            }  
		            return list;  
		         }    	  
		      });
		   return userMaster;
		}
	
	/*public ResultSet get_ResultSet(String query) {
		return jdbcTemplate.query(
				query,
				new ResultSetExtractor<ResultSet>() {
					

					public ResultSet extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						while (rs.next()) {
							
						}
						return rs;
					}
				});

	}*/
	
}
