package com.miscot.springmvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.miscot.springmvc.bean.LoginBean;
import com.miscot.springmvc.dto.pathDTO;
import com.miscot.springmvc.model.UserMaster;
import com.miscot.springmvc.repository.DBUtil;
import com.miscot.springmvc.repository.GetQueryImpl;
import com.miscot.springmvc.service.UserMasterImpl;
import com.miscot.springmvc.service.UserMasterInterface;


@Controller
public class UserAuthenticationController {
@Autowired 
UserMasterImpl uservice;
@Autowired
UserMaster dto;
@Autowired
GetQueryImpl getQuery;

	@RequestMapping(value = "/UserAuthenticationController", method = RequestMethod.POST)
	public String UserAuthentication(HttpSession session,HttpServletRequest request,
			@ModelAttribute("loginBean") LoginBean loginBean,@RequestParam String operation,ModelMap model,
			BindingResult result) {
		
		//pathDTO.path1 = request.getSession().getServletContext().getRealPath("/Include");
		String userid = request.getParameter("userName");
		String passWord = request.getParameter("password");
		
		Boolean res = false;
		String chk = "";
		String sts = "";
		int FailCount = 0;
		
		//String userid  = request.getParameter("userName");
		//String passWord = request.getParameter("password");
		if(userid.equalsIgnoreCase("Miscot") && passWord.equalsIgnoreCase("miscotM1")){
			session = request.getSession(true);
			session.setAttribute("user_name","Miscot systems.");
			session.setAttribute("user_id","Miscot");
			session.setAttribute("password","");
			session.setAttribute("Role","Admin");
			//System.out.println("Session Value : " + session.getAttribute("user_id"));
			session.setAttribute("LastLogin","");
			session.setAttribute("Chart1flg","Y"); 
			session.setAttribute("Chartflg","Y");
			session.setAttribute("BranchCode","");
			return "Home";
			
		}else{
		
		chk = uservice.AuthenticateUser(userid,passWord,dto,request.getRemoteAddr(),sts);
		System.out.println("chk" + chk);
		if (chk.equals(""))
		{
			System.out.println("dto.getUSER_NAME()==="+dto.getUSER_NAME());
			session = request.getSession(true);
			session.setAttribute("user_name",dto.getUSER_NAME());
			session.setAttribute("user_id",dto.getUSER_ID());
			session.setAttribute("password",passWord);
			System.out.println("dto.getUSER_ROLE_DESC()=="+dto.getUSER_ROLE_DESC());
			System.out.println("dto.getUSER_ROLE()=="+dto.getUSER_ROLE());
			
			session.setAttribute("Role",dto.getUSER_ROLE());
			
			session.setAttribute("LastLogin",dto.getLAST_LOGIN_DATE());
			session.setAttribute("BranchCode","");
			//System.out.println("BranchCode=="+dto.getBRANCHCODE());
			session.setAttribute("Chart1flg","Y");
			 
			session.setAttribute("Chart1","Y");
			session.setAttribute("Chart2","");
			session.setAttribute("Chart3","");
			session.setAttribute("Chart1flg","Y"); 
			session.setAttribute("Chartflg","Y");
		
			session.setAttribute("Chartflg","Y");
			
				String Sessionid=session.getId();
	 
				getQuery.update_tbl_user_master(Sessionid,userid);
			
			return "Home";
		}
		else
		{
			String msg="";
			
		    if (chk!= null )
		    {
		    	if (chk.equals("N"))
		    	{
		    	
		    		msg="User Name and Password do not match.";
		    	
		    	}
		    	else if (chk.equals("A"))
		    	{
			    
		    		msg="Already Logged in.";
			    	
			    	}
		    	else if (chk.equals("D"))
		    	{
			    	
		    		msg="User Disabled. Contact Administrator.";
			    	
			    	}
		    	else if (chk.equals("I"))
		    	{
			    	
		    		msg="User Inactive. Contact Administrator.";
			    	
			    	}
		    	else if (chk.equals("L"))
		    	{
			    	
		    		msg="Account Locked. 3 consecutive login attempts failed";
			    	
			    	}
		    	else if (chk.equals("V"))
		    	{
			    	
		    		msg="Account not yet verified.";
			    	
			    		}
		    	
		    	else if (chk.equals("X"))
		    	{
			    	
		    		msg="User Expired. Contact Administrator.";
			    	
			    	}
		    }
		    System.out.println(msg);
		    	model.addAttribute("err", msg);
		   return "Login";
		}
		}
		
		
	}
}
