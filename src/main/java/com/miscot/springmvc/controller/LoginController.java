package com.miscot.springmvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.miscot.springmvc.repository.GetQueryImpl;

@Controller
public class LoginController {
	
	@Autowired 
	GetQueryImpl getQuery;
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String login() {
		return "Login";
	}

	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public String user( Model model) {
		System.out.println("User Page Requested");
		//model.addAttribute("userName", user.getUserName());
		return "Login";
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request,HttpSession session) {
		
		getQuery.invalidate_user_session1(session.getAttribute("user_id").toString());
		getQuery.invalidate_user_session2(session.getAttribute("user_id").toString() ,request.getRemoteAddr());
		session.invalidate();
		return "Login";
	}
}
